# For Java 8, try this
# FROM openjdk:8-jdk-alpine

# For Java 11, try this
FROM adoptopenjdk/openjdk11:alpine-jre

# RUN apk add --no-cache tzdata
ENV TZ Asia/Bangkok

# variable with default value
ARG SPRING_PROFILES_ACTIVE

# Refer to Maven build -> finalName
ARG JAR_FILE=target/scms-service-1.0.0.jar

# cd /opt/app
WORKDIR /opt/app

# cp target/spring-boot-web.jar /opt/app/app.jar
COPY ${JAR_FILE} scms-service.jar

# Make port 9999 available to the world outside container
EXPOSE 8089

RUN echo $SPRING_PROFILES_ACTIVE

ENV SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dserver.port=8089", "-jar","./scms-service.jar"]
