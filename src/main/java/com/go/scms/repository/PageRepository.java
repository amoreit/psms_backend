package com.go.scms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.go.scms.entity.PageEntity;

@Repository
public interface PageRepository extends JpaRepository<PageEntity,Long>{
	Page<PageEntity> findAllByPageGroupPageGroupNameContainingAndPageGroupPageGroupCodeContaining(String pageGroupName,String pageGroupCode, Pageable pageable);	
}