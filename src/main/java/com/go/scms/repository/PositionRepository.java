/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.PositionEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *s
 * @author kronn
 */
@Repository
public interface PositionRepository extends JpaRepository<PositionEntity,Long>{
       Page<PositionEntity> findAllByRecordStatusNotIn(String recordStatus, Pageable pageable); 
       Page<PositionEntity> findAllByPositionNameThContainingAndRecordStatusNotIn(String positionNameTh, String recordStatus, Pageable pageable); 
       Page<PositionEntity> findAllByPositionNameEnContainingAndRecordStatusNotIn(String positionNameEn, String recordStatus, Pageable pageable); 
       Page<PositionEntity> findAllByPositionNameThContainingAndPositionNameEnContainingAndRecordStatusNotIn(String positionNameTh,String positionNameEn, String recordStatus, Pageable pageable); 
       
       List<PositionEntity> findAllByRecordStatusNotIn(String recordStatus);
       Optional<PositionEntity> findByPositionId(Long PositionId);                        
}
    

