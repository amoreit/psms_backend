/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ActivityPaiEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityPaiRepository extends JpaRepository<ActivityPaiEntity, Long> {

    Page<ActivityPaiEntity> findAllByActivityCodeContainingAndActivityNameContainingAndRecordStatusNotIn(String activityCode, String activityName, String recordStatus, Pageable pageable);

    List<ActivityPaiEntity> findAllByRecordStatusNotIn(String recordStatus);

    Optional<ActivityPaiEntity> findByActivityId(Integer ActivityId);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D'", nativeQuery = true)
    Page<Map> show(Pageable pageable);

    @Query(value = " SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 ", nativeQuery = true)
    Page<Map> classid(Integer classId, Pageable pageable);

    @Query(value = " SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.activity_group_id = ?1 order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D'  AND a.activity_group_id = ?1", nativeQuery = true)
    Page<Map> activityGroupId(Integer activityGroupId, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.activity_name like %?1% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D'  AND a.activity_name like %?1%", nativeQuery = true)
    Page<Map> ActivityName(String activityName, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.location like %?1% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D'  AND a.activity_name like %?1%", nativeQuery = true)
    Page<Map> location(String location, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2", nativeQuery = true)
    Page<Map> classIdactivityGroupId(Integer classId, Integer activityGroupId, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_name  like %?2% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_name  like %?2%", nativeQuery = true)
    Page<Map> classIdActivityName(Integer classId, String activityName, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.location  like %?2% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_name  like %?2%", nativeQuery = true)
    Page<Map> classIdlocation(Integer classId, String location, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.activity_name like %?1% AND a.location  like %?2% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.activity_name like %?1% AND a.location  like %?2%", nativeQuery = true)
    Page<Map> activityNamelocation(String activityName, String location, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.activity_group_id = ?1 AND a.activity_name  like %?2% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.activity_group_id = ?1 AND a.activity_name  like %?2%", nativeQuery = true)
    Page<Map> ActivityGroupIdActivityName(Integer activityGroupId, String activityName, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.activity_group_id = ?1 AND a.location  like %?2% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.activity_group_id = ?1 AND a.activity_name  like %?2%", nativeQuery = true)
    Page<Map> ActivityGroupIdlocation(Integer activityGroupId, String location, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 AND a.activity_name like %?3% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 AND a.activity_name like %?3% ", nativeQuery = true)
    Page<Map> classIdaActivityGroupIdActivityName(Integer classId, Integer activityGroupId, String activityName, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_name like %?2% AND a.location like %?3% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_name like %?2% AND a.location like %?3%", nativeQuery = true)
    Page<Map> classIdActivityNamelocation(Integer classId, String activityName, String location, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 AND a.location like %?3% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 AND a.location like %?3%", nativeQuery = true)
    Page<Map> classIdActivityGroupIdlocation(Integer classId, Integer activityGroupId, String location, Pageable pageable);

    @Query(value = "SELECT a.activity_id,ag.activity_group,a.activity_name,cc.class_sname_th,a.location,a.updated_user,a.updated_date  \n"
            + "                        FROM tb_mst_activity a \n"
            + "                        LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "                        LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "                        WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 AND a.activity_name like %?3% AND a.location like %?4% order by a.activity_group_id,a.cat_class,a.class_id,a.activity_name",
            countQuery = "SELECT count(*) FROM tb_mst_activity a \n"
            + "LEFT JOIN tb_mst_class AS cc ON a.class_id = cc.class_id\n"
            + "LEFT JOIN tb_mst_activity_group AS ag ON a.activity_group_id = ag.activity_group_id\n"
            + "WHERE a.record_status <> 'D' AND a.class_id = ?1 AND a.activity_group_id = ?2 AND a.activity_name like %?3% ", nativeQuery = true)
    Page<Map> classIdaActivityGroupIdActivityNamelocation(Integer classId, Integer activityGroupId, String activityName, String location, Pageable pageable);

}
