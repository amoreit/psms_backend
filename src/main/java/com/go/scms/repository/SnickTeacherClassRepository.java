    package com.go.scms.repository;

import com.go.scms.entity.SnickTeacherClassEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTeacherClassRepository extends JpaRepository<SnickTeacherClassEntity, Long>{       
        
        Page<SnickTeacherClassEntity> findAllByRecordStatusNotIn(String recordStatus, Pageable pageable);
        Page<SnickTeacherClassEntity> findAllByYearGroupName(String yearGroupName, Pageable pageable);
        Page<SnickTeacherClassEntity> findAllByCatClassIdAndRecordStatusNotIn(Integer catClassId, String recordStatus, Pageable pageable);
        Page<SnickTeacherClassEntity> findAllByCatClassIdAndClassNameAndRecordStatusNotIn(Integer catClassId, String className, String recordStatus, Pageable pageable);
        Page<SnickTeacherClassEntity> findAllByYearGroupNameAndRecordStatusNotIn(String yearGroupName, String recordStatus, Pageable pageable);
        Page<SnickTeacherClassEntity> findAllByYearGroupNameAndCatClassIdAndRecordStatusNotIn(String yearGroupName, Integer catClassId, String recordStatus, Pageable pageable);
        Page<SnickTeacherClassEntity> findAllByYearGroupNameAndCatClassIdAndClassNameAndRecordStatusNotIn(String yearGroupName, Integer catClassId, String className, String recordStatus, Pageable pageable);
        
        List<SnickTeacherClassEntity> findAllByRecordStatusNotInAndTeacherId(String recordStatus, Integer teacherId);
        List<SnickTeacherClassEntity> findAllByYearGroupNameAndCatClassIdAndClassNameAndRecordStatusNotInOrderByTeacherClassIdAsc(String yearGroupName, Integer catClassId, String className, String recordStatus);

        @Transactional
        @Modifying
        @Query("UPDATE SnickTeacherClassEntity teaclass SET teaclass.leader = ?1, teaclass.updatedUser = ?2, teaclass.recordStatus = ?3 where teaclass.teacherClassId = ?4")
        void updateLeader(Boolean leader, String userName, String recordStatus, Long getTeacherClassId);
        
        @Transactional
        @Modifying
        @Query(value = "DELETE FROM tb_trn_teacher_class where teacher_class_id = ?1", nativeQuery = true)
        void deleteTeacherClass(Long teacherClassId);
}
