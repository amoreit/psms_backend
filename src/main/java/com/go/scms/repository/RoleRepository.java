package com.go.scms.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.go.scms.entity.RoleEntity;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity,Long>{

	Page<RoleEntity> findAllByRoleCodeContainingAndRoleNameContainingAndRoleStatusNotIn(String roleCode,String roleName,String roleStatus, Pageable pageable);
	List<RoleEntity> findAllByRoleStatusNotIn(String roleStatus);
}
