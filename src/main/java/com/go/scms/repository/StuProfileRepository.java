/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.StuProfileEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface StuProfileRepository extends JpaRepository<StuProfileEntity, Long> {

    List<StuProfileEntity> findAllByRecordStatusNotInAndStuCode(String recordStatus, String stuCode);
    List<StuProfileEntity> findAllByRecordStatusNotInAndCitizenId(String recordStatus, String citizenId);
    List<StuProfileEntity> findAllBySmartPurseId(String smartPurseId);
    Optional<StuProfileEntity> findByStuCode(String stuCode);

    @Query(value = "select stu_profile_id,fullname,class_room from tb_stu_profile where record_status <> 'D' and stu_code =?1", nativeQuery = true)
    List<Map> findddlstucode(String stuCode);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and citizen_id like %?1% and firstname_th like %?2% and lastname_th like %?3% and b.status_desc like %?4% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and citizen_id like %?1% and firstname_th like %?2% and lastname_th like %?3% and b.status_desc like %?4%", nativeQuery = true)
    Page<Map> find(String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and stu_code like %?1% and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and stu_code like %?1% and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5%", nativeQuery = true)
    Page<Map> findstucode(String stuCode, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5%", nativeQuery = true)
    Page<Map> findclassid(String classId, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and d.class_room = ?2 and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and d.class_room = ?2 and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6%", nativeQuery = true)
    Page<Map> findclassidandclassroomid(String classId, String classRoomId, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and d.class_room = ?2 and a.stu_code like %?3% and citizen_id like %?4% and firstname_th like %?5% and lastname_th like %?6% and b.status_desc like %?7% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and d.class_room = ?2 and a.stu_code like %?3% and citizen_id like %?4% and firstname_th like %?5% and lastname_th like %?6% and b.status_desc like %?7%", nativeQuery = true)
    Page<Map> findstucodeandclassidandclassroomid(String classId, String classRoomId, String stuCode, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and a.stu_code like %?2% and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and c.class_sname_th = ?1 and a.stu_code like %?2% and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6%", nativeQuery = true)
    Page<Map> findstucodeandclassid(String classId, String stuCode, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.* from tb_stu_profile a left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = 2 and c.class_sname_th like %?1% and d.class_room like %?2% and a.stu_code like %?3% and a.firstname_th like %?4% and a.lastname_th like %?5% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = 2 and c.class_sname_th like %?1% and d.class_room like %?2% and a.stu_code like %?3% and a.firstname_th like %?4% and a.lastname_th like %?5%", nativeQuery = true)
    Page<Map> findprintcardstu(String classId, String classRoomId, String stuCode, String firstnameTh, String lastnameTh, Pageable pageable);

    //**Search Cer002 by nock**//
    //find all
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and stu.firstname_th like %?5% and stu.lastname_th like %?6% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and stu.firstname_th like %?5% and stu.lastname_th like %?6% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findall(Integer yearGroupId, Integer classId, Integer classRoomId, String stuCode, String firstnameTh, String lastnameTh, Pageable pageable);

    //find year and class and room 
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findyearandclassandroom(Integer yearGroupId, Integer classId, Integer classRoomId, Pageable pageable);

    //find year and class and room and stucode
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findyearandclassandroomandstucode(Integer yearGroupId, Integer classId, Integer classRoomId, String stuCode, Pageable pageable);

    //find year and class and room and firstnameth
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.firstname_th like %?4% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.firstname_th like %?4% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findyearandclassandroomandfirstnameth(Integer yearGroupId, Integer classId, Integer classRoomId, String firstnameTh, Pageable pageable);

    //find year and class and room and lastnameth
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.lastname_th like %?4% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.lastname_th like %?4% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findyearandclassandroomandlastnameth(Integer yearGroupId, Integer classId, Integer classRoomId, String lastnameTh, Pageable pageable);

    //find year and class and room and stucode and firstnameth
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and stu.firstname_th like %?5% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and stu.firstname_th like %?5% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findyearandclassandroomandstucodeandfirstnameth(Integer yearGroupId, Integer classId, Integer classRoomId, String stuCode, String firstnameTh, Pageable pageable);

    //find year and class and room and stucode and lastnameth
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and stu.lastname_th like %?5% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and crm.year_group_id =?1 and stu.class_id =?2 and stu.class_room_id =?3 and stu.stu_code like %?4% and stu.lastname_th like %?5% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findyearandclassandroomandstucodeandlastnameth(Integer yearGroupId, Integer classId, Integer classRoomId, String stuCode, String firstnameTh, Pageable pageable);

    //find stuCode
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.stu_code like %?1% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.stu_code like %?1% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findstuCode(String stuCode, Pageable pageable);

    //find stuCode and firstnameth
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.stu_code like %?1% and stu.firstname_th like %?2% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.stu_code like %?1% and stu.firstname_th like %?2% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findstucodeandfirstnameth(String stuCode, String firstnameTh, Pageable pageable);

    //find stuCode and lastnameth
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.stu_code like %?1% and stu.lastname_th like %?2% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.stu_code like %?1% and stu.lastname_th like %?2% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findstucodeandlastnameth(String stuCode, String lastnameTh, Pageable pageable);

    //find firstnameTh
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.firstname_th like %?1% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.firstname_th like %?1% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findfirstnameTh(String firstnameTh, Pageable pageable);

    //find firstnameTh and lastnameTh
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.firstname_th like %?1% and stu.lastname_th like %?2% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.firstname_th like %?1% and stu.lastname_th like %?2% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findfirstnamethandlastnameth(String firstnameTh, String lastnameTh, Pageable pageable);

    //find lastnameTh
    //for cer002
    @Query(value = "SELECT stu.stu_code,\n"
            + "stu.stu_profile_id,\n"
            + "stu.fullname\n"
            + "from tb_stu_profile as stu\n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.lastname_th like %?1% and mcc.cat_class_id !='1' \n"
            + "order by stu.stu_code,stu.class_room_id,stu.class_id\n ", countQuery = "select COUNT(*) \n"
            + "from tb_stu_profile as stu \n"
            + "left join tb_trn_class_room_member as crm on stu.stu_code = crm.stu_code\n"
            + "left join tb_mst_class as mc on stu.class_id = mc.class_id \n"
            + "left join tb_mst_cat_class as mcc on mc.cat_class_id = mcc.cat_class_id \n"
            + "left join tb_mst_class_room as mcr on stu.class_room_id = mcr.class_room_id \n"
            + "where stu.record_status <> 'D,C' and stu.lastname_th like %?1% and mcc.cat_class_id !='1' \n", nativeQuery = true)

    Page<Map> findlastnameTh(String lastnameTh, Pageable pageable);

    //find class and year and room
    //for rep006
    @Query(value = "SELECT ls.stu_code,\n"
            + "ls.fullname,\n"
            + "ls.class_room\n"
            + "from tb_trn_class_room_lifeskill_score as ls \n"
            + "LEFT JOIN tb_mst_class_room AS mcr ON ls.class_room = mcr.class_room_sname_th \n"
            + "where ls.record_status <> 'D,C' and ls.year_group_id = ?1 and mcr.class_id = ?2 and mcr.class_room_id =?3 \n"
            + "group by ls.stu_code,ls.fullname,ls.class_room ,mcr.class_id,mcr.class_room_id \n "
            + "order by ls.class_room,ls.stu_code \n ", countQuery = "select COUNT(*) \n"
            + "from tb_trn_class_room_lifeskill_score as ls \n"
            + "LEFT JOIN tb_mst_class_room AS mcr ON ls.class_room = mcr.class_room_sname_th \n"
            + "where ls.record_status <> 'D,C' and ls.year_group_id =?1 and mcr.class_id = ?2 and mcr.class_room_id =?3 \n", nativeQuery = true)

    Page<Map> findclassandyearandroom(Integer yearGroupId, Integer classId, Integer classRoomId, Pageable pageable);

    //for gra005
     @Query(value = "SELECT stl.sc_code,stl.year_group_name,stl.set_no,stl.seq_no,\n"
            + "msp.sc_name,msp.sub_district,msp.district,msp.province,\n"
            + "sp.stu_code,sp.citizen_id,mt.desc_th,sp.firstname_th,sp.lastname_th,sp.fullname,fn_thai_date(sp.dob) AS dob,sp.father_title,\n"
            + "sp.father_firstname_th,sp.father_lastname_th,sp.mother_title,sp.mother_firstname_th,sp.mother_lastname_th\n"
            + "FROM tb_stu_transcript_log stl\n"
            + "LEFT JOIN tb_mst_school_profile msp ON msp.sc_code = stl.sc_code\n"
            + "LEFT JOIN tb_stu_profile sp ON sp.stu_profile_id = stl.stu_profile_id\n"
            + "LEFT JOIN tb_mst_title mt ON mt.name = sp.title\n"
            + "WHERE stl.year_group_id = ?1 AND stl.class_id = ?2", nativeQuery = true)
    List<Map<String, String>> findGra005(Integer yearGroupId, Integer classId);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_stu_profile SET stu_status_id = ?1,stu_status = ?2,record_status = ?3,updated_user = ?4 WHERE stu_profile_id = ?5", nativeQuery = true)
    void updategraduate(Integer stuStatusId, String stuStatus,String recordStatus ,String updatedUser, Long stuProfileId);

    //by milo stu010
    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and citizen_id like %?1% and firstname_th like %?2% and lastname_th like %?3% and b.status_desc like %?4% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and citizen_id like %?1% and firstname_th like %?2% and lastname_th like %?3% and b.status_desc like %?4%", nativeQuery = true)
    Page<Map> findStu010(String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and stu_code like %?1% and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and stu_code like %?1% and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5%", nativeQuery = true)
    Page<Map> findstucodeStu010(String stuCode, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and citizen_id like %?2% and firstname_th like %?3% and lastname_th like %?4% and b.status_desc like %?5%", nativeQuery = true)
    Page<Map> findclassidStu010(Integer classId, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and d.class_room = ?2 and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and d.class_room = ?2 and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6%", nativeQuery = true)
    Page<Map> findclassidandclassroomidStu010(Integer classId, String classRoomId, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and d.class_room = ?2 and a.stu_code like %?3% and citizen_id like %?4% and firstname_th like %?5% and lastname_th like %?6% and b.status_desc like %?7% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and d.class_room = ?2 and a.stu_code like %?3% and citizen_id like %?4% and firstname_th like %?5% and lastname_th like %?6% and b.status_desc like %?7%", nativeQuery = true)
    Page<Map> findstucodeandclassidandclassroomidStu010(Integer classId, String classRoomId, String stuCode, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);

    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and a.stu_code like %?2% and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6% order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and c.class_id = ?1 and a.stu_code like %?2% and citizen_id like %?3% and firstname_th like %?4% and lastname_th like %?5% and b.status_desc like %?6%", nativeQuery = true)
    Page<Map> findstucodeandclassidStu010(Integer classId, String stuCode, String citizenId, String firstnameTh, String lastnameTh, String stuStatus, Pageable pageable);
    
    //Aong
    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join 			tb_mst_class_room d on a.class_room_id = d.class_room_id  where a.record_status <> 'D' and a.stu_status_id = '2' and a.firstname_th LIKE %?1% and b.status_desc LIKE %?2% 	order by a.class_id,a.class_room_id,a.stu_code ", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join 	tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and a.firstname_th 	LIKE %?1% and b.status_desc LIKE %?2% order by a.class_id,a.class_room_id,a.stu_code", nativeQuery = true)
    Page<Map> findfirstnamethStu010(String firstnameTh, String stuStatus, Pageable pageable);
    
    //Aong
    @Query(value = "select a.*,b.status_desc from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join tb_mst_class c on a.class_id = c.class_id left join 	tb_mst_class_room d on a.class_room_id = d.class_room_id  where a.record_status <> 'D' and a.stu_status_id = '2' and a.lastname_th LIKE %?1% and b.status_desc LIKE %?2% 	order by a.class_id,a.class_room_id,a.stu_code", countQuery = "select count(*) from tb_stu_profile a left join tb_mst_stu_status b on a.stu_status_id = b.stu_status_id left join 	tb_mst_class c on a.class_id = c.class_id left join tb_mst_class_room d on a.class_room_id = d.class_room_id where a.record_status <> 'D' and a.stu_status_id = '2' and a.lastname_th 	LIKE %?1% and b.status_desc LIKE %?2% order by a.class_id,a.class_room_id,a.stu_code", nativeQuery = true)
    Page<Map> findlastnameThStu010(String lastnameTh, String stuStatus, Pageable pageable);

    //Aong
    @Query(value = "SELECT stp.stu_code,stp.citizen_id,stp.firstname_th,stp.lastname_th,stp.dob,stp.gender,stp.religion,stp.citizenship,stp.father_title,stp.father_firstname_th,stp.father_lastname_th,stp.mother_title,stp.mother_firstname_th,stp.mother_lastname_th\n" +
                "FROM	tb_stu_profile stp\n" +
                "WHERE stp.stu_code like %?1%" , nativeQuery = true)
    List<Map> getStp(String stuCode);
    
    //Aong
    @Query(value = "SELECT a.stu_code,a.firstname_th,a.lastname_th,a.class_room,a.stu_image,a.class_name \n" +
                "FROM tb_stu_profile AS a\n" +
                "WHERE a.stu_code = ?1" , nativeQuery = true)
    List<Map> getStuPic(String stuCode);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_stu_profile SET class_room_id = ?1,class_room = ?2,record_status = ?3,updated_user = ?4\n"
            + "WHERE stu_profile_id = ?5", nativeQuery = true)
    void updateclassroomfromstu(Integer classRoomId,String classRoom,String recordStatus, String updatedUser, Long stuProfileId);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_stu_profile SET class_room_id = null,class_room = null,record_status = ?1,updated_user = ?2\n"
            + "WHERE stu_profile_id = ?3", nativeQuery = true)
    void deleteclassroomfromstudent(String recordStatus, String updatedUser, Long stuProfileId);
    
}
