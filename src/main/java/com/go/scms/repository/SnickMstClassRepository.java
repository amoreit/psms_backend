package com.go.scms.repository;

import com.go.scms.entity.SnickMstClassEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickMstClassRepository extends JpaRepository<SnickMstClassEntity,Long>{
    
        Page<SnickMstClassEntity> findAllByClassLnameThContainingAndCatclassLnameThContainingAndRecordStatusNotIn(String classLnameTh, String lnameTh, String recordStatus, Pageable pageable);
        
	List<SnickMstClassEntity> findAllByRecordStatusNotInOrderByClassId(String recordStatus);
        List<SnickMstClassEntity> findAllByRecordStatusNotInAndLangCodeNotInOrderByClassId(String recordStatus, String langCode);
        List<SnickMstClassEntity> findAllByRecordStatusNotInAndCatclassLnameThContainingOrderByClassId(String recordStatus,String lnameTh);
        List<SnickMstClassEntity> findAllByRecordStatusNotInAndCatclassCatClassIdAndLangCodeNotInOrderByClassId(String recordStatus, Integer catClassId, String langCode);
        List<SnickMstClassEntity> findAllByRecordStatusNotInAndCatclassCatClassIdNotInAndLangCodeNotInOrderByClassId(String recordStatus, Integer catClassId, String langCode);
        
        //------------------------ aca008 dialog teacher
        List<SnickMstClassEntity> findAllByRecordStatusNotInAndClassId(String recordStatus, Long classId);
}
