package com.go.scms.repository;

import com.go.scms.entity.SnickActivityTeacherEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickActivityTeacherRepository extends JpaRepository<SnickActivityTeacherEntity,Long>{
    
    //--------------------------------------- aca008 search teacher
    @Query(value = "SELECT a.activity_group_id, a.activity_id, a.class_id, a.activity_name, a.lesson,act.fullname, act.class_room_name, act.activity_teacher_id \n" +
                "FROM tb_mst_activity a \n" +
                "LEFT JOIN tb_trn_activity_teacher act ON a.sc_code = act.sc_code AND a.activity_group_id = act.activity_group_id AND a.activity_id = act.activity_id \n" +
                "WHERE act.sc_code='PRAMANDA' AND act.year_group_name = ?1 AND act.class_id = ?2 AND act.activity_group_id = ?3 AND act.activity_name = ?4 AND act.record_status <> ?5 \n" +
                "ORDER BY a.activity_name", nativeQuery = true)
    List<Map> findall(String yearGroupName, Integer classId, Integer activityGroupId, String activityName, String recordStatus);
    
    //--------------------------------------- aca008 delete teacher
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM tb_trn_activity_teacher where activity_teacher_id = ?1", nativeQuery = true)
    void deleteActivityTeacher(Long activityTeacherId);
    
    //--------------------------------------- aca008 search teacher report
    @Query(value = "SELECT a.*,tea.fullname FROM\n" +
                    "(\n" +
                        "SELECT act.year_group_name,a.activity_group_id,a.activity_name,act.teacher_id,string_agg(c.class_sname_th,',') AS CLASS \n" +
                        "FROM tb_mst_activity a \n" +
                        "LEFT JOIN tb_trn_activity_teacher act ON a.sc_code = act.sc_code AND a.activity_group_id = act.activity_group_id AND a.activity_id = act.activity_id \n" +
                        "LEFT JOIN tb_mst_class c ON a.class_id=c.class_id\n" +
                        "WHERE act.sc_code='PRAMANDA' AND act.year_group_name = ?1  AND act.activity_group_id = ?2 AND act.activity_name = ?3 AND act.record_status <> 'D'\n" +
                        "GROUP BY  act.year_group_name,a.activity_group_id, a.activity_name,act.teacher_id\n" +
                        "ORDER BY a.activity_name,CLASS\n" +
                    ")AS a\n" +
                    "LEFT JOIN tb_tea_teacher_profile tea ON a.teacher_id=tea.teacher_id", nativeQuery = true)
    List<Map> findallreport(String yearGroupName, Integer activityGroupId, String activityName);
}
