/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbMstSchoolProfileEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kittipong
 */
@Repository
public interface TbMstSchoolProfileRepository extends JpaRepository<TbMstSchoolProfileEntity, Long> {
       
    @Query(value = "SELECT scp.school_profile_id, scp.sc_code, scp.sc_name, scp.sc_name_en, scp.sc_type, scp.sc_register_no, scp.sc_register_authorized, scp.sc_doe, scp.sc_motto, scp.sc_vision, scp.sc_mission, scp.sc_about, scp.sc_logo, scp.sc_announce, scp.sc_announce_staff, scp.sc_announce_student, scp.sc_bank_account, scp.addr_no, scp.village, scp.village_no, scp.alley, scp.road, scp.sub_district, scp.district, scp.province, scp.country, scp.post_code, scp.tel, scp.tel2, scp.tel3, scp.fax, scp.email, scp.email2, scp.email3, scp.website, scp.facebook, scp.twitter \n"
            + "from tb_mst_school_profile scp "
            + "where scp.sc_code = ?1 ", nativeQuery = true)
    List<Map<String,String>> findScProfile (String scCode);
}
