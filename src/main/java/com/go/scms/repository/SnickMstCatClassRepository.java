package com.go.scms.repository;

import com.go.scms.entity.SnickMstCatClassEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickMstCatClassRepository extends JpaRepository<SnickMstCatClassEntity,Integer>{
    
        Page<SnickMstCatClassEntity> findAllByLnameThContaining(String lnameTh, Pageable pageable);
	List<SnickMstCatClassEntity> findAllByRecordStatusNotInOrderByCatClassId(String recordStatus);
        List<SnickMstCatClassEntity> findAllByRecordStatusNotInAndLnameThNotInOrderByCatClassId(String recordStatus, String lnameTh);

        Optional<SnickMstCatClassEntity> findByCatClassId(Integer catClassId);
 
}
