/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.StuStatusEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface StuStatusRepository extends JpaRepository<StuStatusEntity,Integer>{
    List<StuStatusEntity> findAllByRecordStatusNotInOrderByStuStatusId(String recordStatus);
    Optional<StuStatusEntity> findByStuStatusId(Integer stuStatusId);
}
