/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbPrintCardLogEntity;
import com.go.scms.entity.TbStuTranscriptLogEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kittipong
 */
@Repository
public interface TbStuTranscriptLogRepository extends  JpaRepository<TbStuTranscriptLogEntity,Long> {
    
    List<TbPrintCardLogEntity>findAllByRecordStatusNotIn(String recordStatus);
    
    
    @Query(value = "SELECT stl.set_no, stl.seq_no, stl.doctype_id\n" +
                        "FROM tb_stu_transcript_log stl\n" +
                        "WHERE stl.doctype_id = ?1 \n" +
                        "AND stl.set_no = ?2\n" +
                        "AND stl.seq_no = ?3", nativeQuery = true)
    List<Map> checkSetNoSeqNo(Integer doctypeId, String setNo, String seqNo);
    
    @Query(value = "SELECT stl.set_no, stl.seq_no, stl.doctype_id\n" +
                        "FROM tb_stu_transcript_log stl\n" +
                        "WHERE stl.doctype_id = ?1 \n" +
                        "AND stl.set_no = ?2 \n" +
                        "AND stl.seq_no = ?3 \n" +
                        "AND stl.stu_transcript_log_id <> ?4", nativeQuery = true)
    List<Map> checkUpdataSetNoSeqNo(Integer doctypeId, String setNo, String seqNo, Long stuTranscriptLogId);
    
    @Query(value = "SELECT stl.set_no,stl.doctype_id\n" +
                        "FROM tb_stu_transcript_log stl\n" +
                        "WHERE stl.doctype_id = ?1 \n" +
                        "AND stl.set_no = ?2\n" +
                        "AND stl.year_group_id = ?3 \n", nativeQuery = true)
    List<Map> checkSetNo(Integer doctypeId, String setNo,Integer yearGroupId);
    
    @Query(value = "SELECT stl.set_no,stl.doctype_id\n" +
                        "FROM tb_stu_transcript_log stl\n" +
                        "WHERE stl.doctype_id = ?1 \n" +
                        "AND stl.set_no = ?2 \n" +
                        "AND stl.set_no = ?2 \n" +
                        "AND stl.year_group_id = ?3 \n" +
                        "AND stl.stu_transcript_log_id <> ?4", nativeQuery = true)
    List<Map> checkUpdataSetNo(Integer doctypeId, String setNo,Integer yearGroupId,Long stuTranscriptLogId);
    
    //aong
    @Query(value = "SELECT stl.stu_transcript_log_id,stl.transcript_date\n" +
                        "FROM tb_stu_transcript_log stl\n" +
                        "WHERE stl.stu_transcript_log_id = ?1", nativeQuery = true)
    List<Map> TranscriptDate(Integer stuTranscriptLogId);
}
