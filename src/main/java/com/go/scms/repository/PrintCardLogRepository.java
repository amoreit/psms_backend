/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.PrintCardLogEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface PrintCardLogRepository extends JpaRepository<PrintCardLogEntity,Long>{
    
        Page<PrintCardLogEntity> findAllByRecordStatusNotIn(String recordStatus, Pageable pageable);

        @Query(value = "select coalesce(MAX(itemno),0) as max_itemno from tb_stu_card_print_log where stu_profile_id = ?1", nativeQuery = true)
        List<Map> finditemno(Long id);
}
