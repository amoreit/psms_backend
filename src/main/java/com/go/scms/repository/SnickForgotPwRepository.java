package com.go.scms.repository;

import com.go.scms.entity.SnickForgotPwEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickForgotPwRepository extends JpaRepository<SnickForgotPwEntity, Integer>{
    
    List<SnickForgotPwEntity> findAllByCitizenIdAndUsrName(String citizenId, String usrName);
    
    @Transactional
    @Modifying
    @Query("UPDATE SnickForgotPwEntity pw SET pw.password = ?1, pw.activated = true where pw.userId = ?2")
    void updateforgotpw(String password, Integer userId);
}
