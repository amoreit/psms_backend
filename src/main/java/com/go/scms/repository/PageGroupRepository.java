package com.go.scms.repository;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.go.scms.entity.PageGroupEntity;

@Repository
public interface PageGroupRepository extends JpaRepository<PageGroupEntity,Long>{

	Page<PageGroupEntity> findAllByPageGroupCodeContainingAndPageGroupNameContaining(String pageGroupCode,String pageGroupName, Pageable pageable);
	Optional<PageGroupEntity> findByPageGroupCode(String pageGroupCode);
	
	@Query("select pg from PageGroupEntity pg where pg.pageGroupStatus <> ?1 and (pg.pageGroupName like %?2% or pg.pageGroupCode like %?2%)")
	Page<PageGroupEntity> findAllByPageGroupStatusNotIn(String pageGroupStatus,String text, Pageable pageable);
}
