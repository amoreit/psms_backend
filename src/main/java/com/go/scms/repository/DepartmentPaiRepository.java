/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.DepartmentPaiEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DepartmentPaiRepository extends JpaRepository<DepartmentPaiEntity,Long>{  
        Page<DepartmentPaiEntity> findAllByDepartmentCodeContainingAndCattclassLnameThContainingAndNameContainingAndRecordStatusNotIn(String departmentCode,String lnameTh , String name,String recordStatus, Pageable pageable);
        List<DepartmentPaiEntity> findAllByRecordStatusNotIn(String recordStatus);      
}
