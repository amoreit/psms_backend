package com.go.scms.repository;

import com.go.scms.entity.SnickDepartmentEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickDepartmentRepository extends JpaRepository<SnickDepartmentEntity, Long>{
    
        List<SnickDepartmentEntity> findAllByRecordStatusNotInAndCatClassAndName(String recordStatus, Integer catClass, String name);
        List<SnickDepartmentEntity> findAllByCatClassAndRecordStatusNotInOrderByDepartmentIdAsc(Integer catClass, String recordStatus);
}
