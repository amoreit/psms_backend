/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.DepartmentEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Integer> {

    Page<DepartmentEntity> findAllByNameContaining(String name, Pageable pageable);
    Optional<DepartmentEntity> findByDepartmentId(Integer departmentId);
    List<DepartmentEntity> findAllByRecordStatusNotInAndCatclassLnameThContainingOrderByDepartmentId(String recordStatus, String lnameTh);
    List<DepartmentEntity> findAllByRecordStatusNotInOrderByDepartmentIdAsc(String recordStatus);
    
    @Query(value = "SELECT * FROM tb_mst_department WHERE cat_class = ?1 and record_status <> 'D' order BY orders", nativeQuery = true)
    List<Map> findddlfiltercatclass(Integer catClassId);
}
