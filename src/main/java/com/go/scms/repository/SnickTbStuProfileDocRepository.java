package com.go.scms.repository;

import com.go.scms.entity.SnickTbStuProfileDocEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTbStuProfileDocRepository extends JpaRepository<SnickTbStuProfileDocEntity,Long>{        
        List<SnickTbStuProfileDocEntity> findAllByRecordStatusNotInAndStuProfileStuProfileId(String recordStatus, Long stuProfileId);
}
