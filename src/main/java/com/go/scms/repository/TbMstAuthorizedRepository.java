package com.go.scms.repository;

import com.go.scms.entity.TbMstAuthorizedEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

 
@Repository
public interface TbMstAuthorizedRepository extends JpaRepository<TbMstAuthorizedEntity,Long>{

    Page<TbMstAuthorizedEntity> findAllByFullnameThContainingAndFullnameEnContainingAndPositionAndRecordStatusNotIn(String fullnameTh,String fullnameEn,String position, String recordStatus, Pageable pageable);    
    Page<TbMstAuthorizedEntity> findAllByFullnameThContainingAndFullnameEnContainingAndRecordStatusNotIn(String fullnameTh,String fullnameEn, String recordStatus, Pageable pageable); 
    
    List<TbMstAuthorizedEntity> findAllByRecordStatusNotInAndPosition(String recordStatus, String position);
    Optional<TbMstAuthorizedEntity> findByIscurrent(Boolean iscurrent);
           
    @Transactional
    @Modifying
    @Query("UPDATE TbMstAuthorizedEntity y SET y.iscurrent = ?1")
    void changeiscurrent(Boolean iscurrent);
    
    //Aong
    @Query(value = "SELECT a.signature_image_path FROM tb_mst_authorized a WHERE a.position LIKE 'ผู้อำนวยการ' AND a.iscurrent = TRUE", nativeQuery = true)
    List<Map> findSignatureImagePath();
    
    //Aong
    @Query(value = "SELECT aut.fullname_th,aut.iscurrent,aut.position FROM tb_mst_authorized aut" , nativeQuery = true)
    List<Map<String, String>> getPosition();
}
    

