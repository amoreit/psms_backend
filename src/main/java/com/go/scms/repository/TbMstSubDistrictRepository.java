/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbMstSubDistrictEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lumin
 */
@Repository
public interface TbMstSubDistrictRepository extends JpaRepository<TbMstSubDistrictEntity,Long>{
    
    List<TbMstSubDistrictEntity>findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(String recordStatus, String districtTh);
    
    //------------------ P'pai
    List<TbMstSubDistrictEntity>findAllByRecordStatusNotInAndDistrictDistrictThAndSubDistrictTh(String recordStatus, String districtTh , String subDistrictTh);
}
