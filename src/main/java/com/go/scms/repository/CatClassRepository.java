/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.CatClassEntity;
import com.go.scms.entity.DepartmentEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface CatClassRepository extends JpaRepository<CatClassEntity,Long>{
     Page<CatClassEntity> findAllByLnameThContaining(String lnameTh, Pageable pageable);
     List<CatClassEntity> findAllByRecordStatusNotInOrderByCatClassId(String recordStatus);
}
