/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.YearGroupEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface YearGroupRepository extends JpaRepository<YearGroupEntity,Long>{
    
    Page<YearGroupEntity> findAllBynameContainingAndYearYearCodeContaining(String name, String yearCode, Pageable pageable);
    Page<YearGroupEntity> findAllBynameContainingAndYearYearCodeContainingAndRecordStatusNotIn(String name, String yearCode, String recordStatus, Pageable pageable);

    List<YearGroupEntity> findAllByRecordStatusNotInOrderByYearGroupId(String recordStatus);
    List<YearGroupEntity> findAllByRecordStatusNotInOrderByIscurrentDescYearGroupIdDesc(String recordStatus);

    Optional<YearGroupEntity> findByIscurrent(Boolean iscurrent);
    Optional<YearGroupEntity> findByYearGroupId(Long yearGroupId);
    
    List<YearGroupEntity> findAllByRecordStatusNotInAndIscurrentOrderByYearGroupId(String recordStatus, Boolean iscurrent);   
    List<YearGroupEntity> findAllByRecordStatusNotInAndNameOrderByYearGroupId(String recordStatus, String name);  
    
    @Transactional
    @Modifying
    @Query("UPDATE YearGroupEntity y SET y.iscurrent = ?1")
    void changeiscurrent(Boolean iscurrent);
	
    @Query(value = "SELECT yg.year_group_id,yg.year_term_id,yg.name,\n"
            + "CAST(CAST((CAST(SUBSTR(yg.name,3,4) AS TEXT)||CAST(SUBSTR(yg.name,1,1) AS TEXT))AS INT) AS TEXT)AS year_term\n"
            + "FROM tb_mst_year_group yg\n"
            + "WHERE CAST((CAST(SUBSTR(yg.name,3,4) AS TEXT)||CAST(SUBSTR(yg.name,1,1) AS TEXT))AS INT)> ?1 AND yg.record_status <> 'D'\n"
            + "order by year_term ASC", nativeQuery = true)
    List<Map> findddlupperyeargroup(Integer yearGroupSearch);
    
    @Query(value = "SELECT yg.year_group_id,yg.year_term_id,yg.name,yg.iscurrent,\n"
            + "CAST(CAST((CAST(SUBSTR(yg.name,3,4) AS TEXT)||CAST(SUBSTR(yg.name,1,1) AS TEXT))AS INT) AS TEXT)AS year_term\n"
            + "FROM tb_mst_year_group yg\n"
            + "WHERE CAST((CAST(SUBSTR(yg.name,3,4) AS TEXT)||CAST(SUBSTR(yg.name,1,1) AS TEXT))AS INT)>= ?1 AND yg.record_status <> 'D'\n"
            + "order by year_term ASC", nativeQuery = true)
    List<Map> findddlcheckyeargroup(Integer yearGroupSearch);
}
