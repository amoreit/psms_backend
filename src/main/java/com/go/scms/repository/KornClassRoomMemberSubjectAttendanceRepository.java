/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.KornClassRoomMemberSubjectAttendanceEntity;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kronn
 */
@Repository
public interface KornClassRoomMemberSubjectAttendanceRepository extends  JpaRepository<KornClassRoomMemberSubjectAttendanceEntity,Long> {
            
    @Query(value = "SELECT a.class_room_member_id,a.sc_code,a.year_group_id,a.year_group_name,a.class_id,a.class_room_id,a.class_room,a.class_name,a.class_room_no,a.stu_code,a.fullname,a.class_room_subject_id,a.subject_code,a.subject_name_th,a.class_room_member_id AS class_room_member_subject_attendance_id,a.created_date AS date_taken,a.remark AS reason,a.remark \n" 
               + " FROM ( SELECT crm.class_room_member_id,crm.year_group_name,crm.sc_code,crm.year_group_id,crm.class_id,crm.class_room_id,crm.class_room,crm.class_room_no,crm.stu_code,crm.fullname,crm.class_name,crm.created_date,crm.remark,crs.subject_code,crs.subject_name_th,crs.class_room_subject_id FROM tb_trn_class_room_member crm\n" 
               + " INNER JOIN tb_trn_class_room_subject crs ON crm.sc_code=crm.sc_code AND crm.year_group_id=crs.year_group_id \n" 
               + " AND crm.class_room_id=crs.class_room_id\n" 
               + " WHERE crm.year_group_id = ?1 AND crm.class_id = ?2 AND crm.class_room_id = ?3 AND crs.class_room_subject_id = ?4  AND crm.record_status <> 'D' AND crm.role = 'student' \n" 
               + " ORDER BY cast(crm.class_room_no AS INT) ) AS a\n"
               + " ORDER BY cast(a.class_room_no AS INT)", nativeQuery = true)
    List<Map<String,String>> searchallDontDate(Integer yearGroupId, Integer classId, Integer classRoomId, Integer classRoomSubjectId);

    @Query(value = "SELECT a.class_room_member_id,a.sc_code,a.year_group_id,a.year_group_name,a.class_id,a.class_room_id,a.class_name,a.class_room_no,a.stu_code,a.fullname,a.subject_code,a.class_room_subject_id,a.subject_name_th,sa.date_taken,sa.reason,sa.remark,sa.class_room_member_subject_attendance_id\n" 
              + " FROM ( SELECT crm.class_room_member_id,crm.year_group_name,crm.sc_code,crm.year_group_id,crm.class_id,crm.class_room_id,crm.class_room_no,crm.stu_code,crm.fullname,crs.subject_code,crs.subject_name_th,class_name,crs.class_room_subject_id FROM tb_trn_class_room_member crm\n" 
              + " INNER JOIN tb_trn_class_room_subject crs ON crm.sc_code=crm.sc_code AND crm.year_group_id=crs.year_group_id \n" 
              + " AND crm.class_room_id=crs.class_room_id\n" 
              + " WHERE crm.year_group_id = ?1 AND crm.class_id = ?2 AND crm.class_room_id = ?3 AND crs.class_room_subject_id = ?4 AND crm.record_status <> 'D' AND crm.role = 'student'\n" 
              + " ORDER BY cast(crm.class_room_no AS INT) ) AS a\n" 
              + " LEFT JOIN tb_trn_class_room_member_subject_attendance sa ON a.sc_code=sa.sc_code AND a.year_group_id=sa.year_group_id \n" 
              + " AND a.class_room_member_id=sa.class_room_member_id AND a.class_id=sa.class_id AND a.class_room_id=sa.class_room_id WHERE to_char(sa.date_taken,'YYYY-MM-DD') = ?5\n" 
              + " ORDER BY cast(a.class_room_no AS INT)", nativeQuery = true)
    List<Map<String,String>> searchallDate(Integer yearGroupId, Integer classId, Integer classRoomId, Integer classRoomSubjectId, String searchDate);
 
    @Transactional
    @Modifying
    @Query("UPDATE KornClassRoomMemberSubjectAttendanceEntity crs SET crs.yearGroupId = ?1, \n"
            + "crs.yearGroupName = ?2, crs.classRoomId = ?3, crs.classId =?4, \n"
            + "crs.classRoomMemberId = ?5, crs.classRoomSubjectId = ?6, crs.classRoomSnameTh = ?7, crs.stuCode = ?8, \n"
            + "crs.fullname = ?9, crs.classRoomNo = ?10, crs.subjectCode = ?11, crs.subjectName = ?12, crs.reason = ?13, crs.remark = ?14, \n"
            + "crs.updatedUser = ?15, crs.recordStatus = 'U', crs.isactive = true where crs.classRoomMemberSubjectAttendanceId = ?16")
    void manageSubjectAttendance(
            Integer yearGroupId
            ,String yearGroupName
            ,Integer classRoomId
            ,Integer classId
            ,Integer classRoomMemberId
            ,Integer classRoomSubjectId
            ,String classRoomSnameTh
            ,String stuCode
            ,String fullname
            ,String classRoomNo
            ,String subjectCode
            ,String subjectName
            ,String reason
            ,String remark
            ,String userName
            ,Long classRoomMemberSubjectAttendanceId
    );
}

