/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbMstSubDivisionEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sir. Harinn
 */
@Repository
public interface TbMstSubDivisionRepository extends JpaRepository<TbMstSubDivisionEntity, Long> {

    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D'  ORDER BY md.division_id  ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' ", nativeQuery = true)
    Page<Map> show( Pageable pageable);
    
    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND md.division_name_th LIKE %?1% ORDER BY md.division_id ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE md.division_name_th LIKE %?1%", nativeQuery = true)
    Page<Map> findByDivisionNameTh(String divisionNameTh, Pageable pageable);
    
    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND  msd.sub_division_name_th LIKE %?1% ORDER BY md.division_id  ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE  msd.sub_division_name_th LIKE %?1%", nativeQuery = true)
    Page<Map> findBySubDivisionNameTh( String subDivisionNameTh, Pageable pageable);
    
    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND  msd.sub_division_name_en LIKE %?1%  ORDER BY md.division_id  ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE  msd.sub_division_name_en LIKE %?1% ", nativeQuery = true)
    Page<Map> findBySubDivisionNameEn( String subDivisionNameEn, Pageable pageable);
    
    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND md.division_name_th LIKE %?1% AND msd.sub_division_name_th LIKE %?2% ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE md.division_name_th LIKE %?1% AND msd.sub_division_name_th LIKE %?2%", nativeQuery = true)
    Page<Map> findByDNT_SDNT(String divisionNameTh, String subDivisionNameTh, Pageable pageable);
    
    @Query(value = " SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND md.division_name_th LIKE %?1%  AND msd.sub_division_name_en LIKE %?2%  ORDER BY md.division_id ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE md.division_name_th LIKE %?1%  AND msd.sub_division_name_en LIKE %?2% ", nativeQuery = true)
    Page<Map> findByDNT_SDNE(String divisionNameTh, String subDivisionNameEn, Pageable pageable);
    
    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND  msd.sub_division_name_th LIKE %?1% AND msd.sub_division_name_en LIKE %?2%  ORDER BY md.division_id  ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE  msd.sub_division_name_th LIKE %?1% AND msd.sub_division_name_en LIKE %?2% ", nativeQuery = true)
    Page<Map> findBySDNT_SDNE( String subDivisionNameTh, String subDivisionNameEn, Pageable pageable);
    
    @Query(value = "SELECT msd.* , md.division_name_th FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE msd.record_status <> 'D' AND md.division_name_th LIKE %?1% AND msd.sub_division_name_th LIKE %?2% AND msd.sub_division_name_en LIKE %?3%  ORDER BY md.division_id ", countQuery = "select count(*) FROM tb_mst_sub_division msd LEFT JOIN  tb_mst_division md ON msd.division_id = md.division_id WHERE md.division_name_th LIKE %?1% AND msd.sub_division_name_th LIKE %?2% AND msd.sub_division_name_en LIKE %?3% ", nativeQuery = true)
    Page<Map> findByDNT_SDNT_SDNE(String divisionNameTh, String subDivisionNameTh, String subDivisionNameEn, Pageable pageable);

}
