package com.go.scms.repository;

import com.go.scms.entity.SnickAdmRoleEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickAdmRoleRepository extends JpaRepository<SnickAdmRoleEntity, Long>{
        List<SnickAdmRoleEntity> findAllByRoleStatusNotInAndRoleType(String roleStatus, String roleType);
        List<SnickAdmRoleEntity> findAllByRoleStatusNotIn(String roleStatus);
}
