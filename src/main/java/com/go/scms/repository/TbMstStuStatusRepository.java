package com.go.scms.repository;

import com.go.scms.entity.TbMstStuStatusEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TbMstStuStatusRepository extends JpaRepository<TbMstStuStatusEntity,Long>{    
        Optional<TbMstStuStatusEntity> findByStuStatusId(Long stuStatusId);
}
