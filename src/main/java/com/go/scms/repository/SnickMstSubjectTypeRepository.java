package com.go.scms.repository;

import com.go.scms.entity.SnickMstSubjectTypeEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickMstSubjectTypeRepository extends JpaRepository<SnickMstSubjectTypeEntity,Long>{
    
        Page<SnickMstSubjectTypeEntity> findAllBySubjectTypeCodeContainingAndNameContainingAndRecordStatusNotIn(String subjectTypeCode, String name, String recordStatus, Pageable pageable);
	List<SnickMstSubjectTypeEntity> findAllByRecordStatusNotIn(String recordStatus);
}
