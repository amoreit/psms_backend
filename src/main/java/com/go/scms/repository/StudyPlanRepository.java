/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTrnClassRoomMemberEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sir. Harinn
 */
@Repository
public interface StudyPlanRepository extends JpaRepository<TbTrnClassRoomMemberEntity, Long> {
    
 
    @Query(value = "SELECT cr.class_id,  cr.class_room_id,c.class_lname_en, cr.class_room, cr.class_room_sname_th, cc.lname_th, c.class_lname_th, crm.year_group_name\n" +
                    "FROM tb_mst_class_room cr\n" +
                    "LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
                    "LEFT JOIN tb_mst_cat_class cc ON cc.cat_class_id = c.cat_class_id\n" +
                    "LEFT JOIN tb_trn_class_room_member crm ON crm.class_room_id = cr.class_room_id\n" +
                    "WHERE cr.record_status <> 'D' AND cr.class_room NOT LIKE  '%-%' AND crm.year_group_name = ?1  AND cr.class_id = ?2 \n" +
                    "GROUP BY cr.class_id, cr.class_room_id,c.class_lname_en, cr.class_room_sname_th, cc.lname_th, c.class_lname_th, crm.year_group_name\n" +
                    "ORDER BY cr.class_room_id, cr.class_room", countQuery ="select count(*) FROM tb_mst_class_room cr\n" +
                                                                            "LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
                                                                            "LEFT JOIN tb_mst_cat_class cc ON cc.cat_class_id = c.cat_class_id\n" +
                                                                            "LEFT JOIN tb_trn_class_room_member crm ON crm.class_room_id = cr.class_room_id\n" +
                                                                            "WHERE cr.record_status <> 'D' AND cr.class_room NOT LIKE  '%-%' AND crm.year_group_name = ?1  AND cr.class_id = ?2 \n" +
                                                                            "GROUP BY cr.class_id, cr.class_room_id,c.class_lname_en, cr.class_room_sname_th, cc.lname_th, c.class_lname_th, crm.year_group_name\n" +
                                                                            "ORDER BY cr.class_room_id, cr.class_room ", nativeQuery = true)
    Page<Map> findYearClass( String yearGroupName, Integer classId, Pageable pageable);
    
  
    @Query(value = "SELECT d.* FROM \n" +
"	(\n" +
"	SELECT row_number() over (ORDER BY c.subject_type), C.* FROM \n" +
"		(\n" +
"		SELECT a.* from\n" +
"			(\n" +
"			SELECT crs.subject_code, crs.subject_name_th,crs.subject_code2,crs.subject_name_en, coalesce(crs.credit, 0.0) AS credit,s.subject_type, cr.class_room_sname_th,c.class_lname_en,cr.class_room, yg.name, cc.cat_class_id,\n" +
"			coalesce(crs.lesson, '0') AS lesson,\n" +
"			SUM(coalesce(crs.credit, 0.0)) over() AS total_credit_type1,\n" +
"			coalesce(SUM(CAST(crs.lesson AS INT)) over() , 0) AS total_lesson_type1,\n" +
"			coalesce(t2.total_credit_type2,0.0) AS total_credit_type2,\n" +
"			coalesce(t2.total_lesson_type2, 0) AS total_lesson_type2\n" +
"			FROM tb_trn_class_room_subject crs\n" +
"			LEFT JOIN\n" +
"				(\n" +
"				SELECT crs.class_room_id, SUM(coalesce(crs.credit, 0.0)) AS total_credit_type2, SUM(CAST(crs.lesson AS INT)) AS total_lesson_type2\n" +
"				FROM tb_trn_class_room_subject crs\n" +
"				LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"				LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"				LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"				LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"				WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2 AND  crs.class_room_id = ?3 AND s.subject_type=2\n" +
"				GROUP BY crs.class_room_id\n" +
"				) t2\n" +
"				ON t2.class_room_id = crs.class_room_id\n" +
"			LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"			LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"			LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"			LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"			LEFT JOIN tb_mst_cat_class cc ON c.cat_class_id = cc.cat_class_id\n" +
"			WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2 AND  crs.class_room_id = ?3 AND s.subject_type=1\n" +
"			ORDER BY  crs.orders,crs.subject_code\n" +
"			)AS a\n" +
"			UNION ALL\n" +
"			SELECT b.* from\n" +
"			(\n" +
"			SELECT crs.subject_code, crs.subject_name_th,crs.subject_code2,crs.subject_name_en, coalesce(crs.credit, 0.0) AS credit,s.subject_type, cr.class_room_sname_th,c.class_lname_en,cr.class_room, yg.name, cc.cat_class_id,\n" +
"			coalesce(crs.lesson, '0') AS lesson,\n" +
"			SUM(coalesce(crs.credit, 0.0)) over() AS total_credit_type1,\n" +
"			coalesce(SUM(CAST(crs.lesson AS INT)) over() , 0) AS total_lesson_type1,\n" +
"			coalesce(t2.total_credit_type2,0.0) AS total_credit_type2,\n" +
"			coalesce(t2.total_lesson_type2, 0) AS total_lesson_type2\n" +
"			FROM tb_trn_class_room_subject crs\n" +
"			LEFT JOIN\n" +
"				(\n" +
"				SELECT crs.class_room_id, SUM(coalesce(crs.credit, 0.0)) AS total_credit_type2, SUM(CAST(crs.lesson AS INT)) AS total_lesson_type2\n" +
"				FROM tb_trn_class_room_subject crs\n" +
"				LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"				LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"				LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"				LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"				WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2 AND  crs.class_room_id = ?3 AND s.subject_type=2\n" +
"				GROUP BY crs.class_room_id\n" +
"				) t2\n" +
"				ON t2.class_room_id = crs.class_room_id\n" +
"			LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"			LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"			LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"			LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"			LEFT JOIN tb_mst_cat_class cc ON c.cat_class_id = cc.cat_class_id\n" +
"			WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2  AND  crs.class_room_id = ?3 AND s.subject_type=2\n" +
"			ORDER BY  crs.orders,crs.subject_code\n" +
"			)AS b\n" +
"		) C\n" +
"	) AS d", countQuery ="", nativeQuery = true)
    List<Map> findTerm1( String yearGroupName, Integer classId, Integer classRoomId, Pageable pageable);
       
    @Query(value = "SELECT d.* FROM \n" +
"	(\n" +
"	SELECT row_number() over (ORDER BY c.subject_type), C.* FROM \n" +
"		(\n" +
"		SELECT a.* from\n" +
"			(\n" +
"			SELECT crs.subject_code, crs.subject_name_th,crs.subject_code2,crs.subject_name_en, coalesce(crs.credit, 0.0) AS credit,s.subject_type, cr.class_room_sname_th, yg.name, cc.cat_class_id,\n" +
"			coalesce(crs.lesson, '0') AS lesson,\n" +
"			SUM(coalesce(crs.credit, 0.0)) over() AS total_credit_type1,\n" +
"			coalesce(SUM(CAST(crs.lesson AS INT)) over() , 0) AS total_lesson_type1,\n" +
"			coalesce(t2.total_credit_type2,0.0) AS total_credit_type2,\n" +
"			coalesce(t2.total_lesson_type2, 0) AS total_lesson_type2\n" +
"			FROM tb_trn_class_room_subject crs\n" +
"			LEFT JOIN\n" +
"				(\n" +
"				SELECT crs.class_room_id, SUM(coalesce(crs.credit, 0.0)) AS total_credit_type2, SUM(CAST(crs.lesson AS INT)) AS total_lesson_type2\n" +
"				FROM tb_trn_class_room_subject crs\n" +
"				LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"				LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"				LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"				LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"				WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2 AND  crs.class_room_id = ?3 AND s.subject_type=2\n" +
"				GROUP BY crs.class_room_id\n" +
"				) t2\n" +
"				ON t2.class_room_id = crs.class_room_id\n" +
"			LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"			LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"			LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"			LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"			LEFT JOIN tb_mst_cat_class cc ON c.cat_class_id = cc.cat_class_id\n" +
"			WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2 AND  crs.class_room_id = ?3 AND s.subject_type=1\n" +
"			ORDER BY  crs.orders,crs.subject_code\n" +
"			)AS a\n" +
"			UNION ALL\n" +
"			SELECT b.* from\n" +
"			(\n" +
"			SELECT crs.subject_code, crs.subject_name_th,crs.subject_code2,crs.subject_name_en, coalesce(crs.credit, 0.0) AS credit,s.subject_type, cr.class_room_sname_th, yg.name, cc.cat_class_id,\n" +
"			coalesce(crs.lesson, '0') AS lesson,\n" +
"			SUM(coalesce(crs.credit, 0.0)) over() AS total_credit_type1,\n" +
"			coalesce(SUM(CAST(crs.lesson AS INT)) over() , 0) AS total_lesson_type1,\n" +
"			coalesce(t2.total_credit_type2,0.0) AS total_credit_type2,\n" +
"			coalesce(t2.total_lesson_type2, 0) AS total_lesson_type2\n" +
"			FROM tb_trn_class_room_subject crs\n" +
"			LEFT JOIN\n" +
"				(\n" +
"				SELECT crs.class_room_id, SUM(coalesce(crs.credit, 0.0)) AS total_credit_type2, SUM(CAST(crs.lesson AS INT)) AS total_lesson_type2\n" +
"				FROM tb_trn_class_room_subject crs\n" +
"				LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"				LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"				LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"				LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"				WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2 AND  crs.class_room_id = ?3 AND s.subject_type=2\n" +
"				GROUP BY crs.class_room_id\n" +
"				) t2\n" +
"				ON t2.class_room_id = crs.class_room_id\n" +
"			LEFT JOIN tb_mst_subject s ON crs.subject_code=s.subject_code\n" +
"			LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n" +
"			LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n" +
"			LEFT JOIN tb_mst_class c ON cr.class_id = c.class_id\n" +
"			LEFT JOIN tb_mst_cat_class cc ON c.cat_class_id = cc.cat_class_id\n" +
"			WHERE crs.sc_code='PRAMANDA' AND  yg.name = ?1 AND c.class_id = ?2  AND  crs.class_room_id = ?3 AND s.subject_type=2\n" +
"			ORDER BY  crs.orders,crs.subject_code\n" +
"			)AS b\n" +
"		) C\n" +
"	) AS d", countQuery ="", nativeQuery = true)
    Page<Map> findTerm2( String yearGroupName, Integer classId, Integer classRoomId, Pageable pageable);
    
}
