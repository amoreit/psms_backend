package com.go.scms.repository;

import com.go.scms.entity.TbMstEducationEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TbMstEducationRepository extends JpaRepository<TbMstEducationEntity, Long>{   
        List<TbMstEducationEntity> findAllByRecordStatusNotInAndLangCodeOrderByEducationId(String recordStatus, String langCode);
}
