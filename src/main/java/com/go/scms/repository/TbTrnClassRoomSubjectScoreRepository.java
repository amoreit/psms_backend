/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTrnClassRoomSubjectScoreEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lumin
 */
@Repository
public interface TbTrnClassRoomSubjectScoreRepository extends JpaRepository<TbTrnClassRoomSubjectScoreEntity, Long> {

    Page<TbTrnClassRoomSubjectScoreEntity> findAllByyearGroupNameContainingAndClassRoomSnameThContainingAndSubjectCodeContaining(String yearGroupName, String classRoomSnameTh, String subjectCode, Pageable pageable);

    List<TbTrnClassRoomSubjectScoreEntity> findAllByyearGroupIdAndClassRoomSubjectIdOrderByClassRoomNo(Integer yearGroupId, Integer classRoomSubjectId);

    Optional<TbTrnClassRoomSubjectScoreEntity> findByStuCodeAndSubjectCode(String stuCode, String subjectCode);

    @Query(value = "SELECT crm.stu_code,crm.fullname,crm.class_room,crm.year_group_name,\n"
            + "crss.subject_code,crss.subject_name,crss.score1,crss.score2,crss.score_midterm,crss.score_final,crss.full_score,crss.grade,crss.sum_score\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_trn_class_room_subject_score crss ON crm.class_room_member_id = crss.class_room_member_id\n"
            + "WHERE crm.class_room_member_id = ?1 AND crss.isactive = true\n"
            + "ORDER BY crss.class_room_subject_score_id", nativeQuery = true)
    Page<Map> findAca006Form(Integer classRoomMemberId, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE TbTrnClassRoomSubjectScoreEntity y SET y.score1 = ?1, y.score2 = ?2, y.scoreMidterm = ?3, y.scoreFinal = ?4, y.remark = ?5, y.isMorsor = ?6, y.isExchange = ?7, y.recordStatus = ?8, y.updatedUser = ?9 where y.classRoomSubjectScoreId = ?10")
    void updatescore(double score1, double score2, double scoreMidterm, double scoreFinal, String remark, boolean isMorsor, boolean isExchange, String recordStatus, String updatedUser, Long classRoomSubjectScoreId);

    //max
    @Transactional
    @Modifying
    @Query("UPDATE TbTrnClassRoomSubjectScoreEntity y SET y.scoreFinal = ?1, y.recordStatus = ?2, y.updatedUser = ?3 where y.classRoomSubjectScoreId = ?4")
    void updatescore2(double scoreFinal, String recordStatus, String updatedUser, Long classRoomSubjectScoreId);

    //aong
    @Query(value = "SELECT a.year_group_name,a.class_lname_th,a.class_room_sname_th,a.subject_code||' '||a.subject_name AS subject,a.credit,a.grade,a.subject_type FROM \n"
            + "(\n"
            + "SELECT crss.year_group_id,crss.year_group_name,crss.department_id,crss.class_id,crss.class_room_sname_th,crss.subject_code,crss.subject_type,\n"
            + "crss.subject_name,crss.credit,crss.grade,c.class_lname_th\n"
            + "FROM tb_trn_class_room_subject_score crss \n"
            + "LEFT JOIN tb_mst_class c ON crss.class_id=c.class_id\n"
            + "WHERE crss.sc_code='PRAMANDA' AND crss.stu_code = ?1\n"
            + ") a\n"
            + "LEFT JOIN tb_mst_department d ON a.department_id=d.department_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON a.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "ORDER BY CAST(CAST((CAST(SUBSTR(yg.name,3,4) AS TEXT)||CAST(SUBSTR(yg.name,1,1) AS TEXT))AS INT) AS TEXT),\n"
            + "CASE \n"
            + "WHEN cast(a.subject_type as INT) =1 THEN 1 \n"
            + "WHEN cast(a.subject_type as INT )=2 THEN 2 \n"
            + "END,\n"
            + "d.orders,\n"
            + "CASE WHEN SUBSTR(a.subject_name,1,1)='ส' THEN 1\n"
            + "WHEN SUBSTR(a.subject_name,1,1)='ป' THEN 2\n"
            + "ELSE 3\n"
            + "END", nativeQuery = true)
    List<Map<String, String>> getSecondarySubjectScore(String stuCode);

    //aong
    @Query(value = "SELECT REPLACE(d.name,'กลุ่มสาระการเรียนรู้','')AS subject,SUM(CAST((crss.credit)AS DECIMAL(5,2))),CAST(TRUNC(SUM((CAST(CAST((crss.credit)AS DECIMAL(5,2))*CAST((crss.grade)AS DECIMAL(5,2))AS DECIMAL(5,2))))/ SUM(crss.credit),2)AS DECIMAL(5,2)) AS GPA\n"
            + "FROM tb_trn_class_room_subject_score crss \n"
            + "LEFT JOIN tb_mst_class c ON crss.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_department d ON crss.department_id=d.department_id\n"
            + "WHERE crss.sc_code='PRAMANDA' AND  crss.stu_code = ?1\n"
            + "GROUP BY crss.department_id,d.orders,d.name\n"
            + "ORDER BY d.orders", nativeQuery = true)
    List<Map<String, String>> getSumScore(String stuCode);

    //aong
    @Query(value = "SELECT 'ผลการเรียนตลอดหลักสูตร' AS col1,SUM(CAST((crss.credit)AS DECIMAL(5,2))),CAST(TRUNC(SUM((CAST(CAST((crss.credit)AS DECIMAL(5,2))*CAST((crss.grade)AS DECIMAL(5,2))AS DECIMAL(5,2))))/ SUM(crss.credit),2)AS DECIMAL(5,2)) AS GPA\n"
            + "FROM tb_trn_class_room_subject_score crss \n"
            + "LEFT JOIN tb_mst_class c ON crss.class_id=c.class_id\n"
            + "WHERE crss.sc_code='PRAMANDA' AND crss.stu_code = ?1 AND c.cat_class_id =3", nativeQuery = true)
    List<Map<String, String>> getSumGpa(String stuCode);

    //aong
    @Query(value = "SELECT REPLACE (d.name, 'กลุ่มสาระการเรียนรู้', '')AS subject,SUM (CAST ((crss.weight)AS DECIMAL (5, 2))),\n"
            + "CAST(TRUNC(SUM((CAST(CAST ((crss.weight)AS DECIMAL (5, 2)) * CAST ((crss.grade)AS DECIMAL (5, 2))AS DECIMAL (5, 2)))) / SUM (crss.weight),2)AS DECIMAL (5, 2))AS GPA\n"
            + "FROM tb_trn_class_room_subject_score crss \n"
            + "LEFT JOIN tb_mst_class c ON crss.class_id = c.class_id\n"
            + "LEFT JOIN tb_mst_department d ON crss.department_id = d.department_id\n"
            + "WHERE crss.sc_code = 'PRAMANDA' \n"
            + "AND SUBSTR(crss.year_group_name,1,1)='2'\n"
            + "AND crss.stu_code = ?1 \n"
            + "AND crss.class_id IN (7,8,9) \n"
            + "OR crss.class_id IN (19,20,21) \n"
            + "AND c.cat_class_id = 2\n"
            + "GROUP BY crss.department_id,d.orders,d.name\n"
            + "ORDER BY d.orders", nativeQuery = true)
    List<Map<String, String>> getSumScorePrimary(String stuCode);

    //aong
    @Query(value = "SELECT a.year_group_name,a.class_room_sname_th,a.subject_code || ' ' || a.subject_name AS subject,a.lesson,a.grade,a.subject_type,a.class_lname_th\n"
            + "FROM(SELECT crss.year_group_id,crss.year_group_name,crss.department_id,crss.class_id,crss.class_room_sname_th,crss.subject_code,crss.subject_type,crss.subject_name,crss.lesson,crss.grade,c.class_lname_th\n"
            + "FROM tb_trn_class_room_subject_score crss\n"
            + "LEFT JOIN tb_mst_class c ON  crss.class_id = c.class_id\n"
            + "WHERE  crss.sc_code = 'PRAMANDA'AND crss.stu_code = ?1 AND c.cat_class_id = 2 ) a \n"
            + "LEFT JOIN tb_mst_department d ON  a.department_id = d.department_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON  a.year_group_id = yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON  yg.year_id = y.year_id\n"
            + "WHERE yg.year_term_id=2\n"
            + "ORDER BY CAST(CAST((CAST (SUBSTR (yg.name, 3, 4) AS TEXT) || CAST (SUBSTR (yg.name, 1, 1) AS TEXT))AS INT) AS TEXT),\n"
            + "CASE WHEN cast(a.subject_type as INT) = 1 THEN 1\n"
            + "WHEN cast(a.subject_type as INT) = 2 THEN 2\n"
            + "END ,d.orders,\n"
            + "CASE WHEN SUBSTR (a.subject_name, 1, 1) = 'ส' THEN 1\n"
            + "WHEN SUBSTR (a.subject_name, 1, 1) = 'ป' THEN 2\n"
            + "ELSE 3 END \n", nativeQuery = true)
    List<Map<String, String>> getPrimarySubjectScore(String stuCode);

    //aong
    @Query(value = "SELECT 'ผลการเรียนตลอดหลักสูตร'  AS col1,SUM (CAST ((crss.weight)AS DECIMAL (5, 2))),\n"
            + "CAST(TRUNC(SUM((CAST(CAST ((crss.weight)AS DECIMAL (5, 2)) * CAST ((crss.grade)AS DECIMAL (5, 2))AS DECIMAL (5, 2)))) / SUM (crss.weight),2)AS DECIMAL (5, 2))AS GPA\n"
            + "FROM tb_trn_class_room_subject_score crss \n"
            + "LEFT JOIN tb_mst_class c ON crss.class_id = c.class_id\n"
            + "WHERE crss.sc_code = 'PRAMANDA' \n"
            + "AND SUBSTR(crss.year_group_name,1,1)='2'\n"
            + "AND crss.stu_code = ?1 \n"
            + "AND crss.class_id IN (7,8,9) \n"
            + "OR crss.class_id IN (19,20,21) \n"
            + "AND c.cat_class_id = 2", nativeQuery = true)
    List<Map<String, String>> getSumGpaPrimary(String stuCode);

    @Query(value = "SELECT crss.class_room_subject_score_id,crss.class_room_no,crss.stu_code,crss.fullname,split_part(crss.fullname,' ',1) as firstname\n"
            + ",split_part(crss.fullname,' ',2) AS lastname,\n"
            + "crss.subject_code,crss.subject_name,crss.credit,crss.class_room_sname_th,crss.year_group_name,\n"
            + "CAST (crss.score1 AS INTEGER),CAST (crss.score_midterm AS INTEGER),CAST (crss.score2 AS INTEGER),CAST (crss.score_final AS INTEGER),CAST (crss.sum_score AS INTEGER),\n"
            + "crss.grade,crss.full_score1,crss.full_score_midterm,crss.full_score2,crss.full_score_final,crss.full_score,\n"
            + "crss.remark,crss.is_morsor,crss.is_exchange\n"
            + "FROM tb_trn_class_room_subject_score crss\n"
            + "WHERE crss.year_group_id = ?1 AND crss.class_id = ?2 \n"
            + "AND crss.class_room_id = ?3 AND crss.class_room_subject_id = ?4 AND crss.isactive = true\n"
            + "ORDER BY CAST (crss.class_room_no AS INTEGER) ASC", nativeQuery = true)
    List<Map<String, String>> SubjectScore(Integer yearGroupId, Integer classId, Integer classRoomId, Integer classRoomSubjectId);

}
