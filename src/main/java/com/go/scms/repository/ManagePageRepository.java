package com.go.scms.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.go.scms.entity.ManagePageEntity;

@Repository
public interface ManagePageRepository extends JpaRepository<ManagePageEntity,Long>{

	@Query(value = "select \n" + 
			"    p.page_id \n" + 
			"    , p.page_code \n" +  
			"    , p.page_name \n" + 
                        "    , p.page_name_en \n" + 
                        "    , pg.page_group_id \n" + 
			"    , pg.page_group_code \n" + 
			"    , pg.page_group_name \n" + 
                        "    , pg.page_group_name_en \n"+
			"    , r.role_code \n" + 
			"    , r.role_name \n" + 
			"    , rp.is_view \n" + 
			"    , rp.is_add \n" + 
			"    , rp.is_report \n" + 
			"    , rp.is_approve \n" + 
			"    , rp.is_delete \n" + 
			"from adm_page p\n" + 
			"left join adm_role r on r.role_id = ?1 \n" + 
			"left join adm_page_group pg on pg.page_group_id = p.page_group_id\n" + 
			"left join adm_role_page rp on p.page_id = rp.page_id and r.role_id = rp.role_id\n" + 
			"where p.page_status <>'N'\n" + 
			"order by pg.page_group_code,p.page_code;"
	,nativeQuery = true)
	List<Map<String,String>> findAll(Long roleId);
	
}
