package com.go.scms.repository;

import com.go.scms.entity.TeacherProfileEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherProfileRepository extends JpaRepository<TeacherProfileEntity, Long> {

    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n" +
        "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n" +
        "WHERE tp.record_status <> 'D' ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n" +
        "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n" +
        "WHERE tp.record_status <> 'D'",nativeQuery = true)
    Page<Map> show(Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% ", nativeQuery = true)
    Page<Map> firstname(String firstnameTh,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1% ", nativeQuery = true)
    Page<Map> lastname(String lastnameTh,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.lastname_th like %?2% ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.lastname_th like %?2% ", nativeQuery = true)
    Page<Map> firstnameLastname(String firstnameTh,String lastnameTh,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.class_id = ?1 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.class_id = ?1 ", nativeQuery = true)
    Page<Map> classId(Integer classId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.department_id = ?1 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.department_id = ?1 ", nativeQuery = true)
    Page<Map> departmentId(Integer departmentId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.department_id = ?1 AND tp.class_id = ?2  ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.department_id = ?1 AND tp.class_id = ?2 ", nativeQuery = true)
    Page<Map> departmentIdClassId(Integer departmentId, Integer classId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.lastname_th like %?2% AND tp.class_id = ?3 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.lastname_th like %?2% AND tp.class_id = ?3 ", nativeQuery = true)
    Page<Map> firstLastClassId(String firstnameTh,String lastnameTh,Integer classId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.lastname_th like %?2% AND tp.department_id = ?3 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.lastname_th like %?2% AND tp.department_id = ?3 ", nativeQuery = true)
    Page<Map> firstLastDepartmentId(String firstnameTh,String lastnameTh,Integer departmentId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.department_id = ?2  ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.department_id = ?2 ", nativeQuery = true)
    Page<Map> firstdepartmentId(String firstnameTh,Integer departmentId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.class_id = ?2  ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.class_id = ?2 ", nativeQuery = true)
    Page<Map> firstclassId(String firstnameTh,Integer classId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.class_id = ?2 AND tp.department_id = ?3 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.firstname_th like %?1% AND tp.class_id = ?2 AND tp.department_id = ?3", nativeQuery = true)
    Page<Map> firstclassIdDepartmentId(String firstnameTh,Integer classId ,Integer departmentId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.department_id = ?2 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.department_id = ?2", nativeQuery = true)
    Page<Map> lastnameDepartmentId(String lastnameTh,Integer departmentId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.class_id = ?2 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.class_id = ?2", nativeQuery = true)
    Page<Map> lastnameClassId(String lastnameTh,Integer classId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.class_id = ?2 AND tp.department_id = ?3 ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.class_id = ?2 AND tp.department_id = ?3 ", nativeQuery = true)
    Page<Map> lastnameClassIdDepartmentId(String lastnameTh,Integer classId,Integer departmentId,Pageable pageable);
    
    @Query(value = "SELECT tp.teacher_id,tp.teacher_card_id,tp.fullname,tp.department_name,tp.start_work_date,tp.license_no,tp.license_expired,tp.created_date,tp.created_user,tp.updated_date,tp.updated_user,cr.class_room_sname_th FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.class_id = ?2 AND tp.department_id = ?3 AND tp.firstname_th like %?4% ", countQuery = "SELECT count(*) FROM tb_tea_teacher_profile tp \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = tp.class_room_id\n"
            + "WHERE tp.record_status <> 'D' AND tp.lastname_th like %?1%  AND tp.class_id = ?2 AND tp.department_id = ?3 AND tp.firstname_th like %?4% ", nativeQuery = true)
    Page<Map> lastnameClassIdDepartmentIdFirstname(String lastnameTh,Integer classId,Integer departmentId,String firstnameTh,Pageable pageable);

    @Query(value = "SELECT * FROM tb_tea_teacher_profile WHERE department_id = ?1 ORDER BY fullname", nativeQuery = true)
    List<Map> findteacherprofileddlaca002sub03(Integer departmentId);

    List<TeacherProfileEntity> findAllByRecordStatusNotIn(String recordStatus);
    Optional<TeacherProfileEntity> findByTeacherId(Long TeacherId);
    List<TeacherProfileEntity> findAllByRecordStatusNotInAndTeacherCardId( String recordStatus,String teacherCardId);
    
    @Query(value = "SELECT cc.lname_th FROM tb_mst_department d \n" +
                    "LEFT JOIN tb_mst_cat_class cc ON cc.cat_class_id = d.cat_class\n" +
                       "WHERE department_id = ?1", nativeQuery = true)
    List<Map> finddepartment(Integer departmentId);
    
    @Query(value = "SELECT class_sname_th FROM tb_mst_class WHERE class_id = ?1", nativeQuery = true)
    List<Map> findclass(Integer classId);
    
    @Query(value = "SELECT class_room_sname_th FROM tb_mst_class_room WHERE class_room_id = ?1", nativeQuery = true)
    List<Map> findclassroom(Integer classRoomId);
    
    @Query(value = "SELECT * FROM tb_tea_teacher_profile WHERE department_id = ?1 ORDER BY fullname", nativeQuery = true)
    List<Map> findteacherprofileddl(Integer departmentId);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_tea_teacher_profile SET class_room_id = ?1,record_status = ?2,updated_user = ?3\n"
            + "WHERE teacher_id = ?4", nativeQuery = true)
    void updateclassroomfromteacher(Integer classRoomId,String recordStatus, String updatedUser, Long teacherId);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_tea_teacher_profile SET class_room_id = null,record_status = ?1,updated_user = ?2\n"
            + "WHERE teacher_id = ?3", nativeQuery = true)
    void deleteclassroomfromteacher(String recordStatus, String updatedUser, Long teacherId);
    
    @Query(value = "SELECT class_id FROM tb_tea_teacher_profile WHERE record_status <> 'D' AND citizen_id = ?1", nativeQuery = true)
    List<Map> findclassidfromteacherprofile(String citizenId);
    
}


