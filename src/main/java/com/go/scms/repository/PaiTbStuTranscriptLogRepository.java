/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbStuTranscriptLogEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bamboo
 */
@Repository
public interface PaiTbStuTranscriptLogRepository extends JpaRepository<TbStuTranscriptLogEntity,Long> {
    
    @Query(value = "SELECT stl.stu_transcript_log_id,stl.seq_no,stl.fullname,fn_thai_date(stl.dob) AS dob, stl.gender, stl.remark,\n" +
                    "fn_thai_date(stl.transcript_date) AS transcript_date\n" +
                    "FROM tb_stu_transcript_log stl\n" +
                    "LEFT JOIN tb_mst_year_group yg ON stl.year_group_id=yg.year_group_id\n" +
                    "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n" +
                    "WHERE y.year_id= ?1 AND yg.year_term_id = 2", 
                    countQuery = "SELECT count(*) FROM tb_stu_transcript_log stl\n" +
                                "LEFT JOIN tb_mst_year_group yg ON stl.year_group_id=yg.year_group_id\n" +
                                "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"+        
                                "WHERE y.year_id= ?1 AND yg.year_term_id = 2",nativeQuery = true)
    Page<Map> findyear(Integer yearId,Pageable pageable);
    
    @Query(value = "SELECT stl.stu_transcript_log_id,stl.seq_no,stl.fullname,fn_thai_date(stl.dob) AS dob, stl.gender, stl.remark,\n" +
                    "fn_thai_date(stl.transcript_date) AS transcript_date\n" +
                    "FROM tb_stu_transcript_log stl\n" +
                    "LEFT JOIN tb_mst_year_group yg ON stl.year_group_id=yg.year_group_id\n" +
                    "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n" +
                    "WHERE stl.class_id = ?1 AND yg.year_term_id = 2  ", 
                    countQuery = "SELECT count(*) FROM tb_stu_transcript_log stl\n" +
                               "LEFT JOIN tb_mst_year_group yg ON stl.year_group_id=yg.year_group_id\n" +
                               "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"+        
                               "WHERE stl.class_id= ?1 AND yg.year_term_id = 2   ",nativeQuery = true)
    Page<Map> findclass(Integer classId,Pageable pageable);
    
    @Query(value = "SELECT stl.stu_transcript_log_id,stl.seq_no,stl.fullname,fn_thai_date(stl.dob) AS dob, stl.gender, stl.remark,\n" +
                    "fn_thai_date(stl.transcript_date) AS transcript_date\n" +
                    "FROM tb_stu_transcript_log stl\n" +
                    "LEFT JOIN tb_mst_year_group yg ON stl.year_group_id=yg.year_group_id\n" +
                    "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n" +
                    "WHERE y.year_id= ?1 AND yg.year_term_id= 2 AND stl.class_id = ?2", 
                    countQuery = "SELECT count(*) FROM tb_stu_transcript_log stl\n" +
                               "LEFT JOIN tb_mst_year_group yg ON stl.year_group_id=yg.year_group_id\n" +
                               "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"+        
                               "WHERE y.year_id= ?1 AND yg.year_term_id = 2 AND stl.class_id= ?2 ",nativeQuery = true)
    Page<Map> findAll(Integer yearId,Integer classId,Pageable pageable);
}
