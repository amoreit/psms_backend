/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.StuProfileDocEntity;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author P3rdiscoz
 */
public interface StuProfileDocRepository extends JpaRepository<StuProfileDocEntity, Long>{
     Page<StuProfileDocEntity> findAllByRecordStatusNotIn(String recordStatus, Pageable pageable);
     Optional<StuProfileDocEntity> findByStuProfileId(Integer stuProfileId);
}
