package com.go.scms.repository;

import com.go.scms.entity.SnickClassRoomRatwScoreEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickClassRoomRatwScoreRepository extends JpaRepository<SnickClassRoomRatwScoreEntity, Long>{
    
        List<SnickClassRoomRatwScoreEntity>findAllByRecordStatusNotInAndClassRoomSubjectIdAndYearGroupName(String recordStatus, Integer classRoomSubjectId, String yearGroupName);
    
        @Transactional
        @Modifying
        @Query("UPDATE SnickClassRoomRatwScoreEntity rc SET rc.score1 = ?1, rc.score2 = ?2, rc.score3 = ?3, rc.score4 = ?4, rc.score5 = ?5, rc.updatedUser = ?6, rc.recordStatus = ?7, rc.isactive = true where rc.classRoomRatwScoreId = ?8")
        void updateRatwscore(Integer score1, Integer score2, Integer score3, Integer score4, Integer score5, String userName, String recordStatus, Long classRoomRatwScoreId);
}
