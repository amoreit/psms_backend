/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTrnClassRoomSubjectEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lumin
 */
@Repository
public interface TbTrnClassRoomSubjectRepository extends JpaRepository<TbTrnClassRoomSubjectEntity, Long> {

    @Query(value = "SELECT crs.year_group_id,crs.class_room_id,crs.subject_code,crs.subject_name_th,crs.credit FROM tb_trn_class_room_subject crs \n"
            + "  LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n"
            + "  WHERE  crs.year_group_id = ?1 AND cr.class_id = ?2 AND cr.class_room_id = ?3 \n"
            + "  ORDER BY crs.class_room_subject_id ASC", nativeQuery = true)
    List<Map> findSubject(Integer yearGroupId, Integer classId, Integer classRoomId);

    List<TbTrnClassRoomSubjectEntity> findAllByyearGroupIdAndClassRoomId(Integer yearGroupId, Integer classRoomId);
    
    @Query(value = "SELECT crs.class_room_subject_id,crs.year_group_id,mc.cat_class_id,crs.subject_code,crs.subject_name_th,crs.credit\n"
            + "FROM tb_trn_class_room_subject crs\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "LEFT JOIN tb_mst_class mc ON mc.class_id = cr.class_id\n"
            + "WHERE crs.year_group_id = ?1 AND crs.class_room_id = ?2\n"
            + "ORDER BY crs.class_room_subject_id", nativeQuery = true)
    List<Map> findAca004Sub01(Integer yearGroupId, Integer classRoomId);

}
