package com.go.scms.repository;

import com.go.scms.entity.SnickTeacherDepartmentEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTeacherDepartmentRepository extends JpaRepository<SnickTeacherDepartmentEntity, Long>{
     
        List<SnickTeacherDepartmentEntity> findAllByRecordStatusNotInOrderByTeacherDepartmentIdAsc(String recordStatus);
        List<SnickTeacherDepartmentEntity> findAllByYearGroupNameAndCatClassIdAndDepartmentNameAndRecordStatusNotInOrderByTeacherDepartmentIdAsc(String yearGroupName, Integer catClassId, String departmentName, String recordStatus);
        List<SnickTeacherDepartmentEntity> findAllByRecordStatusNotInAndTeacherId(String recordStatus, Integer teacherId);
        List<SnickTeacherDepartmentEntity> findAllByFullnameContainingAndYearGroupNameAndCatClassIdAndDepartmentNameAndRecordStatusNotInOrderByTeacherDepartmentIdAsc(String fullname, String yearGroupName, Integer catClassId, String departmentName, String recordStatus);

        @Transactional
        @Modifying
        @Query("UPDATE SnickTeacherDepartmentEntity teadep SET teadep.leader = ?1, teadep.updatedUser = ?2, teadep.recordStatus = ?3 where teadep.teacherDepartmentId = ?4")
        void updateLeader(Boolean leader, String userName, String recordStatus, Long getTeacherDepartmentId);
        
        @Transactional
        @Modifying
        @Query(value = "DELETE FROM tb_trn_teacher_department where teacher_department_id = ?1", nativeQuery = true)
        void deleteTeacherDapartment(Long teacherDepartmentId);
        
        //------------------------ tec005 search
        @Query(value = "SELECT tea.year_group_id,tea.year_group_name,tea.fullname,tea.department_id,tea.department_name,tea.leader,tea.teacher_department_id \n" +
                    "FROM tb_trn_teacher_department tea\n" +
                    "WHERE tea.sc_code = 'PRAMANDA' AND tea.year_group_name = ?1 AND tea.cat_class_id = ?2 AND  tea.department_name = ?3 AND tea.record_status <> ?4", nativeQuery = true)
        List<Map> findall(String yearGroupName, Integer catClassId, String departmentName, String recordStatus);
}
