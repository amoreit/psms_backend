package com.go.scms.repository;

import com.go.scms.entity.MstCatClassMainEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MstCatClassMainRepository extends JpaRepository<MstCatClassMainEntity,Long>{  
        Page<MstCatClassMainEntity> findAllByLnameThContainingAndRecordStatusNotIn(String lnameTh, String recordStatus, Pageable pageable);
        List<MstCatClassMainEntity> findAllByRecordStatusNotIn(String recordStatus);       
}
