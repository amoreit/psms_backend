package com.go.scms.repository;

import com.go.scms.entity.TbMstFamilyStatusEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TbMstFamilyStatusRepository extends JpaRepository<TbMstFamilyStatusEntity, Long>{    
        List<TbMstFamilyStatusEntity> findAllByRecordStatusNotInOrderByFamilyStatusId(String recordStatus);        
}
