package com.go.scms.repository;

import com.go.scms.entity.TbStuProfileEntity;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TbStuProfileRepository extends JpaRepository<TbStuProfileEntity, Long> {

    @Query(value = "select CAST(max(sp.stu_code) as INTEGER) from tb_stu_profile sp", nativeQuery = true)
    Optional<Map> find();

    @Query(value = "SELECT sp.stu_profile_id,sp.title,sp.firstname_th,sp.lastname_th,sp.stu_code,sp.citizen_id,sp.to_class,sp.stu_status,sp.stu_status_id,sp.class_room_id,sp.class_room FROM tb_stu_profile sp\n"
            + "WHERE sp.record_status <> 'D' \n"
            + "AND sp.stu_code <> '' \n"
            + "AND sp.firstname_th LIKE %?1% AND sp.lastname_th LIKE %?2% AND sp.to_class LIKE %?3% \n"
            + "AND sp.stu_status_id <> 3 AND sp.stu_status_id <> 4 AND sp.stu_status_id <> 5 AND sp.stu_status_id <> 6\n"
            + "AND sp.to_year_group LIKE %?4% AND sp.citizen_id LIKE %?5% AND sp.stu_code LIKE %?6%\n"
            + "ORDER BY sp.class_id,sp.firstname_th", countQuery = "SELECT count(*) FROM tb_stu_profile sp\n"
            + "WHERE sp.record_status <> 'D' AND sp.stu_code <> ''\n"
            + "AND sp.firstname_th LIKE %?1% AND sp.lastname_th LIKE %?2% AND sp.to_class LIKE %?3% \n"
            + "AND sp.stu_status_id <> 3 AND sp.stu_status_id <> 4 AND sp.stu_status_id <> 5 AND sp.stu_status_id <> 6\n"
            + "AND sp.to_year_group LIKE %?4% AND sp.citizen_id LIKE %?5% AND sp.stu_code LIKE %?6%", nativeQuery = true)
    Page<Map> findstu003(String firstnameTh, String lastnameTh, String toClass, String toYearGroup, String citizenId, String stuCode, Pageable pageable);

    @Query(value = "SELECT sp.stu_profile_id,sp.title,sp.firstname_th,sp.lastname_th,sp.stu_code,sp.citizen_id,sp.to_class,sp.stu_status,sp.stu_status_id,sp.class_room_id,sp.class_room FROM tb_stu_profile sp\n"
            + "WHERE sp.record_status <> 'D' AND sp.stu_code <> ''\n"
            + "AND sp.firstname_th LIKE %?1% AND sp.lastname_th LIKE %?2% AND sp.to_class LIKE %?3% \n"
            + "AND sp.to_year_group LIKE %?4% AND sp.citizen_id LIKE %?5% AND sp.stu_code LIKE %?6%\n"
            + "AND sp.stu_status_id = ?7\n"
            + "ORDER BY sp.class_id,sp.firstname_th", countQuery = "SELECT count(*) FROM tb_stu_profile sp\n"
            + "WHERE sp.record_status <> 'D' AND sp.stu_code <> ''\n"
            + "AND sp.firstname_th LIKE %?1% AND sp.lastname_th LIKE %?2% AND sp.to_class LIKE %?3% \n"
            + "AND sp.to_year_group LIKE %?4% AND sp.citizen_id LIKE %?5% AND sp.stu_code LIKE %?6%\n"
            + "AND sp.stu_status_id = ?7", nativeQuery = true)
    Page<Map> findstu003bystustatusid(String firstnameTh, String lastnameTh, String toClass, String toYearGroup, String citizenId, String stuCode, Integer stuStatusId, Pageable pageable);

    @Query(value = "SELECT sp.stu_profile_id,sp.title,sp.firstname_th,sp.lastname_th,sp.citizen_id,sp.to_class FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class c ON c.class_sname_th = sp.to_class\n"
            + "WHERE sp.record_status <> 'D' and sp.stu_code is null AND sp.firstname_th LIKE %?1% AND sp.lastname_th LIKE %?2%\n"
            + "AND sp.to_class LIKE %?3% AND sp.stu_status_id = 1 AND sp.to_year_group LIKE %?4%\n"
            + "ORDER BY c.class_id,sp.firstname_th", countQuery = "SELECT count(*) FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class c ON c.class_sname_th = sp.to_class\n"
            + "WHERE sp.record_status <> 'D' and sp.stu_code is null AND sp.firstname_th LIKE %?1% AND sp.lastname_th LIKE %?2%\n"
            + "AND sp.to_class LIKE %?3% AND sp.stu_status_id = 1 AND sp.to_year_group LIKE %?4%", nativeQuery = true)
    Page<Map> findstu004(String firstnameTh, String lastnameTh, String toClass, String toYearGroup, Pageable pageable);
}
