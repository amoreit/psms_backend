package com.go.scms.repository;

import com.go.scms.entity.MaxClassRoomEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaxClassRoomRepository extends JpaRepository<MaxClassRoomEntity, Long>  {
        Optional<MaxClassRoomEntity> findByClassRoomId(Integer classRoomId);
}
