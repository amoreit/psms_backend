/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbMstDivisionEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sir. Harinn
 */
@Repository
public interface TbMstDivisionRepository extends JpaRepository<TbMstDivisionEntity, Long>{

    Page<TbMstDivisionEntity> findAllByDivisionNameThContainingAndDivisionNameEnContainingAndRecordStatusNotIn(String divisionNameTh, String divisionNameEn, String recordStatus, Pageable pageable);
    List<TbMstDivisionEntity> findAllByRecordStatusNotIn(String recordStatus);

    Optional<TbMstDivisionEntity> findByDivisionId(Long divisionId);
}
