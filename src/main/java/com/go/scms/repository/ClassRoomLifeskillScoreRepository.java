/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ClassRoomLifeskillScoreEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ClassRoomLifeskillScoreRepository extends JpaRepository<ClassRoomLifeskillScoreEntity, Long>{
    @Transactional
    @Modifying
    @Query("UPDATE ClassRoomLifeskillScoreEntity y SET y.score = ?1,y.recordStatus = ?2,y.updatedUser = ?3 where y.classRoomLifeskillScoreId = ?4")
    void updatescore(Integer score, String recordStatus,String updatedUser, Long classRoomLifeskillScoreId);
}
