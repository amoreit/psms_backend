/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTrnClassRoomMemberEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lumin
 */
@Repository
public interface TbTrnClassRoomMemberRepository extends JpaRepository<TbTrnClassRoomMemberEntity, Long> {

    Page<TbTrnClassRoomMemberEntity> findAllByyearGroupNameContainingAndClassNameContainingAndRole(String yearGroupName, String className, String role, Pageable pageable);

    Page<TbTrnClassRoomMemberEntity> findAllByyearGroupIdAndClassIdAndClassRoomIdAndRole(Integer yearGroupId, Integer classId, Integer classRoomId, String role, Pageable pageable);

    Page<TbTrnClassRoomMemberEntity> findAllByyearGroupIdAndClassRoomId(Integer yearGroupId, Integer classRoomId, Pageable pageable);

    List<TbTrnClassRoomMemberEntity> findByyearGroupIdContainingAndClassRoomIdContaining(Integer yearGroupId, Integer classRoomId);

    List<TbTrnClassRoomMemberEntity> findAllByyearGroupIdAndClassIdAndClassRoomIdAndRoleOrderByClassRoomMemberId(Integer yearGroupId, Integer classId, Integer classRoomId, String role);

    @Transactional
    @Modifying
    @Query("UPDATE TbTrnClassRoomMemberEntity y SET y.desiredCharResult = ?1,y.recordStatus = ?2,y.updatedUser = ?3 where y.classRoomMemberId = ?4")
    void updateDesiredCharScore(Integer desiredCharResult, String recordStatus, String updatedUser, Long classRoomMemberId);

    @Query(value = "select cs.class_room_desired_char_score_id,crm.sc_code,crm.year_group_id,crm.year_group_name,crm.class_room_member_id,crs.class_room_subject_id,crm.class_room,crm.stu_code,crm.fullname,\n"
            + "crs.subject_code,crs.subject_name_th,COALESCE(cs.score1,0) as score1,COALESCE(cs.score2,0) as score2,COALESCE(cs.score3,0) as score3,COALESCE(cs.score4,0) as score4,COALESCE(cs.score5,0) as score5,COALESCE(cs.score6,0) as score6,COALESCE(cs.score7,0) as score7,COALESCE(cs.score8,0) as score8,COALESCE(cs.score9,0) as score9\n"
            + "FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_trn_class_room_subject crs ON crm.class_room_id=crs.class_room_id\n"
            + "LEFT JOIN tb_trn_class_room_desired_char_score cs ON cs.class_room_member_id = crm.class_room_member_id\n"
            + "WHERE crs.class_room_subject_id = ?1 AND crm.year_group_id = ?2 AND crm.role = 'student'\n"
            + "ORDER BY crm.class_room_member_id, crm.stu_code", nativeQuery = true)
    List<Map<String, String>> findSub05Form(Integer classRoomSubjectId, Integer yearGroupId);

    //nock rep001
    @Query(value = "select distinct(crm.stu_code),\n"
            + "crm.fullname,\n"
            + "crm.class_room,\n"
            + "crm.class_room_member_id , crm.class_id , crm.class_room_id \n"
            + "FROM tb_trn_class_room_member as crm\n"
            + "LEFT JOIN tb_trn_class_room_subject_score AS crss \n"
            + "ON crm.class_room_member_id = crss.class_room_member_id AND crm.stu_code = crm.stu_code AND crm.year_group_id = crss.year_group_id\n"
            + "WHERE crm.record_status <> 'D' AND crm.year_group_name like %?1% AND crm.class_id = ?2 AND crm.class_room_id = ?3 \n"
            + "ORDER BY crm.stu_code, crm.class_room",
            countQuery = "select distinct(crm.stu_code),\n"
            + "crm.fullname,\n"
            + "crm.class_room,\n"
            + "crm.class_room_member_id\n"
            + "FROM tb_trn_class_room_member as crm\n"
            + "LEFT JOIN tb_trn_class_room_subject_score AS crss \n"
            + "ON crm.class_room_member_id = crss.class_room_member_id AND crm.stu_code = crm.stu_code AND crm.year_group_id = crss.year_group_id\n"
            + "WHERE crm.record_status <> 'D' AND crm.year_group_name like %?1% AND crm.class_id = ?2 AND crm.class_room_id = ?3 ", nativeQuery = true)
    Page<Map> findrep001(String yearGroupName, Integer classId, Integer classRoomId, Pageable pageable);

    @Query(value = "SELECT crm.stu_code, crm.fullname, crm.class_room, crm.class_id, crm.class_room_id\n"
            + "FROM tb_trn_class_room_member as crm\n"
            + "LEFT JOIN tb_trn_class_room_subject_score AS crss\n"
            + "ON crm.class_room_member_id = crss.class_room_member_id AND crm.stu_code = crm.stu_code AND crm.year_group_id = crss.year_group_id\n"
            + "LEFT JOIN tb_mst_class mc\n"
            + "ON crm.class_id = mc.class_id\n"
            + "LEFT JOIN tb_mst_cat_class mcc\n"
            + "ON mc.cat_class_id = mcc.cat_class_id\n"
            + "WHERE crm.record_status <> 'D' AND mcc.cat_class_id = ?1 AND crm.class_room NOT LIKE '%-' AND crm.stu_code IS NOT NULL\n"
            + "GROUP BY crm.stu_code, crm.fullname, crm.class_room, crm.class_id, crm.class_room_id, crm.class_name \n"
            + "ORDER BY crm.class_name , crm.class_room, crm.stu_code",
            countQuery = "SELECT count(*) FROM tb_trn_class_room_member as crm\n"
            + "LEFT JOIN tb_trn_class_room_subject_score AS crss\n"
            + "ON crm.class_room_member_id = crss.class_room_member_id AND crm.stu_code = crm.stu_code AND crm.year_group_id = crss.year_group_id\n"
            + "LEFT JOIN tb_mst_class mc\n"
            + "ON crm.class_id = mc.class_id\n"
            + "LEFT JOIN tb_mst_cat_class mcc\n"
            + "ON mc.cat_class_id = mcc.cat_class_id\n"
            + "WHERE crm.record_status <> 'D' AND mcc.cat_class_id = ?1 AND crm.class_room NOT LIKE '%-' AND crm.stu_code IS NOT NULL\n"
            + "GROUP BY crm.stu_code, crm.fullname, crm.class_room, crm.class_id, crm.class_room_id, crm.class_name ", nativeQuery = true)
    Page<Map> findrep001All(Integer catClassId, Pageable pageable);

    @Query(value = "select a.year_group_name, a.stu_code, a.fullname, a.class_room, a.class_room_member_id, a.class_room_no from tb_trn_class_room_member a LEFT JOIN tb_mst_class b ON a.class_id = b.class_id LEFT JOIN tb_mst_cat_class c ON b.cat_class_id = c.cat_class_id  WHERE c.cat_class_id != '1' and year_group_name = ?1 and a.class_id = ?2 and a.class_room_id = ?3 and a.role = 'student' and a.class_room_no  IS NOT NULL order by a.class_room_id, a.stu_code , CAST(a.class_room_no AS INT)", countQuery = "select count(*) from tb_trn_class_room_member a LEFT JOIN tb_mst_class b ON a.class_id = b.class_id LEFT JOIN tb_mst_cat_class c ON b.cat_class_id = c.cat_class_id  WHERE c.cat_class_id != '1' and year_group_name = ?1 and a.class_id = ?2 and a.class_room_id = ?3 and a.role = 'student' and a.class_room_no  IS NOT NULL", nativeQuery = true)
    Page<Map> findrep002(String yearGroupName, Integer classId, Integer classRoomId, Pageable pageable);

    @Query(value = "select a.year_group_name, a.stu_code, a.fullname, a.class_room, a.class_room_member_id from tb_trn_class_room_member a LEFT JOIN tb_mst_class b ON a.class_id = b.class_id LEFT JOIN tb_mst_cat_class c ON b.cat_class_id = c.cat_class_id  WHERE c.cat_class_id != '1' AND a.class_room NOT LIKE '%-' AND a.stu_code IS NOT NULL  GROUP BY a.year_group_name, a.stu_code, a.fullname, a.class_room, a.class_room_member_id  order BY a.class_room, a.stu_code", countQuery = "select count(*) from tb_trn_class_room_member a LEFT JOIN tb_mst_class b ON a.class_id = b.class_id LEFT JOIN tb_mst_cat_class c ON b.cat_class_id = c.cat_class_id  WHERE c.cat_class_id != '1' AND a.class_room NOT LIKE '%-' AND a.stu_code IS NOT NULL  GROUP BY a.year_group_name, a.stu_code, a.fullname, a.class_room, a.class_room_member_id ", nativeQuery = true)
    Page<Map> findrep002Show(Pageable pageable);

    @Query(value = "SELECT crm.year_group_id,crm.year_group_name, crm.class_name, crm.class_room, crm.fullname, crm.stu_code, crm.class_room_member_id, tsl.set_no, tsl.seq_no, stp.stu_profile_id, tsl.stu_transcript_log_id, c.class_lname_th,c.cat_class_id\n"
            + "FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id = c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "LEFT JOIN tb_stu_profile stp ON stp.stu_code = crm.stu_code\n"
            + "LEFT JOIN tb_stu_transcript_log tsl ON tsl.class_name = crm.class_name\n"
            + "WHERE crm.record_status <> 'D' AND crm.role <> 'teacher'\n"
            + "AND crm.stu_code LIKE %?1% \n"
            + "AND (tsl.doctype_id = '1' or tsl.doctype_id is null)\n"
            + "order by crm.year_group_id", countQuery = "SELECT COUNT (DISTINCT crm.fullname)\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "WHERE crm.record_status <> 'D' AND crm.role <> 'teacher'\n"
            + "and crm.stu_code LIKE %?1%", nativeQuery = true)
    Page<Map> findSturep003(String stuCode, Pageable pageable);

    @Query(value = "SELECT crm.year_group_id,crm.year_group_name, crm.class_name, crm.class_room, crm.fullname, crm.stu_code, crm.class_room_member_id, tsl.set_no, tsl.seq_no, stp.stu_profile_id, tsl.stu_transcript_log_id  \n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "LEFT JOIN tb_stu_profile stp ON stp.stu_code = crm.stu_code \n"
            + "LEFT JOIN tb_stu_transcript_log tsl ON tsl.fullname = crm.fullname \n"
            + "WHERE crm.record_status <> 'D' AND crm.role <> 'teacher' \n"
            + "AND crm.year_group_id = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3 \n"
            + "AND (tsl.doctype_id = '1' or tsl.doctype_id is null) \n"
            + "order by crm.year_group_id", countQuery = "SELECT COUNT (DISTINCT crm.fullname)\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "WHERE crm.record_status <> 'D' AND crm.role <> 'teacher'\n"
            + "AND crm.year_group_id  = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3", nativeQuery = true)
    Page<Map> findrep003(Integer yearGroupId, Integer classId, Integer classRoomId, Pageable pageable);

    @Query(value = "SELECT DISTINCT crm.year_group_id,crm.year_group_name, crm.class_id, crm.class_room_id, crm.class_room, crm.class_name   \n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "WHERE crm.year_group_id = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3 \n"
            + "order by crm.year_group_id", countQuery = "SELECT COUNT (DISTINCT crm.class_room)\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "WHERE crm.year_group_id = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3", nativeQuery = true)
    Page<Map> findrep004(Integer yearGroupId, Integer classId, Integer classRoomId, Pageable pageable);

    //max
    @Query(value = "SELECT crm.class_room_member_id,mc.cat_class_id,crm.class_room_no,crm.sc_code,crm.stu_code,crm.fullname\n"
            + "from tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_class mc ON mc.class_id = crm.class_id\n"
            + "WHERE crm.role = 'student' AND crm.sc_code = 'PRAMANDA' AND crm.year_group_id = ?1 AND crm.class_room_id = ?2\n"
            + "ORDER BY CAST (crm.class_room_no AS INTEGER) ASC", nativeQuery = true)
    List<Map> findAca006Form(Integer yearGroupId, Integer classRoomId);

    //milo
    @Query(value = "SELECT crm.fullname, crm.class_room, crss2.subject_type, crss2.year_group_name, crss2.full_score_final, crss2.score_final, crss1.* \n"
            + "FROM tb_trn_class_room_subject_score crss2\n"
            + "INNER JOIN\n"
            + "\n"
            + "(\n"
            + "	SELECT crss.subject_code, MAX(crss.score_final) AS max_score, MIN(crss.score_final) AS min_score\n"
            + "	FROM tb_trn_class_room_subject_score crss\n"
            + "	WHERE crss.year_group_name = ?1 AND crss.class_id = ?2 AND crss.class_room_id = ?3 \n"
            + "	GROUP BY crss.subject_code\n"
            + ") AS crss1\n"
            + "\n"
            + "ON crss2.subject_code = crss1.subject_code\n"
            + "LEFT JOIN tb_trn_class_room_member crm\n"
            + "ON crss2.class_room_member_id = crm.class_room_member_id AND crss2.stu_code = crm.stu_code\n"
            + "WHERE crss2.year_group_name = ?1 AND crss2.stu_code = ?4 \n"
            + "ORDER BY crss2.subject_type, crss1.subject_code DESC ", countQuery = "", nativeQuery = true)
    Page<Map> findRep001ReportTerm1(String yearGroupName, Integer classId, Integer classRoomId, String stuCode, Pageable pageable);

    @Query(value = "SELECT crm.fullname, crm.class_room, crss2.subject_type, crss2.year_group_name, crss2.full_score_final, crss2.score_final, crss1.* \n"
            + "FROM tb_trn_class_room_subject_score crss2\n"
            + "INNER JOIN\n"
            + "\n"
            + "(\n"
            + "	SELECT crss.subject_code, MAX(crss.score_final) AS max_score, MIN(crss.score_final) AS min_score\n"
            + "	FROM tb_trn_class_room_subject_score crss\n"
            + "	WHERE crss.year_group_name = ?1 AND crss.class_id = ?2 AND crss.class_room_id = ?3 \n"
            + "	GROUP BY crss.subject_code\n"
            + ") AS crss1\n"
            + "\n"
            + "ON crss2.subject_code = crss1.subject_code\n"
            + "LEFT JOIN tb_trn_class_room_member crm\n"
            + "ON crss2.class_room_member_id = crm.class_room_member_id AND crss2.stu_code = crm.stu_code\n"
            + "WHERE crss2.year_group_name = ?1 AND crss2.stu_code = ?4 \n"
            + "ORDER BY crss2.subject_type, crss1.subject_code DESC  ", countQuery = "", nativeQuery = true)
    Page<Map> findRep001ReportTerm2(String yearGroupName, Integer classId, Integer classRoomId, String stuCode, Pageable pageable);

    @Query(value = "SELECT crm.sc_code,crm.year_group_name,crm.class_name,crm.class_room,crm.fullname,crm.role \n"
            + "FROM tb_trn_class_room_member crm\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 AND crm.class_id = ?2 AND crm.class_room_id = ?3 AND crm.role = ?4 ", countQuery = "", nativeQuery = true)
    Page<Map> findRep001ReportTeacher(String yearGroupName, Integer classId, Integer classRoomId, String role, Pageable pageable);

    //aong
    @Query(value = "SELECT crm.class_room_member_id,crm.year_group_id,crm.year_group_name,crm.class_id,crm.class_room_id,crm.fullname,t.*,c.class_lname_th\n"
            + "FROM tb_trn_class_room_member crm\n"
            + "CROSS JOIN LATERAL(VALUES (crm.activity1name,crm.activity1lesson,crm.activity1result,'1. กิจกรรมแนะแนว'),\n"
            + "(crm.activity2name,crm.activity2lesson,crm.activity2result,'2. กิจกรรมลูกเสือเนตรนารี'),\n"
            + "(crm.activity3name,crm.activity3lesson,crm.activity3result,'3. กิจกรรมชมรม'),\n"
            + "(crm.activity4name,crm.activity4lesson,crm.activity4result,'4. กิจกรรมเพื่อสังคมและสาธารณประโยชน์')) AS t (activity_name, lesson, RESULT, activity)\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id = c.class_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id = yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year_term yt ON yg.year_term_id = yt.year_term_id\n"
            + "WHERE crm.sc_code = 'PRAMANDA' AND crm.stu_code = ?1\n"
            + "ORDER BY cast(SUBSTR (yg.name, 3, 4)as INT),cast(SUBSTR(yg.name, 1, 1) as INT),t.activity", nativeQuery = true)
    List<Map> acticityScore(String stuCode);

    //aong
    @Query(value = "SELECT CASE \n"
            + "WHEN(MODE() within group (order by crm.ratw_result DESC)) = 3 THEN 'ดีเยี่ยม'\n"
            + "WHEN(MODE() within group (order by crm.ratw_result DESC)) = 2 THEN 'ดี'\n"
            + "ELSE 'ผ่าน' END \n"
            + "FROM tb_trn_class_room_member crm\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.stu_code=?1 AND crm.class_id=9", nativeQuery = true)
    List<Map> ratwResult(String stuCode);

    //aong
    @Query(value = "SELECT CASE \n"
            + "WHEN(MODE() within group (order by crm.desired_char_result DESC)) = 3 THEN 'ดีเยี่ยม'\n"
            + "WHEN(MODE() within group (order by crm.desired_char_result DESC)) = 2 THEN 'ดี'\n"
            + "ELSE 'ผ่าน'END\n"
            + "FROM tb_trn_class_room_member crm\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.stu_code=?1 AND crm.class_id=9", nativeQuery = true)
    List<Map> desiredCharResult(String stuCode);

    //max
    @Query(value = "SELECT crm.class_room_member_id,crm.year_group_id,crm.year_group_name,crm.member_id,crm.stu_code,crm.class_id,crm.class_name,\n"
            + "crm.class_room_id,crm.class_room,crm.class_room_no,crm.fullname\n"
            + "from tb_trn_class_room_member crm \n"
            + "WHERE crm.role = 'student' AND crm.sc_code = 'PRAMANDA' AND crm.year_group_id = ?1 AND crm.class_id = ?2 AND crm.class_room_id = ?3\n"
            + "ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
    List<Map> searchAca004Sub06(Integer yearGroupId, Integer classId, Integer classRoomId);
}
