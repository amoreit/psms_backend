package com.go.scms.repository;

import com.go.scms.entity.SnickActivityEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickActivityRepository extends JpaRepository<SnickActivityEntity, Long>{
    
    @Query(value = "SELECT a.activity_group_id, a.class_id,a.activity_id, a.activity_name, a.lesson FROM tb_mst_activity a "
                + "WHERE a.sc_code = 'PRAMANDA' \n" 
                + "AND a.activity_group_id = ?1 AND a.class_id = ?2 AND a.isactive = true AND a.record_status <> ?3\n" 
                + "ORDER BY a.activity_name", nativeQuery = true)
    List<Map<String,String>> findall(Integer activityGroupId, Integer classId, String recordStatus);
    
    @Query(value = "SELECT a.activity_group_id, a.class_id,a.activity_id, a.activity_name, a.lesson FROM tb_mst_activity a "
                + "WHERE a.sc_code = 'PRAMANDA' \n" 
                + "AND a.activity_group_id = ?1 AND a.class_id <> ?2 AND a.isactive = true AND a.record_status <> ?3\n" 
                + "ORDER BY a.activity_name", nativeQuery = true)
    List<Map<String,String>> findallreport(Integer activityGroupId, Integer classId, String recordStatus);
}
