/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTecPrintCardLogEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kittipong
 */
@Repository
public interface TbTecPrintCardLogRepository extends JpaRepository<TbTecPrintCardLogEntity,Long> {
    
    List<TbTecPrintCardLogEntity>findAllByRecordStatusNotInAndTeacherCardId(String recordStatus,String teacherCardId);
    
    @Query(value = "SELECT a.* FROM tb_tea_card_print_log as a WHERE a.teacher_card_id like %?1%",nativeQuery = true)
    List<Map> find(String teacherCardId);
}
