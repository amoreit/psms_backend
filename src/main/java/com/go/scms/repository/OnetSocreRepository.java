/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.OnetScoreEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kittipong
 */
@Repository
public interface OnetSocreRepository extends JpaRepository<OnetScoreEntity,Long> {
    
    @Query(value = "SELECT onet.stu_code,onet.fullname,onet.subject_score1,onet.subject_score3,onet.subject_score4,onet.subject_score5,onet.subject_score_avg\n" +
                    "FROM tb_trn_onet_score onet\n" +
                    "WHERE onet.stu_code = ?1", nativeQuery = true)
    List<Map<String, String>> getOnetScore(String stuCode);
    
}
