package com.go.scms.repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.go.scms.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Integer>{
	
            @Query(value = "select\n" +
            "    t.*\n" +
            "from (\n" +
            "    select\n" +
            "        case when req_2.sub_page_id is not null then lpad(cast(pg.page_group_id as varchar),4,'0')||lpad(cast(req_2.page_id as varchar),4,'0')||lpad(cast(req_2.sub_page_id as varchar),4,'0')\n" +
            "             when req_2.page_id is not null then lpad(cast(pg.page_group_id as varchar),4,'0')||lpad(cast(req_2.page_id as varchar),4,'0')\n" +
            "        else lpad(cast(pg.page_group_id as varchar),4,'0') end page_id\n" +
            "        , case when req_2.page_id is not null then p.page_name else pg.page_group_name end page_name\n" +
            "        , case when req_2.page_id is not null then p.page_name_en else pg.page_group_name_en end page_name_en\n" +
            "        , case when req_2.page_id is not null then p.page_icon else pg.page_group_icon end page_icon\n" +
            "        , case when req_2.sub_page_id is not null then lpad(cast(pg.page_group_id as varchar),4,'0')||lpad(cast(req_2.page_id as varchar),4,'0')\n" +
            "               when req_2.page_id is not null then lpad(cast(pg.page_group_id as varchar),4,'0')\n" +
            "        else null end parent_id\n" +
            "        , case when req_2.sub_page_id is not null then p.page_url\n" +
            "               when req_2.page_id is not null and (select count(*) from adm_page where page_parent_id = p.page_id) = 0 then p.page_url\n" +
            "        else null end page_url\n" +
            "        , rp.is_view\n" +
            "        , rp.is_add\n" +
            "        , rp.is_approve\n" +
            "        , rp.is_delete\n" +
            "        , rp.is_report\n" +
            "        , req_2.page_group_id\n" +
            "        , pg.page_group_code\n" +
            "        , p.page_code\n" +
            "        , pg.page_group_order\n" +
            "        , p.page_order\n" +
            "    from (\n" +
            "        select\n" +
            "            distinct\n" +
            "            req_1.*\n" +
            "        from (\n" +
            "            select\n" +
            "                p.page_group_id\n" +
            "                , coalesce(p.page_parent_id,p.page_id) as page_id\n" +
            "                , case when p.page_parent_id is not null then p.page_id end as sub_page_id\n" +
            "            from adm_role_page rp\n" +
            "            inner join adm_page p on rp.page_id = p.page_id\n" +
            "            where\n" +
            "                rp.is_view = 'Y'\n" +
            "                and rp.role_id =?1\n" +
            "        ) req_1\n" +
            "        group by rollup(req_1.page_group_id, req_1.page_id, req_1.sub_page_id)\n" +
            "    ) req_2\n" +
            "    inner join adm_page_group pg on req_2.page_group_id = pg.page_group_id\n" +
            "    left join adm_page p on coalesce(req_2.sub_page_id,req_2.page_id) = p.page_id\n" +
            "    left join adm_role_page rp on p.page_id = rp.page_Id and rp.role_id = ?1\n" +
            "    group by\n" +
            "        req_2.page_group_id\n" +
            "        , req_2.page_id\n" +
            "        , req_2.sub_page_id\n" +
            "        , p.page_id\n" +
            "        , p.page_icon\n" +
            "        , p.page_name\n" +
            "        , p.page_name_en\n" +
            "        , pg.page_group_id\n" +
            "        , pg.page_group_icon\n" +
            "        , pg.page_group_name\n" +
            "        , pg.page_group_name_en\n" +
            "        , p.page_url\n" +
            "        , rp.is_view\n" +
            "        , rp.is_add\n" +
            "        , rp.is_approve\n" +
            "        , rp.is_delete\n" +
            "        , rp.is_report\n" +
            ") t\n" +
            "order by\n" +
            "    t.page_group_order\n" +
            "    , t.parent_id nulls first\n" +
            "    , t.page_group_code\n" +
          //  "    , t.page_code\n" +
            "    , t.page_order",nativeQuery = true)
           
	List<Map<String,String>> getMenu(Long roleId);
	Optional<UserEntity> findByUsrName(String user);
	Optional<UserEntity> findByUsrNameAndUserStatus(String user,String userStatus);
	Page<UserEntity> findAllByFirstNameContainingAndLastNameContainingAndPhoneNoContainingAndRoleRoleCodeContaining(String firstName,String lastName,String phoneNo,String roleCode, Pageable pageable);
}
