/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TrnClassRoomMemberEntity;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TrnClassRoomMemberRepository extends JpaRepository<TrnClassRoomMemberEntity, Long> {

    @Query(value = "select a.*,b.cat_class_id from tb_trn_class_room_member a left join tb_mst_class b on b.class_id = a.class_id where a.record_status <> 'D' AND a.role = 'student' and a.class_room_id = ?1 and a.year_group_id = ?2 order by cast(a.class_room_no AS INTEGER)", nativeQuery = true)
    List<Map> find(Integer classRoom, Integer yearGroupId);

    @Query(value = "SELECT * FROM tb_stu_profile \n"
            + "WHERE record_status <> 'D' and stu_status_id = 2 and firstname_th LIKE %?1% AND lastname_th LIKE %?2% AND stu_code LIKE %?3%\n"
            + "ORDER BY stu_code", countQuery = "select count(*) FROM tb_stu_profile \n"
            + "WHERE record_status <> 'D' and stu_status_id = 2 and firstname_th LIKE %?1% AND lastname_th LIKE %?2% AND stu_code LIKE %?3%", nativeQuery = true)
    Page<Map> findstufordialog(String firstnameTh, String lastnameTh, String stuCode, Pageable pageable);

    @Query(value = "SELECT c.class_id,c.class_sname_th FROM tb_mst_class c\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_id = c.class_id\n"
            + "WHERE cr.class_room_id = ?1", nativeQuery = true)
    List<Map> findclassidandclassnamebyclassroomid(Integer classRoomId);

    @Query(value = "select class_room_member_id,fullname,role,member_id from tb_trn_class_room_member\n"
            + "where record_status <> 'D' AND role = 'teacher'\n"
            + "and class_room_id = ?1 and year_group_id = ?2 order by fullname", nativeQuery = true)
    List<Map> findteacher(Integer classRoom, Integer yearGroupId);

    @Query(value = "select cc.cat_class_id,d.department_id,d.name from tb_mst_department d\n"
            + "left join tb_mst_cat_class cc on d.cat_class=cc.cat_class_id\n"
            + "left join tb_mst_class c on cc.cat_class_id=c.cat_class_id\n"
            + "left join tb_mst_class_room cr on c.class_id=cr.class_id\n"
            + "where cr.class_id = ?1 group BY cc.cat_class_id,d.department_id,d.name \n"
            + "order by d.orders", nativeQuery = true)
    List<Map> finddepartmentfromclass(Integer classRoom);

    @Query(value = "SELECT b.class_id,c.year_group_id,crs.subject_code,crs.subject_name_th,d.name,crs.credit,crs.lesson,d.department_id,e.lang_code from tb_trn_class_room_subject crs\n"
            + "left join tb_mst_class_room b on crs.class_room_id = b.class_room_id\n"
            + "left join tb_mst_year_group c on crs.year_group_id = c.year_group_id\n"
            + "left join tb_mst_department d on crs.department_id = d.department_id\n"
            + "LEFT JOIN tb_mst_subject e ON crs.subject_code = e.subject_code\n"
            + "where b.class_id = ?1 and c.year_group_id = ?2 and crs.department_id = ?3\n"
            + "GROUP BY b.class_id,c.year_group_id,crs.subject_code,crs.subject_name_th,d.name,crs.credit,crs.lesson,crs.orders,d.department_id,e.lang_code\n"
            + "order by crs.orders,crs.subject_code", nativeQuery = true)
    List<Map> findsubjectfromclass(Integer classId, Integer yearGroupId, Integer departmentId);

    @Query(value = "select cc.cat_class_id,d.department_id,d.name,cr.class_room_id from tb_mst_department d\n"
            + "left join tb_mst_cat_class cc on d.cat_class=cc.cat_class_id \n"
            + "left join tb_mst_class c on cc.cat_class_id=c.cat_class_id\n"
            + "left join tb_mst_class_room cr on c.class_id=cr.class_id\n"
            + "where cr.class_room_id = ?1 order by d.orders", nativeQuery = true)
    List<Map> finddepartment(Integer classRoom);

    @Query(value = "select crs.class_room_subject_id,crs.class_room_id,crs.subject_code,crs.subject_name_th,d.name,crs.credit from tb_trn_class_room_subject crs\n"
            + "left join tb_mst_class_room b on crs.class_room_id = b.class_room_id\n"
            + "left join tb_mst_year_group c on crs.year_group_id = c.year_group_id\n"
            + "left join tb_mst_department d on crs.department_id = d.department_id\n"
            + "where b.class_room_id = ?1 and c.year_group_id = ?2 and crs.department_id = ?3\n"
            + "order by crs.orders,crs.subject_code", nativeQuery = true)
    List<Map> findsubjectfromclassroom(Integer classRoom, Integer yearGroupId, Integer departmentId);

    @Query(value = "SELECT crm.class_room_member_id,crm.class_id,crm.class_room_id,crm.fullname,t.*\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "CROSS JOIN LATERAL  (\n"
            + "     values \n"
            + "       (crm.activity1name,crm.activity1lesson, '1. กิจกรรมแนะแนว'),\n"
            + "       (crm.activity2name,crm.activity2lesson, '2. กิจกรรมลูกเสือเนตรนารี'),\n"
            + "       (crm.activity3name,crm.activity3lesson, '3. กิจกรรมชุมนุม'),\n"
            + "       (crm.activity4name,crm.activity4lesson, '4. กิจกรรมเพื่อสังคมและสาธารณประโยชน์')\n"
            + "  ) as t(activity_name,lesson, activity)\n"
            + "WHERE crm.class_room_member_id=?1\n"
            + "ORDER BY t.activity", nativeQuery = true)
    List<Map> findactivity(Integer classRoomMemberId);

    @Query(value = "SELECT a.* FROM tb_trn_class_room_member a LEFT JOIN tb_mst_class_room b ON a.class_room_id = b.class_room_id \n"
            + "WHERE a.role = 'student' and a.year_group_id = ?1 AND a.class_id = ?2 AND b.class_room_id = ?3\n"
            + "order BY cast(a.class_room_no AS INTEGER),a.stu_code", nativeQuery = true)
    List<Map> findstuforevaluation(Integer yearGroupId, Integer classId, Integer classRoomId);

    @Query(value = "SELECT *,class_room_no AS no from tb_trn_class_room_member\n"
            + "where record_status <> 'D' and ROLE = 'student' and class_id = ?1 AND class_room_id = ?2 and year_group_id = ?3 order by stu_code", nativeQuery = true)
    List<Map<String, String>> findstufornumberstucode(Integer classId, Integer classRoomId, Integer yearGroupId);

    @Query(value = "SELECT *,class_room_no AS no from tb_trn_class_room_member\n"
            + "where record_status <> 'D' and ROLE = 'student' and class_id = ?1 AND class_room_id = ?2 and year_group_id = ?3 order by fullname", nativeQuery = true)
    List<Map<String, String>> findstufornumberfullname(Integer classId, Integer classRoomId, Integer yearGroupId);

    @Query(value = "SELECT crcs.class_room_competencies_score_id,crm.sc_code,crm.year_group_id,crm.year_group_name,crm.class_room_member_id,crs.class_room_subject_id,crm.class_room,crm.class_room_no,crm.stu_code,crm.fullname,\n"
            + "COALESCE(crcs.score1,0) AS score1,COALESCE(crcs.score2,0) AS score2,COALESCE(crcs.score3,0) AS score3,COALESCE(crcs.score4,0) AS score4,COALESCE(crcs.score5,0) AS score5 FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_trn_class_room_subject crs ON crs.year_group_id = crm.year_group_id AND crs.class_room_id = crs.class_room_id\n"
            + "LEFT JOIN tb_trn_class_room_competencies_score crcs ON crcs.year_group_id = crm.year_group_id AND crcs.class_room_member_id = crm.class_room_member_id AND crcs.class_room_subject_id = crs.class_room_subject_id\n"
            + "WHERE crm.role = 'student' and crm.year_group_id = ?1 AND crm.class_room_id = ?2 and crs.class_room_subject_id = ?3 order BY cast(crm.class_room_no AS integer),crm.stu_code", nativeQuery = true)
    List<Map<String, String>> findmemberfromsubject(Integer yearGroupId, Integer classRoomId, Integer classRoomSubjectId);

    @Query(value = "SELECT a.class_room_member_id,a.class_room_no,a.stu_code,a.fullname,COALESCE(a.competencies_result,0) AS competencies_result  FROM tb_trn_class_room_member a \n"
            + "LEFT JOIN tb_mst_class_room b ON a.class_room_id = b.class_room_id\n"
            + "WHERE a.role = 'student' and a.year_group_id = ?1 AND a.class_id = ?2 AND b.class_room_id = ?3\n"
            + "order BY cast(a.class_room_no AS INTEGER),a.stu_code", nativeQuery = true)
    List<Map<String, String>> findstuforcompetencies(Integer yearGroupId, Integer classId, Integer classRoomId);

    @Query(value = "SELECT sc_code,'' AS year_group_id,'' as year_group_name, member_id,stu_code,fullname,fullname_en,'' as class_id,\n"
            + "'' AS class_name,'' as class_room_id,'' AS class_room,'' as class_room_no,ROLE,class_room_no AS numberstu,\n"
            + "activity1id,activity1name,activity1lesson,activity2id,activity2name,activity2lesson,activity3id,activity3name,activity3lesson,\n"
            + "activity4id,activity4name,activity4lesson,activity5id,activity5name,activity5lesson FROM tb_trn_class_room_member \n"
            + "WHERE ROLE = 'student' and year_group_id = ?1 AND class_id = ?2 AND class_room_id = ?3\n"
            + "ORDER BY cast(class_room_no AS integer)", nativeQuery = true)
    List<Map> findstuforstu007(Integer yearGroupId, Integer classId, Integer classRoomId);

    @Query(value = "SELECT crm.class_room_no AS numberstu,crm.sc_code,crm.year_group_id,crm.year_group_name,sp.stu_profile_id,crm.stu_code,crm.fullname,crm.class_room AS last_class_room, '' AS graduate_reason,'' AS class_of,'' AS graduate_date,'3' AS stu_status_id,'จบการศึกษา' AS stu_status FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_stu_profile sp ON crm.member_id = sp.stu_profile_id\n"
            + "WHERE crm.role ='student' and crm.year_group_id = ?1 AND crm.class_id = ?2 AND crm.class_room_id = ?3\n"
            + "ORDER BY CAST(class_room_no AS integer)", nativeQuery = true)
    List<Map<String, String>> findstuforstu009(Integer yearGroupId, Integer classId, Integer classRoomId);

    @Query(value = "SELECT  y.year_code AS year,COUNT(*) AS value,'' AS series,'จำนวนนักเรียน' as number FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_id is not null AND yg.year_id>=?1 AND yg.year_id<=?2\n"
            + "GROUP BY y.year_code ORDER BY y.year_code", nativeQuery = true)
    List<Map> findnumberstu(Integer startYearId, Integer endYearId);

    @Query(value = "SELECT  y.year_code AS year,COUNT(*) AS value,'' AS series,'จำนวนนักเรียน' as number FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_id is not null\n"
            + "GROUP BY y.year_code ORDER BY y.year_code", nativeQuery = true)
    List<Map> findnumberstuall();

    @Query(value = "SELECT  y.year_code AS year,COUNT(*) AS value,'' AS series,'จำนวนนักเรียน' as number FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_id is not null AND yg.year_id>=? \n"
            + "GROUP BY y.year_code ORDER BY y.year_code", nativeQuery = true)
    List<Map> findnumberstubystart(Integer startYearId);

    @Query(value = "SELECT  y.year_code AS year,COUNT(*) AS value,'' AS series,'จำนวนนักเรียน' as number FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_id is not null AND yg.year_id<=?1 \n"
            + "GROUP BY y.year_code ORDER BY y.year_code", nativeQuery = true)
    List<Map> findnumberstubyend(Integer endYearId);

    @Query(value = "SELECT crm.sc_code,yg.year_group_id,c.cat_class_id,crm.class_name,\n"
            + "COUNT(CASE WHEN sp.gender='ชาย' THEN sp.gender END) AS MALE,\n"
            + "COUNT(CASE WHEN sp.gender='หญิง' THEN sp.gender END) AS FEMALE\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "left JOIN tb_stu_profile sp ON crm.member_id=sp.stu_profile_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id=cr.class_room_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.role='student' AND cr.isactive=TRUE AND sp.gender IS NOT null\n"
            + "GROUP BY crm.sc_code,yg.year_group_id,c.cat_class_id,crm.class_name\n"
            + "HAVING yg.year_group_id=?1 AND  c.cat_class_id=?2\n"
            + "ORDER BY yg.year_group_id , c.cat_class_id ,crm.class_name", nativeQuery = true)
    List<Map> findnumberstuforcatclass(Integer yearGroupId, Integer catClassId);

    @Query(value = "SELECT crm.sc_code,yg.year_group_id,c.cat_class_id,crm.class_name,\n"
            + "COUNT(CASE WHEN sp.gender='ชาย' THEN sp.gender END) AS MALE,\n"
            + "COUNT(CASE WHEN sp.gender='หญิง' THEN sp.gender END) AS FEMALE\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "left JOIN tb_stu_profile sp ON crm.member_id=sp.stu_profile_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id=cr.class_room_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.role='student' AND cr.isactive=TRUE AND sp.gender IS NOT null\n"
            + "GROUP BY crm.sc_code,yg.year_group_id,c.cat_class_id,crm.class_name\n"
            + "HAVING yg.year_group_id=?1\n"
            + "ORDER BY yg.year_group_id , c.cat_class_id ,crm.class_name", nativeQuery = true)
    List<Map> findnumberstuforcatclassbyyeargroupid(Integer yearGroupId);

    @Query(value = "SELECT crm.sc_code,yg.year_group_id,c.cat_class_id,\n"
            + "COUNT(CASE WHEN sp.gender='ชาย' THEN sp.gender END) AS MALE,\n"
            + "COUNT(CASE WHEN sp.gender='หญิง' THEN sp.gender END) AS FEMALE\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "left JOIN tb_stu_profile sp ON crm.member_id=sp.stu_profile_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id=cr.class_room_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.role='student' AND cr.isactive=TRUE AND sp.gender IS NOT null\n"
            + "GROUP BY crm.sc_code,yg.year_group_id,c.cat_class_id\n"
            + "HAVING yg.year_group_id=?1 AND  c.cat_class_id=?2\n"
            + "ORDER BY yg.year_group_id , c.cat_class_id", nativeQuery = true)
    List<Map> findnumberstuforcatclasspie(Integer yearGroupId, Integer catClassId);

    @Query(value = "SELECT crm.sc_code,yg.year_group_id,\n"
            + "COUNT(CASE WHEN sp.gender='ชาย' THEN sp.gender END) AS MALE,\n"
            + "COUNT(CASE WHEN sp.gender='หญิง' THEN sp.gender END) AS FEMALE\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "left JOIN tb_stu_profile sp ON crm.member_id=sp.stu_profile_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id=cr.class_room_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.role='student' AND cr.isactive=TRUE AND sp.gender IS NOT null\n"
            + "GROUP BY crm.sc_code,yg.year_group_id\n"
            + "HAVING yg.year_group_id=?1\n"
            + "ORDER BY yg.year_group_id", nativeQuery = true)
    List<Map> findnumberstuforcatclassbyyeargroupidpie(Integer yearGroupId);

    @Query(value = "SELECT sp.sc_code,SUM(CASE WHEN sp.stu_status_id = 1 Then 1 Else 0 END) sum_register,\n"
            + "   SUM(CASE WHEN sp.stu_status_id = 2 Then 1 Else 0 End) sum_Student\n"
            + "FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class cl ON sp.to_class=cl.class_lname_th\n"
            + "LEFT JOIN tb_mst_cat_class cc ON cl.cat_class_id=cc.cat_class_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON sp.to_year_group=yg.name\n"
            + "WHERE sp.to_class<>'' and cc.cat_class_id=?1 AND yg.year_group_id = ?2\n"
            + "GROUP BY sp.sc_code", nativeQuery = true)
    List<Map> findforadmrp001sub02pie(Integer catClassId, Integer yearGroupId);

    @Query(value = "SELECT sp.sc_code,SUM(CASE WHEN sp.stu_status_id = 1 Then 1 Else 0 END) sum_register,\n"
            + "SUM(CASE WHEN sp.stu_status_id = 2 Then 1 Else 0 End) sum_Student\n"
            + "FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class cl ON sp.to_class=cl.class_lname_th\n"
            + "LEFT JOIN tb_mst_year_group yg ON sp.to_year_group=yg.name\n"
            + "WHERE sp.to_class<>''AND yg.year_group_id = ?1\n"
            + "GROUP BY sp.sc_code", nativeQuery = true)
    List<Map> findforadmrp001sub02yeargroupidpie(Integer yearGroupId);

    @Query(value = "SELECT cl.class_sname_th,\n"
            + "SUM(CASE WHEN sp.stu_status_id = 1 Then 1 Else 0 End) sum_register,\n"
            + "SUM(CASE WHEN sp.stu_status_id = 2 Then 1 Else 0 End) sum_student   \n"
            + "FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class cl ON sp.sc_code=cl.sc_code AND sp.to_class=cl.class_lname_th\n"
            + "LEFT JOIN tb_mst_cat_class cc ON cl.sc_code=cc.sc_code AND cl.cat_class_id=cc.cat_class_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON sp.sc_code=yg.sc_code AND sp.to_year_group=yg.name\n"
            + "WHERE sp.sc_code='PRAMANDA' AND sp.to_class<>''  AND yg.year_group_id = ?1 AND cc.cat_class_id=?2\n"
            + "GROUP BY cl.class_sname_th , cl.class_id\n"
            + "ORDER BY cl.class_id", nativeQuery = true)
    List<Map> findforadmrp001sub02(Integer yearGroupId, Integer catClassId);

    @Query(value = "SELECT cl.class_sname_th,\n"
            + "SUM(CASE WHEN sp.stu_status_id = 1 Then 1 Else 0 End) sum_register,\n"
            + "SUM(CASE WHEN sp.stu_status_id = 2 Then 1 Else 0 End) sum_student   \n"
            + "FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class cl ON sp.sc_code=cl.sc_code AND sp.to_class=cl.class_lname_th\n"
            + "LEFT JOIN tb_mst_cat_class cc ON cl.sc_code=cc.sc_code AND cl.cat_class_id=cc.cat_class_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON sp.sc_code=yg.sc_code AND sp.to_year_group=yg.name\n"
            + "WHERE sp.sc_code='PRAMANDA' AND sp.to_class<>''  AND yg.year_group_id = ?1 AND cc.cat_class_id IS NOT null\n"
            + "GROUP BY cl.class_sname_th , cl.class_id\n"
            + "ORDER BY cl.class_id", nativeQuery = true)
    List<Map> findforadmrp001sub02yeargroupid(Integer yearGroupId);

    @Query(value = "SELECT sp.sc_code,SUM(CASE WHEN sp.gender_id = 1 Then 1 Else 0 END) male,\n"
            + "   SUM(CASE WHEN sp.gender_id = 2 Then 1 Else 0 End) female\n"
            + "FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class cl ON sp.to_class=cl.class_lname_th\n"
            + "LEFT JOIN tb_mst_cat_class cc ON cl.cat_class_id=cc.cat_class_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON sp.to_year_group=yg.name\n"
            + "WHERE sp.to_class<>'' and cc.cat_class_id=?1 AND yg.year_group_id = ?2\n"
            + "GROUP BY sp.sc_code", nativeQuery = true)
    List<Map> findforadmrp001sub01(Integer catClassId, Integer yearGroupId);

    @Query(value = "SELECT sp.sc_code,SUM(CASE WHEN sp.gender_id = 1 Then 1 Else 0 END) male,\n"
            + "SUM(CASE WHEN sp.gender_id = 2 Then 1 Else 0 End) female\n"
            + "FROM tb_stu_profile sp \n"
            + "LEFT JOIN tb_mst_class cl ON sp.to_class=cl.class_lname_th\n"
            + "LEFT JOIN tb_mst_cat_class cc ON cl.cat_class_id=cc.cat_class_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON sp.to_year_group=yg.name\n"
            + "WHERE sp.to_class<>'' and yg.year_group_id = ?1\n"
            + "GROUP BY sp.sc_code", nativeQuery = true)
    List<Map> findforadmrp001sub01yeargroup(Integer yearGroupId);

    @Query(value = "SELECT tp.education,\n"
            + "COUNT(CASE WHEN tp.ispermanent=true THEN tp.ispermanent END) AS permanent,\n"
            + "COUNT(CASE WHEN tp.ispermanent=false THEN tp.ispermanent END) AS not_permanent\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = tp.class_id\n"
            + "WHERE tp.sc_code='PRAMANDA' AND tp.department_id = ?1 \n"
            + "AND c.cat_class_id = ?2\n"
            + "GROUP BY tp.education\n"
            + "ORDER BY CASE WHEN tp.education='ต่ำกว่าปริญญาตรี' THEN 1\n"
            + "WHEN tp.education='ปริญญาตรี' THEN 2\n"
            + "WHEN tp.education='ปริญญาโท' THEN 3\n"
            + "WHEN tp.education='ปริญญาเอก' THEN 4\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub1(Integer departmentId, Integer catClassId);

    @Query(value = "SELECT tp.education,\n"
            + "COUNT(CASE WHEN tp.ispermanent=true THEN tp.ispermanent END) AS permanent,\n"
            + "COUNT(CASE WHEN tp.ispermanent=false THEN tp.ispermanent END) AS not_permanent\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "WHERE tp.sc_code='PRAMANDA' \n"
            + "GROUP BY tp.education\n"
            + "ORDER BY CASE WHEN tp.education='ต่ำกว่าปริญญาตรี' THEN 1\n"
            + "WHEN tp.education='ปริญญาตรี' THEN 2\n"
            + "WHEN tp.education='ปริญญาโท' THEN 3\n"
            + "WHEN tp.education='ปริญญาเอก' THEN 4\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub1all();

    @Query(value = "SELECT tp.education,\n"
            + "COUNT(CASE WHEN tp.ispermanent=true THEN tp.ispermanent END) AS permanent,\n"
            + "COUNT(CASE WHEN tp.ispermanent=false THEN tp.ispermanent END) AS not_permanent\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = tp.class_id\n"
            + "WHERE tp.sc_code='PRAMANDA' \n"
            + "AND c.cat_class_id = ?1\n"
            + "GROUP BY tp.education\n"
            + "ORDER BY CASE WHEN tp.education='ต่ำกว่าปริญญาตรี' THEN 1\n"
            + "WHEN tp.education='ปริญญาตรี' THEN 2\n"
            + "WHEN tp.education='ปริญญาโท' THEN 3\n"
            + "WHEN tp.education='ปริญญาเอก' THEN 4\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub1byedutioncatclass(Integer catClassId);

    @Query(value = "SELECT tp.education,\n"
            + "COUNT(CASE WHEN EXTRACT (YEAR from AGE(NOW(),tp.dob)) <30  THEN tp.dob END) AS \"<30\",\n"
            + "COUNT(CASE WHEN EXTRACT (YEAR from AGE(NOW(),tp.dob)) >=30 AND EXTRACT (YEAR from AGE(NOW(),tp.dob)) <=45  THEN tp.dob END) AS \"30 - 45\",\n"
            + "COUNT(CASE WHEN EXTRACT (YEAR from AGE(NOW(),tp.dob)) >45  THEN tp.dob END) AS \">45\"\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = tp.class_id\n"
            + "WHERE tp.sc_code='PRAMANDA' AND department_id = ?1 AND c.cat_class_id = ?2\n"
            + "GROUP BY tp.education\n"
            + "ORDER BY CASE WHEN tp.education='ต่ำกว่าปริญญาตรี' THEN 1\n"
            + "WHEN tp.education='ปริญญาตรี' THEN 2\n"
            + "WHEN tp.education='ปริญญาโท' THEN 3\n"
            + "WHEN tp.education='ปริญญาเอก' THEN 4\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub1byage(Integer departmentId, Integer catClassId);

    @Query(value = "SELECT tp.gender,\n"
            + "COUNT(CASE WHEN tp.ispermanent=true THEN tp.ispermanent END) AS permanent,\n"
            + "COUNT(CASE WHEN tp.ispermanent=false THEN tp.ispermanent END) AS not_permanent\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = tp.class_id\n"
            + "WHERE tp.sc_code='PRAMANDA' \n"
            + "AND c.cat_class_id = ?1\n"
            + "GROUP BY tp.gender \n"
            + "ORDER BY CASE WHEN tp.gender='ชาย' THEN 1\n"
            + "WHEN tp.gender='หญิง' THEN 2\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub2catclass(Integer catClassId);

    @Query(value = "SELECT tp.gender,\n"
            + "COUNT(CASE WHEN tp.ispermanent=true THEN tp.ispermanent END) AS permanent,\n"
            + "COUNT(CASE WHEN tp.ispermanent=false THEN tp.ispermanent END) AS not_permanent\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "WHERE tp.sc_code='PRAMANDA' \n"
            + "GROUP BY tp.gender \n"
            + "ORDER BY CASE WHEN tp.gender='ชาย' THEN 1\n"
            + "WHEN tp.gender='หญิง' THEN 2\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub2all();

    @Query(value = "SELECT tp.gender,\n"
            + "COUNT(CASE WHEN tp.ispermanent=true THEN tp.ispermanent END) AS permanent,\n"
            + "COUNT(CASE WHEN tp.ispermanent=false THEN tp.ispermanent END) AS not_permanent\n"
            + "FROM tb_tea_teacher_profile tp\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = tp.class_id\n"
            + "WHERE tp.sc_code='PRAMANDA' AND department_id = ?1\n"
            + "AND c.cat_class_id = ?2\n"
            + "GROUP BY tp.gender \n"
            + "ORDER BY CASE WHEN tp.gender='ชาย' THEN 1\n"
            + "WHEN tp.gender='หญิง' THEN 2\n"
            + "END", nativeQuery = true)
    List<Map> findforadmrp002sub2(Integer departmentId, Integer catClassId);

    @Query(value = "SELECT sc_code,member_id,stu_code,fullname,fullname_en,class_id,class_name,class_room_id,class_room,role,year_group_name AS show_year_group FROM tb_trn_class_room_member\n"
            + "WHERE ROLE = 'teacher' AND year_group_id = ?1 AND class_id = ?2 AND class_room_id = ?3\n"
            + "ORDER BY fullname", nativeQuery = true)
    List<Map> findteachertec010(Integer yearGroupId, Integer classId, Integer classRoomId);

    @Query(value = "SELECT COUNT(*) AS num_stu,\n"
            + "COUNT(CASE WHEN gender_id=1 THEN gender END) AS male,\n"
            + "COUNT(CASE WHEN gender_id=2 THEN gender END) AS female\n"
            + " FROM tb_stu_profile WHERE stu_status_id = 2 AND record_status <> 'D'", nativeQuery = true)
    List<Map> findallnumstuandgender();

    @Query(value = "SELECT COUNT(*) AS num_tea,\n"
            + "COUNT(CASE WHEN gender='ชาย' THEN gender END) AS male,\n"
            + "COUNT(CASE WHEN gender='หญิง' THEN gender END) AS female\n"
            + " FROM tb_tea_teacher_profile WHERE record_status <> 'D'", nativeQuery = true)
    List<Map> findallnumteacherandgender();

    @Query(value = "SELECT crm.sc_code,y.year_code,\n"
            + "COUNT(CASE WHEN sp.gender='ชาย' THEN sp.gender END) AS MALE,\n"
            + "COUNT(CASE WHEN sp.gender='หญิง' THEN sp.gender END) AS FEMALE\n"
            + "FROM tb_trn_class_room_member crm\n"
            + "left JOIN tb_stu_profile sp ON crm.member_id=sp.stu_profile_id\n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_year y ON yg.year_id=y.year_id\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id=cr.class_room_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.role='student' AND cr.isactive=TRUE AND sp.gender IS NOT null\n"
            + "GROUP BY crm.sc_code,y.year_code\n"
            + "ORDER BY y.year_code", nativeQuery = true)
    List<Map> findgraphallnumstubyyear();

    @Query(value = "SELECT crm.sc_code,yg.name,cc.lname_th,\n"
            + "COUNT(*) AS student\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_year_group yg ON crm.year_group_id=yg.year_group_id\n"
            + "LEFT JOIN tb_mst_class c ON crm.class_id=c.class_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id=cr.class_room_id\n"
            + "LEFT JOIN tb_mst_cat_class cc ON cc.cat_class_id = c.cat_class_id\n"
            + "WHERE crm.sc_code='PRAMANDA' AND crm.role='student' \n"
            + "AND cr.isactive=TRUE \n"
            + "AND yg.iscurrent = true\n"
            + "GROUP BY crm.sc_code,yg.name,cc.lname_th,cc.cat_class_id\n"
            + "ORDER BY cc.cat_class_id", nativeQuery = true)
    List<Map> findnumstuallcatclasspie();

    @Transactional
    @Modifying
    @Query("UPDATE TrnClassRoomMemberEntity y SET y.classRoomNo = ?1,y.recordStatus = ?2,y.updatedDate = ?3 where y.classRoomMemberId = ?4")
    void updateclassroomno(String classRoomNo, String recordStatus, Date updatedDate, Long classRoomMemberId);

    @Transactional
    @Modifying
    @Query("UPDATE TrnClassRoomMemberEntity y SET y.competenciesResult = ?1,y.recordStatus = ?2,y.updatedUser = ?3 where y.classRoomMemberId = ?4")
    void updatecompetencies(Integer competenciesResult, String recordStatus, String updatedUser, Long classRoomMemberId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM tb_trn_class_room_member WHERE class_room_member_id = ?1", nativeQuery = true)
    void deletestufromclassroommember(Integer classRoomMemberId);

}
