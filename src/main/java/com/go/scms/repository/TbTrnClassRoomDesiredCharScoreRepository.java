/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTrnClassRoomDesiredCharScoreEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lumin
 */
@Repository
public interface TbTrnClassRoomDesiredCharScoreRepository extends JpaRepository<TbTrnClassRoomDesiredCharScoreEntity,Long>{
    
    @Transactional
    @Modifying
    @Query("UPDATE TbTrnClassRoomDesiredCharScoreEntity y SET y.score1 = ?1,y.score2 = ?2,y.score3 = ?3,y.score4 = ?4,y.score5 = ?5,y.score6 = ?6,y.score7 = ?7,y.score8 = ?8,y.score9 = ?9,y.recordStatus = ?10,y.updatedUser = ?11 where y.classRoomDesiredCharScoreId = ?12")
    void updateDesiredCharScore(Integer score1,Integer score2,Integer score3,Integer score4,Integer score5,Integer score6,Integer score7,Integer score8,Integer score9, String recordStatus,String updatedUser, Long classRoomDesiredCharScoreId);
}
