/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.KornClassRoomMemberSubjectAttendanceEntity;
import com.go.scms.entity.KornClassRoomSubjectEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kronn
 */
@Repository
public interface KsubjectRepository extends JpaRepository<KornClassRoomSubjectEntity, Long> {

    List<KornClassRoomSubjectEntity> findAllByRecordStatusNotIn(String recordStatus);
    List<KornClassRoomSubjectEntity> findAllByClassRoomIdOrderByClassRoomId(Integer classRoomId);

    @Query(value = "SELECT crs.class_room_subject_id,crs.subject_name_th FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_tea_teacher_profile tp ON tp.teacher_id = crs.teacher1\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = cr.class_id\n"
            + "WHERE tp.citizen_id = ?1 AND cr.class_room_id = ?2 AND crs.year_group_id = ?3\n"
            + "ORDER BY crs.orders", nativeQuery = true)
    List<Map> findddlsubjectbycitizenidandclassroomid(String citizenId, Integer classRoomId, Integer yearGroupId);

}
