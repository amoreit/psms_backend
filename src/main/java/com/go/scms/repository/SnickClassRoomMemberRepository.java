package com.go.scms.repository;

import com.go.scms.entity.SnickClassRoomMemberEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickClassRoomMemberRepository extends JpaRepository<SnickClassRoomMemberEntity, Long>{
    
        List<SnickClassRoomMemberEntity> findAllByRecordStatusNotInAndTeacherJoinTeacherId(String recordStatus, Long teacherId);
}
