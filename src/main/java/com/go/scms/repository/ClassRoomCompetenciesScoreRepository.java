/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ClassRoomCompetenciesScoreEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ClassRoomCompetenciesScoreRepository extends JpaRepository<ClassRoomCompetenciesScoreEntity, Long>{
    @Transactional
    @Modifying
    @Query("UPDATE ClassRoomCompetenciesScoreEntity y SET y.score1 = ?1,y.score2 = ?2,y.score3 = ?3,y.score4 = ?4,y.score5 = ?5,y.recordStatus = ?6,y.updatedUser = ?7 where y.classRoomCompetenciesScoreId = ?8")
    void updatecompetenciesscore(Integer score1,Integer score2,Integer score3,Integer score4,Integer score5, String recordStatus,String updatedUser, Long classRoomCompetenciesScoreId);
}
