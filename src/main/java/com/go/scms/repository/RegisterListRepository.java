package com.go.scms.repository;

import com.go.scms.entity.RegisterListEntity;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterListRepository extends JpaRepository<RegisterListEntity,Integer>{
        
    Page<RegisterListEntity> findAllByCitizenIdAndPhoneNoAndFirstNameAndLastNameAndEmail(String citizenId, String phoneNo, String firstName, String lastName, String email, Pageable pageable);
    Page<RegisterListEntity> findAllByCitizenIdContainingAndPhoneNoContainingAndFirstNameContainingAndLastNameContainingAndEmailContaining(String citizenId, String phoneNo, String firstName, String lastName, String email, Pageable pageable);
    Page<RegisterListEntity> findAllByActivated(boolean activated, Pageable pageable);
    
    List<RegisterListEntity> findAllByUsrNameContaining(String usrName);
    List<RegisterListEntity> findAllByEmailContaining(String email);

    Optional<RegisterListEntity> findByUserId(Integer userId);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.user_status = 'S' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.user_status = 'S'", nativeQuery = true)
    Page<Map> findalluserstatus(Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.user_type like %?1% AND usr.first_name like %?2% AND usr.last_name like %?3% AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.user_type like %?1% AND usr.first_name like %?2% AND usr.last_name like %?3% AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findallfield(String userType, String firstName, String lastName, Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.user_type like %?1% AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.user_type like %?1% AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findusertype(String userType, Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.first_name like %?1% AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.first_name like %?1% AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findfirstname(String firstName, Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.last_name like %?1% AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.last_name like %?1% AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findlastname(String lastName, Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.first_name like %?1% AND usr.last_name like %?2% AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.first_name like %?1% AND usr.last_name like %?2% AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findfirstnamelastname(String firstName, String lastName, Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" +
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.activated = true AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.activated = true AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findactivatedtrue(Pageable pageable);
    
    @Query(value = "select usr.user_id, \n" +
                 "usr.first_name, \n" +
                 "usr.last_name, \n" +
                 "usr.usr_name, \n" +
                 "usr.citizen_id, \n" + 
                 "usr.email, \n" + 
                 "usr.phone_no, \n" +
                 "usr.user_type, \n" +
                 "usr.activated,\n" +
                 "usr.user_status, \n" +
                 "usro.role_name \n" +
                 "from adm_user usr \n" +
                 "left join adm_role usro ON usro.role_id = usr.role_id \n" +
                 "where usr.activated = false AND usr.user_status <> 'D' \n",
                countQuery ="select count(*) from adm_user usr left join adm_role usro ON usro.role_id = usr.role_id where usr.activated = false AND usr.user_status <> 'D'", nativeQuery = true)
    Page<Map> findactivatedfalse(Pageable pageable);
    
    @Transactional
    @Modifying
    @Query("UPDATE RegisterListEntity regis SET regis.activated = ?1, regis.userUpdatedDate = ?2, regis.userStatus = 'S' where regis.userId = ?3")
    void updatedCheckAll(Boolean activated, Date userUpdatedDate, Integer userId); 
}
