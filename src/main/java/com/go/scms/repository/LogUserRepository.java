package com.go.scms.repository;

import com.go.scms.entity.LogUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogUserRepository extends JpaRepository<LogUserEntity,Long>{

}
