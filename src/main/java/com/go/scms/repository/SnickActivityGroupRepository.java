package com.go.scms.repository;

import com.go.scms.entity.SnickActivityGroupEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickActivityGroupRepository extends JpaRepository<SnickActivityGroupEntity, Long>{   
        List<SnickActivityGroupEntity> findAllByRecordStatusNotInAndCatClassIdOrderByActivityGroupId(String recordStatus, Integer catClassId);
}
