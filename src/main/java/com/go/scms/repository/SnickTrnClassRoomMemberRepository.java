package com.go.scms.repository;

import com.go.scms.entity.SnickTrnClassRoomMemberEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTrnClassRoomMemberRepository extends JpaRepository<SnickTrnClassRoomMemberEntity,Long>{
    
        Page<SnickTrnClassRoomMemberEntity> findAllByClassRoomIdAndYearGroupIdAndRecordStatusNotIn(Integer classRoomId, Integer yearGroupId, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByRecordStatusNotIn(String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByClassRoomIdAndRecordStatusNotIn(Integer classRoomId, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByYearGroupIdAndRecordStatusNotIn(Integer yearGroupId, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByStuCodeAndRecordStatusNotIn(String stuCode, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByYearGroupIdAndClassIdAndClassRoomIdAndRecordStatusNotIn(Integer yearGroupId, Integer classId, Integer classRoomId, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByClassIdAndRecordStatusNotIn(Integer classId, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByClassIdAndClassRoomIdAndRecordStatusNotIn(Integer classId, Integer classRoomId, String recordStatus, Pageable pageable);
        Page<SnickTrnClassRoomMemberEntity> findAllByYearGroupNameAndClassRoomAndRecordStatusNotIn(String yearGroupName, String classRoom, String recordStatus, Pageable pageable);
        
        List<SnickTrnClassRoomMemberEntity> findAllByClassRoomIdAndYearGroupIdAndRecordStatusNotInOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(Integer classRoomId, Integer yearGroupId, String recordStatus);
        List<SnickTrnClassRoomMemberEntity> findAllByStuCodeAndRecordStatusNotInOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(String stuCode, String recordStatus);
        
        List<SnickTrnClassRoomMemberEntity> findAllByRecordStatusNotIn(String recordStatus);
    
        List<SnickTrnClassRoomMemberEntity> findAllByYearGroupIdAndRecordStatusNotInAndRoleOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(Integer yearGroupId, String recordStatus, String role);
        List<SnickTrnClassRoomMemberEntity> findAllByClassRoomIdAndRecordStatusNotInAndRoleOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(Integer classRoomId, String recordStatus, String role);
        
        //--------------------------------------- aca004-sub04 search
        @Query(value = "SELECT crm.year_group_id,crm.year_group_name,crm.class_id,crm.class_room_id,crm.class_room,crm.class_room_no,crm.fullname,crm.stu_code,crm.ratw_result,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "WHERE crm.sc_code = 'PRAMANDA' AND crm.year_group_id = ?1 and crm.class_id = ?2 AND  crm.class_room_id = ?3 AND crm.record_status <> 'D' AND crm.role = 'student' ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT), crm.class_room_id", nativeQuery = true)
        List<Map<String,String>> multiall(Integer yearGroupId, Integer classId, Integer classRoomId);
        
        //--------------------------------------- aca004-sub04 update
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.ratwResult = ?1, crm.updatedUser = ?2, crm.recordStatus = ?3, crm.isactive = true where crm.classRoomMemberId = ?4")
        void updateMultiSkill(Integer ratwResult, String userName, String recordStatus, Long classRoomMemberId);
        
        //--------------------------------------- aca008 search
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.fullname,crm.activity1name as ac_name1,crm.activity1lesson as ac_lesson1,crm.activity1result,crm.class_room_no,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm \n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity1name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity1(String yearGroupName, Integer classId, String activityName, String recordStatus);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.fullname,crm.activity2name as ac_name2,crm.activity2lesson as ac_lesson2,crm.activity2result,crm.class_room_no,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity2name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity2(String yearGroupName, Integer classId, String activityName, String recordStatus);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.fullname,crm.activity3name as ac_name3,crm.activity3lesson as ac_lesson3,crm.activity3result,crm.class_room_no,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" + 
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity3name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity3(String yearGroupName, Integer classId, String activityName, String recordStatus);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.fullname,crm.activity4name as ac_name4,crm.activity4lesson as ac_lesson4,crm.activity4result,crm.class_room_no,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity4name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity4(String yearGroupName, Integer classId, String activityName, String recordStatus); 
        
        //--------------------------------------- aca008 search dialog
        @Query(value = "SELECT crm.class_id,crm.class_room_id,crm.fullname,crm.class_room,crm.class_room_no,crm.activity1name as ac_name1,crm.activity1lesson as ac_lesson1,crm.class_room_member_id FROM tb_trn_class_room_member crm \n" +
                    "WHERE crm.sc_code = 'PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND crm.record_status <> ?3 AND crm.role = 'student' AND SUBSTR(crm.class_room,5,1)<>'-' AND  (crm.activity1name = '' OR crm.activity1name IS NULL )\n" +
                    "ORDER BY cast(SUBSTR(crm.class_room,5,1) as INT),cast(crm.class_room_no as INT) ", nativeQuery = true)
        List<Map> finddialogactivity1(String yearGroupName, Integer classId, String recordStatus);
        
        @Query(value = "SELECT crm.class_id,crm.class_room_id,crm.fullname,crm.class_room,crm.class_room_no,crm.activity2name as ac_name2,crm.activity2lesson as ac_lesson2,crm.class_room_member_id FROM tb_trn_class_room_member crm \n" +
                    "WHERE crm.sc_code = 'PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND crm.record_status <> ?3 AND crm.role = 'student' AND SUBSTR(crm.class_room,5,1)<>'-' AND  (crm.activity2name = '' OR crm.activity2name IS NULL )\n" +
                    "ORDER BY cast(SUBSTR(crm.class_room,5,1) as INT),cast(crm.class_room_no as INT) ", nativeQuery = true)
        List<Map> finddialogactivity2(String yearGroupName, Integer classId, String recordStatus);
        
        @Query(value = "SELECT crm.class_id,crm.class_room_id,crm.fullname,crm.class_room,crm.class_room_no,crm.activity3name as ac_name3,crm.activity3lesson as ac_lesson3,crm.class_room_member_id FROM tb_trn_class_room_member crm \n" +
                    "WHERE crm.sc_code = 'PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND crm.record_status <> ?3 AND crm.role = 'student' AND SUBSTR(crm.class_room,5,1)<>'-' AND  (crm.activity3name = '' OR crm.activity3name IS NULL )\n" +
                    "ORDER BY cast(SUBSTR(crm.class_room,5,1) as INT),cast(crm.class_room_no as INT) ", nativeQuery = true)
        List<Map> finddialogactivity3(String yearGroupName, Integer classId, String recordStatus);
        
        @Query(value = "SELECT crm.class_id,crm.class_room_id,crm.fullname,crm.class_room,crm.class_room_no,crm.activity4name as ac_name4,crm.activity4lesson as ac_lesson4,crm.class_room_member_id FROM tb_trn_class_room_member crm \n" +
                    "WHERE crm.sc_code = 'PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND crm.record_status <> ?3 AND crm.role = 'student' AND SUBSTR(crm.class_room,5,1)<>'-' AND  (crm.activity4name = '' OR crm.activity4name IS NULL )\n" +
                    "ORDER BY cast(SUBSTR(crm.class_room,5,1) as INT),cast(crm.class_room_no as INT) ", nativeQuery = true)
        List<Map> finddialogactivity4(String yearGroupName, Integer classId, String recordStatus);
        
        //--------------------------------------- aca008 update dialog
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity1id = ?1, crm.activity1name = ?2, crm.activity1lesson = ?3, crm.fullname = ?4, crm.updatedUser = ?5, crm.recordStatus = ?6, crm.isactive = true where crm.classRoomMemberId = ?7")
        void updateActivity1(Integer activity1id, String activity1name, String activity1lesson, String fullname, String userName, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity2id = ?1, crm.activity2name = ?2, crm.activity2lesson = ?3, crm.fullname = ?4, crm.updatedUser = ?5, crm.recordStatus = ?6, crm.isactive = true where crm.classRoomMemberId = ?7")
        void updateActivity2(Integer activity2id, String activity2name, String activity2lesson, String fullname, String userName, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity3id = ?1, crm.activity3name = ?2, crm.activity3lesson = ?3, crm.fullname = ?4, crm.updatedUser = ?5, crm.recordStatus = ?6, crm.isactive = true where crm.classRoomMemberId = ?7")
        void updateActivity3(Integer activity3id, String activity3name, String activity3lesson, String fullname, String userName, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity4id = ?1, crm.activity4name = ?2, crm.activity4lesson = ?3, crm.fullname = ?4, crm.updatedUser = ?5, crm.recordStatus = ?6, crm.isactive = true where crm.classRoomMemberId = ?7")
        void updateActivity4(Integer activity4id, String activity4name, String activity4lesson, String fullname, String userName, String recordStatus, Long classRoomMemberId);
        
        //--------------------------------------- aca008 delete
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity1id = null, crm.activity1name = null, crm.activity1lesson = null, crm.updatedUser = ?1, crm.recordStatus = ?2, crm.isactive = true where crm.classRoomMemberId = ?3")
        void updateDeleteActivity1(String updatedUser, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity2id = null,  crm.activity2name = null, crm.activity2lesson = null, crm.updatedUser = ?1, crm.recordStatus = ?2, crm.isactive = true where crm.classRoomMemberId = ?3")
        void updateDeleteActivity2(String updatedUser, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity3id = null, crm.activity3name = null, crm.activity3lesson = null, crm.updatedUser = ?1, crm.recordStatus = ?2, crm.isactive = true where crm.classRoomMemberId = ?3")
        void updateDeleteActivity3(String updatedUser, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity4id = null, crm.activity4name = null, crm.activity4lesson = null, crm.updatedUser = ?1, crm.recordStatus = ?2, crm.isactive = true where crm.classRoomMemberId = ?3")
        void updateDeleteActivity4(String updatedUser, String recordStatus, Long classRoomMemberId);
        
        //--------------------------------------- aca004-sub02 search
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.class_room_no,crm.fullname,crm.activity1name as ac_name1,crm.activity1lesson as ac_lesson1,crm.activity1result,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity1name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map<String,String>> findactivity1result(String yearGroupName, Integer classId, String activityName, String recordStatus);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.class_room_no,crm.fullname,crm.activity2name as ac_name2,crm.activity2lesson as ac_lesson2,crm.activity2result,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity2name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map<String,String>> findactivity2result(String yearGroupName, Integer classId, String activityName, String recordStatus);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.class_room_no,crm.fullname,crm.activity3name as ac_name3,crm.activity3lesson as ac_lesson3,crm.activity3result,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity3name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map<String,String>> findactivity3result(String yearGroupName, Integer classId, String activityName, String recordStatus);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,stp.lastname_th,crm.year_group_name,crm.class_id,crm.class_name,crm.class_room_id,crm.class_room,crm.class_room_no,crm.fullname,crm.activity4name as ac_name4,crm.activity4lesson as ac_lesson4,crm.activity4result,crm.class_room_member_id\n" +
                    "FROM tb_trn_class_room_member crm\n" +
                    "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id \n" +
                    "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1 and crm.class_id = ?2 AND  crm.activity4name = ?3 AND crm.record_status <> ?4 ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map<String,String>> findactivity4result(String yearGroupName, Integer classId, String activityName, String recordStatus); 
        
        //--------------------------------------- aca004-sub02 update 
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity1result = ?1, crm.updatedUser = ?2, crm.recordStatus = ?3, crm.isactive = true where crm.classRoomMemberId = ?4")
        void updateActivity1result(Boolean activity1result, String userName, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity2result = ?1, crm.updatedUser = ?2, crm.recordStatus = ?3, crm.isactive = true where crm.classRoomMemberId = ?4")
        void updateActivity2result(Boolean activity2result, String userName, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity3result = ?1, crm.updatedUser = ?2, crm.recordStatus = ?3, crm.isactive = true where crm.classRoomMemberId = ?4")
        void updateActivity3result(Boolean activity3result, String userName, String recordStatus, Long classRoomMemberId);
        
        @Transactional
        @Modifying
        @Query("UPDATE SnickTrnClassRoomMemberEntity crm SET crm.activity4result = ?1, crm.updatedUser = ?2, crm.recordStatus = ?3, crm.isactive = true where crm.classRoomMemberId = ?4")
        void updateActivity4result(Boolean activity4result, String userName, String recordStatus, Long classRoomMemberId);
        
        //--------------------------------------- aca008-report
        @Query(value = "SELECT stp.title,stp.firstname_th,\n" +
                        "stp.lastname_th,crm.year_group_name,\n" +
                        "crm.class_id,crm.class_name,\n" +
                        "crm.class_room_id,crm.class_room,\n" +
                        "crm.fullname,\n" +
                        "crm.activity1name as ac_name1,\n" +
                        "crm.activity1lesson as ac_lesson1,\n" +
                        "crm.activity1result,crm.class_room_no,\n" +
                        "crm.class_room_member_id\n" +
                        "FROM tb_trn_class_room_member crm \n" +
                        "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id\n" +
                        "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1  AND  crm.activity1name = ?2 AND crm.record_status <> 'D' \n" +
                        "ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , \n" +
                        "CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity1report(String yearGroupName, String activityName);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,\n" +
                        "stp.lastname_th,crm.year_group_name,\n" +
                        "crm.class_id,crm.class_name,\n" +
                        "crm.class_room_id,crm.class_room,\n" +
                        "crm.fullname,\n" +
                        "crm.activity2name as ac_name2,\n" +
                        "crm.activity2lesson as ac_lesson2,\n" +
                        "crm.activity2result,crm.class_room_no,\n" +
                        "crm.class_room_member_id\n" +
                        "FROM tb_trn_class_room_member crm \n" +
                        "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id\n" +
                        "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1  AND  crm.activity2name = ?2 AND crm.record_status <> 'D' \n" +
                        "ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , \n" +
                        "CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity2report(String yearGroupName, String activityName);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,\n" +
                        "stp.lastname_th,crm.year_group_name,\n" +
                        "crm.class_id,crm.class_name,\n" +
                        "crm.class_room_id,crm.class_room,\n" +
                        "crm.fullname,\n" +
                        "crm.activity3name as ac_name3,\n" +
                        "crm.activity3lesson as ac_lesson3,\n" +
                        "crm.activity3result,crm.class_room_no,\n" +
                        "crm.class_room_member_id\n" +
                        "FROM tb_trn_class_room_member crm \n" +
                        "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id\n" +
                        "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1  AND  crm.activity3name = ?2 AND crm.record_status <> 'D' \n" +
                        "ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , \n" +
                        "CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity3report(String yearGroupName, String activityName);
        
        @Query(value = "SELECT stp.title,stp.firstname_th,\n" +
                        "stp.lastname_th,crm.year_group_name,\n" +
                        "crm.class_id,crm.class_name,\n" +
                        "crm.class_room_id,crm.class_room,\n" +
                        "crm.fullname,\n" +
                        "crm.activity4name as ac_name4,\n" +
                        "crm.activity4lesson as ac_lesson4,\n" +
                        "crm.activity4result,crm.class_room_no,\n" +
                        "crm.class_room_member_id\n" +
                        "FROM tb_trn_class_room_member crm \n" +
                        "INNER JOIN tb_stu_profile stp ON stp.stu_profile_id = crm.member_id\n" +
                        "WHERE crm.sc_code='PRAMANDA' AND crm.year_group_name = ?1  AND  crm.activity4name = ?2 AND crm.record_status <> 'D' \n" +
                        "ORDER BY CASE  WHEN SUBSTR (crm.class_room, 5, 1) <> '-' THEN CAST(SUBSTR (crm.class_room, 5, 1)AS INT)  END , \n" +
                        "CAST(crm.class_room_no AS INT)", nativeQuery = true)
        List<Map> findactivity4report(String yearGroupName, String activityName);
}
