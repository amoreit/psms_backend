/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TrnClassRoomSubjectEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TrnClassRoomSubjectRepository extends JpaRepository<TrnClassRoomSubjectEntity, Long> {

    @Query(value = "SELECT s.subject_code,s.subject_code2,s.name_th,s.name_en,s.subject_type,a.orders,s.credit,s.lesson,s.weight from\n"
            + "(select d.department_id,d.name,cr.class_room_id,d.orders from tb_mst_department d\n"
            + "left join tb_mst_cat_class cc on d.cat_class=cc.cat_class_id \n"
            + "left join tb_mst_class c on cc.cat_class_id=c.cat_class_id\n"
            + "left join tb_mst_class_room cr on c.class_id=cr.class_id\n"
            + "where d.department_id = ?1 order by d.orders)\n"
            + "as a\n" 
            + "left join tb_mst_subject s on a.department_id=s.department_id \n"
            + "GROUP BY s.subject_code,s.subject_code2,s.name_th,s.name_en,s.subject_type,a.orders,s.credit,s.lesson,s.weight\n"
            + "order BY  s.name_th", nativeQuery = true)
    List<Map> findsubjectindepartment(Integer departmentId);

    @Query(value = "SELECT crs.subject_code,crs.subject_name_th FROM tb_trn_class_room_subject crs\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "WHERE crs.year_group_id = ?1 AND cr.class_id = ?2\n"
            + "GROUP BY crs.subject_code,crs.subject_name_th,crs.orders\n"
            + "ORDER by crs.orders", nativeQuery = true)
    List<Map> findsubjectinddlaca002sub03(Integer yearGroupId, Integer classId);

    @Query(value = "SELECT crs.class_room_subject_id,cr.class_room_sname_th,crs.teacher_fullname1,crs.teacher_fullname2,s.department_id FROM tb_trn_class_room_subject crs\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "LEFT JOIN tb_mst_subject s ON s.subject_code = crs.subject_code\n"
            + "WHERE crs.year_group_id = ?1 AND cr.class_id = ?2 AND crs.subject_code = ?3\n"
            + "ORDER BY crs.class_room_id", nativeQuery = true)
    List<Map> findclassroomteacherinaca002sub03(Integer yearGroupId, Integer classId, String subjectCode);

    @Query(value = "SELECT a.class_name,a.stu_code,a.fullname,d.name,a.subject_code,a.subject_name_th,a.credit\n"
            + "FROM \n"
            + "(SELECT crm.year_group_id,crm.class_name,crm.class_room_id,crm.stu_code,crm.fullname,crs.department_id,crs.subject_code,crs.subject_name_th,crs.orders,crs.credit FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_trn_class_room_member crm ON crs.sc_code=crm.sc_code AND crs.year_group_id=crm.year_group_id AND crs.class_room_id=crm.class_room_id\n"
            + ")\n"
            + "AS a\n"
            + "LEFT JOIN tb_mst_department d ON a.department_id=d.department_id\n"
            + "WHERE a.stu_code = ?1 and a.year_group_id = ?2 and a.class_room_id = ?3\n"
            + "ORDER BY a.orders , a.subject_code", nativeQuery = true)
    List<Map> findsubjectinstudent(String stuCode, Integer yeargroupId, Integer classRoomId);

    @Query(value = "SELECT crs.class_room_subject_id,crs.subject_code,crs.subject_name_th FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_mst_class_room cr ON crs.class_room_id = cr.class_room_id\n"
            + "LEFT JOIN tb_tea_teacher_profile tp1 ON tp1.teacher_id = crs.teacher1\n"
            + "LEFT JOIN tb_tea_teacher_profile tp2 ON tp2.teacher_id = crs.teacher2\n"
            + "WHERE cr.class_id = ?1 AND crs.class_room_id = ?2 and crs.year_group_id = ?3\n"
            + "AND (tp1.citizen_id = ?4 OR tp2.citizen_id = ?4) ORDER BY crs.orders,crs.subject_code", nativeQuery = true)
    List<Map> findsubjectaca004sub07(Integer classId, Integer classRoomId, Integer yearGroupId, String citizenId);

    @Query(value = "SELECT crs.subject_code,crs.subject_name_th,yg.name,crs.year_group_id,crs.class_room_id,cr.class_room_sname_th,cr.class_id FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_mst_year_group yg ON crs.year_group_id = yg.year_group_id\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "WHERE crs.class_room_subject_id = ?1", nativeQuery = true)
    List<Map> finddetailsubject(Integer classRoomSubjectId);

    @Query(value = "SELECT cr.sc_code,'' AS year_group_id,'' AS department_id,'' as subject_code,'' as subject_code2,'' as subject_name_th,'' as subject_name_en,cr.class_room_id,cr.is_ep,'' AS score_id,'' AS orders,\n"
            + "'' AS credit,'' AS lesson,'' as weight,'' AS teacher1,'' AS teacher_fullname1,'' as subject_type\n"
            + "FROM tb_mst_class_room cr \n"
            + "where cr.class_id = ?1 AND cr.isactive = true ORDER BY cr.class_room_id", nativeQuery = true)
    List<Map<String, String>> findclassroomindialog(Integer classId);

    @Query(value = "SELECT crs.subject_code,crs.subject_name_th,crs.credit FROM tb_trn_class_room_subject crs LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "WHERE crs.year_group_id = ?1 AND cr.class_id = ?2\n"
            + "GROUP BY crs.subject_code,crs.subject_name_th,crs.credit,crs.orders\n" 
            + "ORDER BY crs.orders,crs.subject_code", nativeQuery = true)
    List<Map> findsubjectincopy(Integer yearGroupId, Integer classId);

    @Query(value = "SELECT crs.subject_code,crs.subject_name_th,crs.score_id,crs.teacher1,crs.teacher_fullname1 FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "WHERE (crs.sc_code='PRAMANDA' AND crs.class_room_id=cr.class_room_id) AND crs.year_group_id= ?1 \n"
            + "AND cr.class_id= ?2 AND crs.subject_code= ?3\n"
            + "GROUP BY crs.subject_code,crs.subject_name_th,crs.score_id,crs.teacher1,crs.teacher_fullname1", nativeQuery = true)
    List<Map<String, String>> findsubjectacac002update(Integer yearGroupId, Integer classId, String subjectCode);

    @Query(value = "SELECT crs.sc_code, '' AS year_group_id,crs.class_room_id,crs.department_id,crs.subject_code,crs.subject_code2,\n"
            + "crs.subject_name_th,crs.subject_name_en,crs.score_id,crs.orders,crs.credit,crs.lesson,crs.weight,crs.teacher1,crs.teacher_fullname1,\n"
            + "crs.teacher2,crs.teacher_fullname2 FROM tb_trn_class_room_subject crs LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "WHERE crs.year_group_id = ?1 AND cr.class_id = ?2", nativeQuery = true)
    List<Map<String, String>> findsubjectallinclassroomcopy(Integer yearGroupId, Integer classId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM tb_trn_class_room_subject crs \n"
            + "USING tb_mst_class_room cr \n"
            + "WHERE (crs.sc_code='PRAMANDA' AND crs.class_room_id=cr.class_room_id) AND crs.year_group_id= ?1 AND cr.class_id= ?2 AND crs.subject_code= ?3", nativeQuery = true)
    void deletesubjectindepartment(Integer yearGroupId, Integer classId, String subjectCode);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject crs SET score_id = ?1,teacher1 = ?2,teacher_fullname1 = ?3,record_status = ?4,updated_user = ?5 \n"
            + "from tb_mst_class_room cr \n"
            + "WHERE cr.class_room_id = crs.class_room_id and crs.year_group_id= ?6 \n"
            + "AND cr.class_id= ?7 AND crs.subject_code= ?8", nativeQuery = true)
    void updatesubjectaca002sub01(Integer scoreId, Integer teacher1, String teacherFullname1, String recordStatus, String updatedUser, Integer yearGroupId, Integer classId, String subjectCode);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject crs SET score_id = ?1,teacher1 = null,teacher_fullname1 = null,record_status = ?2,updated_user = ?3 \n"
            + "from tb_mst_class_room cr \n"
            + "WHERE cr.class_room_id = crs.class_room_id and crs.year_group_id= ?4 \n"
            + "AND cr.class_id= ?5 AND crs.subject_code= ?6", nativeQuery = true)
    void updatesubjectaca002sub01setnull(Integer scoreId, String recordStatus, String updatedUser, Integer yearGroupId, Integer classId, String subjectCode);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject crs SET score_id = ?1,teacher1 = ?2,teacher_fullname1 = ?3,record_status = ?4,updated_user = ?5 \n"
            + "from tb_mst_class_room cr \n"
            + "WHERE cr.class_room_id = crs.class_room_id and crs.year_group_id= ?6 \n"
            + "AND cr.class_id= ?7 AND crs.subject_code= ?8", nativeQuery = true)
    void updatesubjectaca002sub01catclass1(Integer scoreId, Integer teacher1, String teacherFullname1, String recordStatus, String updatedUser, Integer yearGroupId, Integer classId, String subjectCode);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject crs SET score_id = ?1,teacher1 = null,teacher_fullname1 = null,record_status = ?2,updated_user = ?3 \n"
            + "from tb_mst_class_room cr \n"
            + "WHERE cr.class_room_id = crs.class_room_id and crs.year_group_id= ?4 \n"
            + "AND cr.class_id= ?5 AND crs.subject_code= ?6", nativeQuery = true)
    void updatesubjectaca002sub01catclass1setnull(Integer scoreId, String recordStatus, String updatedUser, Integer yearGroupId, Integer classId, String subjectCode);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject SET teacher1 = ?1,teacher_fullname1 =?2,record_status = ?3,updated_user = ?4\n"
            + "WHERE class_room_subject_id = ?5", nativeQuery = true)
    void updateteacher1inaca002sub03(Integer teacher1, String teacherFullname1, String recordStatus, String updatedUser, Long classRoomSubjectId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject SET teacher1 = null,teacher_fullname1 =null,record_status = ?1,updated_user = ?2\n"
            + "WHERE class_room_subject_id = ?3", nativeQuery = true)
    void deleteteacher1inaca002sub03(String recordStatus, String updatedUser, Long classRoomSubjectId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject SET teacher2 = ?1,teacher_fullname2 =?2,record_status = ?3,updated_user = ?4\n"
            + "WHERE class_room_subject_id = ?5", nativeQuery = true)
    void updateteacher2inaca002sub03(Integer teacher2, String teacherFullname2, String recordStatus, String updatedUser, Long classRoomSubjectId);

    @Transactional
    @Modifying
    @Query(value = "UPDATE tb_trn_class_room_subject SET teacher2 = null,teacher_fullname2 =null,record_status = ?1,updated_user = ?2\n"
            + "WHERE class_room_subject_id = ?3", nativeQuery = true)
    void deleteteacher2inaca002sub03(String recordStatus, String updatedUser, Long classRoomSubjectId);

}
