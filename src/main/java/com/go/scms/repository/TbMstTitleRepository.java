/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbMstTitleEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author lumin
 */
@Repository
public interface TbMstTitleRepository extends JpaRepository<TbMstTitleEntity, Long>{
    
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndLangCodeOrderByTitleId(String recordStatus, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameAndGenderAndLangCodeOrderByTitleId(String recordStatus, String name, Integer gender, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndGenderAndLangCodeNotInOrderByTitleId(String recordStatus, String name, Integer gender, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndLangCodeOrderByTitleId(String recordStatus, String name_item1, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndNameNotInAndLangCodeOrderByTitleId(String recordStatus, String name_item1, String name_item2, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndNameNotInAndLangCodeNotInOrderByTitleId(String recordStatus, String name_item1, String name_item2, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndNameNotInAndGenderAndLangCodeOrderByTitleId(String recordStatus, String name_item1, String name_item2, Integer gender, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndNameNotInAndNameNotInAndLangCodeOrderByTitleId(String recordStatus, String name_item1, String name_item2, String name_item3, String langCode);
    List<TbMstTitleEntity> findAllByRecordStatusNotInAndNameNotInAndNameNotInAndNameNotInAndNameNotInAndGenderAndLangCodeOrderByTitleId(String recordStatus, String name_item1, String name_item2, String name_item3, String name_item4, Integer gender, String langCode);

    Optional<TbMstTitleEntity> findByTitleId(Long titleId);
}
