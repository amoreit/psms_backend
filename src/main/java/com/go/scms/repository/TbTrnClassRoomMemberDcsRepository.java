/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TbTrnClassRoomMemberDcsEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author lumin
 */
public interface TbTrnClassRoomMemberDcsRepository extends JpaRepository<TbTrnClassRoomMemberDcsEntity, Long> {

    //max
    @Query(value = "SELECT crmd.class_room_member_dcs_id,crmd.sc_code,crmd.year_group_id,crmd.year_group_name,crmd.class_room_member_id,crmd.class_id,crmd.class_name,\n"
            + "crmd.class_room_id,crmd.class_room,crmd.class_room_no,crmd.stu_code,crmd.fullname,sp.firstname_th,sp.lastname_th,crmd.fullname_en,crmd.behaviours_skills_detail_id,\n"
            + "crmd.item,crmd.item_desc,crmd.itemno,crmd.description,CAST (crmd.score1 AS INTEGER),CAST (crmd.score2 AS INTEGER),CAST (crmd.sum_score AS INTEGER),crmd.item_result\n"
            + "FROM tb_trn_class_room_member_dcs crmd\n"
            + "LEFT JOIN tb_trn_class_room_member crm ON crm.class_room_member_id = crmd.class_room_member_id\n"
            + "LEFT JOIN tb_stu_profile sp ON sp.stu_profile_id = crm.member_id\n"
            + "WHERE crmd.class_room_member_id = ?1 AND crmd.year_group_id = ?2\n"
            + "ORDER BY crmd.class_room_member_dcs_id ASC", nativeQuery = true)
    List<Map<String, String>> findAca004Sub06Form(Integer classRoomMemberId, Integer yearGroupId);

    @Query(value = "SELECT sp.stu_profile_id,sp.stu_code,sp.fullname FROM tb_stu_profile sp\n"
            + "WHERE sp.stu_status_id = ?1 AND sp.stu_code = ?2", countQuery = "SELECT count(*) FROM tb_stu_profile as sp\n"
            + "WHERE sp.stu_status_id = ?1 AND sp.stu_code = ?2", nativeQuery = true)
    Page<Map> findAca014_1(long stuStatusId, String stuCode, Pageable pageable);

    @Query(value = "SELECT sp.stu_profile_id,sp.stu_code,sp.fullname FROM tb_stu_profile sp\n"
            + "WHERE sp.stu_status_id = ?1 AND sp.firstname_th like %?2% AND sp.lastname_th like %?3%", countQuery = "SELECT count(*) FROM tb_stu_profile as sp\n"
            + "WHERE sp.stu_status_id = ?1 AND sp.firstname_th like %?2% AND sp.lastname_th like %?3%", nativeQuery = true)
    Page<Map> findAca014_2(long stuStatusId, String firstnameth, String lastnameth, Pageable pageable);

    @Query(value = "SELECT crmd.class_room_member_dcs_id,crmd.sc_code,crmd.year_group_id,crmd.year_group_name,crmd.class_room_member_id,crmd.class_id,crmd.class_name,\n"
            + "crmd.class_room_id,crmd.class_room,crmd.class_room_no,crmd.stu_code,crmd.fullname,sp.firstname_th,sp.lastname_th,crmd.fullname_en,crmd.behaviours_skills_detail_id,\n"
            + "crmd.item,crmd.item_desc,crmd.itemno,crmd.description,crmd.score1,crmd.score2,crmd.sum_score,crmd.item_result\n"
            + "FROM tb_trn_class_room_member_dcs crmd \n"
            + "LEFT JOIN tb_trn_class_room_member crm ON crm.class_room_member_id = crmd.class_room_member_id\n"
            + "LEFT JOIN tb_stu_profile sp ON sp.stu_profile_id = crm.member_id\n"
            + "WHERE crmd.stu_code = ?1 \n"
            + "ORDER BY crmd.class_room_member_dcs_id ASC", nativeQuery = true)
    List<Map<String, String>> findAca014Html(String stuCode);

    @Transactional
    @Modifying
    @Query("UPDATE TbTrnClassRoomMemberDcsEntity y SET y.score1 = ?1, y.score2 = ?2, y.recordStatus = ?3, y.updatedUser = ?4 where y.classRoomMemberDcsId = ?5")
    void updateScore(Integer score1, Integer score2, String recordStatus, String updatedUser, Long classRoomMemberDcsId);

}
