/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;


import com.go.scms.entity.ActivityPaiEntity;
import com.go.scms.entity.TbStuTranscriptEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kronn
 */

@Repository
public interface TbStuTranscriptRepository extends JpaRepository<TbStuTranscriptEntity,Long>{
  
    List<TbStuTranscriptEntity> findAllByRecordStatusNotIn(String recordStatus);
  
    @Query(value = "select a.stu_code, a.fullname, a.class_room, a.class_room_member_id from tb_trn_class_room_member a LEFT JOIN tb_mst_class b ON a.class_id = b.class_id LEFT JOIN tb_mst_cat_class c ON b.cat_class_id = c.cat_class_id  "
                  + "WHERE c.cat_class_id != '1' and year_group_id = ?1 and a.class_id = ?2 and a.class_room_id = ?3  order by a.class_room_id, a.stu_code", countQuery = "select count(*) from tb_trn_class_room_member a LEFT JOIN tb_mst_class b ON a.class_id = b.class_id LEFT JOIN tb_mst_cat_class c ON b.cat_class_id = c.cat_class_id "
                  + " WHERE c.cat_class_id != '1' and year_group_id = ?1 and a.class_id = ?2 and a.class_room_id = ?3 ", nativeQuery = true)     
    Page<Map> findrep002(Integer yearGroupId, Integer classId, Integer classRoomId, Pageable pageable);
 
}
    
