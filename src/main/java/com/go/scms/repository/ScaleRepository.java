/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ScaleEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface ScaleRepository extends JpaRepository<ScaleEntity,Integer>{
     Optional<ScaleEntity> findByScaleId(Integer scaleId);
     List<ScaleEntity> findAllByRecordStatusNotInOrderByScaleId(String recordStatus);
}
