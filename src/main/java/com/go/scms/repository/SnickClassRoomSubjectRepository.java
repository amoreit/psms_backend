package com.go.scms.repository;

import com.go.scms.entity.SnickClassRoomSubjectEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickClassRoomSubjectRepository extends JpaRepository<SnickClassRoomSubjectEntity, Long>{
    
        Page<SnickClassRoomSubjectEntity>findAllByRecordStatusNotInAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(String recordStatus, Long catClassId, Pageable pageable);
        Page<SnickClassRoomSubjectEntity>findAllByRecordStatusNotInAndClassRoomJoinClassJoinClassIdAndClassRoomJoinClassRoomIdAndYearGroupJoinYearGroupIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(String recordStatus, Integer classId, Integer classRoomId, Integer yearGroupId, Long catClassId, Pageable pageable);
        Page<SnickClassRoomSubjectEntity>findAllByRecordStatusNotInAndYearGroupJoinYearGroupIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(String recordStatus, Integer yearGroupId, Long catClassId, Pageable pageable);
        Page<SnickClassRoomSubjectEntity>findAllByRecordStatusNotInAndClassRoomJoinClassJoinClassIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(String recordStatus, Integer classId, Long catClassId, Pageable pageable);
        Page<SnickClassRoomSubjectEntity>findAllByRecordStatusNotInAndClassRoomJoinClassJoinClassIdAndClassRoomJoinClassRoomIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(String recordStatus, Integer classId, Integer classRoomId, Long catClassId, Pageable pageable);
}
 