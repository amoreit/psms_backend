/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ActivityGroupEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityGroupRepository extends JpaRepository<ActivityGroupEntity, Long>{
    
    Page<ActivityGroupEntity> findAllByRecordStatusNotInAndActivityGroupContainingOrderByActivityGroupId(String recordStatus,String activityGroup,Pageable pageable);
    
}
