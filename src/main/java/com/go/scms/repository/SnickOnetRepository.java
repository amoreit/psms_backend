package com.go.scms.repository;

import com.go.scms.entity.FamilyStatusEntity;
import com.go.scms.entity.SnickOnetEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickOnetRepository extends JpaRepository<SnickOnetEntity, Long>{   
        List<SnickOnetEntity> findAllByRecordStatusNotInAndCitizenId(String recordStatus, String citizenId);    
}
