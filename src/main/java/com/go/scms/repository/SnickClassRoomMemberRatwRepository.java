package com.go.scms.repository;

import com.go.scms.entity.SnickClassRoomMemberRatwEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickClassRoomMemberRatwRepository extends JpaRepository<SnickClassRoomMemberRatwEntity, Long>{
       
        @Query(value = "select crm.class_room_member_id, \n" +
             "crm.sc_code, \n" +
             "crm.class_room_id, \n" +
             "crm.class_room, \n" +
             "crm.class_room_no, \n" +
             "crm.stu_code,\n" +
             "crm.year_group_id,\n" +
             "crm.year_group_name, \n" +
             "crm.fullname, \n" +
             "crs.class_room_subject_id, \n"+
             "crs.subject_code, \n" +
             "crs.subject_name_th as class_room_sname_th, \n" +
             "crrs.score1, \n" +
             "crrs.score2, \n" +
             "crrs.score3, \n" +
             "crrs.score4, \n" +
             "crrs.score5, \n" +
             "crrs.class_room_ratw_score_id \n" +
             "from tb_trn_class_room_member crm \n" +
             "left join tb_trn_class_room_subject crs on crm.class_room_id = crs.class_room_id \n" +
             "left join tb_trn_class_room_ratw_score crrs on crrs.class_room_member_id = crm.class_room_member_id \n" +
             "where crs.class_room_subject_id = ?1 and crm.class_room like %?2% and crm.year_group_name like %?3% and crm.record_status <> 'D' and crm.role = 'student'\n"+ 
             "order by cast(crm.class_room_no as INT), crm.stu_code, crm.class_room_member_id",
             countQuery ="select count(*) \n"+
                     "from tb_trn_class_room_member crm \n" +
                     "left join tb_trn_class_room_subject crs on crm.class_room_id = crs.class_room_id \n" +
                     "left join tb_trn_class_room_ratw_score crrs on crrs.class_room_member_id = crm.class_room_member_id \n" +
                     "where crs.class_room_subject_id = ?1 and crm.class_room like %?2% and crm.year_group_name like %?3% and crm.record_status <> 'D' and crm.role = 'student'", nativeQuery = true)
        List<Map<String,String>> findmember(Long classRoomSubjectId, String classRoom, String yearGroupName);
}
