/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ClassRoomEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassRoomRepository extends JpaRepository<ClassRoomEntity, Long> {

    Page<ClassRoomEntity> findAllByRecordStatusNotInAndClassJoinClassSnameThContaining(String recordStatus, String classSnameTh, Pageable pageable);
    List<ClassRoomEntity> findAllByRecordStatusNotInAndClassJoinClassSnameThContainingOrderByClassRoomIdAsc(String recordStatus, String classSnameTh);
    List<ClassRoomEntity> findAllByRecordStatusNotInOrderByClassRoomIdAsc(String recordStatus);
    List<ClassRoomEntity> findAllByRecordStatusNotInAndClassJoinClassIdOrderByClassRoomIdAscClassJoinClassIdAsc(String recordStatus, Integer classId);
    List<ClassRoomEntity> findAllByRecordStatusNotInAndClassRoomNotInAndClassRoomNotInAndClassJoinClassIdOrderByClassRoomIdAscClassJoinClassIdAsc(String recordStatus, String classRoom1, String classRoom2, Integer classId);
    List<ClassRoomEntity> findAllByRecordStatusNotInAndClassJoinClassSnameThAndIsactiveOrderByClassRoomId(String recordStatus, String classSnameTh,Boolean isactive);
    List<ClassRoomEntity> findAllByRecordStatusNotInOrderByClassRoomId(String recordStatus);
    Optional<ClassRoomEntity> findByClassRoomId(Integer classRoomId);
    Optional<ClassRoomEntity> findByClassRoomId(Long classRoomId);

    @Query(value = "select a.is_ep,a.class_room_id,a.class_room,b.class_sname_th,a.class_room_sname_th,a.created_user,a.created_date,a.updated_user,a.updated_date from tb_mst_class_room a left join tb_mst_class b on a.class_id = b.class_id where a.record_status <> 'D' and b.class_sname_th like %?1% order by a.class_id,cast(NULLIF(regexp_replace(a.class_room, E'\\\\D', '', 'g'), '') AS integer)",countQuery="select count(*) from tb_mst_class_room a left join tb_mst_class b on a.class_id = b.class_id where a.record_status <> 'D' and b.class_sname_th like %?1%",nativeQuery =true)
    Page<Map> find(String nameClass,Pageable pageable);
    
    @Query(value = "SELECT cr.class_room_id, cr.class_room_sname_th FROM tb_mst_class_room cr\n"
            + "WHERE cr.class_id = ?1\n"
            + "GROUP BY cr.class_room_id,cr.class_room_sname_th\n"
            + "ORDER BY cr.class_room_id", countQuery = "select count(*) FROM tb_mst_class_room cr\n"
            + "WHERE cr.class_id = ?1\n"
            + "GROUP BY cr.class_room_id,cr.class_room_sname_th\n"
            + "ORDER BY cr.class_room_id", nativeQuery = true)
    Page<Map> findClassRoom1(Integer classId, Pageable pageable);

    @Query(value = "SELECT cr.class_room_id, cr.class_room_sname_th FROM tb_mst_class_room cr\n"
            + "GROUP BY cr.class_room_id,cr.class_room_sname_th\n"
            + "ORDER BY cr.class_room_id", countQuery = "select count(*) FROM tb_mst_class_room cr\n"
            + "GROUP BY cr.class_room_id,cr.class_room_sname_th\n"
            + "ORDER BY cr.class_room_id", nativeQuery = true)
    Page<Map> findClassRoom2(Pageable pageable);
    
    @Query(value = "SELECT class_room_id,class_room,class_room_sname_th,is_ep FROM tb_mst_class_room WHERE record_status <> 'D' and isactive = true and class_id = ?1 ORDER BY class_room_id", nativeQuery = true)
    List<Map> findclassroomid(Integer classId);
    
    //moss
    @Query(value = "SELECT cr.class_room_id,cr.class_room,cr.class_room_lname_th,cr.class_room_sname_th FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_tea_teacher_profile tp ON tp.teacher_id = crs.teacher1\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = cr.class_id\n"
            + "WHERE tp.citizen_id = ?1 AND c.class_id = ?2 GROUP BY cr.class_room_id,cr.class_room\n"
            + "ORDER BY cr.class_room_id", nativeQuery = true)
    List<Map> findddlfilterbycitizenIdandclassid(String citizenId,Integer classId);

    //**nock
    List<ClassRoomEntity> findAllByRecordStatusNotInAndClassJoinClassIdAndIsactiveOrderByClassRoomId(String recordStatus, Integer classId,Boolean isactive);
    
    //max
    List<ClassRoomEntity> findAllByRecordStatusNotInAndClassJoinClassSnameThContainingOrderByClassRoomId(String recordStatus, String classSnameTh);
    
    @Query(value = "SELECT cr.class_room_id,cr.class_room_sname_th,cr.is_ep FROM tb_mst_class_room cr\n"
            + "WHERE cr.class_id=(SELECT cr.class_id FROM tb_mst_class_room cr WHERE cr.class_room_id=?1) AND cr.isactive = TRUE AND cr.record_status <>'D' AND cr.class_room_id <> ?1\n"
            + "ORDER BY cr.class_room", nativeQuery = true)
    List<Map> findddlfindroombyclassroomid(Integer classRoomId);
	

    @Query(value = "SELECT cr.class_room_id,cr.class_room_sname_th,cr.is_ep,cr.class_id,c.class_sname_th FROM tb_mst_class_room cr\n" +
            "LEFT JOIN tb_mst_class c ON c.class_id = cr.class_id\n" +
            "WHERE cr.record_status <>'D' AND cr.isactive = TRUE ORDER BY cr.class_id,CAST(cr.class_room AS integer)", nativeQuery = true)
    List<Map> findddlclassroomisep(); 
}
