/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.KornClassRoomMemberEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kronn
 */
@Repository
public interface KornClassRoomMemberRepository extends JpaRepository<KornClassRoomMemberEntity, Long>{
    
    @Query(value = "SELECT crm.year_group_id,crm.year_group_name, crm.class_name, crm.class_room, crm.fullname, crm.stu_code, crm.class_room_member_id, tsl.set_no, tsl.seq_no, stp.stu_profile_id,stp.dob, tsl.stu_transcript_log_id, tsl.transcript_date  \n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "LEFT JOIN tb_stu_profile stp ON stp.stu_code = crm.stu_code \n"
            + "LEFT JOIN tb_stu_transcript_log tsl ON tsl.fullname = crm.fullname \n"
            + "WHERE crm.record_status <> 'D' \n"
            + "AND crm.year_group_id = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3 \n"
            + "AND (tsl.doctype_id = '2' or tsl.doctype_id is null )"
            + "AND crm.role='student'"
            + "order by crm.year_group_id", countQuery = "SELECT COUNT (DISTINCT crm.class_room)\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "WHERE crm.record_status <> 'D'"
            + "AND crm.year_group_id  = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3"
            + "AND crm.role='student'", nativeQuery = true)
    Page<Map> findtran(Integer yearGroupId, Integer classId, Integer classRoomId, Pageable pageable);
    
    @Query(value = "SELECT crm.class_name,crm.fullname, crm.stu_code ,crm.class_room_no \n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "LEFT JOIN tb_stu_profile stp ON stp.stu_code = crm.stu_code \n"
            + "LEFT JOIN tb_trn_class_room_subject sub ON sub.class_room_id = crm.class_room_id \n"
            + "WHERE crm.record_status <> 'D' \n"
            + "AND crm.year_group_id = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3 \n"
            + "AND sub.class_room_subject_id = ?4 \n"
            + "AND crm.role='student'"
            + "order by CAST(crm.class_room_no as int) ASC", countQuery = "SELECT COUNT (DISTINCT crm.class_room)\n"
            + "FROM tb_trn_class_room_member crm \n"
            + "LEFT JOIN tb_mst_class_room cr ON crm.class_room_id = cr.class_room_id\n"
            + "LEFT JOIN tb_trn_class_room_subject sub ON sub.class_room_id = crm.class_room_id \n"
            + "WHERE crm.record_status <> 'D'"
            + "AND crm.year_group_id  = ?1 \n"
            + "AND crm.class_id = ?2 \n"
            + "AND cr.class_room_id = ?3 \n"
            + "AND sub.class_room_subject_id = ?4 ", nativeQuery = true)
    Page<Map> sub(Integer yearGroupId, Integer classId, Integer classRoomId,Integer classRoomSubjectId , Pageable pageable);  
    
    @Query(value = " SELECT crs.sc_code,crs.year_group_id , yg.name , crs.class_room_id , crm.class_room_member_id , crm.stu_code , crm.fullname , crs.class_room_subject_id ,crm.class_room_no"
            + " FROM tb_trn_class_room_member crm \n"
            + " LEFT JOIN tb_trn_class_room_subject crs ON crm.sc_code = crs.sc_code \n"
            + " AND crm.year_group_id = crs.year_group_id \n"
            + " AND crm.class_room_id = crs.class_room_id \n"
            + " LEFT JOIN tb_mst_class_room cr ON crm.sc_code = cr.sc_code AND crm.class_room_id= cr.class_room_id \n"
            + " LEFT JOIN tb_mst_year_group yg ON crs.sc_code = yg.sc_code AND crs.year_group_id=yg.year_group_id \n"
            + " LEFT JOIN tb_mst_score sc ON crs.sc_code = sc.sc_code AND crs.score_id=sc.score_id \n"
            + " LEFT JOIN tb_mst_subject s ON crs.sc_code = s.sc_code AND crs.department_id = s.department_id AND crs.subject_code = s.subject_code \n"
            + " WHERE crm.record_status <> 'D' \n"
            + " AND crs.year_group_id = ?1 \n"
            + " AND crm.class_id = ?2 \n "
            + " AND cr.class_room_id = ?3 \n "
            + " AND crs.class_room_subject_id = ?4 \n"
            + "AND crm.role='student' \n"
            + " ORDER BY cast(crm.class_room_no AS INT) ", nativeQuery = true)
    Page<Map> sub1(Integer yearGroupId, Integer classId, Integer classRoomId,Integer classRoomSubjectId , Pageable pageable);
    
}
