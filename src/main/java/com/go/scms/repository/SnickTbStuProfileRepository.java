package com.go.scms.repository;

import com.go.scms.entity.SnickTbStuProfileEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTbStuProfileRepository extends JpaRepository<SnickTbStuProfileEntity,Long>{    
        Optional<SnickTbStuProfileEntity> findByStuProfileId(Long stuProdfileId);    
        
        @Query(value = "SELECT CASE WHEN a.max_ref_no IS NULL THEN CAST('0' AS INT) ELSE a.max_ref_no END \n" +
                    " FROM\n" +
                    "( \n"+
                    " SELECT MAX(request_id) AS max_ref_no \n" +
                    " FROM tb_stu_profile sp \n" +
                    " WHERE substr(CAST(sp.request_id AS TEXT),1,4) = (SELECT SUBSTR(yg.name,3,4) from tb_mst_year_group yg WHERE yg.iscurrent = TRUE) \n" +
                    ") AS a", nativeQuery = true)
        List<Map> findmaxrefno();
}
