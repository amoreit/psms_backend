/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TitleEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TitleRepository extends JpaRepository<TitleEntity, Long> {

    List<TitleEntity> findAllByRecordStatusNotInAndLangCodeContainingAndTitleIdNotIn(String recordStatus, String langCode, Long titleId);

    @Query(value = "SELECT title_id,name FROM tb_mst_title WHERE record_status <> 'D' AND gender =?1 AND lang_code <>'en' AND title_id<>?2\n"
            + "ORDER BY title_id", nativeQuery = true)
    List<Map> findddlfiltergender(Integer genderId, Long titleId);

    @Query(value = "SELECT title_id,name FROM tb_mst_title WHERE record_status <> 'D' AND lang_code <>'en' AND title_id<>1 and title_id<>2\n"
            + "ORDER BY title_id", nativeQuery = true)
    List<Map> findddlparent();

    @Query(value = "SELECT t.title_id,t.name,t.desc_th,g.gender_desc_th,t.updated_date,t.updated_user FROM tb_mst_title t\n"
            + "LEFT JOIN tb_mst_gender g ON g.gender_id = t.gender\n"
            + " WHERE t.record_status <> 'D' and t.name LIKE %?1% AND t.desc_th LIKE %?2% ORDER BY t.title_id", countQuery = "SELECT count(*) FROM tb_mst_title t\n"
            + "LEFT JOIN tb_mst_gender g ON g.gender_id = t.gender\n"
            + " WHERE t.record_status <> 'D' and t.name LIKE %?1% AND t.desc_th LIKE %?2% ", nativeQuery = true)
    Page<Map> findalltitle(String name,String desc,Pageable pageable);
    
     @Query(value = "SELECT t.title_id,t.name,t.desc_th,g.gender_desc_th,t.updated_date,t.updated_user FROM tb_mst_title t\n"
            + "LEFT JOIN tb_mst_gender g ON g.gender_id = t.gender\n"
            + " WHERE t.record_status <> 'D' and t.name LIKE %?1% AND t.desc_th LIKE %?2% and t.gender =?3 ORDER BY t.title_id", countQuery = "SELECT count(*) FROM tb_mst_title t\n"
            + "LEFT JOIN tb_mst_gender g ON g.gender_id = t.gender\n"
            + " WHERE t.record_status <> 'D' and t.name LIKE %?1% AND t.desc_th LIKE %?2% and t.gender =?3", nativeQuery = true)
    Page<Map> findalltitlebygender(String name,String desc,Integer gender,Pageable pageable);
}
