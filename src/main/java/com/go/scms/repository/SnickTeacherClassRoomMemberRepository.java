package com.go.scms.repository;

import com.go.scms.entity.SnickTeacherClassRoomMemberEntity;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTeacherClassRoomMemberRepository extends JpaRepository<SnickTeacherClassRoomMemberEntity, Long>{ 
    
        List<SnickTeacherClassRoomMemberEntity>findAllByRecordStatusNotInAndYearGroupIdAndMemberIdAndRole(String recordStatus, Integer yearGroupId, Integer memberId, String role);
                
        @Query(value = "select crm.class_room_member_id, \n" +
                 "crm.year_group_id, \n" +
                 "crm.year_group_name, \n" +
                 "crm.class_id, \n" +
                 "crm.class_name, \n" + 
                 "crm.class_room_id, \n" +
                 "crm.class_room, \n" +
                 "crm.stu_code,\n" +
                 "crm.fullname, \n" +
                 "crm.fullname_en \n" +
                 "from tb_trn_class_room_member crm \n" +
                 "where crm.record_status <> ?1 and crm.role = ?2 \n",
                countQuery ="select count(*) from tb_trn_class_room_member crm where crm.record_status <> ?1 and crm.role = ?2", nativeQuery = true)
        Page<Map> findrecordstatus(String recordStatus, String role, Pageable pageable);
        
        @Query(value = "select crm.class_room_member_id, \n" +
                 "crm.year_group_id, \n" +
                 "crm.year_group_name, \n" +
                 "crm.class_id, \n" +
                 "crm.class_name, \n" +
                 "crm.class_room_id, \n" +
                 "crm.class_room, \n" +
                 "crm.stu_code,\n" +
                 "crm.fullname, \n" +
                 "crm.fullname_en \n" +
                 "from tb_trn_class_room_member crm \n" +
                 "where crm.year_group_name like %?1% and crm.record_status <> ?2 and crm.role = ?3 \n",
                countQuery ="select count(*) from tb_trn_class_room_member crm where crm.year_group_name like %?1% and crm.record_status <> ?2 and crm.role = ?3", nativeQuery = true)
        Page<Map> findyeargroupname(String yearGroupName, String recordStatus, String role, Pageable pageable);
        
        @Query(value = "select crm.class_room_member_id, \n" +
                 "crm.year_group_id, \n" +
                 "crm.year_group_name, \n" +
                 "crm.class_id, \n" +
                 "crm.class_name, \n" +
                 "crm.class_room_id, \n" +
                 "crm.class_room, \n" +
                 "crm.stu_code,\n" +
                 "crm.fullname, \n" +
                 "crm.fullname_en \n" +
                 "from tb_trn_class_room_member crm \n" +
                 "where crm.year_group_name like %?1% and crm.class_name like %?2% and crm.record_status <> ?3 and crm.role = ?4 \n", 
                countQuery ="select count(*) from tb_trn_class_room_member crm where crm.year_group_name like %?1% and crm.class_name like %?2% and crm.record_status <> ?3 and crm.role = ?4", nativeQuery = true)
        Page<Map> findyeargroupnameclassname(String yearGroupName, String className, String recordStatus, String role, Pageable pageable);
        
        @Query(value = "select crm.class_room_member_id, \n" +
                 "crm.year_group_id, \n" +
                 "crm.year_group_name, \n" +
                 "crm.class_id, \n" +
                 "crm.class_name, \n" +
                 "crm.class_room_id, \n" +
                 "crm.class_room, \n" +
                 "crm.stu_code,\n" +
                 "crm.fullname, \n" +
                 "crm.fullname_en \n" +
                 "from tb_trn_class_room_member crm \n" +
                 "where crm.class_name like %?1% and crm.record_status <> ?2 and crm.role = ?3 \n",
                countQuery ="select count(*) from tb_trn_class_room_member crm where crm.class_name like %?1% and crm.record_status <> ?2 and crm.role = ?3", nativeQuery = true)
        Page<Map> findclassname(String className, String recordStatus, String role, Pageable pageable);
        
        @Query(value = "select crm.class_room_member_id, \n" +
                 "crm.year_group_id, \n" +
                 "crm.year_group_name, \n" +
                 "crm.class_id, \n" +
                 "crm.class_name, \n" +
                 "crm.class_room_id, \n" +
                 "crm.class_room, \n" +
                 "crm.stu_code,\n" +
                 "crm.fullname, \n" +
                 "crm.fullname_en \n" +
                 "from tb_trn_class_room_member crm \n" +
                 "where crm.class_name like %?1% and crm.class_room like %?2% and crm.record_status <> ?3 and crm.role = ?4 \n", 
                countQuery ="select count(*) from tb_trn_class_room_member crm where crm.class_name like %?1% and crm.class_room like %?2% and crm.record_status <> ?3 and crm.role = ?4", nativeQuery = true)
        Page<Map> findclassnameclassroom(String className, String classRoom, String recordStatus, String role, Pageable pageable);
        
        @Query(value = "select crm.class_room_member_id, \n" +
                 "crm.year_group_id, \n" +
                 "crm.year_group_name, \n" +
                 "crm.class_id, \n" +
                 "crm.class_name, \n" +
                 "crm.class_room_id, \n" +
                 "crm.class_room, \n" +
                 "crm.stu_code,\n" +
                 "crm.fullname, \n" +
                 "crm.fullname_en \n" +
                 "from tb_trn_class_room_member crm \n" +
                 "where crm.year_group_name like %?1% and crm.class_name like %?2% and crm.class_room like %?3% and crm.record_status <> ?4 and crm.role = ?5 \n", 
                countQuery ="select count(*) from tb_trn_class_room_member crm where crm.year_group_name like %?1% and crm.class_name like %?2% and crm.class_room like %?3% and crm.record_status <> ?4 and crm.role = ?5", nativeQuery = true)
        Page<Map> findall(String yearGroupName, String className, String classRoom, String recordStatus, String role, Pageable pageable);
        
        @Transactional
        @Modifying
        @Query(value = "DELETE FROM tb_trn_class_room_member where class_room_member_id = ?1", nativeQuery = true)
        void deleteTeacherClassRoomMember(Long classRoomMemberId);
}
