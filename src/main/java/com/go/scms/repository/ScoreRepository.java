/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ScoreEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ScoreRepository extends JpaRepository<ScoreEntity,Long>{
     List<ScoreEntity> findAllByRecordStatusNotInAndIscatclass1OrderByScoreId(String recordStatus, Boolean iscatclass1);
     Page<ScoreEntity> findAllByRecordStatusNotInAndScoreNameContainingOrderByScoreId(String recordStatus, String scoreName,Pageable pageable);
}
