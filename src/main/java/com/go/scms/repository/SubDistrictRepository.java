/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.SubDistrictEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SubDistrictRepository extends JpaRepository<SubDistrictEntity, Integer>{
    List<SubDistrictEntity> findAllByRecordStatusNotInAndJoinSubDistrictDistrictThOrderBySubDistrictTh(String recordStatus, String districtTh);
    List<SubDistrictEntity> findAllByRecordStatusNotInAndJoinSubDistrictDistrictThAndSubDistrictTh(String recordStatus, String districtTh, String subDistrictTh);
}
