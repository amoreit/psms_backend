/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.PaiDivitionEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author bamboo
 */
@Repository
public interface PaiDivitionNameRepository extends JpaRepository<PaiDivitionEntity,Long> {  
        List<PaiDivitionEntity> findAllByRecordStatusNotIn(String recordStatus);
}
