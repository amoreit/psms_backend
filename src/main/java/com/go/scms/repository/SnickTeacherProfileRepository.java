package com.go.scms.repository;

import com.go.scms.entity.SnickTeacherProfileEntity;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SnickTeacherProfileRepository extends JpaRepository<SnickTeacherProfileEntity,Long>{
        
    Page<SnickTeacherProfileEntity> findAllByFirstnameThContainingAndLastnameThContainingAndRecordStatusNotIn(String firstnameTh, String lastnameTh, String recordStatus, Pageable pageable);
    Page<SnickTeacherProfileEntity> findAllByRecordStatusNotIn(String recordStatus, Pageable pageable);
    Page<SnickTeacherProfileEntity> findAllByFirstnameThContainingAndRecordStatusNotIn(String firstnameTh, String recordStatus, Pageable pageable);
    Page<SnickTeacherProfileEntity> findAllByLastnameThContainingAndRecordStatusNotIn(String lastnameTh, String recordStatus, Pageable pageable);
    Page<SnickTeacherProfileEntity> findAllByClassIdAndRecordStatusNotIn(Integer classId, String recordStatus, Pageable pageable);
    Page<SnickTeacherProfileEntity> findAllByClassIdAndClassRoomIdAndRecordStatusNotIn(Integer classId, Integer classRoomId, String recordStatus, Pageable pageable);
    Page<SnickTeacherProfileEntity> findAllByFirstnameThContainingAndLastnameThContainingAndClassIdAndClassRoomIdAndRecordStatusNotIn(String firstnameTh, String lastnameTh, Integer classId, Integer classRoomId, String recordStatus, Pageable pageable);

    Page<SnickTeacherProfileEntity> findAllByFirstnameThContainingAndLastnameThContainingAndFirstnameEnContainingAndLastnameEnContainingAndRecordStatusNotIn(String firstnameTh ,String lastnameTh ,String firstnameEn,String lastnameEn, String recordStatus, Pageable pageable);
    
    List<SnickTeacherProfileEntity> findAllByRecordStatusNotIn(String recordStatus);
    List<SnickTeacherProfileEntity> findAllByRecordStatusNotInAndTeacherId(String recordStatus, Long teacherId);
    List<SnickTeacherProfileEntity> findAllByRecordStatusNotInAndTeacherCardId(String recordStatus, String teacherCardId);
    Optional<SnickTeacherProfileEntity> findByTeacherId(Long teacherId);
    
    @Transactional
    @Modifying
    @Query("UPDATE SnickTeacherProfileEntity tea SET tea.departmentId = ?1, tea.departmentName = ?2, tea.updatedUser = ?3, tea.recordStatus = ?4 where tea.teacherId = ?5")
    void updateDepartment(Integer departmentId, String departmentName, String userName, String recordStatus, Long teacherId);
    
    @Transactional
    @Modifying
    @Query("UPDATE SnickTeacherProfileEntity tea SET tea.classId = ?1, tea.updatedUser = ?2, tea.recordStatus = ?3 where tea.teacherId = ?4")
    void updateClass(Integer classId, String userNameClass, String recordStatus, Long teacherId);
    
    @Transactional
    @Modifying
    @Query("UPDATE SnickTeacherProfileEntity tea SET tea.classId = ?1, tea.classRoomId = ?2, tea.updatedUser = ?3, tea.recordStatus = ?4 where tea.teacherId = ?5")
    void updateClassRoom(Integer classId, Integer classRoomId, String userNameClass, String recordStatus, Long teacherId);
       
    //------------------- UPDATE data ตาม value โดยกระทำเป็นการ delete
    @Transactional
    @Modifying
    @Query("UPDATE SnickTeacherProfileEntity tea SET tea.updatedUser = ?1, tea.recordStatus = ?2, tea.isactive = ?3 , tea.departmentId = null, tea.departmentName = null where tea.fullname = ?4")
    void updateDeleteDepartment(String updatedUser, String recordStatus, Boolean isactive, String fullname); 
    
    @Transactional
    @Modifying
    @Query("UPDATE SnickTeacherProfileEntity tea SET tea.updatedUser = ?1, tea.recordStatus = ?2, tea.isactive = ?3 , tea.classId = null where tea.fullname = ?4")
    void updateDeleteClass(String updatedUser, String recordStatus, Boolean isactive, String fullname); 
    
    @Transactional
    @Modifying
    @Query("UPDATE SnickTeacherProfileEntity tea SET tea.updatedUser = ?1, tea.recordStatus = ?2, tea.isactive = ?3 , tea.classRoomId = null where tea.teacherCardId = ?4")
    void updateDeleteClassRoom(String updatedUser, String recordStatus, Boolean isactive, String teacherCardId);
}
