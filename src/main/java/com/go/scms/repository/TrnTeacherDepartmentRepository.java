/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.TrnTeacherDepartmentEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TrnTeacherDepartmentRepository extends JpaRepository<TrnTeacherDepartmentEntity, Long> {

    @Query(value = "SELECT sc_code, year_group_name AS show_year_group, department_id, department_name, cat_class_id, teacher_id, fullname, fullname_en,\n"
            + "leader, remark FROM tb_trn_teacher_department\n"
            + "WHERE cat_class_id = ?1 AND year_group_id = ?2 ORDER BY department_id", nativeQuery = true)
    List<Map> findteachertec008(Integer catClassId, Integer yearGroupId);
}
