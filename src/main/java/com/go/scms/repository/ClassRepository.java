/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.ClassEntity;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author P3rdiscoz
 */
@Repository
public interface ClassRepository extends JpaRepository<ClassEntity, Integer> {

    List<ClassEntity> findAllByRecordStatusNotInOrderByCatClassJoinCatClassIdAscClassId(String recordStatus);
    Optional<ClassEntity> findByClassId(Integer classId);

    @Query(value = "SELECT class_id,class_sname_th FROM tb_mst_class WHERE cat_class_id <> ?1\n"
            + "ORDER BY class_id", nativeQuery = true)
    List<Map> findclassbycatclass(Long catClassId);

    //nock for rep001
    List<ClassEntity> findAllByRecordStatusNotInAndCatClassJoinCatClassIdOrderByClassId(String recordStatus, Long catClassId);
    
    //nock for cer002 and rep006
    List<ClassEntity> findAllByRecordStatusNotInAndCatClassJoinCatClassIdNotInAndLangCodeNotInOrderByClassId(String recordStatus, Long catClassId, String langCode);
    List<ClassEntity> findAllByRecordStatusNotInAndLangCodeNotInOrderByClassId(String recordStatus, String langCode);
    
    //moss
    @Query(value = "SELECT * FROM tb_mst_class \n"
           + "WHERE record_status <> 'D' AND cat_class_id = ?1 ORDER BY class_id", nativeQuery = true)
    List<Map> findddlfilterbycatclass(Long catClassId);
    
    //moss
    @Query(value = "SELECT c.class_id,c.class_sname_th FROM tb_trn_class_room_subject crs \n"
            + "LEFT JOIN tb_tea_teacher_profile tp ON tp.teacher_id = crs.teacher1\n"
            + "LEFT JOIN tb_mst_class_room cr ON cr.class_room_id = crs.class_room_id\n"
            + "LEFT JOIN tb_mst_class c ON c.class_id = cr.class_id\n"
            + "WHERE tp.citizen_id = ?1 GROUP BY c.class_id,c.class_sname_th\n"
            + "ORDER BY c.class_id", nativeQuery = true)
    List<Map> findddlfilterbycitizenId(String citizenId);

    @Query(value = "SELECT * FROM fn_dropdown_class(1)", nativeQuery = true)
    List<Map> findddlfunction();
    
    @Query(value = "SELECT * FROM fn_dropdown_class(?1)", nativeQuery = true)
    List<Map> findddlfunctionbykey(Integer keyClass);
}
