package com.go.scms.repository;

import com.go.scms.entity.TrnTeacherClassEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TrnTeacherClassRepository extends JpaRepository<TrnTeacherClassEntity, Long> {

    @Query(value = "SELECT sc_code, year_group_name AS show_year_group,class_id,class_name,\n"
            + "cat_class_id,teacher_id,fullname,fullname_en,leader FROM tb_trn_teacher_class\n"
            + "WHERE year_group_id = ?1 AND cat_class_id = ?2 AND class_id = ?3\n"
            + "ORDER BY fullname", nativeQuery = true)
    List<Map> findteachertec009(Integer yearGroupId,Integer catClassId,Integer classId);
}
