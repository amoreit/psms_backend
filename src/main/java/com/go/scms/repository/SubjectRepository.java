/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.SubjectEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, Long> {

    @Query(value = "SELECT s.subject_id,s.subject_code,s.name_th,d.name_short as department_name,st.name as subject_type,CASE WHEN s.credit IS NULL AND s.lesson IS NOT NULL THEN '- / '||s.lesson\n"
            + "WHEN s.credit IS NULL AND s.lesson IS NULL THEN '- / -'\n"
            + "ELSE  s.credit||' / '||s.lesson \n"
            + "END\n"
            + "AS credit_lesson,sc.scale_name,s.updated_user,s.updated_date FROM tb_mst_subject s \n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "LEFT JOIN tb_mst_subject_type st ON st.subject_type_id = s.subject_type\n"
            + "LEFT JOIN tb_mst_scale sc ON sc.scale_id = s.scale\n"
            + "WHERE s.record_status <>'D' and d.cat_class <> 1 and s.subject_code LIKE %?1% AND s.name_th LIKE %?2%\n"
            + "ORDER BY s.department_id,s.subject_code", countQuery = "SELECT count(*) FROM tb_mst_subject s \n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "LEFT JOIN tb_mst_subject_type st ON st.subject_type_id = s.subject_type\n"
            + "LEFT JOIN tb_mst_scale sc ON sc.scale_id = s.scale\n"
            + "WHERE s.record_status <>'D' and d.cat_class <> 1 and s.subject_code LIKE %?1% AND s.name_th LIKE %?2%\n", nativeQuery = true)
    Page<Map> findsubjectall(String subjectCode, String nameTh, Pageable pageable);

    @Query(value = "SELECT s.subject_id,s.subject_code,s.name_th,d.name_short as department_name,st.name as subject_type,CASE WHEN s.credit IS NULL AND s.lesson IS NOT NULL THEN '- / '||s.lesson\n"
            + "WHEN s.credit IS NULL AND s.lesson IS NULL THEN '- / -'\n"
            + "ELSE  s.credit||' / '||s.lesson \n"
            + "END\n"
            + "AS credit_lesson,sc.scale_name,s.updated_user,s.updated_date FROM tb_mst_subject s \n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "LEFT JOIN tb_mst_subject_type st ON st.subject_type_id = s.subject_type\n"
            + "LEFT JOIN tb_mst_scale sc ON sc.scale_id = s.scale\n"
            + "WHERE s.record_status <>'D' AND d.cat_class = ?1 AND s.subject_code LIKE %?2% AND s.name_th LIKE %?3%\n"
            + "ORDER BY s.department_id,s.subject_code", countQuery = "SELECT count(*) FROM tb_mst_subject s \n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "LEFT JOIN tb_mst_subject_type st ON st.subject_type_id = s.subject_type\n"
            + "LEFT JOIN tb_mst_scale sc ON sc.scale_id = s.scale\n"
            + "WHERE s.record_status <>'D' AND d.cat_class = ?1 AND s.subject_code LIKE %?2% AND s.name_th LIKE %?3%\n", nativeQuery = true)
    Page<Map> findsubjectallandcatclassid(Integer catClassId, String subjectCode, String nameTh, Pageable pageable);

    @Query(value = "SELECT s.subject_id,s.subject_code,s.name_th,d.name_short as department_name,st.name as subject_type,CASE WHEN s.credit IS NULL AND s.lesson IS NOT NULL THEN '- / '||s.lesson\n"
            + "WHEN s.credit IS NULL AND s.lesson IS NULL THEN '- / -'\n"
            + "ELSE  s.credit||' / '||s.lesson \n"
            + "END\n"
            + "AS credit_lesson,sc.scale_name,s.updated_user,s.updated_date FROM tb_mst_subject s \n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "LEFT JOIN tb_mst_subject_type st ON st.subject_type_id = s.subject_type\n"
            + "LEFT JOIN tb_mst_scale sc ON sc.scale_id = s.scale\n"
            + "WHERE s.record_status <>'D' AND d.cat_class = ?1 AND s.department_id = ?2 AND s.subject_code LIKE %?3% AND s.name_th LIKE %?4%\n"
            + "ORDER BY s.department_id,s.subject_code", countQuery = "SELECT count(*) FROM tb_mst_subject s \n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "LEFT JOIN tb_mst_subject_type st ON st.subject_type_id = s.subject_type\n"
            + "LEFT JOIN tb_mst_scale sc ON sc.scale_id = s.scale\n"
            + "WHERE s.record_status <>'D' AND d.cat_class = ?1 AND s.department_id = ?2 AND s.subject_code LIKE %?3% AND s.name_th LIKE %?4%", nativeQuery = true)
    Page<Map> findsubjectallandcatclassidanddepartmentid(Integer catClassId, Integer departmentId, String subjectCode, String nameTh, Pageable pageable);

    @Query(value = "SELECT s.*,d.cat_class FROM tb_mst_subject s\n"
            + "LEFT JOIN tb_mst_department d ON d.department_id = s.department_id\n"
            + "WHERE s.record_status <> 'D' AND s.subject_id = ?1", nativeQuery = true)
    List<Map> findsubjectbyid(Integer subjectId);
}
