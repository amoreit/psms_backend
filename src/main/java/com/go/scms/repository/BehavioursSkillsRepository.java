/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.repository;

import com.go.scms.entity.BehavioursSkillsEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BehavioursSkillsRepository extends JpaRepository<BehavioursSkillsEntity, Long> {

    @Query(value = "select ls.class_room_lifeskill_score_id,crm.sc_code,crm.year_group_id,crm.year_group_name,crm.class_id,crm.class_room_member_id,crm.class_room,crm.stu_code,crm.fullname,bsd.item,bsd.item_desc,bs.behaviours_skills_id,bs.description AS behaviours_skills_desc,COALESCE(ls.score,0) AS score FROM tb_trn_class_room_member crm\n"
            + "LEFT JOIN tb_mst_behaviours_skills bs ON crm.class_id = ANY(bs.class_id)\n"
            + "LEFT JOIN tb_mst_behaviours_skills_detail bsd ON bs.behaviours_skills_id=bsd.behaviours_skills_id\n"
            + "LEFT JOIN tb_trn_class_room_lifeskill_score ls ON ls.class_room_member_id = crm.class_room_member_id AND ls.item=bsd.item\n"
            + "WHERE bs.type = 1 AND crm.class_id = ?1 AND crm.class_room_member_id = ?2\n"
            + "ORDER BY bsd.item", nativeQuery = true)
    List<Map<String, String>> findbehavioursskills(Integer classId, Integer classRoomMemberId);

}
