/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.entity.CatClassEntity;
import com.go.scms.repository.DepartmentRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/department")
public class DepartmentDdl {

    @Autowired
    private DepartmentRepository departmentRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddlByCondition")
    public ResponseEntity<?> ddlByCondition(HttpServletRequest request) {
        try {
            String nameCatClass = ValiableUtils.stringIsEmpty(request.getParameter("nameCatClass"));
            List list = departmentRepo.findAllByRecordStatusNotInAndCatclassLnameThContainingOrderByDepartmentId(RecordStatus.DELETE.getCode(), nameCatClass);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DepartmentOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = departmentRepo.findAllByRecordStatusNotInOrderByDepartmentIdAsc(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DepartmentOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlfiltercatclass")
    public ResponseEntity<?> ddlfiltercatclass(HttpServletRequest request) {
        try {
            Integer catClassId = (int) ValiableUtils.IntIsZero(request.getParameter("catClassId"));
            List list = departmentRepo.findddlfiltercatclass(catClassId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DeapartmentFilterCatClass[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
}

class DepartmentOutput {

    private Integer departmentId;
    private String name;
    private CatClassEntity catclass = new CatClassEntity();

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CatClassEntity getCatclass() {
        return catclass;
    }

    public void setCatclass(CatClassEntity catclass) {
        this.catclass = catclass;
    }
}

class DeapartmentFilterCatClass{
    
    private String department_id;
    private String name;

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
