/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbTeaTimeAttendanceRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/0/teacher-attendance")
public class TbTeaTimeAttendanceDdl {
    
    @Autowired
    private TbTeaTimeAttendanceRepository tbTeaTimeAttendanceRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    String cardId = ValiableUtils.stringIsEmpty(request.getParameter("cardId"));
                    List list = tbTeaTimeAttendanceRepository.findAllByCardId(cardId);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TeacherAttendanceOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}

class TeacherAttendanceOutput{
    
    private Date checkTime;
    private String teaTimeAttendanceId;
    private String doorName;

    public String getDoorName() {
        return doorName;
    }

    public void setDoorName(String doorName) {
        this.doorName = doorName;
    }
        
    public String getTeaTimeAttendanceId() {
        return teaTimeAttendanceId;
    }

    public void setTeaTimeAttendanceId(String teaTimeAttendanceId) {
        this.teaTimeAttendanceId = teaTimeAttendanceId;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

}

