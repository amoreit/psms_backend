/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.entity.CatClassEntity;
import com.go.scms.repository.ClassRepository;
import com.go.scms.repository.ScaleRepository;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/class")
public class ClassDdl {

    @Autowired
    private ClassRepository classRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = classRepo.findAllByRecordStatusNotInOrderByCatClassJoinCatClassIdAscClassId(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlByCatclass")
    public ResponseEntity<?> ddlByCatclass(HttpServletRequest request) {
        try {

            Long catClassId = ValiableUtils.LongIsZero(request.getParameter("catClassId"));
            List list = classRepo.findclassbycatclass(catClassId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlfunction")
    public ResponseEntity<?> ddlfunction(HttpServletRequest request) {
        try {
            List list = classRepo.findddlfunction();
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassFunctionDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlfunctionbykey")
    public ResponseEntity<?> ddlfunctionbykey(HttpServletRequest request) {
        try {
            Integer keyClass = (int) ValiableUtils.IntIsZero(request.getParameter("keyClass"));
            List list = classRepo.findddlfunctionbykey(keyClass);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassFunctionDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    //moss
    @GetMapping("/ddlbycitizenid")
    public ResponseEntity<?> ddlbycitizenid(HttpServletRequest request) {
        try {
            String citizenId = ValiableUtils.stringIsEmpty(request.getParameter("citizenId"));
            List list = classRepo.findddlfilterbycitizenId(citizenId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    // ddlCatclass by nock
    // for rep001 only อ1 อ2 อ3
    @GetMapping("/ddlCatclassrep001")
    public ResponseEntity<?> ddlCatclassrep001(HttpServletRequest request) {
        try {
            long catClassId = 1;
            List list = classRepo.findAllByRecordStatusNotInAndCatClassJoinCatClassIdOrderByClassId(RecordStatus.DELETE.getCode(), catClassId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    // ddlCatclass by nock
    // for cer002 and rep006 ประถม ขึ้นไปหมด
    @GetMapping("/ddlCatclass")
    public ResponseEntity<?> ddlCatclass(HttpServletRequest request) {
        try {
            long catClassId = 1;
            String langCode = "en";
            List list = classRepo.findAllByRecordStatusNotInAndCatClassJoinCatClassIdNotInAndLangCodeNotInOrderByClassId(RecordStatus.DELETE.getCode(), catClassId, langCode);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/classddl")
    public ResponseEntity<?> classAllNotEn(HttpServletRequest request) {
        try {
            String langCode = "en";
            List list = classRepo.findAllByRecordStatusNotInAndLangCodeNotInOrderByClassId(RecordStatus.DELETE.getCode(), langCode);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //moss
    @GetMapping("/ddlfilterbycatclass")
    public ResponseEntity<?> ddlfilterbycatclass(HttpServletRequest request) {
        try {
            Long catClassId = ValiableUtils.LongIsZero(request.getParameter("catClassId"));
            List list = classRepo.findddlfilterbycatclass(catClassId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class ClassOutput {

    private String classId;
    private CatClassEntity catClassJoin = new CatClassEntity();
    private String classSnameTh;
    private String classLnameTh;
    private String langCode;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public CatClassEntity getCatClassJoin() {
        return catClassJoin;
    }

    public void setCatClassJoin(CatClassEntity catClassJoin) {
        this.catClassJoin = catClassJoin;
    }        

    public String getClassSnameTh() {
        return classSnameTh;
    }

    public void setClassSnameTh(String classSnameTh) {
        this.classSnameTh = classSnameTh;
    }

    public String getClassLnameTh() {
        return classLnameTh;
    }

    public void setClassLnameTh(String classLnameTh) {
        this.classLnameTh = classLnameTh;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}

class ClassDdlOutput {

    private String class_id;
    private String class_sname_th;

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_sname_th() {
        return class_sname_th;
    }

    public void setClass_sname_th(String class_sname_th) {
        this.class_sname_th = class_sname_th;
    }
}

class ClassFunctionDdlOutput {

    private String class_id;
    private String class_sname_th;
    private String key_class;

    public String getKey_class() {
        return key_class;
    }

    public void setKey_class(String key_class) {
        this.key_class = key_class;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_sname_th() {
        return class_sname_th;
    }

    public void setClass_sname_th(String class_sname_th) {
        this.class_sname_th = class_sname_th;
    }
}
