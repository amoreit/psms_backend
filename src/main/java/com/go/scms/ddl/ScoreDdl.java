package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.ScoreRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/0/score")
public class ScoreDdl {
    @Autowired
    private ScoreRepository scoreRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    Boolean iscatclass1 = Boolean.parseBoolean(request.getParameter("iscatclass1")); 
                    List list = scoreRepo.findAllByRecordStatusNotInAndIscatclass1OrderByScoreId(RecordStatus.DELETE.getCode(),iscatclass1);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,ScoreOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}
class ScoreOutput{
    private String scoreId;
    private String scoreName;

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getScoreName() {
        return scoreName;
    }

    public void setScoreName(String scoreName) {
        this.scoreName = scoreName;
    }
    
}
