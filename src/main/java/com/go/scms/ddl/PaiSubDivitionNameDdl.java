/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.PaiSubDivitionNameRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/0/ddlsubDivitionName")
public class PaiSubDivitionNameDdl {
    
    @Autowired
    private PaiSubDivitionNameRepository paiSubDivitionNameRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            String DivisionName = ValiableUtils.stringIsEmpty(request.getParameter("divisionNameTh"));
            List list = paiSubDivitionNameRepository.findAllByRecordStatusNotInAndDivisionJoinDivisionNameTh(RecordStatus.DELETE.getCode(),DivisionName);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDivitionNameOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class SubDivitionNameOutput {
    private String subDivisionId;
    private String subDivisionNameTh;
    private String subDivisionNameEn;

    public String getSubDivisionId() {
        return subDivisionId;
    }

    public void setSubDivisionId(String subDivisionId) {
        this.subDivisionId = subDivisionId;
    }

    public String getSubDivisionNameTh() {
        return subDivisionNameTh;
    }

    public void setSubDivisionNameTh(String subDivisionNameTh) {
        this.subDivisionNameTh = subDivisionNameTh;
    }

    public String getSubDivisionNameEn() {
        return subDivisionNameEn;
    }

    public void setSubDivisionNameEn(String subDivisionNameEn) {
        this.subDivisionNameEn = subDivisionNameEn;
    }
    
    
}
