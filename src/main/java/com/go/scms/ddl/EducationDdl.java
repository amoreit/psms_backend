/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.EducationRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/education")
public class EducationDdl {
    @Autowired
    private EducationRepository educationRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
        try {
                String langCode = ValiableUtils.stringIsEmpty(request.getParameter("langCode"));
                List list = educationRepo.findAllByRecordStatusNotInAndLangCodeContaining(RecordStatus.DELETE.getCode(),langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,EducationOutput[].class));
        }catch(Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
}

class EducationOutput{
    private String educationId;
    private String name;

    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}

