/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.PaiChildTitleRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/0/childTitle")
public class PaiChildTitleDdl {
    
    @Autowired
    private PaiChildTitleRepository paiChildTitleRepository;
    
    @Autowired
    private ModelMapper modelMapper;
     
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        String langCode = "th";
        String descTh = "นาง";
        try {
            List list = paiChildTitleRepository.findAllByRecordStatusNotInAndDescThNotInAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), descTh, langCode);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleChildOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class TitleChildOutput {

    private String titleId;
    private String name;
    private String descTh;
    private String abbrev;

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescTh() {
        return descTh;
    }

    public void setDescTh(String descTh) {
        this.descTh = descTh;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

     
        

}
