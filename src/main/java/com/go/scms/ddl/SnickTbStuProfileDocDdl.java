package com.go.scms.ddl;

import com.go.scms.repository.SnickTbStuProfileDocRepository;
import com.go.scms.utils.ValiableUtils;
import javax.servlet.http.HttpServletRequest;
import com.go.scms.contant.RecordStatus;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/stu-profile-doc")
public class SnickTbStuProfileDocDdl {
    
        @Autowired
	private SnickTbStuProfileDocRepository stuProfileDocRepo;
	
	@Autowired
	private ModelMapper modelMapper;
        
        @GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
                        Long stuProfileId = ValiableUtils.LongIsZero(request.getParameter("stuProfileId"));
			List list = stuProfileDocRepo.findAllByRecordStatusNotInAndStuProfileStuProfileId(RecordStatus.DELETE.getCode(),stuProfileId);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbStuProfileDocOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}

class TbStuProfileDocOutput{
    private String stuProfileDocId;
    private String stuProfileId;
    private String stuCode;
    private String dataPathImage;
    private String dataPathReligion;
    private String dataPathBirth;
    private String dataPathRegister;
    private String dataPathChangeName;
    private String dataPathTranscript7;
    private String dataPathTranscript1;
    private String dataPathRecord8;
    private String dataPathTransfer;
    private String dataPathFatherRegister;
    private String dataPathFatherCitizenId;
    private String dataPathMotherRegister;
    private String dataPathMotherCitizenId;
    private String dataPathParentRegister;
    private String dataPathParentCitizenId;

        public String getStuProfileDocId() {
            return stuProfileDocId;
        }

        public void setStuProfileDocId(String stuProfileDocId) {
            this.stuProfileDocId = stuProfileDocId;
        }

        public String getStuProfileId() {
            return stuProfileId;
        }

        public void setStuProfileId(String stuProfileId) {
            this.stuProfileId = stuProfileId;
        }

        public String getStuCode() {
            return stuCode;
        }

        public void setStuCode(String stuCode) {
            this.stuCode = stuCode;
        }

        public String getDataPathImage() {
            return dataPathImage;
        }

        public void setDataPathImage(String dataPathImage) {
            this.dataPathImage = dataPathImage;
        }

        public String getDataPathReligion() {
            return dataPathReligion;
        }

        public void setDataPathReligion(String dataPathReligion) {
            this.dataPathReligion = dataPathReligion;
        }

        public String getDataPathBirth() {
            return dataPathBirth;
        }

        public void setDataPathBirth(String dataPathBirth) {
            this.dataPathBirth = dataPathBirth;
        }

        public String getDataPathRegister() {
            return dataPathRegister;
        }

        public void setDataPathRegister(String dataPathRegister) {
            this.dataPathRegister = dataPathRegister;
        }

        public String getDataPathChangeName() {
            return dataPathChangeName;
        }

        public void setDataPathChangeName(String dataPathChangeName) {
            this.dataPathChangeName = dataPathChangeName;
        }

        public String getDataPathTranscript7() {
            return dataPathTranscript7;
        }

        public void setDataPathTranscript7(String dataPathTranscript7) {
            this.dataPathTranscript7 = dataPathTranscript7;
        }

        public String getDataPathTranscript1() {
            return dataPathTranscript1;
        }

        public void setDataPathTranscript1(String dataPathTranscript1) {
            this.dataPathTranscript1 = dataPathTranscript1;
        }

        public String getDataPathRecord8() {
            return dataPathRecord8;
        }

        public void setDataPathRecord8(String dataPathRecord8) {
            this.dataPathRecord8 = dataPathRecord8;
        }

        public String getDataPathTransfer() {
            return dataPathTransfer;
        }

        public void setDataPathTransfer(String dataPathTransfer) {
            this.dataPathTransfer = dataPathTransfer;
        }

        public String getDataPathFatherRegister() {
            return dataPathFatherRegister;
        }

        public void setDataPathFatherRegister(String dataPathFatherRegister) {
            this.dataPathFatherRegister = dataPathFatherRegister;
        }

        public String getDataPathFatherCitizenId() {
            return dataPathFatherCitizenId;
        }

        public void setDataPathFatherCitizenId(String dataPathFatherCitizenId) {
            this.dataPathFatherCitizenId = dataPathFatherCitizenId;
        }

        public String getDataPathMotherRegister() {
            return dataPathMotherRegister;
        }

        public void setDataPathMotherRegister(String dataPathMotherRegister) {
            this.dataPathMotherRegister = dataPathMotherRegister;
        }

        public String getDataPathMotherCitizenId() {
            return dataPathMotherCitizenId;
        }

        public void setDataPathMotherCitizenId(String dataPathMotherCitizenId) {
            this.dataPathMotherCitizenId = dataPathMotherCitizenId;
        }

        public String getDataPathParentRegister() {
            return dataPathParentRegister;
        }

        public void setDataPathParentRegister(String dataPathParentRegister) {
            this.dataPathParentRegister = dataPathParentRegister;
        }

        public String getDataPathParentCitizenId() {
            return dataPathParentCitizenId;
        }

        public void setDataPathParentCitizenId(String dataPathParentCitizenId) {
            this.dataPathParentCitizenId = dataPathParentCitizenId;
        }
    
    
}
