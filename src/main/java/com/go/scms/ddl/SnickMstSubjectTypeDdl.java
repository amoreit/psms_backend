package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickMstSubjectTypeRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbMstSubjectType")
public class SnickMstSubjectTypeDdl {
    
        @Autowired
	private SnickMstSubjectTypeRepository mstSubjectTypeRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
			List list = mstSubjectTypeRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstSubjectTypeOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}

class TbMstSubjectTypeOutput{
    
        private String subjectTypeId;
        private String subjectTypeCode;
        private String name;

            public String getSubjectTypeId() {
                return subjectTypeId;
            }

            public void setSubjectTypeId(String subjectTypeId) {
                this.subjectTypeId = subjectTypeId;
            }

            public String getSubjectTypeCode() {
                return subjectTypeCode;
            }

            public void setSubjectTypeCode(String subjectTypeCode) {
                this.subjectTypeCode = subjectTypeCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
    
}
