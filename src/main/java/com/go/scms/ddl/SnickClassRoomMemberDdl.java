package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickClassRoomMemberRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/classroom-member")
public class SnickClassRoomMemberDdl {
    
        @Autowired
        private SnickClassRoomMemberRepository tbTrnClassRoomMemberRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                Long teacherId = ValiableUtils.LongIsZero(request.getParameter("memberId"));
                List list = tbTrnClassRoomMemberRepo.findAllByRecordStatusNotInAndTeacherJoinTeacherId(RecordStatus.DELETE.getCode(), teacherId);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomMemberOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }  
} 

class ClassRoomMemberOutput{
    
    private String classRoomMemberId;
    private String memberId;

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
