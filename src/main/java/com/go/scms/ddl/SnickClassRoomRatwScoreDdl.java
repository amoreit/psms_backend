package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickClassRoomRatwScoreRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/ratw-score")
public class SnickClassRoomRatwScoreDdl {
    
        @Autowired
        private SnickClassRoomRatwScoreRepository ratwScoreRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(request.getParameter("classRoomSubjectId"));
                String yearGroupName = ValiableUtils.stringIsEmpty(request.getParameter("yearGroupName"));
                List list = ratwScoreRepo.findAllByRecordStatusNotInAndClassRoomSubjectIdAndYearGroupName(RecordStatus.DELETE.getCode(), classRoomSubjectId, yearGroupName);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, RatwScoreOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class RatwScoreOutput{
    
    private Integer classRoomSubjectId;
    private String yearGroupName;

    public Integer getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(Integer classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }
}
