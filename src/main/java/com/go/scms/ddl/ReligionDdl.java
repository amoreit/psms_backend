/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.ReligionRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/religion")
public class ReligionDdl {
    
    @Autowired
    private ReligionRepository religionRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    List list = religionRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,Religion_Output[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}

class Religion_Output{
    private String religionId;
    private String religionNameTh;

    public String getReligionId() {
        return religionId;
    }

    public void setReligionId(String religionId) {
        this.religionId = religionId;
    }

    public String getReligionNameTh() {
        return religionNameTh;
    }

    public void setReligionNameTh(String religionNameTh) {
        this.religionNameTh = religionNameTh;
    }

    
    
}
