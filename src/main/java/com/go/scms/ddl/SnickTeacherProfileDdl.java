package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.entity.ClassEntity;
import com.go.scms.entity.ClassRoomEntity;
import com.go.scms.repository.SnickTeacherProfileRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbTeacherProfile")
public class SnickTeacherProfileDdl {
    
        @Autowired
        private SnickTeacherProfileRepository teacherProfileRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                Long teacherId = ValiableUtils.LongIsZero(request.getParameter("teacherId"));
                List list = teacherProfileRepo.findAllByRecordStatusNotInAndTeacherId(RecordStatus.DELETE.getCode(), teacherId);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TeacherProfileOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/register")
        public ResponseEntity<?> register(HttpServletRequest request) {
            try {
                String teacherCardId = ValiableUtils.stringIsEmpty(request.getParameter("teacherCardId"));
                List list = teacherProfileRepo.findAllByRecordStatusNotInAndTeacherCardId(RecordStatus.DELETE.getCode(), teacherCardId);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TeacherProfileOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class TeacherProfileOutput{
    private String teacherId;
    private String classId;
    private String classRoomId;
    private String teacherCardId;
    private String firstnameTh;
    private String lastnameTh;
    private String titleId;
    private String email;
    private String citizenId;
    private String tel1;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }
}
