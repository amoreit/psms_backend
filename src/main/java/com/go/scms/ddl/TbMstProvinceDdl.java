/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstProvinceRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author S'Noppatat Gabgaeo
 */
@RestController
@RequestMapping("/api/v1/0/tbMstProvince")
public class TbMstProvinceDdl {
    
        @Autowired
        private TbMstProvinceRepository provinceRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlReg")
        public ResponseEntity<?> ddlReg(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlCur")
        public ResponseEntity<?> ddlCur(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlPob")
        public ResponseEntity<?> ddlPob(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlFather")
        public ResponseEntity<?> ddlFather(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlMother")
        public ResponseEntity<?> ddlMother(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlParent")
        public ResponseEntity<?> ddlParent(HttpServletRequest request) {
            try {
                List list = provinceRepo.findAllByRecordStatusNotInOrderByProvinceTh(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ProvinceOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class ProvinceOutput {
    
    private Long provinceId;
    private String provinceTh;

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceTh() {
        return provinceTh;
    }

    public void setProvinceTh(String provinceTh) {
        this.provinceTh = provinceTh;
    }
}
