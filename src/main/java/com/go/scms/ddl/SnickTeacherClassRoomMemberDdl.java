package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickTeacherClassRoomMemberRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/teacher-classRoomMember")
public class SnickTeacherClassRoomMemberDdl {
    
        @Autowired
	private SnickTeacherClassRoomMemberRepository teacherClassRoomMemberRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
                        Integer yearGroupId = (int) ValiableUtils.IntIsZero(request.getParameter("yearGroupId"));
                        Integer memberId = (int) ValiableUtils.IntIsZero(request.getParameter("memberId"));
                        String role = "teacher";
			List list = teacherClassRoomMemberRepo.findAllByRecordStatusNotInAndYearGroupIdAndMemberIdAndRole(RecordStatus.DELETE.getCode(), yearGroupId, memberId, role);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TeacherClassRoomMemberOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}

class TeacherClassRoomMemberOutput{
    
    private String classRoomMemberId;
    private String memberId;
    private String fullname;
    private String fullnameEn;
    private String stuCode;

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
    
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }
}
