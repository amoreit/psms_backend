/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbPrintCardLogRepository;
import com.go.scms.repository.TbTecPrintCardLogRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Kittipong
 */
@RestController
@RequestMapping("/api/v1/0/stu-printcardlog")
public class TbPrintCardLogDdl {
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired
    private TbPrintCardLogRepository tbPrintCardLogRepository;
    
    @Autowired
    private TbTecPrintCardLogRepository tbTecPrintCardLogRepository;
    
    @GetMapping("/stuCode-check")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    String stuCode = ValiableUtils.stringIsEmpty(request.getParameter("stuCode"));
                    List list = tbPrintCardLogRepository.findAllByRecordStatusNotInAndStuCode(RecordStatus.DELETE.getCode(),stuCode);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,StuCodeOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    @GetMapping("/TecCode-check")
    public ResponseEntity<?> ddltec(HttpServletRequest request){
            try {
                    String teacherCardId = ValiableUtils.stringIsEmpty(request.getParameter("teacherCardId"));
                    List list = tbTecPrintCardLogRepository.findAllByRecordStatusNotInAndTeacherCardId(RecordStatus.DELETE.getCode(),teacherCardId);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TecCodeOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
}

class StuCodeOutput{
    
    private String stuCode;

    public String getStuCode() {
        return stuCode;
    }
    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }
}

class TecCodeOutput{
    
    private String teacherCardId;

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }
}
