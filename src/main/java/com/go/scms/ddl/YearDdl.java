/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.YearRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/year")
public class YearDdl {

    @Autowired
    private YearRepository yearRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = yearRepo.findAllByrecordStatusNotInOrderByYearId(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}

class YearOutput {

    private String yearId;
    private String scCode;
    private String yearCode;
    private String recordStatus;

    public String getYearId() {
        return yearId;
    }

    public void setYearId(String yearId) {
        this.yearId = yearId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearCode() {
        return yearCode;
    }

    public void setYearCode(String yearCode) {
        this.yearCode = yearCode;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

}
