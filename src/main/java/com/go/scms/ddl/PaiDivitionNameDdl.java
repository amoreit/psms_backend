/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;


import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.PaiDivitionNameRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/0/ddldivisionname")
public class PaiDivitionNameDdl {
    
    @Autowired
    private PaiDivitionNameRepository paiDivitionNameRepository;

    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = paiDivitionNameRepository.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DivisionNameOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
}

class DivisionNameOutput {
    private String divisionId;
    private String divisionNameTh;
    private String divisionNameEn;

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionNameTh() {
        return divisionNameTh;
    }

    public void setDivisionNameTh(String divisionNameTh) {
        this.divisionNameTh = divisionNameTh;
    }

    public String getDivisionNameEn() {
        return divisionNameEn;
    }

    public void setDivisionNameEn(String divisionNameEn) {
        this.divisionNameEn = divisionNameEn;
    }
    
    
}
