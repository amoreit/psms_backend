/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.ActivityPaiRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbMstActivity")
public class MstActivityPaiDdl {
    
    @Autowired
    private ActivityPaiRepository activityRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    List list = activityRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,tbMstActivityOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }      
    }    
            
}

class tbMstActivityOutput{

        private String activityId;
        private String activityCode;
        private String activityName;

        public String getActivityId() {
            return activityId;
        }

        public void setActivityId(String activityId) {
            this.activityId = activityId;
        }

        public String getActivityCode() {
            return activityCode;
        }

        public void setActivityCode(String activityCode) {
            this.activityCode = activityCode;
        }

        public String getActivityName() {
            return activityName;
        }

        public void setActivityName(String activityName) {
            this.activityName = activityName;
        }

}
