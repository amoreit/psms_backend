/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.YearTermRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/yearTerm")
public class YearTermDdl {

    @Autowired
    private YearTermRepository yearTermRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = yearTermRepo.findAllByrecordStatusNotInOrderByYearTermId(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearTermOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}

class YearTermOutput {

    private String yearTermId;
    private String scCode;
    private String yearTermCode;
    private String name;
    private String recordStatus;

    public String getYearTermId() {
        return yearTermId;
    }

    public void setYearTermId(String yearTermId) {
        this.yearTermId = yearTermId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearTermCode() {
        return yearTermCode;
    }

    public void setYearTermCode(String yearTermCode) {
        this.yearTermCode = yearTermCode;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
