/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SubDistrictRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/0/sub-district")
public class SubDistrictDdl {
    @Autowired
    private SubDistrictRepository subDistrictRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddlByCondition")
    public ResponseEntity<?> ddlByCondition(HttpServletRequest request) {
        try {
            /*** Filter ***/
            String district = ValiableUtils.stringIsEmpty(request.getParameter("district"));
            List list = subDistrictRepo.findAllByRecordStatusNotInAndJoinSubDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), district);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrict_Output[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlByPostcode")
    public ResponseEntity<?> ddlByPostcode(HttpServletRequest request) {
        try {
            /*** Filter ***/
            String district = ValiableUtils.stringIsEmpty(request.getParameter("district"));
            String subDistrict = ValiableUtils.stringIsEmpty(request.getParameter("subDistrict"));
            List list = subDistrictRepo.findAllByRecordStatusNotInAndJoinSubDistrictDistrictThAndSubDistrictTh(RecordStatus.DELETE.getCode(), district, subDistrict);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, Postcode_Output[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class SubDistrict_Output {
    private String subDistrictId;
    private String subDistrictTh;

    public String getSubDistrictId() {
        return subDistrictId;
    }

    public void setSubDistrictId(String subDistrictId) {
        this.subDistrictId = subDistrictId;
    }

    public String getSubDistrictTh() {
        return subDistrictTh;
    }

    public void setSubDistrictTh(String subDistrictTh) {
        this.subDistrictTh = subDistrictTh;
    }
    
}

class Postcode_Output {
    private String postCode;

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
    
}
