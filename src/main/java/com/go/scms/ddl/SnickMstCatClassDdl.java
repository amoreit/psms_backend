package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickMstCatClassRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbMstCatClass")
public class SnickMstCatClassDdl {
    
        @Autowired
	private SnickMstCatClassRepository mstCatClassRepo;
	
	@Autowired
	private ModelMapper modelMapper;
        
        @GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
			List list = mstCatClassRepo.findAllByRecordStatusNotInOrderByCatClassId(RecordStatus.DELETE.getCode());
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstCatClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @GetMapping("/dialog")
	public ResponseEntity<?> ddlDialog(HttpServletRequest request){
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Filter ***/
			String lnameTh = ValiableUtils.stringIsEmpty(request.getParameter("text"));
			
			Pageable pageable = PageRequest.of(PageableUtils.offset(request.getParameter("p")),PageableUtils.result("10"),Sort.by("catClassId").ascending());
			Page list = mstCatClassRepo.findAllByLnameThContaining(lnameTh, pageable);
			
			/*** Response ***/
			output.setResponseData(list.getContent());
			output.setResponseSize(list.getTotalElements());
			
			return ResponseEntity.status(HttpStatus.OK).body(output);
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @GetMapping("/junior")
	public ResponseEntity<?> ddlJunior(HttpServletRequest request){
		try {
                        String lnameTh = "อนุบาล";
			List list = mstCatClassRepo.findAllByRecordStatusNotInAndLnameThNotInOrderByCatClassId(RecordStatus.DELETE.getCode(), lnameTh);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstCatClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
}

class TbMstCatClassOutput{
        
        private String catClassId;
        private String lnameTh;
        private String lnameEn;

        public String getCatClassId() {
            return catClassId;
        }

        public void setCatClassId(String catClassId) {
            this.catClassId = catClassId;
        }

        public String getLnameTh() {
            return lnameTh;
        }

        public void setLnameTh(String lnameTh) {
            this.lnameTh = lnameTh;
        }

        public String getLnameEn() {
            return lnameEn;
        }

        public void setLnameEn(String lnameEn) {
            this.lnameEn = lnameEn;
        }    
}
