package com.go.scms.ddl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.PageGroupRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;

@RestController
@RequestMapping("/api/v1/0/page-group")
public class PageGroupDdl {

	@Autowired
	private PageGroupRepository pageGroupRepo;
	
	@GetMapping("/ddl")
	public ResponseEntity<Object> ddl(HttpServletRequest request){
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Filter ***/
			String text = ValiableUtils.stringIsEmpty(request.getParameter("text"));
			Pageable pageable = PageRequest.of(PageableUtils.offset(request.getParameter("p")),PageableUtils.result("10"),Sort.by("pageGroupCode").ascending());
			Page list = pageGroupRepo.findAllByPageGroupStatusNotIn(RecordStatus.DELETE.getCode(),text,pageable);
			
			/*** Response ***/
			output.setResponseData(list.getContent());
			output.setResponseSize(list.getTotalElements());
			
			return ResponseEntity.status(HttpStatus.OK).body(output);
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
}