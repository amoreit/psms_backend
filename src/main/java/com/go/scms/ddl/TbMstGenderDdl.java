/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstBloodTypeRepository;
import com.go.scms.repository.TbMstGenderRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/tbMstGender")
public class TbMstGenderDdl {

    @Autowired
    private TbMstGenderRepository genderRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = genderRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, GenderOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/titleBoy")
    public ResponseEntity<?> ddlTitleBoy(HttpServletRequest request){
            try {
                    long genderId = 2;
                    List list = genderRepo.findAllByRecordStatusNotInAndGenderIdNotIn(RecordStatus.DELETE.getCode(), genderId);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,Gender_Output[].class));
                  
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    @GetMapping("/titleGirl")
    public ResponseEntity<?> ddlTitleGirl(HttpServletRequest request){
            try {
                    long genderId = 1;
                    List list = genderRepo.findAllByRecordStatusNotInAndGenderIdNotIn(RecordStatus.DELETE.getCode(), genderId);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,Gender_Output[].class));
                  
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}

class GenderOutput {

    private String genderId;
    private String genderDescTh;
    private String genderDescEn;

    public String getGenderId() {
        return genderId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public String getGenderDescTh() {
        return genderDescTh;
    }

    public void setGenderDescTh(String genderDescTh) {
        this.genderDescTh = genderDescTh;
    }

    public String getGenderDescEn() {
        return genderDescEn;
    }

    public void setGenderDescEn(String genderDescEn) {
        this.genderDescEn = genderDescEn;
    }

}
