/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstGenderRepository;
import com.go.scms.repository.TbMstTitleRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/tbMstTitle")
public class TbMstTitleDdl {

        @Autowired
        private TbMstTitleRepository titleRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {          
            String langCode = "th";
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlStu")
        public ResponseEntity<?> ddlStu(HttpServletRequest request) {          
            String name = "นาง";
            String langCode = "th";
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameNotInAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), name, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlFather")
        public ResponseEntity<?> ddlFather(HttpServletRequest request) {            
            String name_item1 = "ด.ช.";
            String langCode = "en";
            Integer gender = 1;
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameNotInAndGenderAndLangCodeNotInOrderByTitleId(RecordStatus.DELETE.getCode(), name_item1, gender, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlMother")
        public ResponseEntity<?> ddlMother(HttpServletRequest request) {  
            String name_item1 = "ด.ญ.";
            String langCode = "en";
            Integer gender = 2;
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameNotInAndGenderAndLangCodeNotInOrderByTitleId(RecordStatus.DELETE.getCode(), name_item1, gender, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class)); 
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlParent")
        public ResponseEntity<?> ddlParent(HttpServletRequest request) {
            String name_item1 = "ด.ช.";
            String name_item2 = "ด.ญ.";
            String langCode = "en";
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameNotInAndNameNotInAndLangCodeNotInOrderByTitleId(RecordStatus.DELETE.getCode(), name_item1, name_item2, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlEn")
        public ResponseEntity<?> ddlEn(HttpServletRequest request) {           
            String langCode = "en";
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlEnFather")
        public ResponseEntity<?> ddlEnFather(HttpServletRequest request) {            
            String name = "MR.";
            String langCode = "en";
            Integer gender = 1;
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameAndGenderAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), name, gender, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlEnMother")
        public ResponseEntity<?> ddlEnMother(HttpServletRequest request) {            
            String name_item1 = "M.";
            String name_item2 = "MR.";
            String langCode = "en";
            Integer gender = 2;
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameNotInAndNameNotInAndGenderAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), name_item1, name_item2, gender, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlEnParent")
        public ResponseEntity<?> ddlEnParent(HttpServletRequest request) {            
            String name_item1 = "M.";
            String langCode = "en";
            try {
                List list = titleRepo.findAllByRecordStatusNotInAndNameNotInAndLangCodeOrderByTitleId(RecordStatus.DELETE.getCode(), name_item1, langCode);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class TitleOutput {

    private String titleId;
    private String name;
    private String descTh;
    private String abbrev;
    private String gender;

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescTh() {
        return descTh;
    }

    public void setDescTh(String descTh) {
        this.descTh = descTh;
    }
    
    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }    

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
