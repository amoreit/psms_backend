/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TeacherProfileRepository;
import com.go.scms.repository.TeacherProfileTimeRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */

@RestController
@RequestMapping("/api/v1/0/teacher")
public class TbTeaTeacherProfileDdl {
    
        @Autowired
        private TeacherProfileTimeRepository teacherProfiletimeRepo;
    
        @Autowired
        private ModelMapper modelMapper;
    
        @GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
                        String teacherCardId = ValiableUtils.stringIsEmpty(request.getParameter("teacherCardId")).toUpperCase();                    
			List list = teacherProfiletimeRepo.findAllByRecordStatusNotInAndTeacherCardId(RecordStatus.DELETE.getCode(),teacherCardId);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TeacherOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/ddlSm")
	public ResponseEntity<?> ddlSm(HttpServletRequest request){          
		try {
                        String smartPurseId = ValiableUtils.stringIsEmpty(request.getParameter("smartPurseId")).toUpperCase();
			List list = teacherProfiletimeRepo.findAllBySmartPurseId(smartPurseId);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TeacherOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}

class TeacherOutput{
    
    private String teacherId;
    private String classId;
    private String teacherCardId;
    private String fullname;
    private String teacherImage;
    private Date checkTime;

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getTeacherImage() {
        return teacherImage;
    }

    public void setTeacherImage(String teacherImage) {
        this.teacherImage = teacherImage;
    }
    

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    
}

