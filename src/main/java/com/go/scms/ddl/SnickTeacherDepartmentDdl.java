package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickTeacherDepartmentRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/teacher-department")
public class SnickTeacherDepartmentDdl {
    
        @Autowired
	private SnickTeacherDepartmentRepository teacherDepartmentRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
                        Integer teacherId = (int) ValiableUtils.IntIsZero(request.getParameter("teacherId"));
			List list = teacherDepartmentRepo.findAllByRecordStatusNotInAndTeacherId(RecordStatus.DELETE.getCode(), teacherId);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TeacherDepartmentOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
} 

class TeacherDepartmentOutput{
    
    private String teacherDepartmentId;
    private String teacherId;
    private String fullname;
    private String leader;

    public String getTeacherDepartmentId() {
        return teacherDepartmentId;
    }

    public void setTeacherDepartmentId(String teacherDepartmentId) {
        this.teacherDepartmentId = teacherDepartmentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }
    
}
