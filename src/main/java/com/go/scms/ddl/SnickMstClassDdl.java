package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.entity.SnickMstClassEntity;
import com.go.scms.repository.SnickMstClassRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbMstClass")
public class SnickMstClassDdl {
    
        @Autowired
	private SnickMstClassRepository mstClassRepo;
	
	@Autowired
	private ModelMapper modelMapper;
        
        @GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
                        String langCode = "en";
			List list = mstClassRepo.findAllByRecordStatusNotInAndLangCodeNotInOrderByClassId(RecordStatus.DELETE.getCode(), langCode);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/ddlFilter")
	public ResponseEntity<?> ddlByCondition(HttpServletRequest request){
		try {
                        String lnameTh = ValiableUtils.stringIsEmpty(request.getParameter("lnameTh"));

			List list = mstClassRepo.findAllByRecordStatusNotInAndCatclassLnameThContainingOrderByClassId(RecordStatus.DELETE.getCode(),lnameTh);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/ddlteacherClass")
	public ResponseEntity<?> ddlteacherClass(HttpServletRequest request){
		try {
                        Integer catClassId = (int) ValiableUtils.IntIsZero(request.getParameter("catClassId"));
                        String langCode = "en";

			List list = mstClassRepo.findAllByRecordStatusNotInAndCatclassCatClassIdAndLangCodeNotInOrderByClassId(RecordStatus.DELETE.getCode(),catClassId, langCode);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/ddlJunior")
	public ResponseEntity<?> ddlJunior(HttpServletRequest request){
		try {
                        Integer catClassId = 1;
                        String langCode = "en";

			List list = mstClassRepo.findAllByRecordStatusNotInAndCatclassCatClassIdNotInAndLangCodeNotInOrderByClassId(RecordStatus.DELETE.getCode(),catClassId, langCode);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        //------------------------ aca008 dialog teacher
        @GetMapping("/ddlAca008")
	public ResponseEntity<?> ddlAca008(HttpServletRequest request){
		try {
                        Long classId = ValiableUtils.IntIsZero(request.getParameter("classId"));

			List list = mstClassRepo.findAllByRecordStatusNotInAndClassId(RecordStatus.DELETE.getCode(), classId);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TbMstClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}

class TbMstClassOutput{
    
    private String classId;
    private String classLnameTh;
    private String classLnameEn;
    private String classSnameTh;
    private String classSnameEn;
    private String age;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassLnameTh() {
        return classLnameTh;
    }

    public void setClassLnameTh(String classLnameTh) {
        this.classLnameTh = classLnameTh;
    }

    public String getClassLnameEn() {
        return classLnameEn;
    }

    public void setClassLnameEn(String classLnameEn) {
        this.classLnameEn = classLnameEn;
    }

    public String getClassSnameTh() {
        return classSnameTh;
    }

    public void setClassSnameTh(String classSnameTh) {
        this.classSnameTh = classSnameTh;
    }

    public String getClassSnameEn() {
        return classSnameEn;
    }

    public void setClassSnameEn(String classSnameEn) {
        this.classSnameEn = classSnameEn;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
        
}
