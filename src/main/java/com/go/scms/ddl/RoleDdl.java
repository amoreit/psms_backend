package com.go.scms.ddl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.RoleRepository;

@RestController
@RequestMapping("/api/v1/0/role")
public class RoleDdl {

	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
			List list = roleRepo.findAllByRoleStatusNotIn(RecordStatus.DELETE.getCode());
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,RoleOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
}
class RoleOutput{
	private String roleId;
	private String roleCode;
	private String roleName;
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}