/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.ClassRoomRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/0/classroom-list")
public class ClassRoomList {

    @Autowired
    private ClassRoomRepository classRoomRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = classRoomRepo.findAllByRecordStatusNotInOrderByClassRoomIdAsc(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlisep")
    public ResponseEntity<?> ddlIsEp(HttpServletRequest request) {
        try {
            List list = classRoomRepo.findddlclassroomisep();
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomIsEp[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlFilter")
    public ResponseEntity<?> ddlFilter(HttpServletRequest request) {
        try {
            Integer classId = (int) ValiableUtils.IntIsZero(request.getParameter("classId"));
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassJoinClassIdOrderByClassRoomIdAscClassJoinClassIdAsc(RecordStatus.DELETE.getCode(), classId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlFilterClassRoom")
    public ResponseEntity<?> ddlFilterClassRoom(HttpServletRequest request) {
        try {
            Integer classId = (int) ValiableUtils.IntIsZero(request.getParameter("classId"));
            String classRoom1 = "-";
            String classRoom2 = "รอย้ายห้อง";
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassRoomNotInAndClassRoomNotInAndClassJoinClassIdOrderByClassRoomIdAscClassJoinClassIdAsc(RecordStatus.DELETE.getCode(), classRoom1, classRoom2, classId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //moss
    @GetMapping("/ddlfiltercitizenid")
    public ResponseEntity<?> ddlfilterbycitizenIdandclassid(HttpServletRequest request) {
        try {
            String citizenId = ValiableUtils.stringIsEmpty(request.getParameter("citizenId"));
            Integer classId = (int) ValiableUtils.IntIsZero(request.getParameter("classId"));
            List list = classRoomRepo.findddlfilterbycitizenIdandclassid(citizenId, classId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomCitizenDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //ddlFilter for cer002 and rep006 and rep001
    @GetMapping("/ddlFilter2")
    public ResponseEntity<?> ddlFilter2(HttpServletRequest request) {
        try {
            Integer classId = (int) ValiableUtils.IntIsZero(request.getParameter("classId"));
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassJoinClassIdAndIsactiveOrderByClassRoomId(RecordStatus.DELETE.getCode(), classId, true);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/className")
    public ResponseEntity<?> className(HttpServletRequest request) {
        try {
            String classSnameTh = ValiableUtils.stringIsEmpty(request.getParameter("classSnameTh"));
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassJoinClassSnameThAndIsactiveOrderByClassRoomId(RecordStatus.DELETE.getCode(), classSnameTh,true);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class ClassRoomListOutput {

    private String classRoomId;
    private String classRoomSnameTh;
    private String classRoom;
    private String isEp;

    public String getIsEp() {
        return isEp;
    }

    public void setIsEp(String isEp) {
        this.isEp = isEp;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }
}

//moss
class ClassRoomCitizenDdlOutput{
    private String class_room_id;
    private String class_room;

    public String getClass_room_id() {
        return class_room_id;
    }

    public void setClass_room_id(String class_room_id) {
        this.class_room_id = class_room_id;
    }

    public String getClass_room() {
        return class_room;
    }

    public void setClass_room(String class_room) {
        this.class_room = class_room;
    }
    
}

class ClassRoomIsEp{
    private String class_room_id;
    private String class_room_sname_th;
    private String is_ep;
    private String class_id;
    private String class_sname_th;

    public String getClass_sname_th() {
        return class_sname_th;
    }

    public void setClass_sname_th(String class_sname_th) {
        this.class_sname_th = class_sname_th;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getClass_room_id() {
        return class_room_id;
    }

    public void setClass_room_id(String class_room_id) {
        this.class_room_id = class_room_id;
    }

    public String getClass_room_sname_th() {
        return class_room_sname_th;
    }

    public void setClass_room_sname_th(String class_room_sname_th) {
        this.class_room_sname_th = class_room_sname_th;
    }

    public String getIs_ep() {
        return is_ep;
    }

    public void setIs_ep(String is_ep) {
        this.is_ep = is_ep;
    }
    
}
