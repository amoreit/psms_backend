package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickActivityGroupRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/activity-grouplist")
public class SnickActivityGroupDdl {
    
    @Autowired
    private SnickActivityGroupRepository activityGroupRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/kindergarten")
    public ResponseEntity<?> kindergarten(HttpServletRequest request) {
        try {
            Integer catClassId = 1;
            List list = activityGroupRepo.findAllByRecordStatusNotInAndCatClassIdOrderByActivityGroupId(RecordStatus.DELETE.getCode(), catClassId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SnickActivityGroupListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/juniorup")
    public ResponseEntity<?> juniorup(HttpServletRequest request) {
        try {
            Integer catClassId = null;
            List list = activityGroupRepo.findAllByRecordStatusNotInAndCatClassIdOrderByActivityGroupId(RecordStatus.DELETE.getCode(), catClassId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SnickActivityGroupListOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class SnickActivityGroupListOutput{
    
    private String activityGroupId;
    private String activityGroup;

    public String getActivityGroupId() {
        return activityGroupId;
    }

    public void setActivityGroupId(String activityGroupId) {
        this.activityGroupId = activityGroupId;
    }

    public String getActivityGroup() {
        return activityGroup;
    }

    public void setActivityGroup(String activityGroup) {
        this.activityGroup = activityGroup;
    }
    
}
