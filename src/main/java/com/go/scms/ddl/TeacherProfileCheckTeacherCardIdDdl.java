/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TeacherProfileCheckTeacherCardIdRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/0/teachercheckteachercardid")
public class TeacherProfileCheckTeacherCardIdDdl {
    
    @Autowired
    private TeacherProfileCheckTeacherCardIdRepository teacherProfileCheckTeacherCardIdRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    String teacherCardId = ValiableUtils.stringIsEmpty(request.getParameter("teacherCardId"));
                    List list = teacherProfileCheckTeacherCardIdRepository.findAllByRecordStatusNotInAndTeacherCardId(RecordStatus.DELETE.getCode(),teacherCardId);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,TeacherCheckOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}

class TeacherCheckOutput{
    
    private String teacherId;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }
    
}
