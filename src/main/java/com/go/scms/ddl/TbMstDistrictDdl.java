/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstDistrictRepository;
import com.go.scms.repository.TbMstGenderRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/tbMstDistrict")
public class TbMstDistrictDdl {

        @Autowired
        private TbMstDistrictRepository districtRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceTh"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlReg")
        public ResponseEntity<?> ddlReg(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceThReg"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlCur")
        public ResponseEntity<?> ddlCur(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceThCur"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlPob")
        public ResponseEntity<?> ddlPob(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceThPob"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlFather")
        public ResponseEntity<?> ddlFather(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceThFather"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlMother")
        public ResponseEntity<?> ddlMother(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceThMother"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlParent")
        public ResponseEntity<?> ddlParent(HttpServletRequest request) {
            try {
                String provinceTh = ValiableUtils.stringIsEmpty(request.getParameter("provinceThParent"));
                List list = districtRepo.findAllByRecordStatusNotInAndProvinceProvinceThOrderByDistrictTh(RecordStatus.DELETE.getCode(),provinceTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class DistrictOutput {

    private String districtId;
    private String districtCode;
    private String districtTh;
    private String districtEn;
    private String provinceId;

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictTh() {
        return districtTh;
    }

    public void setDistrictTh(String districtTh) {
        this.districtTh = districtTh;
    }

    public String getDistrictEn() {
        return districtEn;
    }

    public void setDistrictEn(String districtEn) {
        this.districtEn = districtEn;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }
    
    
}
