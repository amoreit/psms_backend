package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickDepartmentRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/check-departmentId")
public class SnickDepartmentDdl {
    
    @Autowired
    private SnickDepartmentRepository snickDepartmentRepo;

    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            Integer catClass = (int) ValiableUtils.IntIsZero(request.getParameter("catClass"));
            String name = ValiableUtils.stringIsEmpty(request.getParameter("name"));
            List list = snickDepartmentRepo.findAllByRecordStatusNotInAndCatClassAndName(RecordStatus.DELETE.getCode() ,catClass, name);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, DepartmentDataOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class DepartmentDataOutput{
    private String departmentId;
    private String catClass;

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getCatClass() {
        return catClass;
    }

    public void setCatClass(String catClass) {
        this.catClass = catClass;
    }
    
}
