/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstSubDistrictRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/tbMstSubDistrict")
public class TbMstSubDistrictDdl {

        @Autowired
        private TbMstSubDistrictRepository subDistrictRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtTh"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlReg")
        public ResponseEntity<?> ddlReg(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThReg"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlCur")
        public ResponseEntity<?> ddlCur(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThCur"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlPob")
        public ResponseEntity<?> ddlPob(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThPob"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlFather")
        public ResponseEntity<?> ddlFather(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThFather"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlMother")
        public ResponseEntity<?> ddlMother(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThMother"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/ddlParent")
        public ResponseEntity<?> ddlParent(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThParent"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThOrderBySubDistrictTh(RecordStatus.DELETE.getCode(), districtTh);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        //------------------------ P'pai
        @GetMapping("/ddlRegPostcode")
        public ResponseEntity<?> ddlRegPostcode(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThReg"));
                String subDistrict = ValiableUtils.stringIsEmpty(request.getParameter("subdistrictReg"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThAndSubDistrictTh(RecordStatus.DELETE.getCode(), districtTh,subDistrict);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlCurPostcode")
        public ResponseEntity<?> ddlPostCur(HttpServletRequest request) {
            try {
                String districtTh = ValiableUtils.stringIsEmpty(request.getParameter("districtThCur"));
                String subcurDistrict = ValiableUtils.stringIsEmpty(request.getParameter("subdistrictCur"));
                List list = subDistrictRepo.findAllByRecordStatusNotInAndDistrictDistrictThAndSubDistrictTh(RecordStatus.DELETE.getCode(), districtTh,subcurDistrict);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, SubDistrictOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class SubDistrictOutput {

    private String subDistrictId;
    private String subDistrictCode;
    private String subDistrictTh;
    private String subDistrictEn;
    private String districtId;
    private String provinceId;
    private String postCode;
    private String remark;

    public String getSubDistrictId() {
        return subDistrictId;
    }

    public void setSubDistrictId(String subDistrictId) {
        this.subDistrictId = subDistrictId;
    }

    public String getSubDistrictCode() {
        return subDistrictCode;
    }

    public void setSubDistrictCode(String subDistrictCode) {
        this.subDistrictCode = subDistrictCode;
    }

    public String getSubDistrictTh() {
        return subDistrictTh;
    }

    public void setSubDistrictTh(String subDistrictTh) {
        this.subDistrictTh = subDistrictTh;
    }

    public String getSubDistrictEn() {
        return subDistrictEn;
    }

    public void setSubDistrictEn(String subDistrictEn) {
        this.subDistrictEn = subDistrictEn;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    } 
}
