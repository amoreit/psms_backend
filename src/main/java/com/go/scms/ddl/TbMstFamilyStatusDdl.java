package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstFamilyStatusRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbMstFamilyStatus")
public class TbMstFamilyStatusDdl {
    
        @Autowired
        private TbMstFamilyStatusRepository familyStatusRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                List list = familyStatusRepo.findAllByRecordStatusNotInOrderByFamilyStatusId(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, FamilyStatusOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}
class FamilyStatusOutput {
    private String familyStatusId;
    private String statusDesc;
    private String statusDescEn;

    public String getFamilyStatusId() {
        return familyStatusId;
    }

    public void setFamilyStatusId(String familyStatusId) {
        this.familyStatusId = familyStatusId;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusDescEn() {
        return statusDescEn;
    }

    public void setStatusDescEn(String statusDescEn) {
        this.statusDescEn = statusDescEn;
    }
}

