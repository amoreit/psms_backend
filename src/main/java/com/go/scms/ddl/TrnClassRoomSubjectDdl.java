/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.repository.TrnClassRoomSubjectRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/class-room-subject")
public class TrnClassRoomSubjectDdl {

    @Autowired
    private TrnClassRoomSubjectRepository trnClassRoomSubjectRepo;

    @Autowired
    private ModelMapper modelMapper;
    

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));
            List list = trnClassRoomSubjectRepo.findsubjectindepartment(departmentId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomSubject[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlaca002sub03")
    public ResponseEntity<?> ddlaca002sub03(HttpServletRequest request) {
        try {          
            Integer classId = (int) ValiableUtils.IntIsZero(request.getParameter("classId"));
            Integer yearGroupId = (int) ValiableUtils.IntIsZero(request.getParameter("yearGroupId"));
            List list = trnClassRoomSubjectRepo.findsubjectinddlaca002sub03(yearGroupId, classId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomSubjectDdl[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class ClassRoomSubject {

    private String subject_code;
    private String subject_code2;
    private String name_th;
    private String name_en;
    private String subject_type;
    private String orders;
    private String credit;
    private String lesson;
    private String weight;

    public String getSubject_type() {
        return subject_type;
    }

    public void setSubject_type(String subject_type) {
        this.subject_type = subject_type;
    }

    public String getSubject_code2() {
        return subject_code2;
    }

    public void setSubject_code2(String subject_code2) {
        this.subject_code2 = subject_code2;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getName_th() {
        return name_th;
    }

    public void setName_th(String name_th) {
        this.name_th = name_th;
    }

}

class ClassRoomSubjectDdl{
    private String subject_code;
    private String subject_name_th;

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getSubject_name_th() {
        return subject_name_th;
    }

    public void setSubject_name_th(String subject_name_th) {
        this.subject_name_th = subject_name_th;
    }
    
}
