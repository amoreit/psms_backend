/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.repository.TeacherProfileRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/teacher-profile")
public class TeacherProfileDdl {

    @Autowired
    private TeacherProfileRepository teacherProfileRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));
            List list = teacherProfileRepo.findteacherprofileddl(departmentId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TeacherProfileDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlaca002sub03")
    public ResponseEntity<?> ddlaca002sub03(HttpServletRequest request) {
        try {
            Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));
            List list = teacherProfileRepo.findteacherprofileddlaca002sub03(departmentId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TeacherProfileDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class TeacherProfileDdlOutput {

    private String teacher_id;
    private String fullname;
    private String section_name;

    public String getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(String teacher_id) {
        this.teacher_id = teacher_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }
}
