package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickAdmRoleRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/adm-role")
public class SnickAdmRoleDdl {
    
    @Autowired
    private SnickAdmRoleRepository admRoleRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
        try { 
                String roleType = ValiableUtils.stringIsEmpty(request.getParameter("roleType"));
                List list = admRoleRepo.findAllByRoleStatusNotInAndRoleType(RecordStatus.DELETE.getCode(), roleType);
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,AdmRole_Output[].class));
        }catch(Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlAll")
    public ResponseEntity<?> ddlAll(HttpServletRequest request){
        try { 
                List list = admRoleRepo.findAllByRoleStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,AdmRole_Output[].class));
        }catch(Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class AdmRole_Output {
    
    private String roleId;
    private String roleCode;
    private String roleName;
    private String roleType;
    private String roleStatus;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }
    
    public String getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(String roleStatus) {
        this.roleStatus = roleStatus;
    }
}

