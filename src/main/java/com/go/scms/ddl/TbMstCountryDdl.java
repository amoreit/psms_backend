package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstCountryRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/tbMstCountry")
public class TbMstCountryDdl {
    
        @Autowired
        private TbMstCountryRepository countryRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                List list = countryRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, CountryOutput1[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlFather")
        public ResponseEntity<?> ddlFather(HttpServletRequest request) {
            try {
                List list = countryRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, CountryOutput1[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlMother")
        public ResponseEntity<?> ddlMother(HttpServletRequest request) {
            try {
                List list = countryRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, CountryOutput1[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlParent")
        public ResponseEntity<?> ddlParent(HttpServletRequest request) {
            try {
                List list = countryRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, CountryOutput1[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class CountryOutput1 {
    
    private String countryId;
    private String countryCode;
    private String countryTh;
    private String countryEn;

            public String getCountryId() {
                return countryId;
            }

            public void setCountryId(String countryId) {
                this.countryId = countryId;
            }

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getCountryTh() {
                return countryTh;
            }

            public void setCountryTh(String countryTh) {
                this.countryTh = countryTh;
            }

            public String getCountryEn() {
                return countryEn;
            }

            public void setCountryEn(String countryEn) {
                this.countryEn = countryEn;
            }
}
