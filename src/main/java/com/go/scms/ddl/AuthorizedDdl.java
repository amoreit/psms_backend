/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstAuthorizedRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kronn
 */
@RestController
@RequestMapping("/api/v1/0/ddlAutho")
public class AuthorizedDdl {
    
    @Autowired
    private TbMstAuthorizedRepository tbMstauthorizedRepo;

    @Autowired
    private ModelMapper modelMapper;
    
    
    @GetMapping("/ddlAuthoriz")
    public ResponseEntity<?> ddlAuthoriz(HttpServletRequest request){

            try {
                    String authorizedId = ValiableUtils.stringIsEmpty(request.getParameter("authorizedId"));
                    List list = tbMstauthorizedRepo.findAllByRecordStatusNotInAndPosition(RecordStatus.DELETE.getCode(),authorizedId);
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,AuthoOutput[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    //Aong
    @GetMapping("/ddlSignaturePath")
    public ResponseEntity<?> ddlSignaturePath(HttpServletRequest request){

            try {
                    List list = tbMstauthorizedRepo.findSignatureImagePath();
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,SignaturePath[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}
    
class AuthoOutput{
    
    private String authorizedId;
    private String scCode;
    private String fullnameTh;
    private String iscurrent;
    private String fullnameEn;
    private String signatureImagePath;
    private String position;
    private Boolean isactive;
    private String langCode;

    public String getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(String iscurrent) {
        this.iscurrent = iscurrent;
    }
    
    public String getAuthorizedId() {
        return authorizedId;
    }

    public void setAuthorizedId(String authorizedId) {
        this.authorizedId = authorizedId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getFullnameTh() {
        return fullnameTh;
    }

    public void setFullnameTh(String fullnameTh) {
        this.fullnameTh = fullnameTh;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public String getSignatureImagePath() {
        return signatureImagePath;
    }

    public void setSignatureImagePath(String signatureImagePath) {
        this.signatureImagePath = signatureImagePath;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}

class SignaturePath{
    private String signature_image_path;

    public String getSignature_image_path() {
        return signature_image_path;
    }

    public void setSignature_image_path(String signature_image_path) {
        this.signature_image_path = signature_image_path;
    }
}
