/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.entity.ProvinceEntity;
import com.go.scms.repository.DistrictRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/district")
public class DistrictDdl {
    @Autowired
    private DistrictRepository districtRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddlByCondition")
    public ResponseEntity<?> ddlByCondition(HttpServletRequest request) {
        try {
            String province = ValiableUtils.stringIsEmpty(request.getParameter("province"));
            List list = districtRepo.findAllByRecordStatusNotInAndJoinProvinceProvinceThContainingOrderByDistrictTh(RecordStatus.DELETE.getCode(), province);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, District_Output[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class District_Output {
    private String districtId;
    private String districtTh;
    private ProvinceEntity joinProvince = new ProvinceEntity();

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictTh() {
        return districtTh;
    }

    public void setDistrictTh(String districtTh) {
        this.districtTh = districtTh;
    }

    public ProvinceEntity getJoinProvince() {
        return joinProvince;
    }

    public void setJoinProvince(ProvinceEntity joinProvince) {
        this.joinProvince = joinProvince;
    }
    
    
}
