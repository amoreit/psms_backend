package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.ClassRoomRepository;
import com.go.scms.repository.DepartmentRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/classroom")
public class ClassRoomNoDdl {

    @Autowired
    private ClassRoomRepository classRoomRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddlByCondition")
    public ResponseEntity<?> ddlByCondition(HttpServletRequest request) {
        try {
            String classId = ValiableUtils.stringIsEmpty(request.getParameter("classId"));
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassJoinClassSnameThContainingOrderByClassRoomIdAsc(RecordStatus.DELETE.getCode(), classId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlByClassId")
    public ResponseEntity<?> ddlByClassId(HttpServletRequest request) {
        try {
            String classId = ValiableUtils.stringIsEmpty(request.getParameter("classId"));
            List list = classRoomRepo.findclassroomid(Integer.parseInt(classId));
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlClassId2")
    public ResponseEntity<?> ddlByCondition2(HttpServletRequest request) {
        try {
            Integer classId = (int) ValiableUtils.IntIsZero(request.getParameter("classId"));
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassJoinClassIdAndIsactiveOrderByClassRoomId(RecordStatus.DELETE.getCode(), classId, true);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //max
    @GetMapping("/ddlClassId3")
    public ResponseEntity<?> ddlByCondition3(HttpServletRequest request) {
        try {
            String classId = ValiableUtils.stringIsEmpty(request.getParameter("classId"));
            List list = classRoomRepo.findAllByRecordStatusNotInAndClassJoinClassSnameThContainingOrderByClassRoomId(RecordStatus.DELETE.getCode(), classId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlfindroombyclassroomid")
    public ResponseEntity<?> ddlfindroombyclassroomid(HttpServletRequest request) {
        try {
            Integer classRoomId = (int) ValiableUtils.IntIsZero(request.getParameter("classRoomId"));
            List list = classRoomRepo.findddlfindroombyclassroomid(classRoomId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ClassRoomDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}

class ClassRoomOutput {

    private String classRoomId;
    private String classRoom;

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

}

class ClassRoomDdlOutput {

    private String class_room_id;
    private String class_room;
    private String class_room_sname_th;
    private String is_ep;

    public String getIs_ep() {
        return is_ep;
    }

    public void setIs_ep(String is_ep) {
        this.is_ep = is_ep;
    }

    public String getClass_room_id() {
        return class_room_id;
    }

    public void setClass_room_id(String class_room_id) {
        this.class_room_id = class_room_id;
    }

    public String getClass_room() {
        return class_room;
    }

    public void setClass_room(String class_room) {
        this.class_room = class_room;
    }

    public String getClass_room_sname_th() {
        return class_room_sname_th;
    }

    public void setClass_room_sname_th(String class_room_sname_th) {
        this.class_room_sname_th = class_room_sname_th;
    }
}
