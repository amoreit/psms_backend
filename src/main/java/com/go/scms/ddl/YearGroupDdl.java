/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.YearGroupRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/year-group")
public class YearGroupDdl {

    @Autowired
    private YearGroupRepository yearGroupRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            List list = yearGroupRepo.findAllByRecordStatusNotInOrderByIscurrentDescYearGroupIdDesc(RecordStatus.DELETE.getCode());
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearGroupOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlYear")
    public ResponseEntity<?> ddlYear(HttpServletRequest request) {
        try {
            Boolean iscurrent = true;
            List list = yearGroupRepo.findAllByRecordStatusNotInAndIscurrentOrderByYearGroupId(RecordStatus.DELETE.getCode(), iscurrent);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearGroupOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/searchId")
    public ResponseEntity<?> searchId(HttpServletRequest request) {
        try {
            String name = ValiableUtils.stringIsEmpty(request.getParameter("yearGroupName"));
            List list = yearGroupRepo.findAllByRecordStatusNotInAndNameOrderByYearGroupId(RecordStatus.DELETE.getCode(), name);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearGroupOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //moss
    @GetMapping("/ddlupperyeargroup")
    public ResponseEntity<?> searchddlupperyeargroup(HttpServletRequest request) {
        try {
            Integer yearGroupSearch = (int) ValiableUtils.IntIsZero(request.getParameter("yearGroupSearch"));
            List list = yearGroupRepo.findddlupperyeargroup(yearGroupSearch);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearGroupDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //nick
    @GetMapping("/ddlcheckyeargroup")
    public ResponseEntity<?> searchddlcheckyeargroup(HttpServletRequest request) {
        try {
            Integer yearGroupSearch = (int) ValiableUtils.IntIsZero(request.getParameter("yearGroupSearch"));
            List list = yearGroupRepo.findddlcheckyeargroup(yearGroupSearch);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, YearGroupDdlOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class YearGroupOutput {
    private String yearGroupId;
    private String name;
    private String iscurrent;

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(String iscurrent) {
        this.iscurrent = iscurrent;
    }
}

class YearGroupDdlOutput{
    private String year_group_id;
    private String name;

    public String getYear_group_id() {
        return year_group_id;
    }

    public void setYear_group_id(String year_group_id) {
        this.year_group_id = year_group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
