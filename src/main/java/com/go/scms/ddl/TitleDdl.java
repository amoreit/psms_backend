/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TitleRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/title")
public class TitleDdl {

    @Autowired
    private TitleRepository titleRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request) {
        try {
            String langCode = ValiableUtils.stringIsEmpty(request.getParameter("langCode"));
            List list = titleRepo.findAllByRecordStatusNotInAndLangCodeContainingAndTitleIdNotIn(RecordStatus.DELETE.getCode(), langCode,(long)5);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, Title_Output[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/ddlfiltergender")
    public ResponseEntity<?> ddlfiltergender(HttpServletRequest request) {
        try {
            Integer genderId = (int) ValiableUtils.IntIsZero(request.getParameter("genderId"));
            Long titleId = (long) ValiableUtils.LongIsZero(request.getParameter("titleId"));

            List list = titleRepo.findddlfiltergender(genderId, titleId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutputNative[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/ddlparent")
    public ResponseEntity<?> ddlparent(HttpServletRequest request) {
        try {
            List list = titleRepo.findddlparent();
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, TitleOutputNative[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}

class Title_Output {

    private String titleId;
    private String name;
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

class TitleOutputNative{
    private String title_id;
    private String name;
    private String gender;

    public String getTitle_id() {
        return title_id;
    }

    public void setTitle_id(String title_id) {
        this.title_id = title_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    
}
