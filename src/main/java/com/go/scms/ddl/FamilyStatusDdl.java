/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.FamilyStatusRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/0/family-status")
public class FamilyStatusDdl {
    @Autowired
    private FamilyStatusRepository familyStatusRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
    public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
                    List list = familyStatusRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                    return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,FamilyStatus_Output[].class));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}

class FamilyStatus_Output{
    private String familyStatusId;
    private String statusDesc;

    public String getFamilyStatusId() {
        return familyStatusId;
    }

    public void setFamilyStatusId(String familyStatusId) {
        this.familyStatusId = familyStatusId;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
    
}
