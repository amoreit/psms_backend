package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.SnickOnetRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
//import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/0/_onet")
public class SnickOnetDdl {
    
        @Autowired
	private SnickOnetRepository onetRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
		try {
                        String citizenId = ValiableUtils.stringIsEmpty(request.getParameter("citizenId"));
			List list = onetRepo.findAllByRecordStatusNotInAndCitizenId(RecordStatus.DELETE.getCode(), citizenId);
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,OnetOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
}

class OnetOutput{
    
    private String stuOnetScoreId;
    private String citizenId;
    private String fullname;

    public String getStuOnetScoreId() {
        return stuOnetScoreId;
    }

    public void setStuOnetScoreId(String stuOnetScoreId) {
        this.stuOnetScoreId = stuOnetScoreId;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
 
}
