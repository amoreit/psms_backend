/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.KornSubjectRepository;
import com.go.scms.repository.KsubjectRepository;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kronn
 */
@RestController
@RequestMapping("/api/v1/0/KsubjectType")
public class KsubjectDdl {

    @Autowired
    private KsubjectRepository kSubjectTypeRepo;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/Dedd2")
    public ResponseEntity<?> Dedd2(HttpServletRequest request) {
        try {
            String citizenId = ValiableUtils.stringIsEmpty(request.getParameter("citizenId"));
            Integer classRoomId = (int) ValiableUtils.IntIsZero(request.getParameter("classRoomId"));
            Integer yearGroupId = (int) ValiableUtils.IntIsZero(request.getParameter("yearGroupId"));
            List list = kSubjectTypeRepo.findddlsubjectbycitizenidandclassroomid(citizenId, classRoomId, yearGroupId);
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, KsubjectOutput[].class));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}

class KsubjectOutput{
    private String class_room_subject_id;
    private String subject_name_th;

    public String getClass_room_subject_id() {
        return class_room_subject_id;
    }

    public void setClass_room_subject_id(String class_room_subject_id) {
        this.class_room_subject_id = class_room_subject_id;
    }

    public String getSubject_name_th() {
        return subject_name_th;
    }

    public void setSubject_name_th(String subject_name_th) {
        this.subject_name_th = subject_name_th;
    }
    
}
