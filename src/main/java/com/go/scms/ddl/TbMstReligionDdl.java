/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.TbMstGenderRepository;
import com.go.scms.repository.TbMstReligionRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/0/tbMstReligion")
public class TbMstReligionDdl {

        @Autowired
        private TbMstReligionRepository religionRepo;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("/ddl")
        public ResponseEntity<?> ddl(HttpServletRequest request) {
            try {
                List list = religionRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ReligionOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlFather")
        public ResponseEntity<?> ddlFather(HttpServletRequest request) {
            try {
                List list = religionRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ReligionOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlMother")
        public ResponseEntity<?> ddlMother(HttpServletRequest request) {
            try {
                List list = religionRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ReligionOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/ddlParent")
        public ResponseEntity<?> ddlParent(HttpServletRequest request) {
            try {
                List list = religionRepo.findAllByRecordStatusNotIn(RecordStatus.DELETE.getCode());
                return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list, ReligionOutput[].class));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}

class ReligionOutput {

    private String religionID;
    private String religionNameTh;
    private String religionNameEn;

    public String getReligionID() {
        return religionID;
    }

    public void setReligionID(String religionID) {
        this.religionID = religionID;
    }

    public String getReligionNameTh() {
        return religionNameTh;
    }

    public void setReligionNameTh(String religionNameTh) {
        this.religionNameTh = religionNameTh;
    }

    public String getReligionNameEn() {
        return religionNameEn;
    }

    public void setReligionNameEn(String religionNameEn) {
        this.religionNameEn = religionNameEn;
    }

}
