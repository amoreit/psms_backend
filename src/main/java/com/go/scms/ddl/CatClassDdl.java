/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.ddl;

import com.go.scms.contant.RecordStatus;
import com.go.scms.repository.CatClassRepository;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/0/catclass")
public class CatClassDdl {
    @Autowired
    private CatClassRepository catClassRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @GetMapping("/ddl")
	public ResponseEntity<?> ddl(HttpServletRequest request){
            try {
			List list = catClassRepo.findAllByRecordStatusNotInOrderByCatClassId(RecordStatus.DELETE.getCode());
			return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(list,CatClassOutput[].class));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}

class CatClassOutput{
    private String catClassId;
    private String lnameTh;

    public String getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(String catClassId) {
        this.catClassId = catClassId;
    }

    public String getLnameTh() {
        return lnameTh;
    }

    public void setLnameTh(String lnameTh) {
        this.lnameTh = lnameTh;
    }
    
}
