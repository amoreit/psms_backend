package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnClassRoomMember")
public class SnickTrnClassRoomMemberEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classRoomMemberId;

    @Column
    private Integer memberId;

    @Column
    private Integer classRoomId;

    @Column
    private Integer yearGroupId;

    @Column
    private String scCode;

    @Column
    private String yearGroupName;

    @Column
    private String stuCode;

    @Column
    private String fullname;

    @Column
    private Integer classId;

    @Column
    private String className;

    @Column
    private String classRoom;

    @Column
    private String classRoomNo;
    
    @Column
    private Integer activity1id;
    
    @Column
    private String activity1name;
    
    @Column
    private String activity1lesson;
    
    @Column
    private Boolean activity1result;
    
    @Column
    private Integer activity2id;
    
    @Column
    private String activity2name;
    
    @Column
    private String activity2lesson;
    
    @Column
    private Boolean activity2result;
    
    @Column
    private Integer activity3id;
    
    @Column
    private String activity3name;
    
    @Column
    private String activity3lesson;
    
    @Column
    private Boolean activity3result;
    
    @Column
    private Integer activity4id;
    
    @Column
    private String activity4name;
    
    @Column
    private String activity4lesson;
    
    @Column
    private Boolean activity4result;
    
    @Column
    private Integer ratwResult;
    
    @Column
    private String role;

    @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private String recordStatus;

    @Column
    private Boolean isactive;

    public Long getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Long classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public Integer getActivity1id() {
        return activity1id;
    }

    public void setActivity1id(Integer activity1id) {
        this.activity1id = activity1id;
    }

    public String getActivity1name() {
        return activity1name;
    }

    public void setActivity1name(String activity1name) {
        this.activity1name = activity1name;
    }

    public String getActivity1lesson() {
        return activity1lesson;
    }

    public void setActivity1lesson(String activity1lesson) {
        this.activity1lesson = activity1lesson;
    }

    public Boolean getActivity1result() {
        return activity1result;
    }

    public void setActivity1result(Boolean activity1result) {
        this.activity1result = activity1result;
    }

    public Integer getActivity2id() {
        return activity2id;
    }

    public void setActivity2id(Integer activity2id) {
        this.activity2id = activity2id;
    }
    
    public String getActivity2name() {
        return activity2name;
    }

    public void setActivity2name(String activity2name) {
        this.activity2name = activity2name;
    }

    public String getActivity2lesson() {
        return activity2lesson;
    }

    public void setActivity2lesson(String activity2lesson) {
        this.activity2lesson = activity2lesson;
    }

    public Boolean getActivity2result() {
        return activity2result;
    }

    public void setActivity2result(Boolean activity2result) {
        this.activity2result = activity2result;
    }

    public Integer getActivity3id() {
        return activity3id;
    }

    public void setActivity3id(Integer activity3id) {
        this.activity3id = activity3id;
    }
    
    public String getActivity3name() {
        return activity3name;
    }

    public void setActivity3name(String activity3name) {
        this.activity3name = activity3name;
    }

    public String getActivity3lesson() {
        return activity3lesson;
    }

    public void setActivity3lesson(String activity3lesson) {
        this.activity3lesson = activity3lesson;
    }

    public Boolean getActivity3result() {
        return activity3result;
    }

    public void setActivity3result(Boolean activity3result) {
        this.activity3result = activity3result;
    }

    public Integer getActivity4id() {
        return activity4id;
    }

    public void setActivity4id(Integer activity4id) {
        this.activity4id = activity4id;
    }
    
    public String getActivity4name() {
        return activity4name;
    }

    public void setActivity4name(String activity4name) {
        this.activity4name = activity4name;
    }

    public String getActivity4lesson() {
        return activity4lesson;
    }

    public void setActivity4lesson(String activity4lesson) {
        this.activity4lesson = activity4lesson;
    }

    public Boolean getActivity4result() {
        return activity4result;
    }

    public void setActivity4result(Boolean activity4result) {
        this.activity4result = activity4result;
    }

    public Integer getRatwResult() {
        return ratwResult;
    }

    public void setRatwResult(Integer ratwResult) {
        this.ratwResult = ratwResult;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
    
}
