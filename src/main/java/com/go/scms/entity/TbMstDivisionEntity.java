/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author Sir. Harinn
 */
@Entity
@Table(name = "tbMstDivision")
public class TbMstDivisionEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long divisionId;
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The scCode is a required field.")
    @NotBlank(message="The scCode is a required not empty.")
    private String scCode;
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The divisionNameTh is a required field.")
    @NotBlank(message="The divisionNameTh is a required not empty.")
    private String divisionNameTh;
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The divisionNameEn is a required field.")
    @NotBlank(message="The divisionNameEn is a required not empty.")    
    private String divisionNameEn;
    
    @Column
    private String vision;
    
    @Column
    private String mission;
    
    @Column
    private String strategy;
    
    @Column
    private String goals;
    
    @Column
    private String remark;
    
    @Column
    private Date createdDate;
    
    @Column
    private String createdPage;
    
    @Column
    private String createdUser;
    
    @Column
    private String ipaddr;
    
    @Column
    private Date updatedDate;
    
    @Column
    private String updatedPage;
    
    @Column
    private String updatedUser;
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The recordStatus is a required field.")
    @NotBlank(message="The recordStatus is a required not empty.")
    private String recordStatus;
    
    @Column
    private Boolean isactive;
    
    public Long getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Long divisionId) {
        this.divisionId = divisionId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = "PRAMANDA";
    }

    public String getDivisionNameTh() {
        return divisionNameTh;
    }

    public void setDivisionNameTh(String divisionNameTh) {
        this.divisionNameTh = divisionNameTh;
    }

    public String getDivisionNameEn() {
        return divisionNameEn;
    }

    public void setDivisionNameEn(String divisionNameEn) {
        this.divisionNameEn = divisionNameEn;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String getGoals() {
        return goals;
    }

    public void setGoals(String goals) {
        this.goals = goals;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
}
