package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbMstClassRoom")
public class SnickClassRoomEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer classRoomId;

    @Column
    private String scCode;

    @ManyToOne 
    @JoinColumn(name="classId")
    private ClassEntity classJoin = new ClassEntity();

        public ClassEntity getClassJoin() {
            return classJoin;
        }

        public void setClassJoin(ClassEntity classJoin) {
            this.classJoin = classJoin;
        }
        
    @Column
    private String classRoom;

    @Column
    private String classRoomLnameTh;

    @Column
    private String classRoomLnameEn;

    @Column
    private String classRoomSnameTh;

    @Column
    private String classRoomSnameEn;
    
    @Column
    private Integer teacherAdvisor1;
    
    @Column
    private Integer teacherAdvisor2;
    
    @Column
    private Integer teacherAdvisor3;
    @Column
    private String recordStatus;

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomLnameTh() {
        return classRoomLnameTh;
    }

    public void setClassRoomLnameTh(String classRoomLnameTh) {
        this.classRoomLnameTh = classRoomLnameTh;
    }

    public String getClassRoomLnameEn() {
        return classRoomLnameEn;
    }

    public void setClassRoomLnameEn(String classRoomLnameEn) {
        this.classRoomLnameEn = classRoomLnameEn;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getClassRoomSnameEn() {
        return classRoomSnameEn;
    }

    public void setClassRoomSnameEn(String classRoomSnameEn) {
        this.classRoomSnameEn = classRoomSnameEn;
    }

    public Integer getTeacherAdvisor1() {
        return teacherAdvisor1;
    }

    public void setTeacherAdvisor1(Integer teacherAdvisor1) {
        this.teacherAdvisor1 = teacherAdvisor1;
    }

    public Integer getTeacherAdvisor2() {
        return teacherAdvisor2;
    }

    public void setTeacherAdvisor2(Integer teacherAdvisor2) {
        this.teacherAdvisor2 = teacherAdvisor2;
    }

    public Integer getTeacherAdvisor3() {
        return teacherAdvisor3;
    }

    public void setTeacherAdvisor3(Integer teacherAdvisor3) {
        this.teacherAdvisor3 = teacherAdvisor3;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }
}
