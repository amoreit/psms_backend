package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="tbMstSubjectType")
public class SnickMstSubjectTypeEntity {
    
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long subjectTypeId;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The subjectTypeCode is a required field.")
	@NotBlank(message="The subjectTypeCode is a required not empty.")
	private String subjectTypeCode; 
        
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The name is a required field.")
	@NotBlank(message="The name is a required not empty.")
	private String name;

        @Column(nullable = false,unique = true)
	@NotEmpty(message="The scCode is a required field.")
	@NotBlank(message="The scCode is a required not empty.")
	private String scCode;
	
	@Column(nullable = false)
	@NotEmpty(message="The status is a required field.")
	@NotBlank(message="The status is a required not empty.")
	private String recordStatus;
	
	@Column(updatable = false)
	private String createdPage;
	
	@Column(updatable = false)
	private Date createdDate;
        
        @Column(updatable = false)
	private String createdUser;
	
	@Column
	private String updatedPage;
	
	@Column
	private Date updatedDate;
        
        @Column
	private String updatedUser;

        public Long getSubjectTypeId() {
            return subjectTypeId;
        }

        public void setSubjectTypeId(Long subjectTypeId) {
            this.subjectTypeId = subjectTypeId;
        }

        public String getSubjectTypeCode() {
            return subjectTypeCode;
        }

        public void setSubjectTypeCode(String subjectTypeCode) {
            this.subjectTypeCode = subjectTypeCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = "PRAMANDA";
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }
    
        @PrePersist
	private void insert() {
		this.createdDate = new Date();
	}       
}
