package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnClassRoomSubject")
public class SnickClassRoomSubjectEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classRoomSubjectId;

    @ManyToOne
    @JoinColumn(name = "yearGroupId")
    private SnickYearGroupEntity yearGroupJoin = new SnickYearGroupEntity();

        public SnickYearGroupEntity getYearGroupJoin() {
            return yearGroupJoin;
        }

        public void setYearGroupJoin(SnickYearGroupEntity yearGroupJoin) {
            this.yearGroupJoin = yearGroupJoin;
        }

    @ManyToOne
    @JoinColumn(name = "classRoomId")
    private SnickClassRoomEntity classRoomJoin = new SnickClassRoomEntity();

        public SnickClassRoomEntity getClassRoomJoin() {
            return classRoomJoin;
        }

        public void setClassRoomJoin(SnickClassRoomEntity classRoomJoin) {
            this.classRoomJoin = classRoomJoin;
        }

    @ManyToOne
    @JoinColumn(name = "departmentId")
    private DepartmentEntity departmentJoin = new DepartmentEntity();

        public DepartmentEntity getDepartmentJoin() {
            return departmentJoin;
        }

        public void setDepartmentJoin(DepartmentEntity departmentJoin) {
            this.departmentJoin = departmentJoin;
        }
    
    @Column
    private String scCode;
    
    @Column
    private String subjectCode;
    
    @Column
    private String subjectNameTh;
    
    @Column
    private String subjectNameEn;
    
    @Column
    private Integer scoreId;
    
    @Column
    private Integer teacher1;
    
    @Column
    private Integer teacher2;
    
    @Column
    private Integer teacher3;
    
    @Column
    private Integer orders;
    
    @Column
    private Date createdDate;
    
    @Column
    private String createdPage;
    
    @Column
    private String createdUser;
    
    @Column
    private String ipaddr;
    
    @Column
    private Date updatedDate;
    
    @Column
    private String updatedPage;
    
    @Column
    private String updatedUser;
    
    @Column
    private Boolean isactive;
    
    @Column
    private String recordStatus;
    
    @Column
    private Integer credit;
    
    @Column
    private String lesson;

    public Long getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(Long classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }

    public String getSubjectNameEn() {
        return subjectNameEn;
    }

    public void setSubjectNameEn(String subjectNameEn) {
        this.subjectNameEn = subjectNameEn;
    }

    public Integer getScoreId() {
        return scoreId;
    }

    public void setScoreId(Integer scoreId) {
        this.scoreId = scoreId;
    }

    public Integer getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(Integer teacher1) {
        this.teacher1 = teacher1;
    }

    public Integer getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(Integer teacher2) {
        this.teacher2 = teacher2;
    }

    public Integer getTeacher3() {
        return teacher3;
    }

    public void setTeacher3(Integer teacher3) {
        this.teacher3 = teacher3;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }
    
}
