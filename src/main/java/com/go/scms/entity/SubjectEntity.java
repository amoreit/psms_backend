/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="tbMstSubject")
public class SubjectEntity {
    
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long subjectId;
	
	@Column	
	private String scCode;	
        
        @Column	
	private Integer departmentId;	

        @Column
	private String subjectCode;
        
        @Column
	private String subjectCode2;
        
        @Column
        private String nameTh;

        @Column
    	private Integer orders;
        
        @Column
        private String nameEn;
        
        @Column
        private String nameShort;
        
        @Column	
	private Integer subjectType;	

        @Column
        private Double credit;
        
        @Column
        private String lesson;
        
        @Column	
	private Integer scale;	
        
        @Column
        private Date createdDate;
        
        @Column
        private String createdPage;
        
        @Column
        private String createdUser;
        
        @Column
        private String ipaddr;
        
        @Column
        private Date updatedDate;
        
        @Column
        private String updatedPage;
        
        @Column
        private String updatedUser;
        
        @Column
        private String recordStatus;
        
        @Column
        private Boolean isactive;

        public Long getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(Long subjectId) {
            this.subjectId = subjectId;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public Integer getDepartmentId() {
            return departmentId;
        }

        public void setDepartmentId(Integer departmentId) {
            this.departmentId = departmentId;
        }

        public Integer getSubjectType() {
            return subjectType;
        }

        public void setSubjectType(Integer subjectType) {
            this.subjectType = subjectType;
        }

        public Integer getScale() {
            return scale;
        }

        public void setScale(Integer scale) {
            this.scale = scale;
        }

        public String getSubjectCode() {
            return subjectCode;
        }

        public void setSubjectCode(String subjectCode) {
            this.subjectCode = subjectCode;
        }

        public String getSubjectCode2() {
            return subjectCode2;
        }

        public void setSubjectCode2(String subjectCode2) {
            this.subjectCode2 = subjectCode2;
        }

        public String getNameTh() {
            return nameTh;
        }

        public void setNameTh(String nameTh) {
            this.nameTh = nameTh;
        }

        public Integer getOrders() {
            return orders;
        }

        public void setOrders(Integer orders) {
            this.orders = orders;
        }

        public String getNameEn() {
            return nameEn;
        }

        public void setNameEn(String nameEn) {
            this.nameEn = nameEn;
        }

        public String getNameShort() {
            return nameShort;
        }

        public void setNameShort(String nameShort) {
            this.nameShort = nameShort;
        }

        public Double getCredit() {
            return credit;
        }

        public void setCredit(Double credit) {
            this.credit = credit;
        }

        public String getLesson() {
            return lesson;
        }

        public void setLesson(String lesson) {
            this.lesson = lesson;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }
}
