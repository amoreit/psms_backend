/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbTrnClassRoomMemberDcs")
public class TbTrnClassRoomMemberDcsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classRoomMemberDcsId;

    @Column
    private String scCode;

    @Column
    private Integer yearGroupId;

    @Column
    private String yearGroupName;

    @Column
    private Integer classRoomMemberId;

    @Column
    private Integer classId;

    @Column
    private String className;

    @Column
    private Integer classRoomId;

    @Column
    private String classRoom;

    @Column
    private String classRoomNo;

    @Column
    private String stuCode;

    @Column
    private String fullname;

    @Column
    private String fullnameEn;

    @Column
    private Integer behavioursSkillsDetailId;

    @Column
    private String item;

    @Column
    private String itemDesc;

    @Column
    private String itemno;

    @Column
    private String description;

    @Column
    private Integer score1;

    @Column
    private Integer score2;

    @Column
    private Integer sumScore;

    @Column
    private String recordStatus;

    @Column
    private String ipaddr;

    @Column
    private String createdUser;

    @Column
    private String createdPage;

    @Column
    private String updatedUser;

    @Column
    private String updatedPage;

    public Long getClassRoomMemberDcsId() {
        return classRoomMemberDcsId;
    }

    public void setClassRoomMemberDcsId(Long classRoomMemberDcsId) {
        this.classRoomMemberDcsId = classRoomMemberDcsId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Integer classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public Integer getBehavioursSkillsDetailId() {
        return behavioursSkillsDetailId;
    }

    public void setBehavioursSkillsDetailId(Integer behavioursSkillsDetailId) {
        this.behavioursSkillsDetailId = behavioursSkillsDetailId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemno() {
        return itemno;
    }

    public void setItemno(String itemno) {
        this.itemno = itemno;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    public Integer getSumScore() {
        return sumScore;
    }

    public void setSumScore(Integer sumScore) {
        this.sumScore = sumScore;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

}
