/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author bamboo
 */
@Entity
@Table(name="tbMstDepartment")

public class DepartmentPaiEntity {
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long departmentId;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The code is a required field.")
	@NotBlank(message="The code is a required not empty.")
	private String departmentCode;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The name is a required field.")
	@NotBlank(message="The name is a required not empty.")
	private String name;
	
	@Column(nullable = false)
	@NotEmpty(message="The status is a required field.")
	@NotBlank(message="The status is a required not empty.")
	private String recordStatus;
        
        @Column(nullable = false,unique = true)
	@NotEmpty(message="The scCode is a required field.")
	@NotBlank(message="The scCode is a required not empty.")
	private String scCode;
        
        @Column(nullable = false,unique = true)
	@NotEmpty(message="The scCode is a required field.")
	@NotBlank(message="The scCode is a required not empty.")
	private String nameShort;
        
        @Column
	private Date updatedDate;
        
        @Column
	private String updatedUser;

        @Column(nullable = false)
        private String createdUser;

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }


        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        @Column(nullable = false)
         private Date createdDate;

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        @Column(nullable = false)
        private String createdPage;


        public Long getDepartmentId() {
            return departmentId;
        }

        public void setDepartmentId(Long departmentId) {
            this.departmentId = departmentId;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        public String getDepartmentCode() {
            return departmentCode;
        }

        public void setDepartmentCode(String departmentCode) {
            this.departmentCode = departmentCode;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = "PRAMANDA";
        }

        public String getNameShort() {
            return nameShort;
        }

        public void setNameShort(String nameShort) {
            this.nameShort = nameShort;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }
        
        @ManyToOne
        @JoinColumn(name="catClass")
	public SnickMstCatClassEntity cattclass = new SnickMstCatClassEntity();

            public SnickMstCatClassEntity getCattclass() {
                return cattclass;
            }

            public void setCattclass(SnickMstCatClassEntity cattclass) {
                this.cattclass = cattclass;
            }
            
        @PrePersist
	private void insert() {
            this.createdDate = new Date();
	}

}
