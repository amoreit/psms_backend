package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="tbMstCatClass")
public class SnickMstCatClassEntity {
    
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer catClassId;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The lnameTh is a required field.")
	@NotBlank(message="The lnameTh is a required not empty.")
	private String lnameTh; 
        
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The lnameEn is a required field.")
	@NotBlank(message="The lnameEn is a required not empty.")
	private String lnameEn;
	
	@Column(updatable = false)
	private String createdPage;
	
	@Column(updatable = false)
	private Date createdDate;
        
        @Column(updatable = false)
	private String createdUser;
	
	@Column
	private String updatedPage;
	
	@Column
	private Date updatedDate;
        
        @Column
	private String updatedUser;

        @Column
        private String recordStatus;
        
        public Integer getCatClassId() {
            return catClassId;
        }

        public void setCatClassId(Integer catClassId) {
            this.catClassId = catClassId;
        }

        public String getLnameTh() {
            return lnameTh;
        }

        public void setLnameTh(String lnameTh) {
            this.lnameTh = lnameTh;
        }

        public String getLnameEn() {
            return lnameEn;
        }

        public void setLnameEn(String lnameEn) {
            this.lnameEn = lnameEn;
        }      

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }           

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }            
}
