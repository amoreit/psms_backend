/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author Sir. Harinn
 */
@Entity
@Table(name = "tbMstSubDivision")
public class TbMstSubDivisionEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long subDivisionId;
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The scCode is a required field.")
    @NotBlank(message="The scCode is a required not empty.")
    private String scCode;
    
    @ManyToOne
    @JoinColumn(name = "divisionId")
    private TbMstDivisionEntity division = new TbMstDivisionEntity();  
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The subDivisionNameTh is a required field.")
    @NotBlank(message="The subDivisionNameTh is a required not empty.")
    private String subDivisionNameTh;
    
    @Column(nullable = true)
    private String subDivisionNameEn;
    
    @Column
    private Date createdDate;
    
    @Column
    private String createdPage;
    
    @Column
    private String createdUser;
    
    @Column
    private String ipaddr;
    
    @Column
    private Date updatedDate;
    
    @Column
    private String updatedPage;
    
    @Column
    private String updatedUser;
    
    @Column(nullable = false,unique = true)
    @NotEmpty(message="The recordStatus is a required field.")
    @NotBlank(message="The recordStatus is a required not empty.")
    private String recordStatus;
    
    @Column
    private Boolean isactive;

    
    public Long getSubDivisionId() {
        return subDivisionId;
    }

    public void setSubDivisionId(Long subDivisionId) {
        this.subDivisionId = subDivisionId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = "PRAMANDA";
    }

    public TbMstDivisionEntity getDivision() {
        return division;
    }

    public void setDivision(TbMstDivisionEntity division) {
        this.division = division;
    }
    
    public String getSubDivisionNameTh() {
        return subDivisionNameTh;
    }

    public void setSubDivisionNameTh(String subDivisionNameTh) {
        this.subDivisionNameTh = subDivisionNameTh;
    }

    public String getSubDivisionNameEn() {
        return subDivisionNameEn;
    }

    public void setSubDivisionNameEn(String subDivisionNameEn) {
        this.subDivisionNameEn = subDivisionNameEn;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
    
}
