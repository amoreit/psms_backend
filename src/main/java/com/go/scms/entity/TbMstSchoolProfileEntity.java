/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Kittipong
 */
@Entity
@Table(name="tbMstSchoolProfile")
public class TbMstSchoolProfileEntity {
    
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long schoolProfileId;
        
        @Column
        private String scCode;
        
        @Column
        private String scName;
        
        @Column
        private String scNameEn;
        
        @Column
        private String scType;
        
        @Column
        private String scRegisterAuthorized;
        
        @Column
        private String scRegisterNo;
        
        @Column
        private Date scDoe;
        
        @Column
        private String scMotto;
        
        @Column
        private String scVision;
        
        @Column
        private String scMission;
        
        @Column
        private String scAbout;
        
        @Column
        private String scLogo;
        
        @Column
        private String scAnnounce;
        
        @Column
        private String scAnnounceStaff;
        
        @Column
        private String scAnnounceStudent;
        
        @Column
        private String scBankAccount;
        
        @Column
        private String addrNo;
        
        @Column
        private String village;
        
        @Column
        private String villageNo;
        
        @Column
        private String alley;
        
        @Column
        private String road;
        
        @Column
        private String subDistrict;
        
        @Column
        private String district;
        
        @Column
        private String province;
        
        @Column
        private String country;
        
        @Column
        private String postCode;
        
        @Column
        private String tel;
        
        @Column
        private String tel2;
        
        @Column
        private String tel3;
        
        @Column
        private String fax;
        
        @Column
        private String email;
        
        @Column
        private String email2;
        
        @Column
        private String email3;
        
        @Column
        private String website;
        
        @Column
        private String facebook;
        
        @Column
        private String twitter;
        
        @Column
        private Date createdDate;
        
        @Column
        private String createdPage;
        
        @Column
        private String createdUser;
        
        @Column
        private String ipaddr;
        
        @Column
        private Date updatedDate;
        
        @Column
        private String updatedPage;
        
        @Column
        private String updatedUser;
        
        @Column
        private String recordStatus;
        
        @Column
        private Boolean isactive;

        public Long getSchoolProfileId() {
            return schoolProfileId;
        }

        public void setSchoolProfileId(Long schoolProfileId) {
            this.schoolProfileId = schoolProfileId;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getScName() {
            return scName;
        }

        public void setScName(String scName) {
            this.scName = scName;
        }

        public String getScNameEn() {
            return scNameEn;
        }

        public void setScNameEn(String scNameEn) {
            this.scNameEn = scNameEn;
        }

        public String getScType() {
            return scType;
        }

        public void setScType(String scType) {
            this.scType = scType;
        }

        public String getScRegisterAuthorized() {
            return scRegisterAuthorized;
        }

        public void setScRegisterAuthorized(String scRegisterAuthorized) {
            this.scRegisterAuthorized = scRegisterAuthorized;
        }

        public String getScRegisterNo() {
            return scRegisterNo;
        }

        public void setScRegisterNo(String scRegisterNo) {
            this.scRegisterNo = scRegisterNo;
        }

        public Date getScDoe() {
            return scDoe;
        }

        public void setScDoe(Date scDoe) {
            this.scDoe = scDoe;
        }

        public String getScMotto() {
            return scMotto;
        }

        public void setScMotto(String scMotto) {
            this.scMotto = scMotto;
        }

        public String getScVision() {
            return scVision;
        }

        public void setScVision(String scVision) {
            this.scVision = scVision;
        }

        public String getScMission() {
            return scMission;
        }

        public void setScMission(String scMission) {
            this.scMission = scMission;
        }

        public String getScAbout() {
            return scAbout;
        }

        public void setScAbout(String scAbout) {
            this.scAbout = scAbout;
        }

        public String getScLogo() {
            return scLogo;
        }

        public void setScLogo(String scLogo) {
            this.scLogo = scLogo;
        }

        public String getScAnnounce() {
            return scAnnounce;
        }

        public void setScAnnounce(String scAnnounce) {
            this.scAnnounce = scAnnounce;
        }

        public String getScAnnounceStaff() {
            return scAnnounceStaff;
        }

        public void setScAnnounceStaff(String scAnnounceStaff) {
            this.scAnnounceStaff = scAnnounceStaff;
        }

        public String getScAnnounceStudent() {
            return scAnnounceStudent;
        }

        public void setScAnnounceStudent(String scAnnounceStudent) {
            this.scAnnounceStudent = scAnnounceStudent;
        }

        public String getScBankAccount() {
            return scBankAccount;
        }

        public void setScBankAccount(String scBankAccount) {
            this.scBankAccount = scBankAccount;
        }

        public String getAddrNo() {
            return addrNo;
        }

        public void setAddrNo(String addrNo) {
            this.addrNo = addrNo;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getVillageNo() {
            return villageNo;
        }

        public void setVillageNo(String villageNo) {
            this.villageNo = villageNo;
        }

        public String getAlley() {
            return alley;
        }

        public void setAlley(String alley) {
            this.alley = alley;
        }

        public String getRoad() {
            return road;
        }

        public void setRoad(String road) {
            this.road = road;
        }

        public String getSubDistrict() {
            return subDistrict;
        }

        public void setSubDistrict(String subDistrict) {
            this.subDistrict = subDistrict;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getTel2() {
            return tel2;
        }

        public void setTel2(String tel2) {
            this.tel2 = tel2;
        }

        public String getTel3() {
            return tel3;
        }

        public void setTel3(String tel3) {
            this.tel3 = tel3;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail2() {
            return email2;
        }

        public void setEmail2(String email2) {
            this.email2 = email2;
        }

        public String getEmail3() {
            return email3;
        }

        public void setEmail3(String email3) {
            this.email3 = email3;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        } 
}
