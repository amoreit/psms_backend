/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author kronn
 */
@Entity
@Table(name="tbMstAuthorized")
public class TbMstAuthorizedEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long authorizedId;
    
    @Column
    private Boolean iscurrent;
    
    @Column
    private String scCode;
   
    @Column
    private String fullnameTh;
   
    @Column
    private String fullnameEn;
   
    @Column
    private String signatureImagePath;
   
    @Column
    private String position;
   
    @Column
    private Date createdDate;
   
    @Column
    private String createdPage;
   
    @Column
    private String createdUser;
   
    @Column
    private String ipaddr;
   
    @Column
    private String updatedUser;
   
    @Column
    private Date updatedDate;
   
    @Column
    private String updatedPage;
   
    @Column
    private Boolean isactive;
   
    @Column
    private String langCode;
   
    @Column
    private String recordStatus;

    public Boolean getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(Boolean iscurrent) {
        this.iscurrent = iscurrent;
    }

    public Long getAuthorizedId() {
        return authorizedId;
    }

    public void setAuthorizedId(Long authorizedId) {
        this.authorizedId = authorizedId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getFullnameTh() {
        return fullnameTh.trim();
    }

    public void setFullnameTh(String fullnameTh) {
        this.fullnameTh = fullnameTh.trim();
    }

    public String getFullnameEn() {
        return fullnameEn.trim();
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn.trim();
    }

    public String getSignatureImagePath() {
        return signatureImagePath;
    }

    public void setSignatureImagePath(String signatureImagePath) {
        this.signatureImagePath = signatureImagePath;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }
}
