/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="tbStuTimeAttendance")
public class TbStuTimeAttendanceEntity {
    
        @Id
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long stuTimeAttendanceId;

        @Column(nullable = false,unique = true)
        private Date checkTime;
        
        @Column
        private String cardId;  
        
        @Column
        private Integer yearGroupId;
        
        @Column
        private String scCode;
        
        @Column
        private String stuCode;
        
        @Column
        private String fullName;
        
        @Column
        private String doorName;
        
        @Column
        private String recordStatus;
        
        @Column
	private Date updatedDate;

        @Column
	private String createdUser;

        @Column
	private String updatedUser;
        
        @Column
	private Boolean isactive;
        
        @Column
        private Date createdDate;
        
        @Column
        private Boolean islate;

        public String getStuCode() {
            return stuCode;
        }

        public void setStuCode(String stuCode) {
            this.stuCode = stuCode;
        } 

        public String getDoorName() {
            return doorName;
        }

        public void setDoorName(String doorName) {
            this.doorName = doorName;
        }

        public Integer getYearGroupId() {
            return yearGroupId;
        }

        public void setYearGroupId(Integer yearGroupId) {
            this.yearGroupId = yearGroupId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Long getStuTimeAttendanceId() {
            return stuTimeAttendanceId;
        }

        public void setStuTimeAttendanceId(Long stuTimeAttendanceId) {
            this.stuTimeAttendanceId = stuTimeAttendanceId;
        }

        public Date getCheckTime() {
            return checkTime;
        }

        public void setCheckTime(Date checkTime) {
            this.checkTime = checkTime;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public Boolean getIslate() {
            return islate;
        }

        public void setIslate(Boolean islate) {
            this.islate = islate;
        }
}
