/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name = "tbStuProfileDoc")
public class StuProfileDocEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stuProfileDocId;

    @Column
    private String scCode;

    @Column
    private Integer stuProfileId;

    @Column
    private String stuCode;

    @Column
    private Integer itemno;

    @Column
    private String dataPath;

    @Column
    private String docType;

    @Column
    private String recordStatus;

    @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private Boolean isactive;

    @Column
    private String dataPathImage;

    @Column
    private String dataPathBirth;

    @Column
    private String dataPathRegister;

    @Column
    private String dataPathReligion;

    @Column
    private String dataPathChangeName;

    @Column
    private String dataPathTranscript7;

    @Column
    private String dataPathTranscript1;

    @Column
    private String dataPathRecord8;

    @Column
    private String dataPathTransfer;

    @Column
    private String dataPathFatherRegister;

    @Column
    private String dataPathFatherCitizenId;

    @Column
    private String dataPathMotherRegister;

    @Column
    private String dataPathMotherCitizenId;

    @Column
    private String dataPathParentRegister;

    @Column
    private String dataPathParentCitizenId;

    public Long getStuProfileDocId() {
        return stuProfileDocId;
    }

    public void setStuProfileDocId(Long stuProfileDocId) {
        this.stuProfileDocId = stuProfileDocId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(Integer stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getDataPathImage() {
        return dataPathImage;
    }

    public void setDataPathImage(String dataPathImage) {
        this.dataPathImage = dataPathImage;
    }

    public String getDataPathBirth() {
        return dataPathBirth;
    }

    public void setDataPathBirth(String dataPathBirth) {
        this.dataPathBirth = dataPathBirth;
    }

    public String getDataPathRegister() {
        return dataPathRegister;
    }

    public void setDataPathRegister(String dataPathRegister) {
        this.dataPathRegister = dataPathRegister;
    }

    public String getDataPathReligion() {
        return dataPathReligion;
    }

    public void setDataPathReligion(String dataPathReligion) {
        this.dataPathReligion = dataPathReligion;
    }

    public String getDataPathChangeName() {
        return dataPathChangeName;
    }

    public void setDataPathChangeName(String dataPathChangeName) {
        this.dataPathChangeName = dataPathChangeName;
    }

    public String getDataPathTranscript7() {
        return dataPathTranscript7;
    }

    public void setDataPathTranscript7(String dataPathTranscript7) {
        this.dataPathTranscript7 = dataPathTranscript7;
    }

    public String getDataPathTranscript1() {
        return dataPathTranscript1;
    }

    public void setDataPathTranscript1(String dataPathTranscript1) {
        this.dataPathTranscript1 = dataPathTranscript1;
    }

    public String getDataPathRecord8() {
        return dataPathRecord8;
    }

    public void setDataPathRecord8(String dataPathRecord8) {
        this.dataPathRecord8 = dataPathRecord8;
    }

    public String getDataPathTransfer() {
        return dataPathTransfer;
    }

    public void setDataPathTransfer(String dataPathTransfer) {
        this.dataPathTransfer = dataPathTransfer;
    }

    public String getDataPathFatherRegister() {
        return dataPathFatherRegister;
    }

    public void setDataPathFatherRegister(String dataPathFatherRegister) {
        this.dataPathFatherRegister = dataPathFatherRegister;
    }

    public String getDataPathFatherCitizenId() {
        return dataPathFatherCitizenId;
    }

    public void setDataPathFatherCitizenId(String dataPathFatherCitizenId) {
        this.dataPathFatherCitizenId = dataPathFatherCitizenId;
    }

    public String getDataPathMotherRegister() {
        return dataPathMotherRegister;
    }

    public void setDataPathMotherRegister(String dataPathMotherRegister) {
        this.dataPathMotherRegister = dataPathMotherRegister;
    }

    public String getDataPathMotherCitizenId() {
        return dataPathMotherCitizenId;
    }

    public void setDataPathMotherCitizenId(String dataPathMotherCitizenId) {
        this.dataPathMotherCitizenId = dataPathMotherCitizenId;
    }

    public String getDataPathParentRegister() {
        return dataPathParentRegister;
    }

    public void setDataPathParentRegister(String dataPathParentRegister) {
        this.dataPathParentRegister = dataPathParentRegister;
    }

    public String getDataPathParentCitizenId() {
        return dataPathParentCitizenId;
    }

    public void setDataPathParentCitizenId(String dataPathParentCitizenId) {
        this.dataPathParentCitizenId = dataPathParentCitizenId;
    }

}
