/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbStuProfile")
public class TbStuProfileEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stuProfileId;

    @Column
    private String scCode;

    @Column
    private Integer requestId;

    @Column
    private String stuCode;

    @Column
    private String className;

    @Column
    private String classRoom;

    @Column
    private String title;

    @Column
    private String titleEn;

    @Column
    private String citizenId;

    @Column
    private String firstnameTh;

    @Column
    private String lastnameTh;

    @Column
    private String firstnameEn;

    @Column
    private String lastnameEn;

    @Column
    private String nickname;

    @Column
    private String fullname;

    @Column
    private String fullnameEn;

    @ManyToOne
    @JoinColumn(name = "genderId")
    private TbMstGenderEntity genderJoin = new TbMstGenderEntity();

            public TbMstGenderEntity getGenderJoin() {
                return genderJoin;
            }

            public void setGenderJoin(TbMstGenderEntity genderJoin) {
                this.genderJoin = genderJoin;
            }

    @Column
    private String gender;

    @Column
    private String regHouseNo;

    @Column
    private String regAddrNo;

    @Column
    private String regVillage;

    @Column
    private String regVillageNo;

    @Column
    private String regRoad;

    @Column
    private String regSubDistrict;

    @Column
    private String regDistrict;

    @Column
    private String regProvince;

    @Column
    private String regCountry;

    @Column
    private String regPostCode;

    @Column
    private String curAddrNo;

    @Column
    private String curVillage;

    @Column
    private String curVillageNo;

    @Column
    private String curRoad;

    @Column
    private String curSubDistrict;

    @Column
    private String curDistrict;

    @Column
    private String curProvince;

    @Column
    private String curCountry;

    @Column
    private String curPostCode;

    @Column
    private String ethnicity;

    @Column
    private String citizenship;

    @Column
    private String religion;

    @Column
    private String saint;

    @Column
    private String church;

    @Column
    private Integer noOfRelatives;

    @Column
    private String relativesStuCode1;

    @Column
    private String relativesStuCode2;

    @Column
    private String relativesStuCode3;

    @Column
    private Float exGpa;

    @Column
    private Date dob;

    @Column
    private String email;

    @Column
    private String tel1;

    @Column
    private String tel2;

    @Column
    private String bloodType;

    @Column
    private String weight;

    @Column
    private String height;

    @Column
    private Boolean isDisabled;

    @Column
    private String disability;

    @Column
    private String pob;

    @Column
    private String pobSubDistrict;

    @Column
    private String pobDistrict;

    @Column
    private String pobProvince;

    @Column
    private String stuImage;

    @Column
    private String toClass;

    @Column
    private String toYearGroup;

    @Column
    private String exSchool;

    @Column
    private String lastClass;

    @Column
    private String specialSkill;

    @Column
    private String medicalInfo;

    @ManyToOne
    @JoinColumn(name = "stuStatusId")
    private TbMstStuStatusEntity stuStatusJoin = new TbMstStuStatusEntity();

            public TbMstStuStatusEntity getStuStatusJoin() {
                return stuStatusJoin;
            }

            public void setStuStatusJoin(TbMstStuStatusEntity stuStatusJoin) {
                this.stuStatusJoin = stuStatusJoin;
            }
            
    @ManyToOne
    @JoinColumn(name = "classId")
    private ClassEntity classJoin = new ClassEntity();

            public ClassEntity getClassJoin() {
                return classJoin;
            }

            public void setClassJoin(ClassEntity classJoin) {
                this.classJoin = classJoin;
            }

    @ManyToOne
    @JoinColumn(name = "classRoomId")
    private MaxClassRoomEntity classRoomJoin = new MaxClassRoomEntity();

            public MaxClassRoomEntity getClassRoomJoin() {
                return classRoomJoin;
            }

            public void setClassRoomJoin(MaxClassRoomEntity classRoomJoin) {
                this.classRoomJoin = classRoomJoin;
            }

    @Column
    private String stuStatus;

    @Column
    private String userName;

    @Column
    private String password;

    @Column
    private String fatherTitle;

    @Column
    private String fatherTitleEn;

    @Column
    private String fatherFirstnameTh;

    @Column
    private String fatherLastnameTh;

    @Column
    private String fatherFirstnameEn;

    @Column
    private String fatherLastnameEn;

    @Column
    private String fatherReligion;

    @Column
    private String fatherSaint;

    @Column
    private String fatherEthnicity;

    @Column
    private String fatherCitizenship;

    @Column
    private String fatherCitizenId;

    @Column
    private String fatherOccupation;

    @Column
    private String fatherOffice;

    @Column
    private String fatherEducation;

    @Column
    private Integer fatherAnnualIncome;

    @Column
    private String fatherAddrNo;

    @Column
    private String fatherVillage;

    @Column
    private String fatherVillageNo;

    @Column
    private String fatherRoad;

    @Column
    private String fatherSubDistrict;

    @Column
    private String fatherDistrict;

    @Column
    private String fatherProvince;

    @Column
    private String fatherCountry;

    @Column
    private String fatherPostCode;

    @Column
    private String fatherEmail;

    @Column
    private String fatherTel1;

    @Column
    private String fatherTel2;

    @Column
    private String fatherStuImage;

    @Column
    private Boolean fatherAlive;

    @Column
    private String motherTitle;

    @Column
    private String motherTitleEn;

    @Column
    private String motherFirstnameTh;

    @Column
    private String motherLastnameTh;

    @Column
    private String motherFirstnameEn;

    @Column
    private String motherLastnameEn;

    @Column
    private String motherReligion;

    @Column
    private String motherSaint;

    @Column
    private String motherEthnicity;

    @Column
    private String motherCitizenship;

    @Column
    private String motherCitizenId;

    @Column
    private String motherOccupation;

    @Column
    private String motherOffice;

    @Column
    private String motherEducation;

    @Column
    private Integer motherAnnualIncome;

    @Column
    private String motherAddrNo;

    @Column
    private String motherVillage;

    @Column
    private String motherVillageNo;

    @Column
    private String motherRoad;

    @Column
    private String motherSubDistrict;

    @Column
    private String motherDistrict;

    @Column
    private String motherProvince;

    @Column
    private String motherCountry;

    @Column
    private String motherPostCode;

    @Column
    private String motherEmail;

    @Column
    private String motherTel1;

    @Column
    private String motherTel2;

    @Column
    private String motherStuImage;

    @Column
    private Boolean motherAlive;

    @Column
    private String parentTitle;

    @Column
    private String parentTitleEn;

    @Column
    private String parentFirstnameTh;

    @Column
    private String parentLastnameTh;

    @Column
    private String parentFirstnameEn;

    @Column
    private String parentLastnameEn;

    @Column
    private String parentReligion;

    @Column
    private String parentSaint;

    @Column
    private String parentEthnicity;

    @Column
    private String parentCitizenship;

    @Column
    private String parentCitizenId;

    @Column
    private String parentOccupation;

    @Column
    private String parentOffice;

    @Column
    private String parentEducation;

    @Column
    private Integer parentAnnualIncome;

    @Column
    private String parentAddrNo;

    @Column
    private String parentVillage;

    @Column
    private String parentVillageNo;

    @Column
    private String parentRoad;

    @Column
    private String parentSubDistrict;

    @Column
    private String parentDistrict;

    @Column
    private String parentProvince;

    @Column
    private String parentCountry;

    @Column
    private String parentPostCode;

    @Column
    private String parentEmail;

    @Column
    private String parentTel1;

    @Column
    private String parentTel2;

    @Column
    private String parentStuImage;

    @Column
    private String parentStatus;

    @Column
    private String familyStatus;

    @Column
    private Boolean isactive;

    @Column
    private String recordStatus;

    @Column
    private String updatedUser;

    @Column
    private String updatedPage;

    @Column
    private Date updatedDate;

    @Column
    private String createdUser;

    @Column
    private String createdPage;

    @Column
    private Date createdDate;

    @Column
    private String ipaddr;

    @Column
    private String regAlley;

    @Column
    private String curAlley;

    @Column
    private String fatherAlley;

    @Column
    private String motherAlley;

    @Column
    private String parentAlley;

    public Long getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(Long stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(String firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRegHouseNo() {
        return regHouseNo;
    }

    public void setRegHouseNo(String regHouseNo) {
        this.regHouseNo = regHouseNo;
    }

    public String getRegAddrNo() {
        return regAddrNo;
    }

    public void setRegAddrNo(String regAddrNo) {
        this.regAddrNo = regAddrNo;
    }

    public String getRegVillage() {
        return regVillage;
    }

    public void setRegVillage(String regVillage) {
        this.regVillage = regVillage;
    }

    public String getRegVillageNo() {
        return regVillageNo;
    }

    public void setRegVillageNo(String regVillageNo) {
        this.regVillageNo = regVillageNo;
    }

    public String getRegRoad() {
        return regRoad;
    }

    public void setRegRoad(String regRoad) {
        this.regRoad = regRoad;
    }

    public String getRegSubDistrict() {
        return regSubDistrict;
    }

    public void setRegSubDistrict(String regSubDistrict) {
        this.regSubDistrict = regSubDistrict;
    }

    public String getRegDistrict() {
        return regDistrict;
    }

    public void setRegDistrict(String regDistrict) {
        this.regDistrict = regDistrict;
    }

    public String getRegProvince() {
        return regProvince;
    }

    public void setRegProvince(String regProvince) {
        this.regProvince = regProvince;
    }

    public String getRegCountry() {
        return regCountry;
    }

    public void setRegCountry(String regCountry) {
        this.regCountry = regCountry;
    }

    public String getRegPostCode() {
        return regPostCode;
    }

    public void setRegPostCode(String regPostCode) {
        this.regPostCode = regPostCode;
    }

    public String getCurAddrNo() {
        return curAddrNo;
    }

    public void setCurAddrNo(String curAddrNo) {
        this.curAddrNo = curAddrNo;
    }

    public String getCurVillage() {
        return curVillage;
    }

    public void setCurVillage(String curVillage) {
        this.curVillage = curVillage;
    }

    public String getCurVillageNo() {
        return curVillageNo;
    }

    public void setCurVillageNo(String curVillageNo) {
        this.curVillageNo = curVillageNo;
    }

    public String getCurRoad() {
        return curRoad;
    }

    public void setCurRoad(String curRoad) {
        this.curRoad = curRoad;
    }

    public String getCurSubDistrict() {
        return curSubDistrict;
    }

    public void setCurSubDistrict(String curSubDistrict) {
        this.curSubDistrict = curSubDistrict;
    }

    public String getCurDistrict() {
        return curDistrict;
    }

    public void setCurDistrict(String curDistrict) {
        this.curDistrict = curDistrict;
    }

    public String getCurProvince() {
        return curProvince;
    }

    public void setCurProvince(String curProvince) {
        this.curProvince = curProvince;
    }

    public String getCurCountry() {
        return curCountry;
    }

    public void setCurCountry(String curCountry) {
        this.curCountry = curCountry;
    }

    public String getCurPostCode() {
        return curPostCode;
    }

    public void setCurPostCode(String curPostCode) {
        this.curPostCode = curPostCode;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getSaint() {
        return saint;
    }

    public void setSaint(String saint) {
        this.saint = saint;
    }

    public String getChurch() {
        return church;
    }

    public void setChurch(String church) {
        this.church = church;
    }

    public Integer getNoOfRelatives() {
        return noOfRelatives;
    }

    public void setNoOfRelatives(Integer noOfRelatives) {
        this.noOfRelatives = noOfRelatives;
    }

    public String getRelativesStuCode1() {
        return relativesStuCode1;
    }

    public void setRelativesStuCode1(String relativesStuCode1) {
        this.relativesStuCode1 = relativesStuCode1;
    }

    public String getRelativesStuCode2() {
        return relativesStuCode2;
    }

    public void setRelativesStuCode2(String relativesStuCode2) {
        this.relativesStuCode2 = relativesStuCode2;
    }

    public String getRelativesStuCode3() {
        return relativesStuCode3;
    }

    public void setRelativesStuCode3(String relativesStuCode3) {
        this.relativesStuCode3 = relativesStuCode3;
    }

    public Float getExGpa() {
        return exGpa;
    }

    public void setExGpa(Float exGpa) {
        this.exGpa = exGpa;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Boolean getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getPobSubDistrict() {
        return pobSubDistrict;
    }

    public void setPobSubDistrict(String pobSubDistrict) {
        this.pobSubDistrict = pobSubDistrict;
    }

    public String getPobDistrict() {
        return pobDistrict;
    }

    public void setPobDistrict(String pobDistrict) {
        this.pobDistrict = pobDistrict;
    }

    public String getPobProvince() {
        return pobProvince;
    }

    public void setPobProvince(String pobProvince) {
        this.pobProvince = pobProvince;
    }

    public String getStuImage() {
        return stuImage;
    }

    public void setStuImage(String stuImage) {
        this.stuImage = stuImage;
    }

    public String getToClass() {
        return toClass;
    }

    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    public String getToYearGroup() {
        return toYearGroup;
    }

    public void setToYearGroup(String toYearGroup) {
        this.toYearGroup = toYearGroup;
    }

    public String getExSchool() {
        return exSchool;
    }

    public void setExSchool(String exSchool) {
        this.exSchool = exSchool;
    }

    public String getLastClass() {
        return lastClass;
    }

    public void setLastClass(String lastClass) {
        this.lastClass = lastClass;
    }

    public String getSpecialSkill() {
        return specialSkill;
    }

    public void setSpecialSkill(String specialSkill) {
        this.specialSkill = specialSkill;
    }

    public String getMedicalInfo() {
        return medicalInfo;
    }

    public void setMedicalInfo(String medicalInfo) {
        this.medicalInfo = medicalInfo;
    }

    public String getStuStatus() {
        return stuStatus;
    }

    public void setStuStatus(String stuStatus) {
        this.stuStatus = stuStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFatherTitle() {
        return fatherTitle;
    }

    public void setFatherTitle(String fatherTitle) {
        this.fatherTitle = fatherTitle;
    }

    public String getFatherTitleEn() {
        return fatherTitleEn;
    }

    public void setFatherTitleEn(String fatherTitleEn) {
        this.fatherTitleEn = fatherTitleEn;
    }

    public String getFatherFirstnameTh() {
        return fatherFirstnameTh;
    }

    public void setFatherFirstnameTh(String fatherFirstnameTh) {
        this.fatherFirstnameTh = fatherFirstnameTh;
    }

    public String getFatherLastnameTh() {
        return fatherLastnameTh;
    }

    public void setFatherLastnameTh(String fatherLastnameTh) {
        this.fatherLastnameTh = fatherLastnameTh;
    }

    public String getFatherFirstnameEn() {
        return fatherFirstnameEn;
    }

    public void setFatherFirstnameEn(String fatherFirstnameEn) {
        this.fatherFirstnameEn = fatherFirstnameEn;
    }

    public String getFatherLastnameEn() {
        return fatherLastnameEn;
    }

    public void setFatherLastnameEn(String fatherLastnameEn) {
        this.fatherLastnameEn = fatherLastnameEn;
    }

    public String getFatherReligion() {
        return fatherReligion;
    }

    public void setFatherReligion(String fatherReligion) {
        this.fatherReligion = fatherReligion;
    }

    public String getFatherSaint() {
        return fatherSaint;
    }

    public void setFatherSaint(String fatherSaint) {
        this.fatherSaint = fatherSaint;
    }

    public String getFatherEthnicity() {
        return fatherEthnicity;
    }

    public void setFatherEthnicity(String fatherEthnicity) {
        this.fatherEthnicity = fatherEthnicity;
    }

    public String getFatherCitizenship() {
        return fatherCitizenship;
    }

    public void setFatherCitizenship(String fatherCitizenship) {
        this.fatherCitizenship = fatherCitizenship;
    }

    public String getFatherCitizenId() {
        return fatherCitizenId;
    }

    public void setFatherCitizenId(String fatherCitizenId) {
        this.fatherCitizenId = fatherCitizenId;
    }

    public String getFatherOccupation() {
        return fatherOccupation;
    }

    public void setFatherOccupation(String fatherOccupation) {
        this.fatherOccupation = fatherOccupation;
    }

    public String getFatherOffice() {
        return fatherOffice;
    }

    public void setFatherOffice(String fatherOffice) {
        this.fatherOffice = fatherOffice;
    }

    public String getFatherEducation() {
        return fatherEducation;
    }

    public void setFatherEducation(String fatherEducation) {
        this.fatherEducation = fatherEducation;
    }

    public Integer getFatherAnnualIncome() {
        return fatherAnnualIncome;
    }

    public void setFatherAnnualIncome(Integer fatherAnnualIncome) {
        this.fatherAnnualIncome = fatherAnnualIncome;
    }

    public String getFatherAddrNo() {
        return fatherAddrNo;
    }

    public void setFatherAddrNo(String fatherAddrNo) {
        this.fatherAddrNo = fatherAddrNo;
    }

    public String getFatherVillage() {
        return fatherVillage;
    }

    public void setFatherVillage(String fatherVillage) {
        this.fatherVillage = fatherVillage;
    }

    public String getFatherVillageNo() {
        return fatherVillageNo;
    }

    public void setFatherVillageNo(String fatherVillageNo) {
        this.fatherVillageNo = fatherVillageNo;
    }

    public String getFatherRoad() {
        return fatherRoad;
    }

    public void setFatherRoad(String fatherRoad) {
        this.fatherRoad = fatherRoad;
    }

    public String getFatherSubDistrict() {
        return fatherSubDistrict;
    }

    public void setFatherSubDistrict(String fatherSubDistrict) {
        this.fatherSubDistrict = fatherSubDistrict;
    }

    public String getFatherDistrict() {
        return fatherDistrict;
    }

    public void setFatherDistrict(String fatherDistrict) {
        this.fatherDistrict = fatherDistrict;
    }

    public String getFatherProvince() {
        return fatherProvince;
    }

    public void setFatherProvince(String fatherProvince) {
        this.fatherProvince = fatherProvince;
    }

    public String getFatherCountry() {
        return fatherCountry;
    }

    public void setFatherCountry(String fatherCountry) {
        this.fatherCountry = fatherCountry;
    }

    public String getFatherPostCode() {
        return fatherPostCode;
    }

    public void setFatherPostCode(String fatherPostCode) {
        this.fatherPostCode = fatherPostCode;
    }

    public String getFatherEmail() {
        return fatherEmail;
    }

    public void setFatherEmail(String fatherEmail) {
        this.fatherEmail = fatherEmail;
    }

    public String getFatherTel1() {
        return fatherTel1;
    }

    public void setFatherTel1(String fatherTel1) {
        this.fatherTel1 = fatherTel1;
    }

    public String getFatherTel2() {
        return fatherTel2;
    }

    public void setFatherTel2(String fatherTel2) {
        this.fatherTel2 = fatherTel2;
    }

    public String getFatherStuImage() {
        return fatherStuImage;
    }

    public void setFatherStuImage(String fatherStuImage) {
        this.fatherStuImage = fatherStuImage;
    }

    public Boolean getFatherAlive() {
        return fatherAlive;
    }

    public void setFatherAlive(Boolean fatherAlive) {
        this.fatherAlive = fatherAlive;
    }

    public String getMotherTitle() {
        return motherTitle;
    }

    public void setMotherTitle(String motherTitle) {
        this.motherTitle = motherTitle;
    }

    public String getMotherTitleEn() {
        return motherTitleEn;
    }

    public void setMotherTitleEn(String motherTitleEn) {
        this.motherTitleEn = motherTitleEn;
    }

    public String getMotherFirstnameTh() {
        return motherFirstnameTh;
    }

    public void setMotherFirstnameTh(String motherFirstnameTh) {
        this.motherFirstnameTh = motherFirstnameTh;
    }

    public String getMotherLastnameTh() {
        return motherLastnameTh;
    }

    public void setMotherLastnameTh(String motherLastnameTh) {
        this.motherLastnameTh = motherLastnameTh;
    }

    public String getMotherFirstnameEn() {
        return motherFirstnameEn;
    }

    public void setMotherFirstnameEn(String motherFirstnameEn) {
        this.motherFirstnameEn = motherFirstnameEn;
    }

    public String getMotherLastnameEn() {
        return motherLastnameEn;
    }

    public void setMotherLastnameEn(String motherLastnameEn) {
        this.motherLastnameEn = motherLastnameEn;
    }

    public String getMotherReligion() {
        return motherReligion;
    }

    public void setMotherReligion(String motherReligion) {
        this.motherReligion = motherReligion;
    }

    public String getMotherSaint() {
        return motherSaint;
    }

    public void setMotherSaint(String motherSaint) {
        this.motherSaint = motherSaint;
    }

    public String getMotherEthnicity() {
        return motherEthnicity;
    }

    public void setMotherEthnicity(String motherEthnicity) {
        this.motherEthnicity = motherEthnicity;
    }

    public String getMotherCitizenship() {
        return motherCitizenship;
    }

    public void setMotherCitizenship(String motherCitizenship) {
        this.motherCitizenship = motherCitizenship;
    }

    public String getMotherCitizenId() {
        return motherCitizenId;
    }

    public void setMotherCitizenId(String motherCitizenId) {
        this.motherCitizenId = motherCitizenId;
    }

    public String getMotherOccupation() {
        return motherOccupation;
    }

    public void setMotherOccupation(String motherOccupation) {
        this.motherOccupation = motherOccupation;
    }

    public String getMotherOffice() {
        return motherOffice;
    }

    public void setMotherOffice(String motherOffice) {
        this.motherOffice = motherOffice;
    }

    public String getMotherEducation() {
        return motherEducation;
    }

    public void setMotherEducation(String motherEducation) {
        this.motherEducation = motherEducation;
    }

    public Integer getMotherAnnualIncome() {
        return motherAnnualIncome;
    }

    public void setMotherAnnualIncome(Integer motherAnnualIncome) {
        this.motherAnnualIncome = motherAnnualIncome;
    }

    public String getMotherAddrNo() {
        return motherAddrNo;
    }

    public void setMotherAddrNo(String motherAddrNo) {
        this.motherAddrNo = motherAddrNo;
    }

    public String getMotherVillage() {
        return motherVillage;
    }

    public void setMotherVillage(String motherVillage) {
        this.motherVillage = motherVillage;
    }

    public String getMotherVillageNo() {
        return motherVillageNo;
    }

    public void setMotherVillageNo(String motherVillageNo) {
        this.motherVillageNo = motherVillageNo;
    }

    public String getMotherRoad() {
        return motherRoad;
    }

    public void setMotherRoad(String motherRoad) {
        this.motherRoad = motherRoad;
    }

    public String getMotherSubDistrict() {
        return motherSubDistrict;
    }

    public void setMotherSubDistrict(String motherSubDistrict) {
        this.motherSubDistrict = motherSubDistrict;
    }

    public String getMotherDistrict() {
        return motherDistrict;
    }

    public void setMotherDistrict(String motherDistrict) {
        this.motherDistrict = motherDistrict;
    }

    public String getMotherProvince() {
        return motherProvince;
    }

    public void setMotherProvince(String motherProvince) {
        this.motherProvince = motherProvince;
    }

    public String getMotherCountry() {
        return motherCountry;
    }

    public void setMotherCountry(String motherCountry) {
        this.motherCountry = motherCountry;
    }

    public String getMotherPostCode() {
        return motherPostCode;
    }

    public void setMotherPostCode(String motherPostCode) {
        this.motherPostCode = motherPostCode;
    }

    public String getMotherEmail() {
        return motherEmail;
    }

    public void setMotherEmail(String motherEmail) {
        this.motherEmail = motherEmail;
    }

    public String getMotherTel1() {
        return motherTel1;
    }

    public void setMotherTel1(String motherTel1) {
        this.motherTel1 = motherTel1;
    }

    public String getMotherTel2() {
        return motherTel2;
    }

    public void setMotherTel2(String motherTel2) {
        this.motherTel2 = motherTel2;
    }

    public String getMotherStuImage() {
        return motherStuImage;
    }

    public void setMotherStuImage(String motherStuImage) {
        this.motherStuImage = motherStuImage;
    }

    public Boolean getMotherAlive() {
        return motherAlive;
    }

    public void setMotherAlive(Boolean motherAlive) {
        this.motherAlive = motherAlive;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentTitleEn() {
        return parentTitleEn;
    }

    public void setParentTitleEn(String parentTitleEn) {
        this.parentTitleEn = parentTitleEn;
    }

    public String getParentFirstnameTh() {
        return parentFirstnameTh;
    }

    public void setParentFirstnameTh(String parentFirstnameTh) {
        this.parentFirstnameTh = parentFirstnameTh;
    }

    public String getParentLastnameTh() {
        return parentLastnameTh;
    }

    public void setParentLastnameTh(String parentLastnameTh) {
        this.parentLastnameTh = parentLastnameTh;
    }

    public String getParentFirstnameEn() {
        return parentFirstnameEn;
    }

    public void setParentFirstnameEn(String parentFirstnameEn) {
        this.parentFirstnameEn = parentFirstnameEn;
    }

    public String getParentLastnameEn() {
        return parentLastnameEn;
    }

    public void setParentLastnameEn(String parentLastnameEn) {
        this.parentLastnameEn = parentLastnameEn;
    }

    public String getParentReligion() {
        return parentReligion;
    }

    public void setParentReligion(String parentReligion) {
        this.parentReligion = parentReligion;
    }

    public String getParentSaint() {
        return parentSaint;
    }

    public void setParentSaint(String parentSaint) {
        this.parentSaint = parentSaint;
    }

    public String getParentEthnicity() {
        return parentEthnicity;
    }

    public void setParentEthnicity(String parentEthnicity) {
        this.parentEthnicity = parentEthnicity;
    }

    public String getParentCitizenship() {
        return parentCitizenship;
    }

    public void setParentCitizenship(String parentCitizenship) {
        this.parentCitizenship = parentCitizenship;
    }

    public String getParentCitizenId() {
        return parentCitizenId;
    }

    public void setParentCitizenId(String parentCitizenId) {
        this.parentCitizenId = parentCitizenId;
    }

    public String getParentOccupation() {
        return parentOccupation;
    }

    public void setParentOccupation(String parentOccupation) {
        this.parentOccupation = parentOccupation;
    }

    public String getParentOffice() {
        return parentOffice;
    }

    public void setParentOffice(String parentOffice) {
        this.parentOffice = parentOffice;
    }

    public String getParentEducation() {
        return parentEducation;
    }

    public void setParentEducation(String parentEducation) {
        this.parentEducation = parentEducation;
    }

    public Integer getParentAnnualIncome() {
        return parentAnnualIncome;
    }

    public void setParentAnnualIncome(Integer parentAnnualIncome) {
        this.parentAnnualIncome = parentAnnualIncome;
    }

    public String getParentAddrNo() {
        return parentAddrNo;
    }

    public void setParentAddrNo(String parentAddrNo) {
        this.parentAddrNo = parentAddrNo;
    }

    public String getParentVillage() {
        return parentVillage;
    }

    public void setParentVillage(String parentVillage) {
        this.parentVillage = parentVillage;
    }

    public String getParentVillageNo() {
        return parentVillageNo;
    }

    public void setParentVillageNo(String parentVillageNo) {
        this.parentVillageNo = parentVillageNo;
    }

    public String getParentRoad() {
        return parentRoad;
    }

    public void setParentRoad(String parentRoad) {
        this.parentRoad = parentRoad;
    }

    public String getParentSubDistrict() {
        return parentSubDistrict;
    }

    public void setParentSubDistrict(String parentSubDistrict) {
        this.parentSubDistrict = parentSubDistrict;
    }

    public String getParentDistrict() {
        return parentDistrict;
    }

    public void setParentDistrict(String parentDistrict) {
        this.parentDistrict = parentDistrict;
    }

    public String getParentProvince() {
        return parentProvince;
    }

    public void setParentProvince(String parentProvince) {
        this.parentProvince = parentProvince;
    }

    public String getParentCountry() {
        return parentCountry;
    }

    public void setParentCountry(String parentCountry) {
        this.parentCountry = parentCountry;
    }

    public String getParentPostCode() {
        return parentPostCode;
    }

    public void setParentPostCode(String parentPostCode) {
        this.parentPostCode = parentPostCode;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentTel1() {
        return parentTel1;
    }

    public void setParentTel1(String parentTel1) {
        this.parentTel1 = parentTel1;
    }

    public String getParentTel2() {
        return parentTel2;
    }

    public void setParentTel2(String parentTel2) {
        this.parentTel2 = parentTel2;
    }

    public String getParentStuImage() {
        return parentStuImage;
    }

    public void setParentStuImage(String parentStuImage) {
        this.parentStuImage = parentStuImage;
    }

    public String getParentStatus() {
        return parentStatus;
    }

    public void setParentStatus(String parentStatus) {
        this.parentStatus = parentStatus;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getRegAlley() {
        return regAlley;
    }

    public void setRegAlley(String regAlley) {
        this.regAlley = regAlley;
    }

    public String getCurAlley() {
        return curAlley;
    }

    public void setCurAlley(String curAlley) {
        this.curAlley = curAlley;
    }

    public String getFatherAlley() {
        return fatherAlley;
    }

    public void setFatherAlley(String fatherAlley) {
        this.fatherAlley = fatherAlley;
    }

    public String getMotherAlley() {
        return motherAlley;
    }

    public void setMotherAlley(String motherAlley) {
        this.motherAlley = motherAlley;
    }

    public String getParentAlley() {
        return parentAlley;
    }

    public void setParentAlley(String parentAlley) {
        this.parentAlley = parentAlley;
    }

}
