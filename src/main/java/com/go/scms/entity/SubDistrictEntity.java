/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name="tbMstSubDistrict")
public class SubDistrictEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer subDistrictId;

    @Column
    private String subDistrictTh;

    @Column
    private String recordStatus;
    
    @Column
    private String postCode;
    
    @ManyToOne
    @JoinColumn(name="districtId")
    private DistrictEntity joinSubDistrict = new DistrictEntity();

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Integer getSubDistrictId() {
        return subDistrictId;
    }

    public void setSubDistrictId(Integer subDistrictId) {
        this.subDistrictId = subDistrictId;
    }

    public String getSubDistrictTh() {
        return subDistrictTh;
    }

    public void setSubDistrictTh(String subDistrictTh) {
        this.subDistrictTh = subDistrictTh;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public DistrictEntity getJoinSubDistrict() {
        return joinSubDistrict;
    }

    public void setJoinSubDistrict(DistrictEntity joinSubDistrict) {
        this.joinSubDistrict = joinSubDistrict;
    }
    
}
