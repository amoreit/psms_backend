package com.go.scms.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="admUserLog")
public class LogUserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userLogId;

    @Column
    private String scCode;

    @Column
    private String loginUser;

    @Column
    private Date loginDate;

    @Column
    private Date logoutDate;

    @Column
    private String fromIpaddr;

    @Column
    private String fromBrowser;

    @Column
    private String fromDevice;

    @Column
    private Boolean loginSuccess;

    @Column
    private Integer loginCode;

    @Column
    private String loginReason;

    public Long getUserLogId() {
        return userLogId;
    }

    public void setUserLogId(Long userLogId) {
        this.userLogId = userLogId;
    }

    public String getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(Date logoutDate) {
        this.logoutDate = logoutDate;
    }

    public String getFromIpaddr() {
        return fromIpaddr;
    }

    public void setFromIpaddr(String fromIpaddr) {
        this.fromIpaddr = fromIpaddr;
    }

    public String getFromBrowser() {
        return fromBrowser;
    }

    public void setFromBrowser(String fromBrowser) {
        this.fromBrowser = fromBrowser;
    }

    public String getFromDevice() {
        return fromDevice;
    }

    public void setFromDevice(String fromDevice) {
        this.fromDevice = fromDevice;
    }

    public Boolean getLoginSuccess() {
        return loginSuccess;
    }

    public void setLoginSuccess(Boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public String getLoginReason() {
        return loginReason;
    }

    public void setLoginReason(String loginReason) {
        this.loginReason = loginReason;
    }

    public Integer getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(Integer loginCode) {
        this.loginCode = loginCode;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }
}
