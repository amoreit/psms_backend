/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name="tbMstDepartment")
public class DepartmentEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer departmentId;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name="catClass")
    private CatClassEntity catclass = new CatClassEntity();

    @Column
    private String recordStatus;

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public CatClassEntity getCatclass() {
        return catclass;
    }

    public void setCatclass(CatClassEntity catclass) {
        this.catclass = catclass;
    }
	
}
