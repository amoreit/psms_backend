package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbMstFamilyStatus")
public class TbMstFamilyStatusEntity {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long familyStatusId;

        @Column
        private String statusDesc;

        @Column
        private String statusDescEn;

        @Column
        private String recordStatus;

        public Long getFamilyStatusId() {
            return familyStatusId;
        }

        public void setFamilyStatusId(Long familyStatusId) {
            this.familyStatusId = familyStatusId;
        }

        public String getStatusDesc() {
            return statusDesc;
        }

        public void setStatusDesc(String statusDesc) {
            this.statusDesc = statusDesc;
        }

        public String getStatusDescEn() {
            return statusDescEn;
        }

        public void setStatusDescEn(String statusDescEn) {
            this.statusDescEn = statusDescEn;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }
            
}
