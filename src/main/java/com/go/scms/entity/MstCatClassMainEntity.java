package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "tbMstCatClass")
public class MstCatClassMainEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long catClassId;

    @Column
    private String scCode;

    @Column
    private String lnameTh;

    @Column
    private String lnameEn;

    @Column
    private Date createdDate;

    @Column
    private String createdUser;

    @Column
    private Date updatedDate;

    @Column
    private String updatedUser;

    @Column
    private String recordStatus;

    public Long getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(Long catClassId) {
        this.catClassId = catClassId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getLnameTh() {
        return lnameTh;
    }

    public void setLnameTh(String lnameTh) {
        this.lnameTh = lnameTh;
    }

    public String getLnameEn() {
        return lnameEn;
    }

    public void setLnameEn(String lnameEn) {
        this.lnameEn = lnameEn;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

}
