/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbTrnClassRoomLifeskillScore")
public class ClassRoomLifeskillScoreEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long classRoomLifeskillScoreId;

    @Column	
    private String scCode;

    @Column	
    private Integer yearGroupId;

    @Column	
    private String yearGroupName;

    @Column	
    private Integer classRoomMemberId;

    @Column	
    private String classRoom;

    @Column	
    private String stuCode;

    @Column	
    private String fullname;

    @Column	
    private Integer behavioursSkillsId;

    @Column	
    private String behavioursSkillsDesc;

    @Column	
    private Integer item;

    @Column	
    private String itemDesc;

    @Column	
    private Integer score;

    @Column	
    private Integer orders;

     @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private String recordStatus;

    @Column
    private Boolean isactive;

    public Long getClassRoomLifeskillScoreId() {
        return classRoomLifeskillScoreId;
    }

    public void setClassRoomLifeskillScoreId(Long classRoomLifeskillScoreId) {
        this.classRoomLifeskillScoreId = classRoomLifeskillScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Integer classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getBehavioursSkillsId() {
        return behavioursSkillsId;
    }

    public void setBehavioursSkillsId(Integer behavioursSkillsId) {
        this.behavioursSkillsId = behavioursSkillsId;
    }

    public String getBehavioursSkillsDesc() {
        return behavioursSkillsDesc;
    }

    public void setBehavioursSkillsDesc(String behavioursSkillsDesc) {
        this.behavioursSkillsDesc = behavioursSkillsDesc;
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
        
}
