package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnOnetScore")
public class SnickOnetEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stuOnetScoreId;
    
    @Column
    private String scCode;
    
    @Column
    private Integer stuProfileId;
    
    @Column
    private Integer classId;
    
    @Column
    private String className;
    
    @Column
    private Integer classRoomId;
    
    @Column
    private String classRoomName;
    
    @Column
    private String stuCode;
    
    @Column
    private String citizenId;
    
    @Column
    private String fullname;
    
    @Column
    private String fullnameEn;
    
    @Column
    private Double subjectScore1;
    
    @Column
    private Double subjectScore2;
    
    @Column
    private Double subjectScore3;
    
    @Column
    private Double subjectScore4;
    
    @Column
    private Double subjectScore5;
    
    @Column
    private Double subjectScore6;
    
    @Column
    private Double subjectScore7;
    
    @Column
    private Double subjectScore8;
    
    @Column
    private Double subjectScore9;
    
    @Column
    private Double subjectScore10;
    
    @Column
    private Double subjectScoreAvg;
    
    @Column
    private Boolean iscurrent;
    
    @Column
    private Date createdDate;
    
    @Column
    private String createdPage;
    
    @Column
    private String createdUser;
    
    @Column
    private Date updatedDate;
    
    @Column
    private String updatedPage;
    
    @Column
    private String updatedUser;
    
    @Column
    private String recordStatus;
    
    @Column
    private Boolean isactive;

    public Long getStuOnetScoreId() {
        return stuOnetScoreId;
    }

    public void setStuOnetScoreId(Long stuOnetScoreId) {
        this.stuOnetScoreId = stuOnetScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(Integer stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoomName() {
        return classRoomName;
    }

    public void setClassRoomName(String classRoomName) {
        this.classRoomName = classRoomName;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public Double getSubjectScore1() {
        return subjectScore1;
    }

    public void setSubjectScore1(Double subjectScore1) {
        this.subjectScore1 = subjectScore1;
    }

    public Double getSubjectScore2() {
        return subjectScore2;
    }

    public void setSubjectScore2(Double subjectScore2) {
        this.subjectScore2 = subjectScore2;
    }

    public Double getSubjectScore3() {
        return subjectScore3;
    }

    public void setSubjectScore3(Double subjectScore3) {
        this.subjectScore3 = subjectScore3;
    }

    public Double getSubjectScore4() {
        return subjectScore4;
    }

    public void setSubjectScore4(Double subjectScore4) {
        this.subjectScore4 = subjectScore4;
    }

    public Double getSubjectScore5() {
        return subjectScore5;
    }

    public void setSubjectScore5(Double subjectScore5) {
        this.subjectScore5 = subjectScore5;
    }

    public Double getSubjectScore6() {
        return subjectScore6;
    }

    public void setSubjectScore6(Double subjectScore6) {
        this.subjectScore6 = subjectScore6;
    }

    public Double getSubjectScore7() {
        return subjectScore7;
    }

    public void setSubjectScore7(Double subjectScore7) {
        this.subjectScore7 = subjectScore7;
    }

    public Double getSubjectScore8() {
        return subjectScore8;
    }

    public void setSubjectScore8(Double subjectScore8) {
        this.subjectScore8 = subjectScore8;
    }

    public Double getSubjectScore9() {
        return subjectScore9;
    }

    public void setSubjectScore9(Double subjectScore9) {
        this.subjectScore9 = subjectScore9;
    }

    public Double getSubjectScore10() {
        return subjectScore10;
    }

    public void setSubjectScore10(Double subjectScore10) {
        this.subjectScore10 = subjectScore10;
    }

    public Double getSubjectScoreAvg() {
        return subjectScoreAvg;
    }

    public void setSubjectScoreAvg(Double subjectScoreAvg) {
        this.subjectScoreAvg = subjectScoreAvg;
    }

    public Boolean getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(Boolean iscurrent) {
        this.iscurrent = iscurrent;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
}
