/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbMstBehavioursSkills")
public class BehavioursSkillsEntity {
     @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long behavioursSkillsId;

    @Column
    private String scCode;

    public Long getBehavioursSkillsId() {
        return behavioursSkillsId;
    }

    public void setBehavioursSkillsId(Long behavioursSkillsId) {
        this.behavioursSkillsId = behavioursSkillsId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }
    
}
