package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="tbMstClass")
public class SnickMstClassEntity {
    
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long classId;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The classLnameTh is a required field.")
	@NotBlank(message="The classLnameTh is a required not empty.")
	private String classLnameTh; 
        
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The classLnameEh is a required field.")
	@NotBlank(message="The classLnameEh is a required not empty.")
	private String classLnameEn;
        
        @Column(nullable = false,unique = true)
	@NotEmpty(message="The classSnameTh is a required field.")
	@NotBlank(message="The classSnameTh is a required not empty.")
	private String classSnameTh;
        
        @Column(nullable = false,unique = true)
	@NotEmpty(message="The classSnameEn is a required field.")
	@NotBlank(message="The classSnameEn is a required not empty.")
	private String classSnameEn;
        
        @Column
	private String age;

        @Column(nullable = false,unique = true)
	@NotEmpty(message="The scCode is a required field.")
	@NotBlank(message="The scCode is a required not empty.")
	private String scCode;
	
	@Column
	private String recordStatus;
	
	@Column(updatable = false)
	private String createdPage;
	
	@Column(updatable = false)
	private Date createdDate;
        
        @Column(updatable = false)
	private String createdUser;
	
	@Column
	private String updatedPage;
	
	@Column
	private Date updatedDate;
        
        @Column
	private String updatedUser;
        
        @Column
	private String langCode;

        public Long getClassId() {
            return classId;
        }

        public void setClassId(Long classId) {
            this.classId = classId;
        }

        public String getClassLnameTh() {
            return classLnameTh;
        }

        public void setClassLnameTh(String classLnameTh) {
            this.classLnameTh = classLnameTh;
        }

        public String getClassLnameEn() {
            return classLnameEn;
        }

        public void setClassLnameEn(String classLnameEn) {
            this.classLnameEn = classLnameEn;
        }

        public String getClassSnameTh() {
            return classSnameTh;
        }

        public void setClassSnameTh(String classSnameTh) {
            this.classSnameTh = classSnameTh;
        }

        public String getClassSnameEn() {
            return classSnameEn;
        }

        public void setClassSnameEn(String classSnameEn) {
            this.classSnameEn = classSnameEn;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getLangCode() {
            return langCode;
        }

        public void setLangCode(String langCode) {
            this.langCode = langCode;
        }

        @ManyToOne
        @JoinColumn(name="catClassId")
	public SnickMstCatClassEntity catclass = new SnickMstCatClassEntity();

            public SnickMstCatClassEntity getCatclass() {
                return catclass;
            }

            public void setCatclass(SnickMstCatClassEntity catclass) {
                this.catclass = catclass;
            }
        
        @PrePersist
	private void insert() {
		this.createdDate = new Date();
	}
        
}
