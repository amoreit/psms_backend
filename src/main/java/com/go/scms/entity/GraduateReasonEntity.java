/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbMstGraduateReason")
public class GraduateReasonEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer graduateReasonId;

    @Column
    private String graduateReason;

    @Column
    private String recordStatus;

    public Integer getGraduateReasonId() {
        return graduateReasonId;
    }

    public void setGraduateReasonId(Integer graduateReasonId) {
        this.graduateReasonId = graduateReasonId;
    }

    public String getGraduateReason() {
        return graduateReason;
    }

    public void setGraduateReason(String graduateReason) {
        this.graduateReason = graduateReason;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }
}
