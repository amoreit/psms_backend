package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbMstActivityGroup")
public class SnickActivityGroupEntity {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long activityGroupId;
    
    @Column
    private String scCode;
    
    @Column
    private Integer catClassId;
    
    @Column
    private String activityGroup;
    
    @Column
    private String recordStatus;

    public Long getActivityGroupId() {
        return activityGroupId;
    }

    public void setActivityGroupId(Long activityGroupId) {
        this.activityGroupId = activityGroupId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(Integer catClassId) {
        this.catClassId = catClassId;
    }

    public String getActivityGroup() {
        return activityGroup;
    }

    public void setActivityGroup(String activityGroup) {
        this.activityGroup = activityGroup;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }
}
