package com.go.scms.entity;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="admRolePage")
@AssociationOverrides({
@AssociationOverride(name = "rolePage.page",joinColumns = @JoinColumn(name = "pageId")),
@AssociationOverride(name = "rolePage.role",joinColumns = @JoinColumn(name = "roleId")) })
public class ManagePageEntity {

	@EmbeddedId
	private RolePageEMB rolePage = new RolePageEMB();
	
	@Column(length = 1)
	private String isView;
	
	@Column(length = 1)
	private String isAdd;
	
	@Column(length = 1)
	private String isDelete;
	
	@Column(length = 1)
	private String isApprove;
	
	@Column(length = 1)
	private String isReport;
	
	public String getIsView() {
		return isView;
	}

	public void setIsView(String isView) {
		this.isView = isView;
	}

	public String getIsAdd() {
		return isAdd;
	}

	public void setIsAdd(String isAdd) {
		this.isAdd = isAdd;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public String getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(String isApprove) {
		this.isApprove = isApprove;
	}

	public String getIsReport() {
		return isReport;
	}

	public void setIsReport(String isReport) {
		this.isReport = isReport;
	}

	public RolePageEMB getRolePage() {
		return rolePage;
	}
	
	public void setRoleGroup(RoleEntity role) {
		this.rolePage.setRole(role);
	}
	
	public RoleEntity getRoleGroup() {
		return this.rolePage.getRole();
	}
	
	public void setPage(PageEntity page) {
		this.rolePage.setPage(page);
	}
	
	public PageEntity getPage() {
		return this.rolePage.getPage();
	}

	public void setRoleGroupPage(RolePageEMB rolePage) {
		this.rolePage = rolePage;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ManagePageEntity that = (ManagePageEntity) o;

		if (this.getRolePage() != null ? !this.getRolePage().equals(that.getRolePage()) : that.getRolePage() != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (this.getRolePage() != null ? this.getRolePage().hashCode() : 0);
	}

}
@Embeddable
class RolePageEMB implements Serializable{

	@ManyToOne
	public RoleEntity role = new RoleEntity();

	@ManyToOne
	public PageEntity page = new PageEntity();
	
	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public PageEntity getPage() {
		return page;
	}

	public void setPage(PageEntity page) {
		this.page = page;
	}

	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RolePageEMB that = (RolePageEMB) o;

        if (this.role != null ? !role.equals(that.role) : that.role!= null) 
        	return false;
        if (this.page != null ? !page.equals(that.page) : that.page != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (role != null ? role.hashCode() : 0);
        result = 31 * result + (page != null ? page.hashCode() : 0);
        return result;
    }
}