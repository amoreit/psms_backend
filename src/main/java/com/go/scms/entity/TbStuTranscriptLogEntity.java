/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Kittipong
 */
@Entity
@Table(name="tbStuTranscriptLog")
public class TbStuTranscriptLogEntity {   
     
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long stuTranscriptLogId;
    
    @Column
    private String scCode;
    
    @Column
    private Integer yearGroupId;
    
    @Column
    private String yearGroupName;
    
    @Column
    private Integer doctypeId;
    
    @Column
    private String setNo;
    
    @Column
    private String seqNo;
    
    @Column
    private Integer catClassId;
    
    @Column
    private Integer classId;
    
    @Column
    private String className;
    
    @Column
    private Integer stuProfileId;
    
    @Column
    private String fullname;
    
    @Column
    private String fullnameEn;
    
    @Column
    private Boolean iscurrent;
    
    @Column
    private Date createdDate;
    
    @Column
    private Date transcriptDate;
    
    @Column
    private String createdPage;
    
    @Column
    private String createdUser;
    
    @Column
    private String ipaddr;
    
    @Column
    private Date updatedDate;
    
    @Column
    private String updatedPage;
    
    @Column
    private String updatedUser;
    
    @Column
    private String recordStatus;
    
    @Column
    private Boolean isactive;
    
    @Column
    private Date dob;  

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getTranscriptDate() {
        return transcriptDate;
    }

    public void setTranscriptDate(Date transcriptDate) {
        this.transcriptDate = transcriptDate;
    }
    
    public long getStuTranscriptLogId() {
        return stuTranscriptLogId;
    }

    public void setStuTranscriptLogId(long stuTranscriptLogId) {
        this.stuTranscriptLogId = stuTranscriptLogId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getDoctypeId() {
        return doctypeId;
    }

    public void setDoctypeId(Integer doctypeId) {
        this.doctypeId = doctypeId;
    }

    public String getSetNo() {
        return setNo;
    }

    public void setSetNo(String setNo) {
        this.setNo = setNo;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String  seqNo) {
        this.seqNo = seqNo;
    }

    public Integer getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(Integer catClassId) {
        this.catClassId = catClassId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(Integer stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public Boolean getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(Boolean iscurrent) {
        this.iscurrent = iscurrent;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
    
}
