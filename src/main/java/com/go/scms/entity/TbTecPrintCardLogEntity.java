/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Kittipong
 */
@Entity
@Table(name = "tbTeaCardPrintLog")
public class TbTecPrintCardLogEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long teacherCardPrintLogId;
    
    @Column
    private String scCode;
    
    @Column
    private Integer teacherId;
    
    @Column
    private String teacherCardId;
    
    @Column
    private Date startDate;
     
    @Column
    private Date expiredDate;
    
    @Column
    private String createdUser;
    
    @Column
    private Date createdDate;
    
    @Column
    private String updatedUser;
    
    @Column
    private Date updatedDate;
    
    @Column
    private Boolean isactive;
    
    @Column
    private String recordStatus;
    
    @Column
    private Integer itemno;

    public Long getTeacherCardPrintLogId() {
        return teacherCardPrintLogId;
    }

    public void setTeacherCardPrintLogId(Long teacherCardPrintLogId) {
        this.teacherCardPrintLogId = teacherCardPrintLogId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = "PRAMANDA";
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }
}
