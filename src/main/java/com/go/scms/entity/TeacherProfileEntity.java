/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="tbTeaTeacherProfile")
public class TeacherProfileEntity {
    
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long teacherId;
        
        @Column
        private String smartPurseId;
    
        @Column
	private String scCode;
    
        @Column
	private String titleId;
     
        @Column
	private String titleIdEn;
        
        @Column
	private Integer classId;
     
        @Column
	private String citizenId;

        @Column
	private String teacherCardId;
        
        @Column
	private String departmentName;
       
        @Column
	private Integer departmentId;
    
        @Column
	private String firstnameTh;
    
        @Column
	private String lastnameTh;
     
        @Column
	private String nickname;
      
        @Column
	private String firstnameEn;
      
        @Column
	private String lastnameEn;
      
        @Column
	private String fullname;
      
        @Column
	private String fullnameEn;

        @Column
	private String gender;
      
        @Column
	private String citizenship;

        @Column
	private String religion;

        @Column
	private String ethnicity;
      
        @Column
	private String saint;

        @Column
	private String familyStatus;
      
        @Column
	private String education;
      
        @Column
	private String email;
        
        //ตามทะเบียนบ้าน
        @Column
	private String regHouseNo;
           
        @Column
	private String regAddrNo;
            
        @Column
	private String regVillage;
             
        @Column
	private String regVillageNo;
              
        @Column
	private String regRoad;
        
        @Column
	private String regAlley;
               
        @Column
	private String regSubDistrict;
           
        @Column
	private String regDistrict;
            
        @Column
	private String regProvince;
             
        @Column
	private String regCountry;
              
        @Column
	private String regPostCode;
               
         //ที่อยู่ปัจจุบัน
        @Column
	private String curAddrNo;
           
        @Column
	private String curVillage;
            
        @Column
	private String curVillageNo;
             
        @Column
	private String curRoad;
        
        @Column
	private String curAlley;
              
        @Column
	private String curSubDistrict;
               
        @Column
	private String curDistrict;
           
        @Column
	private String curProvince;
            
        @Column
	private String curCountry;
             
        @Column
	private String curPostCode;
        
        //ข้อมูลบิดา
        @Column
	private String fatherTitle;
           
        @Column
	private String fatherFirstnameTh;
            
        @Column
	private String fatherLastnameTh;
        
        //ข้อมูลมารดา
        @Column
	private String motherTitle;
           
        @Column
	private String motherFirstnameTh;
            
        @Column
	private String motherLastnameTh;
        
        //ข้อมูลคู่สมรส
        @Column
	private String spouseTitle;
           
        @Column
	private String spouseFirstname;
            
        @Column
	private String spouseLastname;
        
        @Column
	private String spouseReligion;
           
        @Column
	private String spouseSaint;
            
        @Column
	private String spouseChurch;
        
        @Column
	private String spouseOffice;
           
        @Column
	private String spouseTel1;
            
        @Column
	private String spouseTel2;
        
         //จำนวนบุตร
        @Column
	private Integer noOfChild;

            public Integer getNoOfChild() {
                return noOfChild;
            }

            public void setNoOfChild(Integer noOfChild) {
                this.noOfChild = noOfChild;
            }
  
        //ข้อมูลบุตรคนที่ 1
        @Column
	private String childTitle1;
           
        @Column
	private String childFirstname1;
            
        @Column
	private String childLastname1;
        
        @Column
	private String childGender1;
           
        @Column
	private Date childDob1;
            
        @Column
	private String childClass1;
        
        //ข้อมูลบุตรคนที่ 2
        @Column
	private String childTitle2;
           
        @Column
	private String childFirstname2;
            
        @Column
	private String childLastname2;
        
        @Column
	private String childGender2;
           
        @Column
	private Date childDob2;
            
        @Column
	private String childClass2;
      
        //ข้อมูลบุตรคนที่ 3
        @Column
	private String childTitle3;
           
        @Column
	private String childFirstname3;
            
        @Column
	private String childLastname3;
        
        @Column
	private String childGender3;
           
        @Column
	private Date childDob3;
            
        @Column
	private String childClass3;
        
        //ข้อมูลบรรจุ
        @Column
	private String teacherImage;
        
        @Column
	private Date startWorkDate;
         
        @Column
	private Date registeredDate;
         
        @Column
	private String licenseNo;
         
        @Column
	private Date licenseStart;
         
        @Column
	private Date licenseExpired;
         
        @Column
	private Integer positionId;
          
        @Column
	private String positionName;  
          
        @Column
	private String sectionName;
          
        @Column
	private String subDivisionName;
           
        @Column
	private String divisionName;
            
        @Column
	private String tel1;
           
        @Column
	private String tel2;
           
        @Column
	private Date dob;
        
        @Column
	private Boolean ispermanent;
        
        @Column
	private Boolean isinstructor;
        
        @Column
	private String scoutRank;
        
        @Column
	private String educationDesc;

        public Boolean getIsinstructor() {
            return isinstructor;
        }

        public void setIsinstructor(Boolean isinstructor) {
            this.isinstructor = isinstructor;
        }

        public String getTel1() {
            return tel1;
        }

        public void setTel1(String tel1) {
            this.tel1 = tel1;
        }

        public String getTel2() {
            return tel2;
        }

        public void setTel2(String tel2) {
            this.tel2 = tel2;
        }

        public String getSubDivisionName() {
            return subDivisionName;
        }

        public void setSubDivisionName(String subDivisionName) {
            this.subDivisionName = subDivisionName;
        }

        public String getDivisionName() {
            return divisionName;
        }

        public void setDivisionName(String divisionName) {
            this.divisionName = divisionName;
        }

        public String getSectionName() {
            return sectionName;
        }

        public void setSectionName(String sectionName) {
            this.sectionName = sectionName;
        }

        public Integer getPositionId() {
            return positionId;
        }

        public void setPositionId(Integer positionId) {
            this.positionId = positionId;
        }

        public String getPositionName() {
            return positionName;
        }

        public void setPositionName(String positionName) {
            this.positionName = positionName;
        }

        public String getSpouseTitle() {
            return spouseTitle;
        }

        public void setSpouseTitle(String spouseTitle) {
            this.spouseTitle = spouseTitle;
        }

        public String getSpouseFirstname() {
            return spouseFirstname;
        }

        public void setSpouseFirstname(String spouseFirstname) {
            this.spouseFirstname = spouseFirstname;
        }

        public String getSpouseLastname() {
            return spouseLastname;
        }

        public void setSpouseLastname(String spouseLastname) {
            this.spouseLastname = spouseLastname;
        }

        public String getSpouseReligion() {
            return spouseReligion;
        }

        public void setSpouseReligion(String spouseReligion) {
            this.spouseReligion = spouseReligion;
        }

        public String getSpouseSaint() {
            return spouseSaint;
        }

        public void setSpouseSaint(String spouseSaint) {
            this.spouseSaint = spouseSaint;
        }

        public String getSpouseChurch() {
            return spouseChurch;
        }

        public void setSpouseChurch(String spouseChurch) {
            this.spouseChurch = spouseChurch;
        }

        public String getSpouseOffice() {
            return spouseOffice;
        }

        public void setSpouseOffice(String spouseOffice) {
            this.spouseOffice = spouseOffice;
        }

        public String getSpouseTel1() {
            return spouseTel1;
        }

        public void setSpouseTel1(String spouseTel1) {
            this.spouseTel1 = spouseTel1;
        }

        public String getSpouseTel2() {
            return spouseTel2;
        }

        public void setSpouseTel2(String spouseTel2) {
            this.spouseTel2 = spouseTel2;
        }

        public String getFatherTitle() {
            return fatherTitle;
        }

        public void setFatherTitle(String fatherTitle) {
            this.fatherTitle = fatherTitle;
        }

        public String getFatherFirstnameTh() {
            return fatherFirstnameTh;
        }

        public void setFatherFirstnameTh(String fatherFirstnameTh) {
            this.fatherFirstnameTh = fatherFirstnameTh;
        }

        public String getFatherLastnameTh() {
            return fatherLastnameTh;
        }

        public void setFatherLastnameTh(String fatherLastnameTh) {
            this.fatherLastnameTh = fatherLastnameTh;
        }

        public String getMotherTitle() {
            return motherTitle;
        }

        public void setMotherTitle(String motherTitle) {
            this.motherTitle = motherTitle;
        }

        public String getMotherFirstnameTh() {
            return motherFirstnameTh;
        }

        public void setMotherFirstnameTh(String motherFirstnameTh) {
            this.motherFirstnameTh = motherFirstnameTh;
        }

        public String getMotherLastnameTh() {
            return motherLastnameTh;
        }

        public void setMotherLastnameTh(String motherLastnameTh) {
            this.motherLastnameTh = motherLastnameTh;
        }

        public String getRegCountry() {
            return regCountry;
        }

        public void setRegCountry(String regCountry) {
            this.regCountry = regCountry;
        }

        public String getRegPostCode() {
            return regPostCode;
        }

        public void setRegPostCode(String regPostCode) {
            this.regPostCode = regPostCode;
        }

        public String getCurAddrNo() {
            return curAddrNo;
        }

        public void setCurAddrNo(String curAddrNo) {
            this.curAddrNo = curAddrNo;
        }

        public String getCurVillage() {
            return curVillage;
        }

        public void setCurVillage(String curVillage) {
            this.curVillage = curVillage;
        }

        public String getCurVillageNo() {
            return curVillageNo;
        }

        public void setCurVillageNo(String curVillageNo) {
            this.curVillageNo = curVillageNo;
        }

        public String getCurRoad() {
            return curRoad;
        }

        public void setCurRoad(String curRoad) {
            this.curRoad = curRoad;
        }

        public String getCurSubDistrict() {
            return curSubDistrict;
        }

        public void setCurSubDistrict(String curSubDistrict) {
            this.curSubDistrict = curSubDistrict;
        }

        public String getCurDistrict() {
            return curDistrict;
        }

        public void setCurDistrict(String curDistrict) {
            this.curDistrict = curDistrict;
        }

        public String getCurProvince() {
            return curProvince;
        }

        public void setCurProvince(String curProvince) {
            this.curProvince = curProvince;
        }

        public String getCurCountry() {
            return curCountry;
        }

        public void setCurCountry(String curCountry) {
            this.curCountry = curCountry;
        }

        public String getCurPostCode() {
            return curPostCode;
        }

        public void setCurPostCode(String curPostCode) {
            this.curPostCode = curPostCode;
        }
     
        @Column
	private String recordStatus;
     
        @Column
	private Date updatedDate;

        @Column
	private String createdUser;

        @Column
	private String updatedUser;
        
        @Column
	private Boolean isactive;
        
        @Column
        private Date createdDate;

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }


        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Long getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(Long teacherId) {
            this.teacherId = teacherId;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getTeacherCardId() {
            return teacherCardId;
        }

        public void setTeacherCardId(String teacherCardId) {
            this.teacherCardId = teacherCardId;
        }

        public String getFirstnameTh() {
            return firstnameTh;
        }

        public void setFirstnameTh(String firstnameTh) {
            this.firstnameTh = firstnameTh;
        }

        public String getLastnameTh() {
            return lastnameTh;
        }

        public void setLastnameTh(String lastnameTh) {
            this.lastnameTh = lastnameTh;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getFirstnameEn() {
                return firstnameEn;
            }

        public void setFirstnameEn(String firstnameEn) {
            this.firstnameEn = firstnameEn;
        }

        public String getLastnameEn() {
            return lastnameEn;
        }

        public void setLastnameEn(String lastnameEn) {
            this.lastnameEn = lastnameEn;
        }

        public String getGender() {
                return gender;
            }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEthnicity() {
            return ethnicity;
        }

        public void setEthnicity(String ethnicity) {
            this.ethnicity = ethnicity;
        }

        public String getFamilyStatus() {
            return familyStatus;
        }

        public void setFamilyStatus(String familyStatus) {
            this.familyStatus = familyStatus;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getCitizenship() {
            return citizenship;
        }

        public void setCitizenship(String citizenship) {
            this.citizenship = citizenship;
        }

        public String getTitleId() {
            return titleId;
        }

        public void setTitleId(String titleId) {
            this.titleId = titleId;
        }

        public String getCitizenId() {
            return citizenId;
        }

        public void setCitizenId(String citizenId) {
            this.citizenId = citizenId;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getSaint() {
            return saint;
        }

        public void setSaint(String saint) {
            this.saint = saint;
        }

        public String getRegHouseNo() {
            return regHouseNo;
        }

        public void setRegHouseNo(String regHouseNo) {
            this.regHouseNo = regHouseNo;
        }

        public String getRegAddrNo() {
            return regAddrNo;
        }

            public void setRegAddrNo(String regAddrNo) {
            this.regAddrNo = regAddrNo;
        }

        public String getRegVillage() {
            return regVillage;
        }

        public void setRegVillage(String regVillage) {
            this.regVillage = regVillage;
        }

        public String getRegVillageNo() {
            return regVillageNo;
        }

        public void setRegVillageNo(String regVillageNo) {
            this.regVillageNo = regVillageNo;
        }

        public String getRegRoad() {
            return regRoad;
        }

        public void setRegRoad(String regRoad) {
            this.regRoad = regRoad;
        }

        public String getRegSubDistrict() {
            return regSubDistrict;
        }

        public void setRegSubDistrict(String regSubDistrict) {
            this.regSubDistrict = regSubDistrict;
        }

        public String getRegDistrict() {
            return regDistrict;
        }

        public void setRegDistrict(String regDistrict) {
            this.regDistrict = regDistrict;
        }

        public String getRegProvince() {
            return regProvince;
        }

        public void setRegProvince(String regProvince) {
            this.regProvince = regProvince;
        }

        public String getRegAlley() {
            return regAlley;
        }

        public void setRegAlley(String regAlley) {
            this.regAlley = regAlley;
        }

        public String getCurAlley() {
            return curAlley;
        }

        public void setCurAlley(String curAlley) {
            this.curAlley = curAlley;
        }

        public Date getStartWorkDate() {
            return startWorkDate;
        }

        public void setStartWorkDate(Date startWorkDate) {
            this.startWorkDate = startWorkDate;
        }

        public Date getRegisteredDate() {
            return registeredDate;
        }

        public void setRegisteredDate(Date registeredDate) {
            this.registeredDate = registeredDate;
        }

        public String getLicenseNo() {
            return licenseNo;
        }

        public void setLicenseNo(String licenseNo) {
            this.licenseNo = licenseNo;
        }

        public Date getLicenseStart() {
            return licenseStart;
        }

        public void setLicenseStart(Date licenseStart) {
            this.licenseStart = licenseStart;
        }

        public Date getLicenseExpired() {
            return licenseExpired;
        }

        public void setLicenseExpired(Date licenseExpired) {
            this.licenseExpired = licenseExpired;
        }

        public String getChildTitle1() {
            return childTitle1;
        }

        public void setChildTitle1(String childTitle1) {
            this.childTitle1 = childTitle1;
        }

        public String getChildFirstname1() {
            return childFirstname1;
        }

        public void setChildFirstname1(String childFirstname1) {
            this.childFirstname1 = childFirstname1;
        }

        public String getChildLastname1() {
            return childLastname1;
        }

        public void setChildLastname1(String childLastname1) {
            this.childLastname1 = childLastname1;
        }

        public String getChildGender1() {
            return childGender1;
        }

        public void setChildGender1(String childGender1) {
            this.childGender1 = childGender1;
        }

        public Date getChildDob1() {
            return childDob1;
        }

        public void setChildDob1(Date childDob1) {
            this.childDob1 = childDob1;
        }

        public String getChildClass1() {
            return childClass1;
        }

        public void setChildClass1(String childClass1) {
            this.childClass1 = childClass1;
        }

        public String getChildTitle2() {
            return childTitle2;
        }

        public void setChildTitle2(String childTitle2) {
            this.childTitle2 = childTitle2;
        }

        public String getChildFirstname2() {
            return childFirstname2;
        }

        public void setChildFirstname2(String childFirstname2) {
            this.childFirstname2 = childFirstname2;
        }

        public String getChildLastname2() {
            return childLastname2;
        }

        public void setChildLastname2(String childLastname2) {
            this.childLastname2 = childLastname2;
        }

        public String getChildGender2() {
            return childGender2;
        }

        public void setChildGender2(String childGender2) {
            this.childGender2 = childGender2;
        }

        public Date getChildDob2() {
            return childDob2;
        }

        public void setChildDob2(Date childDob2) {
            this.childDob2 = childDob2;
        }

        public String getChildClass2() {
            return childClass2;
        }

        public void setChildClass2(String childClass2) {
            this.childClass2 = childClass2;
        }

        public String getChildTitle3() {
            return childTitle3;
        }

        public void setChildTitle3(String childTitle3) {
            this.childTitle3 = childTitle3;
        }

        public String getChildFirstname3() {
            return childFirstname3;
        }

        public void setChildFirstname3(String childFirstname3) {
            this.childFirstname3 = childFirstname3;
        }

        public String getChildLastname3() {
            return childLastname3;
        }

        public void setChildLastname3(String childLastname3) {
            this.childLastname3 = childLastname3;
        }

        public String getChildGender3() {
            return childGender3;
        }

        public void setChildGender3(String childGender3) {
            this.childGender3 = childGender3;
        }

        public Date getChildDob3() {
            return childDob3;
        }

        public void setChildDob3(Date childDob3) {
            this.childDob3 = childDob3;
        }

        public String getChildClass3() {
            return childClass3;
        }

        public void setChildClass3(String childClass3) {
            this.childClass3 = childClass3;
        }

        public String getFullnameEn() {
            return fullnameEn;
        }

        public void setFullnameEn(String fullnameEn) {
            this.fullnameEn = fullnameEn;
        }

        public String getTeacherImage() {
            return teacherImage;
        }

        public void setTeacherImage(String teacherImage) {
            this.teacherImage = teacherImage;
        }

        public String getDepartmentName() {
            return departmentName;
        }

        public void setDepartmentName(String departmentName) {
            this.departmentName = departmentName;
        }

        public Integer getDepartmentId() {
            return departmentId;
        }

        public void setDepartmentId(Integer departmentId) {
            this.departmentId = departmentId;
        }

        public Integer getClassId() {
            return classId;
        }

        public void setClassId(Integer classId) {
            this.classId = classId;
        }

        public String getTitleIdEn() {
            return titleIdEn;
        }

        public void setTitleIdEn(String titleIdEn) {
            this.titleIdEn = titleIdEn;
        }

        public Date getDob() {
            return dob;
        }

        public void setDob(Date dob) {
            this.dob = dob;
        }

        public Boolean getIspermanent() {
            return ispermanent;
        }

        public void setIspermanent(Boolean ispermanent) {
            this.ispermanent = ispermanent;
        }

        public String getSmartPurseId() {
            return smartPurseId;
        }

        public void setSmartPurseId(String smartPurseId) {
            this.smartPurseId = smartPurseId;
        }

        @Column
        private Integer classRoomId;

            public Integer getClassRoomId() {
                return classRoomId;
            }

            public void setClassRoomId(Integer classRoomId) {
                this.classRoomId = classRoomId;
            }

        public String getScoutRank() {
            return scoutRank;
        }

        public void setScoutRank(String scoutRank) {
            this.scoutRank = scoutRank;
        }

        public String getEducationDesc() {
            return educationDesc;
        }

        public void setEducationDesc(String educationDesc) {
            this.educationDesc = educationDesc;
        }
       
}
