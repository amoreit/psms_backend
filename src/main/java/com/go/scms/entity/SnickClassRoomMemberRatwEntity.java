package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnClassRoomMember")
public class SnickClassRoomMemberRatwEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classRoomMemberId;

    @Column
    private Integer memberId;
    
    @ManyToOne
    @JoinColumn(name = "classRoomId")
    private SnickClassRoomSubjectRatwEntity subjectJoin = new SnickClassRoomSubjectRatwEntity();

        public SnickClassRoomSubjectRatwEntity getSubjectJoin() {
            return subjectJoin;
        }

        public void setSubjectJoin(SnickClassRoomSubjectRatwEntity subjectJoin) {
            this.subjectJoin = subjectJoin;
        }
        
    @ManyToOne
    @JoinColumn(name = "yearGroupId")
    private SnickClassRoomRatwScoreEntity ratwScoreJoin = new SnickClassRoomRatwScoreEntity();

        public SnickClassRoomRatwScoreEntity getRatwScoreJoin() {
            return ratwScoreJoin;
        }

        public void setRatwScoreJoin(SnickClassRoomRatwScoreEntity ratwScoreJoin) {
            this.ratwScoreJoin = ratwScoreJoin;
        }

    @Column
    private String scCode;

    @Column
    private String yearGroupName;

    @Column
    private String stuCode;

    @Column
    private String fullname;

    @Column
    private Integer classId;

    @Column
    private String className;

    @Column
    private String classRoom;

    @Column
    private String classRoomNo;
    
    @Column
    private String activityCode;
    
    @Column
    private String activityName;
    
    @Column
    private String activityResult;

    @Column
    private String role;

    @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private String recordStatus;

    @Column
    private Boolean isactive;

    public Long getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Long classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityResult() {
        return activityResult;
    }

    public void setActivityResult(String activityResult) {
        this.activityResult = activityResult;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
}
