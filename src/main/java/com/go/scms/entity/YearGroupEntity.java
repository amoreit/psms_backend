/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbMstYearGroup")
public class YearGroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long yearGroupId;

    @Column
    private String scCode;

    @Column
    private String name;

    @Column
    private Boolean iscurrent;

    @ManyToOne
    @JoinColumn(name = "yearId")
    private YearEntity year = new YearEntity();

    @ManyToOne
    @JoinColumn(name = "yearTermId")
    private YearTermEntity yearTerm = new YearTermEntity();

    @Column
    private String recordStatus;

    @Column
    private Date createdDate;

    @Column
    private String createdUser;

    @Column
    private Date updatedDate;

    @Column
    private String updatedUser;

    @Column
    private Date firstday;

    @Column
    private Date lastday;

    public Long getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Long yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(Boolean iscurrent) {
        this.iscurrent = iscurrent;
    }

    public YearEntity getYear() {
        return year;
    }

    public void setYear(YearEntity year) {
        this.year = year;
    }

    public YearTermEntity getYearTerm() {
        return yearTerm;
    }

    public void setYearTerm(YearTermEntity yearTerm) {
        this.yearTerm = yearTerm;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getFirstday() {
        return firstday;
    }

    public void setFirstday(Date firstday) {
        this.firstday = firstday;
    }

    public Date getLastday() {
        return lastday;
    }

    public void setLastday(Date lastday) {
        this.lastday = lastday;
    }

}
