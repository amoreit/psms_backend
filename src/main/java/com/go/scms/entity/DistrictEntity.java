/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name="tbMstDistrict")
public class DistrictEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer districtId;

    @Column
    private String districtTh;

    @Column
    private String recordStatus;
    
    @ManyToOne
    @JoinColumn(name="provinceId")
    private ProvinceEntity joinProvince = new ProvinceEntity();

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getDistrictTh() {
        return districtTh;
    }

    public void setDistrictTh(String districtTh) {
        this.districtTh = districtTh;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public ProvinceEntity getJoinProvince() {
        return joinProvince;
    }

    public void setJoinProvince(ProvinceEntity joinProvince) {
        this.joinProvince = joinProvince;
    }
    
}
