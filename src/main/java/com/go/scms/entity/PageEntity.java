package com.go.scms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="admPage")
public class PageEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pageId;

	@Column(nullable = false,unique = true)
	@NotEmpty(message="The code is a required field.")
	@NotBlank(message="The code is a required not empty.")
	private String pageCode;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The name is a required field.")
	@NotBlank(message="The name is a required not empty.")
	private String pageName;

	@Column
	private String pageNameEn;
	
	@Column
	private String pageUrl;
	
	@Column
	private String pageIcon;
	
	@ManyToOne
	@JoinColumn(name="pageGroupId")
	private PageGroupEntity pageGroup = new PageGroupEntity();
	
	@Column(nullable = false)
	private String pageStatus;
	
	@Column(updatable = false)
	private Date pageCreatedDate;
	
	@Column(updatable = false)
	private String pageCreatedBy;
	
	@Column
	private Date pageUpdatedDate;
	
	@Column
	private String pageUpdatedBy;

	public Long getPageId() {
		return pageId;
	}

	public void setPageId(Long pageId) {
		this.pageId = pageId;
	}

	public String getPageCode() {
		return pageCode;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public PageGroupEntity getPageGroup() {
		return pageGroup;
	}

	public void setPageGroup(PageGroupEntity pageGroup) {
		this.pageGroup = pageGroup;
	}

	public String getPageStatus() {
		return pageStatus;
	}

	public void setPageStatus(String pageStatus) {
		this.pageStatus = pageStatus;
	}

	public Date getPageCreatedDate() {
		return pageCreatedDate;
	}

	public void setPageCreatedDate(Date pageCreatedDate) {
		this.pageCreatedDate = pageCreatedDate;
	}

	public String getPageCreatedBy() {
		return pageCreatedBy;
	}

	public void setPageCreatedBy(String pageCreatedBy) {
		this.pageCreatedBy = pageCreatedBy;
	}

	public Date getPageUpdatedDate() {
		return pageUpdatedDate;
	}

	public void setPageUpdatedDate(Date pageUpdatedDate) {
		this.pageUpdatedDate = pageUpdatedDate;
	}

	public String getPageUpdatedBy() {
		return pageUpdatedBy;
	}

	public void setPageUpdatedBy(String pageUpdatedBy) {
		this.pageUpdatedBy = pageUpdatedBy;
	}
	
	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public String getPageIcon() {
		return pageIcon;
	}

	public void setPageIcon(String pageIcon) {
		this.pageIcon = pageIcon;
	}

	public String getPageNameEn() {
		return pageNameEn;
	}

	public void setPageNameEn(String pageNameEn) {
		this.pageNameEn = pageNameEn;
	}

	@PrePersist
	private void insert() {
		this.pageCreatedDate = new Date();
	}
}
