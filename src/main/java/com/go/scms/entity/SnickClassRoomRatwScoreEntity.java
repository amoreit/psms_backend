package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnClassRoomRatwScore")
public class SnickClassRoomRatwScoreEntity {
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classRoomRatwScoreId;

    @Column
    private String scCode;

    @Column
    private Integer yearGroupId;

    @Column
    private String yearGroupName;
    
    @Column
    private Integer classRoomMemberId;

    @Column
    private Integer classRoomSubjectId;

    @Column
    private String classRoomSnameTh;

    @Column
    private String stuCode;

    @Column
    private String fullname;
    
    @Column
    private Integer behavioursSkillsId;
    
    @Column
    private String behavioursSkillsDesc;
    
    @Column
    private Integer itemno;
    
    @Column
    private String itemDesc;
    
    @Column
    private Integer score1;
    
    @Column
    private Integer score2;
    
    @Column
    private Integer score3;
    
    @Column
    private Integer score4;
    
    @Column
    private Integer score5;
    
    @Column
    private String recordStatus;
    
    @Column
    private Date createdDate;
    
    @Column
    private String createdPage;
    
    @Column
    private String createdUser;
    
    @Column
    private String ipaddr;
    
    @Column
    private Date updatedDate;
    
    @Column
    private String updatedPage;
    
    @Column
    private String updatedUser;
    
    @Column
    private Boolean isactive;

    public Long getClassRoomRatwScoreId() {
        return classRoomRatwScoreId;
    }

    public void setClassRoomRatwScoreId(Long classRoomRatwScoreId) {
        this.classRoomRatwScoreId = classRoomRatwScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Integer classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }
    
    public Integer getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(Integer classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getBehavioursSkillsId() {
        return behavioursSkillsId;
    }

    public void setBehavioursSkillsId(Integer behavioursSkillsId) {
        this.behavioursSkillsId = behavioursSkillsId;
    }

    public String getBehavioursSkillsDesc() {
        return behavioursSkillsDesc;
    }

    public void setBehavioursSkillsDesc(String behavioursSkillsDesc) {
        this.behavioursSkillsDesc = behavioursSkillsDesc;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    public Integer getScore3() {
        return score3;
    }

    public void setScore3(Integer score3) {
        this.score3 = score3;
    }

    public Integer getScore4() {
        return score4;
    }

    public void setScore4(Integer score4) {
        this.score4 = score4;
    }

    public Integer getScore5() {
        return score5;
    }

    public void setScore5(Integer score5) {
        this.score5 = score5;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
}
