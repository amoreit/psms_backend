/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name = "tbMstClass")
public class ClassEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer classId;

    @Column
    private String classSnameTh;

    @Column
    private String recordStatus;

    @Column
    private String classLnameTh;

    @Column
    private String langCode;
    
    @ManyToOne
    @JoinColumn(name = "catClassId")
    private CatClassEntity catClassJoin = new CatClassEntity();

    public CatClassEntity getCatClassJoin() {
        return catClassJoin;
    }

    public void setCatClassJoin(CatClassEntity catClassJoin) {
        this.catClassJoin = catClassJoin;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassSnameTh() {
        return classSnameTh;
    }

    public void setClassSnameTh(String classSnameTh) {
        this.classSnameTh = classSnameTh;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getClassLnameTh() {
        return classLnameTh;
    }

    public void setClassLnameTh(String classLnameTh) {
        this.classLnameTh = classLnameTh;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

}
