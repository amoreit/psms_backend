/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbMstSubDistrict")
public class TbMstSubDistrictEntity {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long subDistrictId;

        @Column
        private String subDistrictCode;

        @Column
        private String subDistrictTh;

        @Column
        private String subDistrictEn;

        @ManyToOne
        @JoinColumn(name = "districtId")
        private TbMstDistrictEntity district = new TbMstDistrictEntity();

                public TbMstDistrictEntity getDistrict() {
                    return district;
                }

                public void setDistrict(TbMstDistrictEntity district) {
                    this.district = district;
                }

        @ManyToOne
        @JoinColumn(name = "provinceId")
        private TbMstProvinceEntity province = new TbMstProvinceEntity();

                public TbMstProvinceEntity getProvince() {
                    return province;
                }

                public void setProvince(TbMstProvinceEntity province) {
                    this.province = province;
                }

        @Column
        private String postCode;

        @Column
        private Date createdDate;

        @Column
        private String createdPage;

        @Column
        private String createdUser;

        @Column
        private Date updatedDate;

        @Column
        private String updatedPage;

        @Column
        private String updatedUser;

        @Column
        private String ipaddr;

        @Column
        private String recordStatus;

        @Column
        private Boolean isactive;

        public Long getSubDistrictId() {
            return subDistrictId;
        }

        public void setSubDistrictId(Long subDistrictId) {
            this.subDistrictId = subDistrictId;
        }

        public String getSubDistrictCode() {
            return subDistrictCode;
        }

        public void setSubDistrictCode(String subDistrictCode) {
            this.subDistrictCode = subDistrictCode;
        }

        public String getSubDistrictTh() {
            return subDistrictTh;
        }

        public void setSubDistrictTh(String subDistrictTh) {
            this.subDistrictTh = subDistrictTh;
        }

        public String getSubDistrictEn() {
            return subDistrictEn;
        }

        public void setSubDistrictEn(String subDistrictEn) {
            this.subDistrictEn = subDistrictEn;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }
    
}
