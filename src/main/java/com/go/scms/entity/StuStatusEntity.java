/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name = "tbMstStuStatus")
public class StuStatusEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer stuStatusId;

    @Column
    private String statusDesc;
    
    @Column
    private String recordStatus;

    public Integer getStuStatusId() {
        return stuStatusId;
    }

    public void setStuStatusId(Integer stuStatusId) {
        this.stuStatusId = stuStatusId;
    }

   

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }  
}
