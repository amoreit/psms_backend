package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbMstClassRoom")
public class MaxClassRoomEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer classRoomId;

    @Column
    private String scCode;

    @ManyToOne 
    @JoinColumn(name="classId")
    private ClassEntity classJoin = new ClassEntity();
    
    @Column
    private String classRoom;

    @Column
    private String classRoomLnameTh;

    @Column
    private String classRoomLnameEn;

    @Column
    private String classRoomSnameTh;

    @Column
    private String classRoomSnameEn;
    
    @Column
    private Integer teacherAdvisor1;
    
    @Column
    private Integer teacherAdvisor2;
    
    @Column
    private Integer teacherAdvisor3;

    @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private String recordStatus;

    @Column
    private Boolean isactive;

    public Integer getTeacherAdvisor1() {
        return teacherAdvisor1;
    }

    public void setTeacherAdvisor1(Integer teacherAdvisor1) {
        this.teacherAdvisor1 = teacherAdvisor1;
    }

    public Integer getTeacherAdvisor2() {
        return teacherAdvisor2;
    }

    public void setTeacherAdvisor2(Integer teacherAdvisor2) {
        this.teacherAdvisor2 = teacherAdvisor2;
    }

    public Integer getTeacherAdvisor3() {
        return teacherAdvisor3;
    }

    public void setTeacherAdvisor3(Integer teacherAdvisor3) {
        this.teacherAdvisor3 = teacherAdvisor3;
    }
    
    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public ClassEntity getClassJoin() {
        return classJoin;
    }

    public void setClassJoin(ClassEntity classJoin) {
        this.classJoin = classJoin;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomLnameTh() {
        return classRoomLnameTh;
    }

    public void setClassRoomLnameTh(String classRoomLnameTh) {
        this.classRoomLnameTh = classRoomLnameTh;
    }

    public String getClassRoomLnameEn() {
        return classRoomLnameEn;
    }

    public void setClassRoomLnameEn(String classRoomLnameEn) {
        this.classRoomLnameEn = classRoomLnameEn;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getClassRoomSnameEn() {
        return classRoomSnameEn;
    }

    public void setClassRoomSnameEn(String classRoomSnameEn) {
        this.classRoomSnameEn = classRoomSnameEn;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
    
}
