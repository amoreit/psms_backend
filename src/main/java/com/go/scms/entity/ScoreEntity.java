/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbMstScore")
public class ScoreEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long scoreId;
    
    @Column
    private String scCode;

    @Column
    private String scoreName;
    
    @Column
    private Double fullScoreTotal;
    
    @Column
    private Double fullScore1;
    
    @Column
    private Double fullScoreMidterm;
    
    @Column
    private Double fullScore2;
    
    @Column
    private Double fullScoreFinal;
    
    @Column
    private String scoreDesc;

    @Column
    private Boolean iscatclass1;

    @Column
    private String recordStatus;

    @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private Boolean isactive;

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Long getScoreId() {
        return scoreId;
    }

    public void setScoreId(Long scoreId) {
        this.scoreId = scoreId;
    }

    public Double getFullScoreTotal() {
        return fullScoreTotal;
    }

    public void setFullScoreTotal(Double fullScoreTotal) {
        this.fullScoreTotal = fullScoreTotal;
    }

    public Double getFullScore1() {
        return fullScore1;
    }

    public void setFullScore1(Double fullScore1) {
        this.fullScore1 = fullScore1;
    }

    public Double getFullScoreMidterm() {
        return fullScoreMidterm;
    }

    public void setFullScoreMidterm(Double fullScoreMidterm) {
        this.fullScoreMidterm = fullScoreMidterm;
    }

    public Double getFullScore2() {
        return fullScore2;
    }

    public void setFullScore2(Double fullScore2) {
        this.fullScore2 = fullScore2;
    }

    public Double getFullScoreFinal() {
        return fullScoreFinal;
    }

    public void setFullScoreFinal(Double fullScoreFinal) {
        this.fullScoreFinal = fullScoreFinal;
    }

    public String getScoreDesc() {
        return scoreDesc;
    }

    public void setScoreDesc(String scoreDesc) {
        this.scoreDesc = scoreDesc;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Boolean getIscatclass1() {
        return iscatclass1;
    }

    public void setIscatclass1(Boolean iscatclass1) {
        this.iscatclass1 = iscatclass1;
    }


    public String getScoreName() {
        return scoreName;
    }

    public void setScoreName(String scoreName) {
        this.scoreName = scoreName;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

}
