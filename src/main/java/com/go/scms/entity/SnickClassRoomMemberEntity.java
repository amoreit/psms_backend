package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnClassRoomMember")
public class SnickClassRoomMemberEntity {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long classRoomMemberId;
        
        @ManyToOne
        @JoinColumn(name = "yearGroupId")
        private YearGroupEntity yearGroupJoin = new YearGroupEntity();

            public YearGroupEntity getYearGroupJoin() {
                return yearGroupJoin;
            }

            public void setYearGroupJoin(YearGroupEntity yearGroupJoin) {
                this.yearGroupJoin = yearGroupJoin;
            }
        
        @ManyToOne
        @JoinColumn(name = "classRoomId")
        private ClassRoomEntity classRoomJoin = new ClassRoomEntity();

            public ClassRoomEntity getClassRoomJoin() {
                return classRoomJoin;
            }

            public void setClassRoomJoin(ClassRoomEntity classRoomJoin) {
                this.classRoomJoin = classRoomJoin;
            }
               
        @Column
        private String scCode;
        
        @Column
        private String yearGroupName;
        
        @ManyToOne
        @JoinColumn(name = "memberId")
        private SnickTeacherProfileEntity teacherJoin = new SnickTeacherProfileEntity();

            public SnickTeacherProfileEntity getTeacherJoin() {
                return teacherJoin; 
            }
  
            public void setTeacherJoin(SnickTeacherProfileEntity teacherJoin) {
                this.teacherJoin = teacherJoin;
            }
        
        @Column
        private String stuCode;
        
        @Column
        private Integer classId;
        
        @Column
        private String className;
        
        @Column
        private String classRoom;
        
        @Column
        private String classRoomNo;
        
        @Column
        private String role;
        
        @Column
        private Date createdDate;
        
        @Column
        private String createdPage;
        
        @Column
        private String createdUser;
        
        @Column
        private String ipaddr;
        
        @Column
        private Date updatedDate;
        
        @Column
        private String updatedPage;
        
        @Column
        private String updatedUser;
        
        @Column
        private String recordStatus;
        
        @Column
        private Boolean isactive;

        public Long getClassRoomMemberId() {
            return classRoomMemberId;
        }

        public void setClassRoomMemberId(Long classRoomMemberId) {
            this.classRoomMemberId = classRoomMemberId;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = "PRAMANDA";
        }

        public String getYearGroupName() {
            return yearGroupName;
        }

        public void setYearGroupName(String yearGroupName) {
            this.yearGroupName = yearGroupName;
        }

        public String getStuCode() {
            return stuCode;
        }

        public void setStuCode(String stuCode) {
            this.stuCode = stuCode;
        }

        public Integer getClassId() {
            return classId;
        }

        public void setClassId(Integer classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassRoom() {
            return classRoom;
        }

        public void setClassRoom(String classRoom) {
            this.classRoom = classRoom;
        }

        public String getClassRoomNo() {
            return classRoomNo;
        }

        public void setClassRoomNo(String classRoomNo) {
            this.classRoomNo = classRoomNo;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }            
}
