/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author P3rdiscoz
 */
@Entity
@Table(name="tbMstCatClass")
public class CatClassEntity {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long catClassId;

    @Column
    private String lnameTh;

    @Column
    private String recordStatus;

    public Long getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(Long catClassId) {
        this.catClassId = catClassId;
    }

    public String getLnameTh() {
        return lnameTh;
    }

    public void setLnameTh(String lnameTh) {
        this.lnameTh = lnameTh;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }  
        
}
