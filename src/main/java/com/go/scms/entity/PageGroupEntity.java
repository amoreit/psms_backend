package com.go.scms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="admPageGroup")
public class PageGroupEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pageGroupId;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The code is a required field.")
	@NotBlank(message="The code is a required not empty.")
	private String pageGroupCode;

	@Column(nullable = false,unique = true)
	@NotEmpty(message="The name is a required field.")
	@NotBlank(message="The name is a required not empty.")
	private String pageGroupName;

	@Column
	private String pageGroupNameEn;
	
	@Column
	private String pageGroupIcon;
	
	@Column(nullable = false)
	private String pageGroupStatus;
	
	@Column(updatable = false)
	private Date pageGroupCreatedDate;
	
	@Column(updatable = false)
	private String pageGroupCreatedBy;
	
	@Column
	private Date pageGroupUpdatedDate;
	
	@Column
	private String pageGroupUpdatedBy;

	public Long getPageGroupId() {
		return pageGroupId;
	}

	public void setPageGroupId(Long pageGroupId) {
		this.pageGroupId = pageGroupId;
	}

	public String getPageGroupCode() {
		return pageGroupCode;
	}

	public void setPageGroupCode(String pageGroupCode) {
		this.pageGroupCode = pageGroupCode;
	}

	public String getPageGroupName() {
		return pageGroupName;
	}

	public void setPageGroupName(String pageGroupName) {
		this.pageGroupName = pageGroupName;
	}

	public String getPageGroupStatus() {
		return pageGroupStatus;
	}

	public void setPageGroupStatus(String pageGroupStatus) {
		this.pageGroupStatus = pageGroupStatus;
	}

	public Date getPageGroupCreatedDate() {
		return pageGroupCreatedDate;
	}

	public void setPageGroupCreatedDate(Date pageGroupCreatedDate) {
		this.pageGroupCreatedDate = pageGroupCreatedDate;
	}

	public String getPageGroupCreatedBy() {
		return pageGroupCreatedBy;
	}

	public void setPageGroupCreatedBy(String pageGroupCreatedBy) {
		this.pageGroupCreatedBy = pageGroupCreatedBy;
	}

	public Date getPageGroupUpdatedDate() {
		return pageGroupUpdatedDate;
	}

	public void setPageGroupUpdatedDate(Date pageGroupUpdatedDate) {
		this.pageGroupUpdatedDate = pageGroupUpdatedDate;
	}

	public String getPageGroupUpdatedBy() {
		return pageGroupUpdatedBy;
	}

	public void setPageGroupUpdatedBy(String pageGroupUpdatedBy) {
		this.pageGroupUpdatedBy = pageGroupUpdatedBy;
	}
	
	public String getPageGroupIcon() {
		return pageGroupIcon;
	}

	public void setPageGroupIcon(String pageGroupIcon) {
		this.pageGroupIcon = pageGroupIcon;
	}

	public String getPageGroupNameEn() {
		return pageGroupNameEn;
	}

	public void setPageGroupNameEn(String pageGroupNameEn) {
		this.pageGroupNameEn = pageGroupNameEn;
	}

	@PrePersist
	private void insert() {
		this.pageGroupCreatedDate = new Date();
	}
	
}
