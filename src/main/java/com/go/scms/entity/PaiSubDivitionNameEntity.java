/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author bamboo
 */
@Entity
@Table(name = "tbMstSubDivision")
public class PaiSubDivitionNameEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long subDivisionId;
    
    @Column
    private String subDivisionNameTh;
     
    @Column
    private String subDivisionNameEn;
      
    @Column
    private String recordStatus;

    public Long getSubDivisionId() {
        return subDivisionId;
    }

    public void setSubDivisionId(Long subDivisionId) {
        this.subDivisionId = subDivisionId;
    }

    public String getSubDivisionNameTh() {
        return subDivisionNameTh;
    }

    public void setSubDivisionNameTh(String subDivisionNameTh) {
        this.subDivisionNameTh = subDivisionNameTh;
    }

    public String getSubDivisionNameEn() {
        return subDivisionNameEn;
    }

    public void setSubDivisionNameEn(String subDivisionNameEn) {
        this.subDivisionNameEn = subDivisionNameEn;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }
    
    @ManyToOne
    @JoinColumn(name ="divisionId")
    private PaiDivitionEntity divisionJoin = new PaiDivitionEntity();

    public PaiDivitionEntity getDivisionJoin() {
        return divisionJoin;
    }

    public void setDivisionJoin(PaiDivitionEntity divisionJoin) {
        this.divisionJoin = divisionJoin;
    } 
}
