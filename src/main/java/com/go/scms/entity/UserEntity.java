package com.go.scms.entity;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.gson.JsonArray;

@Entity
@Table(name = "admUser")
public class UserEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @Column(nullable = false, length = 255)
    @NotEmpty(message = "The first name is a required field.")
    @NotBlank(message = "The first name is a required not empty.")
    @Size(min = 1, max = 255, message = "The first name should be between 1 and 255 characters.")
    private String firstName;

    @Column(nullable = false, length = 255)
    @NotEmpty(message = "The last name is a required field.")
    @NotBlank(message = "The last name is a required not empty.")
    @Size(min = 1, max = 255, message = "The first name should be between 1 and 255 characters.")
    private String lastName;

    @Column(nullable = false, length = 255)
    @NotEmpty(message = "The last phone no is a required field.")
    @NotBlank(message = "The last phone no is a required not empty.")
    @Size(min = 1, max = 255, message = "The first phone no should be between 1 and 255 characters.")
    private String phoneNo;

    @Column(unique = true, nullable = false, length = 255)
    @NotEmpty(message = "The email is a required field.")
    @NotBlank(message = "The email is a required not empty.")
    @Size(min = 1, max = 255, message = "The email should be between 1 and 255 characters.")
    private String email;

    @Column(unique = true, nullable = false, length = 255)
    @NotEmpty(message = "The email is a required field.")
    @NotBlank(message = "The email is a required not empty.")
    @Size(min = 1, max = 255, message = "The email should be between 1 and 255 characters.")
    private String usrName;

    @Column(unique = true, nullable = false, length = 255)
    @NotEmpty(message = "The password is a required field.")
    @NotBlank(message = "The password is a required not empty.")
    @Size(min = 1, max = 255, message = "The password should be between 1 and 255 characters.")
    private String password;

    @Column(nullable = false, length = 10)
    @NotEmpty(message = "The status is a required field.")
    @NotBlank(message = "The status is a required not empty.")
    @Size(min = 1, max = 10, message = "The status should be between 1 and 10 characters.")
    private String userStatus;

    @Column
    private String scCode;

    @Column
    private String citizenId;

    @Column
    private String title;

    @Column
    private String userType;

    @Column
    private Boolean activated;

    @Column(updatable = false)
    private Date userCreatedDate;

    @Column
    private Date userUpdatedDate;

    @Column
    private Integer loginCount;

    @ManyToOne
    @JoinColumn(name = "roleId")
    private RoleEntity role = new RoleEntity();

    @Transient
    private List<GrantedAuthority> grantedAuthorities;

    @Transient
    private JsonArray menu;

    public UserEntity(String usrName, String email, String title, String password, String firstName, String lastName, String citizenId, RoleEntity role, JsonArray menu) {
        this.usrName = usrName;
        this.email = email;
        this.title = title;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.menu = menu;
        this.citizenId = citizenId;
        this.role = role;

        String[] authorities = {"ROLE_USER"};
        this.grantedAuthorities = AuthorityUtils.createAuthorityList(authorities);
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public UserEntity() {
        super();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public List<GrantedAuthority> getGrantedAuthorities() {
        return grantedAuthorities;
    }

    public void setGrantedAuthorities(List<GrantedAuthority> grantedAuthorities) {
        this.grantedAuthorities = grantedAuthorities;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Date getUserCreatedDate() {
        return userCreatedDate;
    }

    public void setUserCreatedDate(Date userCreatedDate) {
        this.userCreatedDate = userCreatedDate;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public Date getUserUpdatedDate() {
        return userUpdatedDate;
    }

    public void setUserUpdatedDate(Date userUpdatedDate) {
        this.userUpdatedDate = userUpdatedDate;
    }

    public JsonArray getMenu() {
        return menu;
    }

    public void setMenu(JsonArray menu) {
        this.menu = menu;
    }

    public Integer getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO Auto-generated method stub
        return grantedAuthorities;
    }

    @Override
    public String getUsername() {
        // TODO Auto-generated method stub
        return this.firstName + " " + this.lastName;
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }
}
