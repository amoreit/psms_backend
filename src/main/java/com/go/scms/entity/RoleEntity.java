package com.go.scms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="admRole")
public class RoleEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long roleId;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The code is a required field.")
	@NotBlank(message="The code is a required not empty.")
	private String roleCode;
	
	@Column(nullable = false,unique = true)
	@NotEmpty(message="The name is a required field.")
	@NotBlank(message="The name is a required not empty.")
	private String roleName;
	
	@Column(nullable = false)
	@NotEmpty(message="The status is a required field.")
	@NotBlank(message="The status is a required not empty.")
	private String roleStatus;
	
	@Column(updatable = false)
	private String roleCreatedBy;
	
	@Column(updatable = false)
	private Date roleCreatedDate;
	
	@Column
	private String roleUpdatedBy;
	
	@Column
	private Date roleUpdatedDate;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleStatus() {
		return roleStatus;
	}

	public void setRoleStatus(String roleStatus) {
		this.roleStatus = roleStatus;
	}

	public String getRoleCreatedBy() {
		return roleCreatedBy;
	}

	public void setRoleCreatedBy(String roleCreatedBy) {
		this.roleCreatedBy = roleCreatedBy;
	}

	public Date getRoleCreatedDate() {
		return roleCreatedDate;
	}

	public void setRoleCreatedDate(Date roleCreatedDate) {
		this.roleCreatedDate = roleCreatedDate;
	}

	public String getRoleUpdatedBy() {
		return roleUpdatedBy;
	}

	public void setRoleUpdatedBy(String roleUpdatedBy) {
		this.roleUpdatedBy = roleUpdatedBy;
	}

	public Date getRoleUpdatedDate() {
		return roleUpdatedDate;
	}

	public void setRoleUpdatedDate(Date roleUpdatedDate) {
		this.roleUpdatedDate = roleUpdatedDate;
	}
	
	@PrePersist
	private void insert() {
		this.roleCreatedDate = new Date();
	}
}
