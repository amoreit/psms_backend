/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbMstYearTerm")
public class YearTermEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer yearTermId;

    @Column
    private String scCode;

    @Column
    private String yearTermCode;
    
    @Column
    private String name;
    
    @Column
    private String recordStatus;
    
    public Integer getYearTermId() {
        return yearTermId;
    }

    public void setYearTermId(Integer yearTermId) {
        this.yearTermId = yearTermId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearTermCode() {
        return yearTermCode;
    }

    public void setYearTermCode(String yearTermCode) {
        this.yearTermCode = yearTermCode;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
