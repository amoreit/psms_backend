/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbMstCountry")
public class TbMstCountryEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long countryId;

        @Column
        private String countryCode;

        @Column
        private String countryTh;

        @Column
        private String countryEn;

        @Column
        private Date createdDate;

        @Column
        private String createdPage;

        @Column
        private String createdUser;

        @Column
        private Date updatedDate;

        @Column
        private String updatedPage;

        @Column
        private String updatedUser;

        @Column
        private String ipaddr;

        @Column
        private String recordStatus;

        @Column
        private Boolean isactive;

        public Long getCountryId() {
            return countryId;
        }

        public void setCountryId(Long countryId) {
            this.countryId = countryId;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getCountryTh() {
            return countryTh;
        }

        public void setCountryTh(String countryTh) {
            this.countryTh = countryTh;
        }

        public String getCountryEn() {
            return countryEn;
        }

        public void setCountryEn(String countryEn) {
            this.countryEn = countryEn;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public Date getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(Date updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public Boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(Boolean isactive) {
            this.isactive = isactive;
        }

}
