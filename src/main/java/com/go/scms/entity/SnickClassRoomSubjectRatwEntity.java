package com.go.scms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbTrnClassRoomSubject")
public class SnickClassRoomSubjectRatwEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long classRoomSubjectId;
    
    @Column
    private Integer classRoomId;
    
    @Column
    private String scCode;
    
    @Column
    private String subjectCode;
    
    @Column
    private String subjectNameTh;

    public Long getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(Long classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }
    
}
