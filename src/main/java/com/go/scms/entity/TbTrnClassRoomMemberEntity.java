/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lumin
 */
@Entity
@Table(name = "tbTrnClassRoomMember")
public class TbTrnClassRoomMemberEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long classRoomMemberId;

    @Column
    private String scCode;

    @Column
    private Integer yearGroupId;

    @Column
    private String yearGroupName;

    @Column
    private Integer memberId;

    @Column
    private String stuCode;

    @Column
    private String fullname;

    @Column
    private String fullnameEn;

    @Column
    private Integer classId;

    @Column
    private String className;

    @Column
    private Integer classRoomId;

    @Column
    private String classRoom;

    @Column
    private String classRoomNo;

    @Column
    private String role;

    @Column
    private Boolean iscurrent;

    @Column
    private Integer ratwResult;

    @Column
    private Integer desiredCharResult;

    @Column
    private Integer competenciesResult;

    @Column
    private Date createdDate;

    @Column
    private String createdPage;

    @Column
    private String createdUser;

    @Column
    private String ipaddr;

    @Column
    private Date updatedDate;

    @Column
    private String updatedPage;

    @Column
    private String updatedUser;

    @Column
    private String recordStatus;

    @Column
    private Boolean isactive;
    
    @Column
    private Boolean isEp;

    public Boolean getIsEp() {
        return isEp;
    }

    public void setIsEp(Boolean isEp) {
        this.isEp = isEp;
    }

    public Long getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Long classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(Boolean iscurrent) {
        this.iscurrent = iscurrent;
    }

    public Integer getRatwResult() {
        return ratwResult;
    }

    public void setRatwResult(Integer ratwResult) {
        this.ratwResult = ratwResult;
    }

    public Integer getDesiredCharResult() {
        return desiredCharResult;
    }

    public void setDesiredCharResult(Integer desiredCharResult) {
        this.desiredCharResult = desiredCharResult;
    }

    public Integer getCompetenciesResult() {
        return competenciesResult;
    }

    public void setCompetenciesResult(Integer competenciesResult) {
        this.competenciesResult = competenciesResult;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
}
