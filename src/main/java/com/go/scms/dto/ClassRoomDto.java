/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;

public class ClassRoomDto {

    private String classRoomId;
    private String scCode;
    private String nameClass;
    private Integer nameClassId;
    private String classRoomSnameTh;
    private Integer classRoom;
    private Integer teacherAdvisor1;
    private Integer teacherAdvisor2;
    private Integer teacherAdvisor3;
    private String isEp;
    private String yearGroupName;
    private String religion;
    private String recordStatus;
    private String userName;
    private String p;
    private String result;
    private String yearGroupId;
    private String classId;

    public String getIsEp() {
        return isEp;
    }

    public void setIsEp(String isEp) {
        this.isEp = isEp;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getNameClassId() {
        return nameClassId;
    }

    public void setNameClassId(Integer nameClassId) {
        this.nameClassId = nameClassId;
    }

    public Integer getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(Integer classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getNameClass() {
        return nameClass;
    }

    public void setNameClass(String nameClass) {
        this.nameClass = nameClass;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public Integer getTeacherAdvisor1() {
        return teacherAdvisor1;
    }

    public void setTeacherAdvisor1(Integer teacherAdvisor1) {
        this.teacherAdvisor1 = teacherAdvisor1;
    }

    public Integer getTeacherAdvisor2() {
        return teacherAdvisor2;
    }

    public void setTeacherAdvisor2(Integer teacherAdvisor2) {
        this.teacherAdvisor2 = teacherAdvisor2;
    }

    public Integer getTeacherAdvisor3() {
        return teacherAdvisor3;
    }

    public void setTeacherAdvisor3(Integer teacherAdvisor3) {
        this.teacherAdvisor3 = teacherAdvisor3;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String toString() {
        return new Gson().toJson(this);
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

}
