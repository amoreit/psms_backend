/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import java.util.Date;

/**
 *
 * @author lumin
 */
public class TbTrnClassRoomSubjectScoreDto {

    private Long classRoomSubjectScoreId;
    private String scCode;
    private Integer yearGroupId;
    private String yearGroupName;
    private Integer classRoomMemberId;
    private Integer classRoomSubjectId;
    private Integer classId;
    private Integer classRoomId;
    private String classRoomSnameTh;
    private String stuCode;
    private String fullname;
    private String classRoomNo;
    private Integer departmentId;
    private String subjectCode;
    private String subjectName;
    private String subjectType;
    private Integer credit;
    private Integer itemno;
    private String score1;
    private String fullScore1;
    private String scoreMidterm;
    private String fullScoreMidterm;
    private String score2;
    private String fullScore2;
    private String scoreFinal;
    private String fullScoreFinal;
    private String sumScore;
    private String fullScore;
    private String grade;
    private Boolean passed;
    private Integer orders;
    private Date createDate;
    private String createPage;
    private String createUser;
    private String ipaddr;
    private Date updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private Boolean isactive;
    private String p;
    private String result;
    private String user;
    private String isMorsor;
    private String isExchange;
    private String remark;

    public Long getClassRoomSubjectScoreId() {
        return classRoomSubjectScoreId;
    }

    public void setClassRoomSubjectScoreId(Long classRoomSubjectScoreId) {
        this.classRoomSubjectScoreId = classRoomSubjectScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Integer classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public Integer getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(Integer classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getFullScore1() {
        return fullScore1;
    }

    public void setFullScore1(String fullScore1) {
        this.fullScore1 = fullScore1;
    }

    public String getScoreMidterm() {
        return scoreMidterm;
    }

    public void setScoreMidterm(String scoreMidterm) {
        this.scoreMidterm = scoreMidterm;
    }

    public String getFullScoreMidterm() {
        return fullScoreMidterm;
    }

    public void setFullScoreMidterm(String fullScoreMidterm) {
        this.fullScoreMidterm = fullScoreMidterm;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getFullScore2() {
        return fullScore2;
    }

    public void setFullScore2(String fullScore2) {
        this.fullScore2 = fullScore2;
    }

    public String getScoreFinal() {
        return scoreFinal;
    }

    public void setScoreFinal(String scoreFinal) {
        this.scoreFinal = scoreFinal;
    }

    public String getFullScoreFinal() {
        return fullScoreFinal;
    }

    public void setFullScoreFinal(String fullScoreFinal) {
        this.fullScoreFinal = fullScoreFinal;
    }

    public String getSumScore() {
        return sumScore;
    }

    public void setSumScore(String sumScore) {
        this.sumScore = sumScore;
    }

    public String getFullScore() {
        return fullScore;
    }

    public void setFullScore(String fullScore) {
        this.fullScore = fullScore;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Boolean getPassed() {
        return passed;
    }

    public void setPassed(Boolean passed) {
        this.passed = passed;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreatePage() {
        return createPage;
    }

    public void setCreatePage(String createPage) {
        this.createPage = createPage;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIsMorsor() {
        return isMorsor;
    }

    public void setIsMorsor(String isMorsor) {
        this.isMorsor = isMorsor;
    }

    public String getIsExchange() {
        return isExchange;
    }

    public void setIsExchange(String isExchange) {
        this.isExchange = isExchange;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
