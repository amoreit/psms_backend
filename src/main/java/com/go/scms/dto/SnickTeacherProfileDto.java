package com.go.scms.dto;

import java.util.Date;

public class SnickTeacherProfileDto {
    
    private String teacherId; 
    private String scCode; 
    private String departmentId;
    private String departmentName; 
    private Integer classId;
    private Integer classRoomId;
    private String titleId; 
    private String citizenId;
    private String firstnameTh;
    private String lastnameTh;
    private String fullname;
    private String fullnameEn;
    private String firstnameEn;
    private String lastnameEn;
    private String nickname;
    private String gender;
    private String ethnicity;
    private String citizenship;
    private String religion;
    private String saint;
    private String email;
    private String tel1;
    private String tel2;
    private String regHouseNo;
    private String regAddrNo;
    private String regVillage;
    private String regVillageNo;
    private String regAlley;
    private String regRoad;
    private String regSubDistrict;
    private String regDistrict;
    private String regProvince;
    private String regCountry;
    private String regPostCode;
    private String curAddrNo;
    private String curVillage;
    private String curVillageNo;
    private String curRoad;
    private String curAlley;
    private String curSubDistrict;
    private String curDistrict;
    private String curProvince;
    private String curCountry;
    private String curPostCode;
    private String education;
    private String fatherTitle;
    private String fatherFirstnameTh;
    private String fatherLastnameTh;
    private String motherTitle;
    private String motherFirstnameTh;
    private String motherLastnameTh;
    private String spouseTitle;
    private String spouseFirstname;
    private String spouseLastname;
    private String spouseReligion;
    private String spouseSaint;
    private String spouseChurch;
    private String spouseOffice;
    private String spouseTel1;
    private String spouseTel2;
    private String familyStatus;
    private Integer noOfChild;
    private String childTitle1;
    private String childFirstname1;
    private String childLastname1;
    private String childGender1;
    private Date childDob1;
    private String childClass1;
    private String childTitle2;
    private String childFirstname2;
    private String childLastname2;
    private String childGender2;
    private Date childDob2;
    private String childClass2;
    private String childTitle3;
    private String childFirstname3;
    private String childLastname3;
    private String childGender3;
    private Date childDob3;
    private String childClass3;
    private String teacherImage;
    private Date startWorkDate;
    private Date registeredDate;
    private String leaveDate;
    private String licenseNo;
    private Date licenseStart;
    private Date licenseExpired;
    private String createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private String updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private String p;
    private String result;
    private String userName;
    private String isactive;
    private String teacherCardId;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public String getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(String firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getSaint() {
        return saint;
    }

    public void setSaint(String saint) {
        this.saint = saint;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getRegHouseNo() {
        return regHouseNo;
    }

    public void setRegHouseNo(String regHouseNo) {
        this.regHouseNo = regHouseNo;
    }

    public String getRegAddrNo() {
        return regAddrNo;
    }

    public void setRegAddrNo(String regAddrNo) {
        this.regAddrNo = regAddrNo;
    }

    public String getRegVillage() {
        return regVillage;
    }

    public void setRegVillage(String regVillage) {
        this.regVillage = regVillage;
    }

    public String getRegVillageNo() {
        return regVillageNo;
    }

    public void setRegVillageNo(String regVillageNo) {
        this.regVillageNo = regVillageNo;
    }

    public String getRegAlley() {
        return regAlley;
    }

    public void setRegAlley(String regAlley) {
        this.regAlley = regAlley;
    }

    public String getRegRoad() {
        return regRoad;
    }

    public void setRegRoad(String regRoad) {
        this.regRoad = regRoad;
    }

    public String getRegSubDistrict() {
        return regSubDistrict;
    }

    public void setRegSubDistrict(String regSubDistrict) {
        this.regSubDistrict = regSubDistrict;
    }

    public String getRegDistrict() {
        return regDistrict;
    }

    public void setRegDistrict(String regDistrict) {
        this.regDistrict = regDistrict;
    }

    public String getRegProvince() {
        return regProvince;
    }

    public void setRegProvince(String regProvince) {
        this.regProvince = regProvince;
    }

    public String getRegCountry() {
        return regCountry;
    }

    public void setRegCountry(String regCountry) {
        this.regCountry = regCountry;
    }

    public String getRegPostCode() {
        return regPostCode;
    }

    public void setRegPostCode(String regPostCode) {
        this.regPostCode = regPostCode;
    }

    public String getCurAddrNo() {
        return curAddrNo;
    }

    public void setCurAddrNo(String curAddrNo) {
        this.curAddrNo = curAddrNo;
    }

    public String getCurVillage() {
        return curVillage;
    }

    public void setCurVillage(String curVillage) {
        this.curVillage = curVillage;
    }

    public String getCurVillageNo() {
        return curVillageNo;
    }

    public void setCurVillageNo(String curVillageNo) {
        this.curVillageNo = curVillageNo;
    }

    public String getCurRoad() {
        return curRoad;
    }

    public void setCurRoad(String curRoad) {
        this.curRoad = curRoad;
    }

    public String getCurAlley() {
        return curAlley;
    }

    public void setCurAlley(String curAlley) {
        this.curAlley = curAlley;
    }

    public String getCurSubDistrict() {
        return curSubDistrict;
    }

    public void setCurSubDistrict(String curSubDistrict) {
        this.curSubDistrict = curSubDistrict;
    }

    public String getCurDistrict() {
        return curDistrict;
    }

    public void setCurDistrict(String curDistrict) {
        this.curDistrict = curDistrict;
    }

    public String getCurProvince() {
        return curProvince;
    }

    public void setCurProvince(String curProvince) {
        this.curProvince = curProvince;
    }

    public String getCurCountry() {
        return curCountry;
    }

    public void setCurCountry(String curCountry) {
        this.curCountry = curCountry;
    }

    public String getCurPostCode() {
        return curPostCode;
    }

    public void setCurPostCode(String curPostCode) {
        this.curPostCode = curPostCode;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getFatherTitle() {
        return fatherTitle;
    }

    public void setFatherTitle(String fatherTitle) {
        this.fatherTitle = fatherTitle;
    }

    public String getFatherFirstnameTh() {
        return fatherFirstnameTh;
    }

    public void setFatherFirstnameTh(String fatherFirstnameTh) {
        this.fatherFirstnameTh = fatherFirstnameTh;
    }

    public String getFatherLastnameTh() {
        return fatherLastnameTh;
    }

    public void setFatherLastnameTh(String fatherLastnameTh) {
        this.fatherLastnameTh = fatherLastnameTh;
    }

    public String getMotherTitle() {
        return motherTitle;
    }

    public void setMotherTitle(String motherTitle) {
        this.motherTitle = motherTitle;
    }

    public String getMotherFirstnameTh() {
        return motherFirstnameTh;
    }

    public void setMotherFirstnameTh(String motherFirstnameTh) {
        this.motherFirstnameTh = motherFirstnameTh;
    }

    public String getMotherLastnameTh() {
        return motherLastnameTh;
    }

    public void setMotherLastnameTh(String motherLastnameTh) {
        this.motherLastnameTh = motherLastnameTh;
    }

    public String getSpouseTitle() {
        return spouseTitle;
    }

    public void setSpouseTitle(String spouseTitle) {
        this.spouseTitle = spouseTitle;
    }

    public String getSpouseFirstname() {
        return spouseFirstname;
    }

    public void setSpouseFirstname(String spouseFirstname) {
        this.spouseFirstname = spouseFirstname;
    }

    public String getSpouseLastname() {
        return spouseLastname;
    }

    public void setSpouseLastname(String spouseLastname) {
        this.spouseLastname = spouseLastname;
    }

    public String getSpouseReligion() {
        return spouseReligion;
    }

    public void setSpouseReligion(String spouseReligion) {
        this.spouseReligion = spouseReligion;
    }

    public String getSpouseSaint() {
        return spouseSaint;
    }

    public void setSpouseSaint(String spouseSaint) {
        this.spouseSaint = spouseSaint;
    }

    public String getSpouseChurch() {
        return spouseChurch;
    }

    public void setSpouseChurch(String spouseChurch) {
        this.spouseChurch = spouseChurch;
    }

    public String getSpouseOffice() {
        return spouseOffice;
    }

    public void setSpouseOffice(String spouseOffice) {
        this.spouseOffice = spouseOffice;
    }

    public String getSpouseTel1() {
        return spouseTel1;
    }

    public void setSpouseTel1(String spouseTel1) {
        this.spouseTel1 = spouseTel1;
    }

    public String getSpouseTel2() {
        return spouseTel2;
    }

    public void setSpouseTel2(String spouseTel2) {
        this.spouseTel2 = spouseTel2;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public Integer getNoOfChild() {
        return noOfChild;
    }

    public void setNoOfChild(Integer noOfChild) {
        this.noOfChild = noOfChild;
    }

    public String getChildTitle1() {
        return childTitle1;
    }

    public void setChildTitle1(String childTitle1) {
        this.childTitle1 = childTitle1;
    }

    public String getChildFirstname1() {
        return childFirstname1;
    }

    public void setChildFirstname1(String childFirstname1) {
        this.childFirstname1 = childFirstname1;
    }

    public String getChildLastname1() {
        return childLastname1;
    }

    public void setChildLastname1(String childLastname1) {
        this.childLastname1 = childLastname1;
    }

    public String getChildGender1() {
        return childGender1;
    }

    public void setChildGender1(String childGender1) {
        this.childGender1 = childGender1;
    }

    public Date getChildDob1() {
        return childDob1;
    }

    public void setChildDob1(Date childDob1) {
        this.childDob1 = childDob1;
    }

    public String getChildClass1() {
        return childClass1;
    }

    public void setChildClass1(String childClass1) {
        this.childClass1 = childClass1;
    }

    public String getChildTitle2() {
        return childTitle2;
    }

    public void setChildTitle2(String childTitle2) {
        this.childTitle2 = childTitle2;
    }

    public String getChildFirstname2() {
        return childFirstname2;
    }

    public void setChildFirstname2(String childFirstname2) {
        this.childFirstname2 = childFirstname2;
    }

    public String getChildLastname2() {
        return childLastname2;
    }

    public void setChildLastname2(String childLastname2) {
        this.childLastname2 = childLastname2;
    }

    public String getChildGender2() {
        return childGender2;
    }

    public void setChildGender2(String childGender2) {
        this.childGender2 = childGender2;
    }

    public Date getChildDob2() {
        return childDob2;
    }

    public void setChildDob2(Date childDob2) {
        this.childDob2 = childDob2;
    }

    public String getChildClass2() {
        return childClass2;
    }

    public void setChildClass2(String childClass2) {
        this.childClass2 = childClass2;
    }

    public String getChildTitle3() {
        return childTitle3;
    }

    public void setChildTitle3(String childTitle3) {
        this.childTitle3 = childTitle3;
    }

    public String getChildFirstname3() {
        return childFirstname3;
    }

    public void setChildFirstname3(String childFirstname3) {
        this.childFirstname3 = childFirstname3;
    }

    public String getChildLastname3() {
        return childLastname3;
    }

    public void setChildLastname3(String childLastname3) {
        this.childLastname3 = childLastname3;
    }

    public String getChildGender3() {
        return childGender3;
    }

    public void setChildGender3(String childGender3) {
        this.childGender3 = childGender3;
    }

    public Date getChildDob3() {
        return childDob3;
    }

    public void setChildDob3(Date childDob3) {
        this.childDob3 = childDob3;
    }

    public String getChildClass3() {
        return childClass3;
    }

    public void setChildClass3(String childClass3) {
        this.childClass3 = childClass3;
    }

    public String getTeacherImage() {
        return teacherImage;
    }

    public void setTeacherImage(String teacherImage) {
        this.teacherImage = teacherImage;
    }

    public Date getStartWorkDate() {
        return startWorkDate;
    }

    public void setStartWorkDate(Date startWorkDate) {
        this.startWorkDate = startWorkDate;
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public Date getLicenseStart() {
        return licenseStart;
    }

    public void setLicenseStart(Date licenseStart) {
        this.licenseStart = licenseStart;
    }

    public Date getLicenseExpired() {
        return licenseExpired;
    }

    public void setLicenseExpired(Date licenseExpired) {
        this.licenseExpired = licenseExpired;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }
    
    //aong
    private String smartPurseId;

    public String getSmartPurseId() {
        return smartPurseId;
    }

    public void setSmartPurseId(String smartPurseId) {
        this.smartPurseId = smartPurseId;
    }
    
}
