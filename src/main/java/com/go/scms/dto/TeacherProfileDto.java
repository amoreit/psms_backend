package com.go.scms.dto;

import com.google.gson.Gson;
import java.util.Date;

public class TeacherProfileDto {
    private String teacherId; //Running no.
    private String scCode; //รหัสโรงเรียน PRAMANDA
    private Integer departmentId; //กลุ่มสาระวิชา
    private String departmentName; //ชื่อกลุ่มสาระวิชา
    private Integer classId;//ระดับชั้นเรียนที่สังกัด
    private String titleId; //คำนำหน้าชื่อ
    private String titleIdEn;//คำนำหน้าชื่อ ENG
    private String citizenId; //เลขบัตรประชาชน
    private String teacherCardId;//เลขบัตรประจำตัวอาจารย์
    private String firstnameTh;//ชื่อภาษาไทย
    private String lastnameTh;//นามสกุลภาษาไทย 
    private String fullname;//ชื่อเต็ม
    private String fullnameEn;//ชื่อเต็มภาษาอังกฤษ
    private String firstnameEn;//ชื่อภาษาอังกฤษ
    private String lastnameEn;//นามสกุลภาษาอังกฤษ
    private String nickname;//ชื่อเล่น
    private String gender;//เพศ
    private Date dob;//เพศ
    private String ethnicity;//	เชื้อชาติ
    private String citizenship;//สัญชาติ
    private String religion;//ศาสนา
    private String saint;//นักบุญ
    private String email;//อีเมลล์
    private String tel1;//เบอร์โทรศัพท์ 1
    private String tel2;//เบอร์โทรศัพท์ 2
    private String regHouseNo;//เลขรหัสประจำบ้าน
    private String regAddrNo;//ที่อยู่ตามทะเบียนบ้าน
    private String regVillage;//หมู่บ้าน
    private String regVillageNo;//หมู่
    private String regAlley;//ซอย
    private String regRoad;//ถนน
    private String regSubDistrict;//ตำบล
    private String regDistrict;//อำเภอ
    private String regProvince;//จังหวัด
    private String regCountry;//ประเทศ
    private String regPostCode;//รหัสไปรษณีย์
    private String curAddrNo;//ที่อยู่ปัจจุบัน
    private String curVillage;//หมู่บ้าน
    private String curVillageNo;//หมู่
    private String curRoad;//ถนน
    private String curAlley;//ซอย
    private String curSubDistrict;//ตำบล
    private String curDistrict;//อำเภอ
    private String curProvince;//จังหวัด
    private String curCountry;//ประเทศ
    private String curPostCode;//รหัสไปรษณีย์
    private String education;//ระดับการศึกษา
    private String fatherTitle;//คำหน้าชื่อบิดา
    private String fatherFirstnameTh;//ชื่อบิดาภาษาไทย
    private String fatherLastnameTh;//นามสกุลบิดาภาษาไทย
    private String motherTitle;//คำหน้าชื่อมารดา
    private String motherFirstnameTh;//ชื่อมารดาภาษาไทย
    private String motherLastnameTh;//นามสกุลมารดาภาษาไทย
    private String spouseTitle;//คำนำหน้าชื่อคู่สมรส
    private String spouseFirstname;//ชื่อคู่สมรส
    private String spouseLastname;//นามสกุลคู่สมรส
    private String spouseReligion;//ศาสนาคู่สมรส
    private String spouseSaint;//นักบุญคู่สมรส
    private String spouseChurch;//คู่สมรสสังกัดสัตบุรษวัด
    private String spouseOffice;//ที่ทำงานคู่สมรส
    private String spouseTel1;//เบอร์โทรศัพท์คู่สมรส1
    private String spouseTel2;//เบอร์โทรศัพท์คู่สมรส2    
    private String familyStatus;//สถานะครอบครัว
    private Integer noOfChild;//จำนวนบุตร    
    private String childTitle1;//คำนำหน้าชื่อบุตร1
    private String childFirstname1;//ชื่อบุตร1
    private String childLastname1;//นามสกุลบุตร1
    private String childGender1;//เพศบุตร1
    private Date childDob1;//วันเกิดบุตร1
    private String childClass1;//ชั้นเรียนบุตร1   
    private String childTitle2;//คำนำหน้าชื่อบุตร2
    private String childFirstname2;//ชื่อบุตร2
    private String childLastname2;//นามสกุลบุตร2
    private String childGender2;//เพศบุตร2
    private Date childDob2;//วันเกิดบุตร2
    private String childClass2;//ชั้นเรียนบุตร2
    private String childTitle3;//คำนำหน้าชื่อบุตร3
    private String childFirstname3;//ชื่อบุตร3
    private String childLastname3;//นามสกุลบุตร3
    private String childGender3;//เพศบุตร3
    private Date childDob3;//วันเกิดบุตร3
    private String childClass3;//ชั้นเรียนบุตร3
    private String teacherImage;//path เก็บรูปอาจารย์
    private Date startWorkDate;//วันที่เข้าทำงานวันแรก
    private Date registeredDate;//วันที่ได้รับบรรจุ
    private String leaveDate;//วันที่สิ้นสุดการทำงาน
    private String licenseNo;//เลขที่ใบอนุญาต
    private Date licenseStart;//วันที่ใบอนุญาตเริ่มต้น
    private Date licenseExpired;//วันที่ใบอนุญาตหมดอายุ
    private Integer positionId;
    private String positionName;
    private Integer classRoomId;
    private Integer sectionId;
    private String sectionName;
    private Integer divisionId;
    private String divisionName;
    private Integer subDivisionId;
    private String subDivisionName;
    private Boolean ispermanent;
    private Boolean isinstructor;
    private String createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private String updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private String p;
    private String result;
    private String userName;
    private String isactive;
    private String smartPurseId;
    private String scoutRank;
    private String educationDesc;
    
    public String getSmartPurseId() {
        return smartPurseId;
    }

    public void setSmartPurseId(String smartPurseId) {
        this.smartPurseId = smartPurseId;
    }

    public Boolean getIsinstructor() {
        return isinstructor;
    }

    public void setIsinstructor(Boolean isinstructor) {
        this.isinstructor = isinstructor;
    }
    
    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }
    
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getRegHouseNo() {
        return regHouseNo;
    }

    public void setRegHouseNo(String regHouseNo) {
        this.regHouseNo = regHouseNo;
    }

    public String getRegAddrNo() {
        return regAddrNo;
    }

    public void setRegAddrNo(String regAddrNo) {
        this.regAddrNo = regAddrNo;
    }

    public String getRegVillage() {
        return regVillage;
    }

    public void setRegVillage(String regVillage) {
        this.regVillage = regVillage;
    }

    public String getRegVillageNo() {
        return regVillageNo;
    }

    public void setRegVillageNo(String regVillageNo) {
        this.regVillageNo = regVillageNo;
    }

    public String getRegRoad() {
        return regRoad;
    }

    public void setRegRoad(String regRoad) {
        this.regRoad = regRoad;
    }

    public String getRegSubDistrict() {
        return regSubDistrict;
    }

    public void setRegSubDistrict(String regSubDistrict) {
        this.regSubDistrict = regSubDistrict;
    }

    public String getRegDistrict() {
        return regDistrict;
    }

    public void setRegDistrict(String regDistrict) {
        this.regDistrict = regDistrict;
    }

    public String getRegProvince() {
        return regProvince;
    }

    public void setRegProvince(String regProvince) {
        this.regProvince = regProvince;
    }

    public String getRegCountry() {
        return regCountry;
    }

    public void setRegCountry(String regCountry) {
        this.regCountry = regCountry;
    }

    public String getRegPostCode() {
        return regPostCode;
    }

    public void setRegPostCode(String regPostCode) {
        this.regPostCode = regPostCode;
    }

    public String getCurAddrNo() {
        return curAddrNo;
    }

    public void setCurAddrNo(String curAddrNo) {
        this.curAddrNo = curAddrNo;
    }

    public String getCurVillage() {
        return curVillage;
    }

    public void setCurVillage(String curVillage) {
        this.curVillage = curVillage;
    }

    public String getCurVillageNo() {
        return curVillageNo;
    }

    public void setCurVillageNo(String curVillageNo) {
        this.curVillageNo = curVillageNo;
    }

    public String getCurRoad() {
        return curRoad;
    }

    public void setCurRoad(String curRoad) {
        this.curRoad = curRoad;
    }

    public String getCurSubDistrict() {
        return curSubDistrict;
    }

    public void setCurSubDistrict(String curSubDistrict) {
        this.curSubDistrict = curSubDistrict;
    }

    public String getCurDistrict() {
        return curDistrict;
    }

    public void setCurDistrict(String curDistrict) {
        this.curDistrict = curDistrict;
    }

    public String getCurProvince() {
        return curProvince;
    }

    public void setCurProvince(String curProvince) {
        this.curProvince = curProvince;
    }

    public String getCurCountry() {
        return curCountry;
    }

    public void setCurCountry(String curCountry) {
        this.curCountry = curCountry;
    }

    public String getCurPostCode() {
        return curPostCode;
    }

    public void setCurPostCode(String curPostCode) {
        this.curPostCode = curPostCode;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public Date getLicenseStart() {
        return licenseStart;
    }

    public void setLicenseStart(Date licenseStart) {
        this.licenseStart = licenseStart;
    }

    public Date getLicenseExpired() {
        return licenseExpired;
    }

    public void setLicenseExpired(Date licenseExpired) {
        this.licenseExpired = licenseExpired;
    }

    public String getFatherFirstnameTh() {
        return fatherFirstnameTh;
    }

    public void setFatherFirstnameTh(String fatherFirstnameTh) {
        this.fatherFirstnameTh = fatherFirstnameTh;
    }

    public String getFatherLastnameTh() {
        return fatherLastnameTh;
    }

    public void setFatherLastnameTh(String fatherLastnameTh) {
        this.fatherLastnameTh = fatherLastnameTh;
    }

    public String getMotherFirstnameTh() {
        return motherFirstnameTh;
    }

    public void setMotherFirstnameTh(String motherFirstnameTh) {
        this.motherFirstnameTh = motherFirstnameTh;
    }

    public String getMotherLastnameTh() {
        return motherLastnameTh;
    }

    public void setMotherLastnameTh(String motherLastnameTh) {
        this.motherLastnameTh = motherLastnameTh;
    }

    public String getSpouseTitle() {
        return spouseTitle;
    }

    public void setSpouseTitle(String spouseTitle) {
        this.spouseTitle = spouseTitle;
    }

    public String getSpouseFirstname() {
        return spouseFirstname;
    }

    public void setSpouseFirstname(String spouseFirstname) {
        this.spouseFirstname = spouseFirstname;
    }

    public String getSpouseLastname() {
        return spouseLastname;
    }

    public void setSpouseLastname(String spouseLastname) {
        this.spouseLastname = spouseLastname;
    }

    public String getSpouseReligion() {
        return spouseReligion;
    }

    public void setSpouseReligion(String spouseReligion) {
        this.spouseReligion = spouseReligion;
    }

    public String getSpouseSaint() {
        return spouseSaint;
    }

    public void setSpouseSaint(String spouseSaint) {
        this.spouseSaint = spouseSaint;
    }

    public String getSpouseChurch() {
        return spouseChurch;
    }

    public void setSpouseChurch(String spouseChurch) {
        this.spouseChurch = spouseChurch;
    }

    public String getSpouseOffice() {
        return spouseOffice;
    }

    public void setSpouseOffice(String spouseOffice) {
        this.spouseOffice = spouseOffice;
    }

    public String getSpouseTel1() {
        return spouseTel1;
    }

    public void setSpouseTel1(String spouseTel1) {
        this.spouseTel1 = spouseTel1;
    }

    public String getSpouseTel2() {
        return spouseTel2;
    }

    public void setSpouseTel2(String spouseTel2) {
        this.spouseTel2 = spouseTel2;
    }

    public Integer getNoOfChild() {
        return noOfChild;
    }

    public void setNoOfChild(Integer noOfChild) {
        this.noOfChild = noOfChild;
    }

    public Date getStartWorkDate() {
        return startWorkDate;
    }

    public void setStartWorkDate(Date startWorkDate) {
        this.startWorkDate = startWorkDate;
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(String firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getSaint() {
        return saint;
    }

    public void setSaint(String saint) {
        this.saint = saint;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }
    
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getTeacherImage() {
        return teacherImage;
    }

    public void setTeacherImage(String teacherImage) {
        this.teacherImage = teacherImage;
    }

    public String getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(String leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getFatherTitle() {
        return fatherTitle;
    }

    public void setFatherTitle(String fatherTitle) {
        this.fatherTitle = fatherTitle;
    }

    public String getMotherTitle() {
        return motherTitle;
    }

    public void setMotherTitle(String motherTitle) {
        this.motherTitle = motherTitle;
    }


    public String getCurAlley() {
        return curAlley;
    }

    public void setCurAlley(String curAlley) {
        this.curAlley = curAlley;
    }

    public String getRegAlley() {
        return regAlley;
    }

    public void setRegAlley(String regAlley) {
        this.regAlley = regAlley;
    }


    public String getChildTitle1() {
        return childTitle1;
    }

    public void setChildTitle1(String childTitle1) {
        this.childTitle1 = childTitle1;
    }

    public String getChildFirstname1() {
        return childFirstname1;
    }

    public void setChildFirstname1(String childFirstname1) {
        this.childFirstname1 = childFirstname1;
    }

    public String getChildLastname1() {
        return childLastname1;
    }

    public void setChildLastname1(String childLastname1) {
        this.childLastname1 = childLastname1;
    }

    public String getChildGender1() {
        return childGender1;
    }

    public void setChildGender1(String childGender1) {
        this.childGender1 = childGender1;
    }

    public Date getChildDob1() {
        return childDob1;
    }

    public void setChildDob1(Date childDob1) {
        this.childDob1 = childDob1;
    }

    public String getChildClass1() {
        return childClass1;
    }

    public void setChildClass1(String childClass1) {
        this.childClass1 = childClass1;
    }

    public String getChildTitle2() {
        return childTitle2;
    }

    public void setChildTitle2(String childTitle2) {
        this.childTitle2 = childTitle2;
    }

    public String getChildFirstname2() {
        return childFirstname2;
    }

    public void setChildFirstname2(String childFirstname2) {
        this.childFirstname2 = childFirstname2;
    }

    public String getChildLastname2() {
        return childLastname2;
    }

    public void setChildLastname2(String childLastname2) {
        this.childLastname2 = childLastname2;
    }

    public String getChildGender2() {
        return childGender2;
    }

    public void setChildGender2(String childGender2) {
        this.childGender2 = childGender2;
    }

    public Date getChildDob2() {
        return childDob2;
    }

    public void setChildDob2(Date childDob2) {
        this.childDob2 = childDob2;
    }

    public String getChildClass2() {
        return childClass2;
    }

    public void setChildClass2(String childClass2) {
        this.childClass2 = childClass2;
    }

    public String getChildTitle3() {
        return childTitle3;
    }

    public void setChildTitle3(String childTitle3) {
        this.childTitle3 = childTitle3;
    }

    public String getChildFirstname3() {
        return childFirstname3;
    }

    public void setChildFirstname3(String childFirstname3) {
        this.childFirstname3 = childFirstname3;
    }

    public String getChildLastname3() {
        return childLastname3;
    }

    public void setChildLastname3(String childLastname3) {
        this.childLastname3 = childLastname3;
    }

    public String getChildGender3() {
        return childGender3;
    }

    public void setChildGender3(String childGender3) {
        this.childGender3 = childGender3;
    }

    public Date getChildDob3() {
        return childDob3;
    }

    public void setChildDob3(Date childDob3) {
        this.childDob3 = childDob3;
    }

    public String getChildClass3() {
        return childClass3;
    }

    public void setChildClass3(String childClass3) {
        this.childClass3 = childClass3;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    
    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getTitleIdEn() {
        return titleIdEn;
    }

    public void setTitleIdEn(String titleIdEn) {
        this.titleIdEn = titleIdEn;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public Integer getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Integer divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public Integer getSubDivisionId() {
        return subDivisionId;
    }

    public void setSubDivisionId(Integer subDivisionId) {
        this.subDivisionId = subDivisionId;
    }

    public String getSubDivisionName() {
        return subDivisionName;
    }

    public void setSubDivisionName(String subDivisionName) {
        this.subDivisionName = subDivisionName;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Boolean getIspermanent() {
        return ispermanent;
    }

    public void setIspermanent(Boolean ispermanent) {
        this.ispermanent = ispermanent;
    }

    public String getScoutRank() {
        return scoutRank;
    }

    public void setScoutRank(String scoutRank) {
        this.scoutRank = scoutRank;
    }

    public String getEducationDesc() {
        return educationDesc;
    }

    public void setEducationDesc(String educationDesc) {
        this.educationDesc = educationDesc;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
}
