/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;
import java.util.Date;

public class StuProfileDto {

    private String stuProfileId;
    private String teacherId;
    private String scCode;
    private String stuCode;
    private String citizenId;
    private String title;
    private String firstnameTh;
    private String lastnameTh;
    private String fullname;
    private String titleEn;
    private String firstnameEn;
    private String lastnameEn;
    private String fullnameEn;
    private String nickname;
    private String gender;
    private Integer genderId;
    private String religion;
    private String saint;
    private String church;
    private Date dob;
    private String familyStatus;
    private String ethnicity;
    private String citizenship;
    private String bloodType;
    private String weight;
    private String height;
    private String tel1;
    private String tel2;
    private String email;
    private String specialSkill;
    private String toClass;
    private String toYearGroup;
    private String exSchool;
    private String lastClass;
    private Double exGpa;
    private String regVillage;
    private String regAddrNo;
    private String regVillageNo;
    private String regRoad;
    private String regHouseNo;
    private String regAlley;
    private String regCountry;
    private String regProvince;
    private String regDistrict;
    private String regSubDistrict;
    private String regPostCode;
    private String curVillage;
    private String curAddrNo;
    private String curVillageNo;
    private String curRoad;
    private String curAlley;
    private String curCountry;
    private String curProvince;
    private String curDistrict;
    private String curSubDistrict;
    private String curPostCode;
    private String pob;
    private String pobProvince;
    private String pobDistrict;
    private String pobSubDistrict;
    private Integer noOfRelatives;
    private String relativesStuCode1;
    private String relativesStuCode2;
    private String relativesStuCode3;
    private String medicalInfo;
    private Boolean isDisabled;
    private String disability;
    private String fatherCitizenId;
    private String fatherTitle;
    private String fatherTitleEn;
    private String fatherFirstnameTh;
    private String fatherLastnameTh;
    private String fatherFirstnameEn;
    private String fatherLastnameEn;
    private String fatherEthnicity;
    private String fatherCitizenship;
    private String fatherReligion;
    private String fatherSaint;
    private String fatherEducation;
    private String fatherOccupation;
    private String fatherOffice;
    private Integer fatherAnnualIncome;
    private String fatherVillage;
    private String fatherAddrNo;
    private String fatherVillageNo;
    private String fatherRoad;
    private String fatherAlley;
    private String fatherCountry;
    private String fatherProvince;
    private String fatherDistrict;
    private String fatherSubDistrict;
    private String fatherPostCode;
    private String fatherTel1;
    private String fatherTel2;
    private String fatherEmail;
    private Boolean fatherAlive;
    private String motherCitizenId;
    private String motherTitle;
    private String motherTitleEn;
    private String motherFirstnameTh;
    private String motherLastnameTh;
    private String motherFirstnameEn;
    private String motherLastnameEn;
    private String motherEthnicity;
    private String motherCitizenship;
    private String motherReligion;
    private String motherSaint;
    private String motherEducation;
    private String motherOccupation;
    private String motherOffice;
    private Integer motherAnnualIncome;
    private String motherVillage;
    private String motherAddrNo;
    private String motherVillageNo;
    private String motherRoad;
    private String motherAlley;
    private String motherCountry;
    private String motherProvince;
    private String motherDistrict;
    private String motherSubDistrict;
    private String motherPostCode;
    private String motherTel1;
    private String motherTel2;
    private String motherEmail;
    private Boolean motherAlive;
    private String parentCitizenId;
    private String parentTitle;
    private String parentTitleEn;
    private String parentFirstnameTh;
    private String parentLastnameTh;
    private String parentFirstnameEn;
    private String parentLastnameEn;
    private String parentEthnicity;
    private String parentCitizenship;
    private String parentReligion;
    private String parentSaint;
    private String parentEducation;
    private String parentOccupation;
    private String parentOffice;
    private Integer parentAnnualIncome;
    private String parentStatus;
    private String parentVillage;
    private String parentAddrNo;
    private String parentVillageNo;
    private String parentRoad;
    private String parentAlley;
    private String parentCountry;
    private String parentProvince;
    private String parentDistrict;
    private String parentSubDistrict;
    private String parentPostCode;
    private String parentTel1;
    private String parentTel2;
    private String parentEmail;
    private String classId;
    private String classRoomId;
    private String classRoom;
    private String stuStatus;
    private String userName;
    private String stuImage;
    private String isleft;
    private String leftReason;
    private String p;
    private String result; 

    private String stuProfileDocId;
    private String dataPathImage;
    private String dataPathBirth;
    private String dataPathRegister;
    private String dataPathReligion;
    private String dataPathChangeName;
    private String dataPathTranscript7;
    private String dataPathTranscript1;
    private String dataPathRecord8;
    private String dataPathTransfer;
    private String dataPathFatherRegister;
    private String dataPathFatherCitizenId;
    private String dataPathMotherRegister;
    private String dataPathMotherCitizenId;
    private String dataPathParentRegister;
    private String dataPathParentCitizenId;

    private String smartPurseId;
    private String smartPurseCardNo;
    
    //by Aong
    private String signatureImage;

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }
    
    //by Moss
    private String stuStatusId;

    public String getIsleft() {
        return isleft;
    }

    public void setIsleft(String isleft) {
        this.isleft = isleft;
    }

    public String getLeftReason() {
        return leftReason;
    }

    public void setLeftReason(String leftReason) {
        this.leftReason = leftReason;
    }

    public String getStuStatusId() {
        return stuStatusId;
    }

    public void setStuStatusId(String stuStatusId) {
        this.stuStatusId = stuStatusId;
    }

    //create yeargroupId by nock
    private Integer yearGroupId;

    public String getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(String stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getFirstnameEn() {
        return firstnameEn;
    }

    public void setFirstnameEn(String firstnameEn) {
        this.firstnameEn = firstnameEn;
    }

    public String getLastnameEn() {
        return lastnameEn;
    }

    public void setLastnameEn(String lastnameEn) {
        this.lastnameEn = lastnameEn;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getSaint() {
        return saint;
    }

    public void setSaint(String saint) {
        this.saint = saint;
    }

    public String getChurch() {
        return church;
    }

    public void setChurch(String church) {
        this.church = church;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpecialSkill() {
        return specialSkill;
    }

    public void setSpecialSkill(String specialSkill) {
        this.specialSkill = specialSkill;
    }

    public String getToClass() {
        return toClass;
    }

    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    public String getToYearGroup() {
        return toYearGroup;
    }

    public void setToYearGroup(String toYearGroup) {
        this.toYearGroup = toYearGroup;
    }

    public String getExSchool() {
        return exSchool;
    }

    public void setExSchool(String exSchool) {
        this.exSchool = exSchool;
    }

    public String getLastClass() {
        return lastClass;
    }

    public void setLastClass(String lastClass) {
        this.lastClass = lastClass;
    }

    public Double getExGpa() {
        return exGpa;
    }

    public void setExGpa(Double exGpa) {
        this.exGpa = exGpa;
    }

    public String getRegVillage() {
        return regVillage;
    }

    public void setRegVillage(String regVillage) {
        this.regVillage = regVillage;
    }

    public String getRegAddrNo() {
        return regAddrNo;
    }

    public void setRegAddrNo(String regAddrNo) {
        this.regAddrNo = regAddrNo;
    }

    public String getRegVillageNo() {
        return regVillageNo;
    }

    public void setRegVillageNo(String regVillageNo) {
        this.regVillageNo = regVillageNo;
    }

    public String getRegRoad() {
        return regRoad;
    }

    public void setRegRoad(String regRoad) {
        this.regRoad = regRoad;
    }

    public String getRegHouseNo() {
        return regHouseNo;
    }

    public void setRegHouseNo(String regHouseNo) {
        this.regHouseNo = regHouseNo;
    }

    public String getRegAlley() {
        return regAlley;
    }

    public void setRegAlley(String regAlley) {
        this.regAlley = regAlley;
    }

    public String getRegCountry() {
        return regCountry;
    }

    public void setRegCountry(String regCountry) {
        this.regCountry = regCountry;
    }

    public String getRegProvince() {
        return regProvince;
    }

    public void setRegProvince(String regProvince) {
        this.regProvince = regProvince;
    }

    public String getRegDistrict() {
        return regDistrict;
    }

    public void setRegDistrict(String regDistrict) {
        this.regDistrict = regDistrict;
    }

    public String getRegSubDistrict() {
        return regSubDistrict;
    }

    public void setRegSubDistrict(String regSubDistrict) {
        this.regSubDistrict = regSubDistrict;
    }

    public String getRegPostCode() {
        return regPostCode;
    }

    public void setRegPostCode(String regPostCode) {
        this.regPostCode = regPostCode;
    }

    public String getCurVillage() {
        return curVillage;
    }

    public void setCurVillage(String curVillage) {
        this.curVillage = curVillage;
    }

    public String getCurAddrNo() {
        return curAddrNo;
    }

    public void setCurAddrNo(String curAddrNo) {
        this.curAddrNo = curAddrNo;
    }

    public String getCurVillageNo() {
        return curVillageNo;
    }

    public void setCurVillageNo(String curVillageNo) {
        this.curVillageNo = curVillageNo;
    }

    public String getCurRoad() {
        return curRoad;
    }

    public void setCurRoad(String curRoad) {
        this.curRoad = curRoad;
    }

    public String getCurAlley() {
        return curAlley;
    }

    public void setCurAlley(String curAlley) {
        this.curAlley = curAlley;
    }

    public String getCurCountry() {
        return curCountry;
    }

    public void setCurCountry(String curCountry) {
        this.curCountry = curCountry;
    }

    public String getCurProvince() {
        return curProvince;
    }

    public void setCurProvince(String curProvince) {
        this.curProvince = curProvince;
    }

    public String getCurDistrict() {
        return curDistrict;
    }

    public void setCurDistrict(String curDistrict) {
        this.curDistrict = curDistrict;
    }

    public String getCurSubDistrict() {
        return curSubDistrict;
    }

    public void setCurSubDistrict(String curSubDistrict) {
        this.curSubDistrict = curSubDistrict;
    }

    public String getCurPostCode() {
        return curPostCode;
    }

    public void setCurPostCode(String curPostCode) {
        this.curPostCode = curPostCode;
    }

    public String getPob() {
        return pob;
    }

    public void setPob(String pob) {
        this.pob = pob;
    }

    public String getPobProvince() {
        return pobProvince;
    }

    public void setPobProvince(String pobProvince) {
        this.pobProvince = pobProvince;
    }

    public String getPobDistrict() {
        return pobDistrict;
    }

    public void setPobDistrict(String pobDistrict) {
        this.pobDistrict = pobDistrict;
    }

    public String getPobSubDistrict() {
        return pobSubDistrict;
    }

    public void setPobSubDistrict(String pobSubDistrict) {
        this.pobSubDistrict = pobSubDistrict;
    }

    public Integer getNoOfRelatives() {
        return noOfRelatives;
    }

    public void setNoOfRelatives(Integer noOfRelatives) {
        this.noOfRelatives = noOfRelatives;
    }

    public String getRelativesStuCode1() {
        return relativesStuCode1;
    }

    public void setRelativesStuCode1(String relativesStuCode1) {
        this.relativesStuCode1 = relativesStuCode1;
    }

    public String getRelativesStuCode2() {
        return relativesStuCode2;
    }

    public void setRelativesStuCode2(String relativesStuCode2) {
        this.relativesStuCode2 = relativesStuCode2;
    }

    public String getRelativesStuCode3() {
        return relativesStuCode3;
    }

    public void setRelativesStuCode3(String relativesStuCode3) {
        this.relativesStuCode3 = relativesStuCode3;
    }

    public String getMedicalInfo() {
        return medicalInfo;
    }

    public void setMedicalInfo(String medicalInfo) {
        this.medicalInfo = medicalInfo;
    }

    public Boolean getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Boolean isDisabled) {
        this.isDisabled = isDisabled;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getFatherCitizenId() {
        return fatherCitizenId;
    }

    public void setFatherCitizenId(String fatherCitizenId) {
        this.fatherCitizenId = fatherCitizenId;
    }

    public String getFatherTitle() {
        return fatherTitle;
    }

    public void setFatherTitle(String fatherTitle) {
        this.fatherTitle = fatherTitle;
    }

    public String getFatherTitleEn() {
        return fatherTitleEn;
    }

    public void setFatherTitleEn(String fatherTitleEn) {
        this.fatherTitleEn = fatherTitleEn;
    }

    public String getFatherFirstnameTh() {
        return fatherFirstnameTh;
    }

    public void setFatherFirstnameTh(String fatherFirstnameTh) {
        this.fatherFirstnameTh = fatherFirstnameTh;
    }

    public String getFatherLastnameTh() {
        return fatherLastnameTh;
    }

    public void setFatherLastnameTh(String fatherLastnameTh) {
        this.fatherLastnameTh = fatherLastnameTh;
    }

    public String getFatherFirstnameEn() {
        return fatherFirstnameEn;
    }

    public void setFatherFirstnameEn(String fatherFirstnameEn) {
        this.fatherFirstnameEn = fatherFirstnameEn;
    }

    public String getFatherLastnameEn() {
        return fatherLastnameEn;
    }

    public void setFatherLastnameEn(String fatherLastnameEn) {
        this.fatherLastnameEn = fatherLastnameEn;
    }

    public String getFatherEthnicity() {
        return fatherEthnicity;
    }

    public void setFatherEthnicity(String fatherEthnicity) {
        this.fatherEthnicity = fatherEthnicity;
    }

    public String getFatherCitizenship() {
        return fatherCitizenship;
    }

    public void setFatherCitizenship(String fatherCitizenship) {
        this.fatherCitizenship = fatherCitizenship;
    }

    public String getFatherReligion() {
        return fatherReligion;
    }

    public void setFatherReligion(String fatherReligion) {
        this.fatherReligion = fatherReligion;
    }

    public String getFatherSaint() {
        return fatherSaint;
    }

    public void setFatherSaint(String fatherSaint) {
        this.fatherSaint = fatherSaint;
    }

    public String getFatherEducation() {
        return fatherEducation;
    }

    public void setFatherEducation(String fatherEducation) {
        this.fatherEducation = fatherEducation;
    }

    public String getFatherOccupation() {
        return fatherOccupation;
    }

    public void setFatherOccupation(String fatherOccupation) {
        this.fatherOccupation = fatherOccupation;
    }

    public String getFatherOffice() {
        return fatherOffice;
    }

    public void setFatherOffice(String fatherOffice) {
        this.fatherOffice = fatherOffice;
    }

    public Integer getFatherAnnualIncome() {
        return fatherAnnualIncome;
    }

    public void setFatherAnnualIncome(Integer fatherAnnualIncome) {
        this.fatherAnnualIncome = fatherAnnualIncome;
    }

    public String getFatherVillage() {
        return fatherVillage;
    }

    public void setFatherVillage(String fatherVillage) {
        this.fatherVillage = fatherVillage;
    }

    public String getFatherAddrNo() {
        return fatherAddrNo;
    }

    public void setFatherAddrNo(String fatherAddrNo) {
        this.fatherAddrNo = fatherAddrNo;
    }

    public String getFatherVillageNo() {
        return fatherVillageNo;
    }

    public void setFatherVillageNo(String fatherVillageNo) {
        this.fatherVillageNo = fatherVillageNo;
    }

    public String getFatherRoad() {
        return fatherRoad;
    }

    public void setFatherRoad(String fatherRoad) {
        this.fatherRoad = fatherRoad;
    }

    public String getFatherAlley() {
        return fatherAlley;
    }

    public void setFatherAlley(String fatherAlley) {
        this.fatherAlley = fatherAlley;
    }

    public String getFatherCountry() {
        return fatherCountry;
    }

    public void setFatherCountry(String fatherCountry) {
        this.fatherCountry = fatherCountry;
    }

    public String getFatherProvince() {
        return fatherProvince;
    }

    public void setFatherProvince(String fatherProvince) {
        this.fatherProvince = fatherProvince;
    }

    public String getFatherDistrict() {
        return fatherDistrict;
    }

    public void setFatherDistrict(String fatherDistrict) {
        this.fatherDistrict = fatherDistrict;
    }

    public String getFatherSubDistrict() {
        return fatherSubDistrict;
    }

    public void setFatherSubDistrict(String fatherSubDistrict) {
        this.fatherSubDistrict = fatherSubDistrict;
    }

    public String getFatherPostCode() {
        return fatherPostCode;
    }

    public void setFatherPostCode(String fatherPostCode) {
        this.fatherPostCode = fatherPostCode;
    }

    public String getFatherTel1() {
        return fatherTel1;
    }

    public void setFatherTel1(String fatherTel1) {
        this.fatherTel1 = fatherTel1;
    }

    public String getFatherTel2() {
        return fatherTel2;
    }

    public void setFatherTel2(String fatherTel2) {
        this.fatherTel2 = fatherTel2;
    }

    public String getFatherEmail() {
        return fatherEmail;
    }

    public void setFatherEmail(String fatherEmail) {
        this.fatherEmail = fatherEmail;
    }

    public Boolean getFatherAlive() {
        return fatherAlive;
    }

    public void setFatherAlive(Boolean fatherAlive) {
        this.fatherAlive = fatherAlive;
    }

    public String getMotherCitizenId() {
        return motherCitizenId;
    }

    public void setMotherCitizenId(String motherCitizenId) {
        this.motherCitizenId = motherCitizenId;
    }

    public String getMotherTitle() {
        return motherTitle;
    }

    public void setMotherTitle(String motherTitle) {
        this.motherTitle = motherTitle;
    }

    public String getMotherTitleEn() {
        return motherTitleEn;
    }

    public void setMotherTitleEn(String motherTitleEn) {
        this.motherTitleEn = motherTitleEn;
    }

    public String getMotherFirstnameTh() {
        return motherFirstnameTh;
    }

    public void setMotherFirstnameTh(String motherFirstnameTh) {
        this.motherFirstnameTh = motherFirstnameTh;
    }

    public String getMotherLastnameTh() {
        return motherLastnameTh;
    }

    public void setMotherLastnameTh(String motherLastnameTh) {
        this.motherLastnameTh = motherLastnameTh;
    }

    public String getMotherFirstnameEn() {
        return motherFirstnameEn;
    }

    public void setMotherFirstnameEn(String motherFirstnameEn) {
        this.motherFirstnameEn = motherFirstnameEn;
    }

    public String getMotherLastnameEn() {
        return motherLastnameEn;
    }

    public void setMotherLastnameEn(String motherLastnameEn) {
        this.motherLastnameEn = motherLastnameEn;
    }

    public String getMotherEthnicity() {
        return motherEthnicity;
    }

    public void setMotherEthnicity(String motherEthnicity) {
        this.motherEthnicity = motherEthnicity;
    }

    public String getMotherCitizenship() {
        return motherCitizenship;
    }

    public void setMotherCitizenship(String motherCitizenship) {
        this.motherCitizenship = motherCitizenship;
    }

    public String getMotherReligion() {
        return motherReligion;
    }

    public void setMotherReligion(String motherReligion) {
        this.motherReligion = motherReligion;
    }

    public String getMotherSaint() {
        return motherSaint;
    }

    public void setMotherSaint(String motherSaint) {
        this.motherSaint = motherSaint;
    }

    public String getMotherEducation() {
        return motherEducation;
    }

    public void setMotherEducation(String motherEducation) {
        this.motherEducation = motherEducation;
    }

    public String getMotherOccupation() {
        return motherOccupation;
    }

    public void setMotherOccupation(String motherOccupation) {
        this.motherOccupation = motherOccupation;
    }

    public String getMotherOffice() {
        return motherOffice;
    }

    public void setMotherOffice(String motherOffice) {
        this.motherOffice = motherOffice;
    }

    public Integer getMotherAnnualIncome() {
        return motherAnnualIncome;
    }

    public void setMotherAnnualIncome(Integer motherAnnualIncome) {
        this.motherAnnualIncome = motherAnnualIncome;
    }

    public String getMotherVillage() {
        return motherVillage;
    }

    public void setMotherVillage(String motherVillage) {
        this.motherVillage = motherVillage;
    }

    public String getMotherAddrNo() {
        return motherAddrNo;
    }

    public void setMotherAddrNo(String motherAddrNo) {
        this.motherAddrNo = motherAddrNo;
    }

    public String getMotherVillageNo() {
        return motherVillageNo;
    }

    public void setMotherVillageNo(String motherVillageNo) {
        this.motherVillageNo = motherVillageNo;
    }

    public String getMotherRoad() {
        return motherRoad;
    }

    public void setMotherRoad(String motherRoad) {
        this.motherRoad = motherRoad;
    }

    public String getMotherAlley() {
        return motherAlley;
    }

    public void setMotherAlley(String motherAlley) {
        this.motherAlley = motherAlley;
    }

    public String getMotherCountry() {
        return motherCountry;
    }

    public void setMotherCountry(String motherCountry) {
        this.motherCountry = motherCountry;
    }

    public String getMotherProvince() {
        return motherProvince;
    }

    public void setMotherProvince(String motherProvince) {
        this.motherProvince = motherProvince;
    }

    public String getMotherDistrict() {
        return motherDistrict;
    }

    public void setMotherDistrict(String motherDistrict) {
        this.motherDistrict = motherDistrict;
    }

    public String getMotherSubDistrict() {
        return motherSubDistrict;
    }

    public void setMotherSubDistrict(String motherSubDistrict) {
        this.motherSubDistrict = motherSubDistrict;
    }

    public String getMotherPostCode() {
        return motherPostCode;
    }

    public void setMotherPostCode(String motherPostCode) {
        this.motherPostCode = motherPostCode;
    }

    public String getMotherTel1() {
        return motherTel1;
    }

    public void setMotherTel1(String motherTel1) {
        this.motherTel1 = motherTel1;
    }

    public String getMotherTel2() {
        return motherTel2;
    }

    public void setMotherTel2(String motherTel2) {
        this.motherTel2 = motherTel2;
    }

    public String getMotherEmail() {
        return motherEmail;
    }

    public void setMotherEmail(String motherEmail) {
        this.motherEmail = motherEmail;
    }

    public Boolean getMotherAlive() {
        return motherAlive;
    }

    public void setMotherAlive(Boolean motherAlive) {
        this.motherAlive = motherAlive;
    }

    public String getParentCitizenId() {
        return parentCitizenId;
    }

    public void setParentCitizenId(String parentCitizenId) {
        this.parentCitizenId = parentCitizenId;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentTitleEn() {
        return parentTitleEn;
    }

    public void setParentTitleEn(String parentTitleEn) {
        this.parentTitleEn = parentTitleEn;
    }

    public String getParentFirstnameTh() {
        return parentFirstnameTh;
    }

    public void setParentFirstnameTh(String parentFirstnameTh) {
        this.parentFirstnameTh = parentFirstnameTh;
    }

    public String getParentLastnameTh() {
        return parentLastnameTh;
    }

    public void setParentLastnameTh(String parentLastnameTh) {
        this.parentLastnameTh = parentLastnameTh;
    }

    public String getParentFirstnameEn() {
        return parentFirstnameEn;
    }

    public void setParentFirstnameEn(String parentFirstnameEn) {
        this.parentFirstnameEn = parentFirstnameEn;
    }

    public String getParentLastnameEn() {
        return parentLastnameEn;
    }

    public void setParentLastnameEn(String parentLastnameEn) {
        this.parentLastnameEn = parentLastnameEn;
    }

    public String getParentEthnicity() {
        return parentEthnicity;
    }

    public void setParentEthnicity(String parentEthnicity) {
        this.parentEthnicity = parentEthnicity;
    }

    public String getParentCitizenship() {
        return parentCitizenship;
    }

    public void setParentCitizenship(String parentCitizenship) {
        this.parentCitizenship = parentCitizenship;
    }

    public String getParentReligion() {
        return parentReligion;
    }

    public void setParentReligion(String parentReligion) {
        this.parentReligion = parentReligion;
    }

    public String getParentSaint() {
        return parentSaint;
    }

    public void setParentSaint(String parentSaint) {
        this.parentSaint = parentSaint;
    }

    public String getParentEducation() {
        return parentEducation;
    }

    public void setParentEducation(String parentEducation) {
        this.parentEducation = parentEducation;
    }

    public String getParentOccupation() {
        return parentOccupation;
    }

    public void setParentOccupation(String parentOccupation) {
        this.parentOccupation = parentOccupation;
    }

    public String getParentOffice() {
        return parentOffice;
    }

    public void setParentOffice(String parentOffice) {
        this.parentOffice = parentOffice;
    }

    public Integer getParentAnnualIncome() {
        return parentAnnualIncome;
    }

    public void setParentAnnualIncome(Integer parentAnnualIncome) {
        this.parentAnnualIncome = parentAnnualIncome;
    }

    public String getParentStatus() {
        return parentStatus;
    }

    public void setParentStatus(String parentStatus) {
        this.parentStatus = parentStatus;
    }

    public String getParentVillage() {
        return parentVillage;
    }

    public void setParentVillage(String parentVillage) {
        this.parentVillage = parentVillage;
    }

    public String getParentAddrNo() {
        return parentAddrNo;
    }

    public void setParentAddrNo(String parentAddrNo) {
        this.parentAddrNo = parentAddrNo;
    }

    public String getParentVillageNo() {
        return parentVillageNo;
    }

    public void setParentVillageNo(String parentVillageNo) {
        this.parentVillageNo = parentVillageNo;
    }

    public String getParentRoad() {
        return parentRoad;
    }

    public void setParentRoad(String parentRoad) {
        this.parentRoad = parentRoad;
    }

    public String getParentAlley() {
        return parentAlley;
    }

    public void setParentAlley(String parentAlley) {
        this.parentAlley = parentAlley;
    }

    public String getParentCountry() {
        return parentCountry;
    }

    public void setParentCountry(String parentCountry) {
        this.parentCountry = parentCountry;
    }

    public String getParentProvince() {
        return parentProvince;
    }

    public void setParentProvince(String parentProvince) {
        this.parentProvince = parentProvince;
    }

    public String getParentDistrict() {
        return parentDistrict;
    }

    public void setParentDistrict(String parentDistrict) {
        this.parentDistrict = parentDistrict;
    }

    public String getParentSubDistrict() {
        return parentSubDistrict;
    }

    public void setParentSubDistrict(String parentSubDistrict) {
        this.parentSubDistrict = parentSubDistrict;
    }

    public String getParentPostCode() {
        return parentPostCode;
    }

    public void setParentPostCode(String parentPostCode) {
        this.parentPostCode = parentPostCode;
    }

    public String getParentTel1() {
        return parentTel1;
    }

    public void setParentTel1(String parentTel1) {
        this.parentTel1 = parentTel1;
    }

    public String getParentTel2() {
        return parentTel2;
    }

    public void setParentTel2(String parentTel2) {
        this.parentTel2 = parentTel2;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getStuStatus() {
        return stuStatus;
    }

    public void setStuStatus(String stuStatus) {
        this.stuStatus = stuStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStuImage() {
        return stuImage;
    }

    public void setStuImage(String stuImage) {
        this.stuImage = stuImage;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStuProfileDocId() {
        return stuProfileDocId;
    }

    public void setStuProfileDocId(String stuProfileDocId) {
        this.stuProfileDocId = stuProfileDocId;
    }

    public String getDataPathImage() {
        return dataPathImage;
    }

    public void setDataPathImage(String dataPathImage) {
        this.dataPathImage = dataPathImage;
    }

    public String getDataPathBirth() {
        return dataPathBirth;
    }

    public void setDataPathBirth(String dataPathBirth) {
        this.dataPathBirth = dataPathBirth;
    }

    public String getDataPathRegister() {
        return dataPathRegister;
    }

    public void setDataPathRegister(String dataPathRegister) {
        this.dataPathRegister = dataPathRegister;
    }

    public String getDataPathReligion() {
        return dataPathReligion;
    }

    public void setDataPathReligion(String dataPathReligion) {
        this.dataPathReligion = dataPathReligion;
    }

    public String getDataPathChangeName() {
        return dataPathChangeName;
    }

    public void setDataPathChangeName(String dataPathChangeName) {
        this.dataPathChangeName = dataPathChangeName;
    }

    public String getDataPathTranscript7() {
        return dataPathTranscript7;
    }

    public void setDataPathTranscript7(String dataPathTranscript7) {
        this.dataPathTranscript7 = dataPathTranscript7;
    }

    public String getDataPathTranscript1() {
        return dataPathTranscript1;
    }

    public void setDataPathTranscript1(String dataPathTranscript1) {
        this.dataPathTranscript1 = dataPathTranscript1;
    }

    public String getDataPathRecord8() {
        return dataPathRecord8;
    }

    public void setDataPathRecord8(String dataPathRecord8) {
        this.dataPathRecord8 = dataPathRecord8;
    }

    public String getDataPathTransfer() {
        return dataPathTransfer;
    }

    public void setDataPathTransfer(String dataPathTransfer) {
        this.dataPathTransfer = dataPathTransfer;
    }

    public String getDataPathFatherRegister() {
        return dataPathFatherRegister;
    }

    public void setDataPathFatherRegister(String dataPathFatherRegister) {
        this.dataPathFatherRegister = dataPathFatherRegister;
    }

    public String getDataPathFatherCitizenId() {
        return dataPathFatherCitizenId;
    }

    public void setDataPathFatherCitizenId(String dataPathFatherCitizenId) {
        this.dataPathFatherCitizenId = dataPathFatherCitizenId;
    }

    public String getDataPathMotherRegister() {
        return dataPathMotherRegister;
    }

    public void setDataPathMotherRegister(String dataPathMotherRegister) {
        this.dataPathMotherRegister = dataPathMotherRegister;
    }

    public String getDataPathMotherCitizenId() {
        return dataPathMotherCitizenId;
    }

    public void setDataPathMotherCitizenId(String dataPathMotherCitizenId) {
        this.dataPathMotherCitizenId = dataPathMotherCitizenId;
    }

    public String getDataPathParentRegister() {
        return dataPathParentRegister;
    }

    public void setDataPathParentRegister(String dataPathParentRegister) {
        this.dataPathParentRegister = dataPathParentRegister;
    }

    public String getDataPathParentCitizenId() {
        return dataPathParentCitizenId;
    }

    public void setDataPathParentCitizenId(String dataPathParentCitizenId) {
        this.dataPathParentCitizenId = dataPathParentCitizenId;
    }

    public String getSmartPurseId() {
        return smartPurseId;
    }

    public void setSmartPurseId(String smartPurseId) {
        this.smartPurseId = smartPurseId;
    }

    public String getSmartPurseCardNo() {
        return smartPurseCardNo;
    }

    public void setSmartPurseCardNo(String smartPurseCardNo) {
        this.smartPurseCardNo = smartPurseCardNo;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String toString() {
        return new Gson().toJson(this);
    }

}
