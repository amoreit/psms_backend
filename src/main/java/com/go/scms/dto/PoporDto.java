package com.go.scms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class PoporDto implements Serializable {
    private Integer totals;
    private Integer catClassId;
    private Integer female;
    private Integer male;
}
