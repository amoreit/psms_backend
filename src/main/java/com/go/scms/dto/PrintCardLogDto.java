/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import java.util.Date;

/**
 *
 * @author P3rdiscoz
 */
public class PrintCardLogDto {
    private String stuCardPrintLogId;
    private String scCode;
    private Integer stuProfileId;
    private String stuCode;
    private Integer itemno;
    private Date startDate;
    private Date expiredDate;
    private String recordStatus;
    private String userName;

    public String getStuCardPrintLogId() {
        return stuCardPrintLogId;
    }

    public void setStuCardPrintLogId(String stuCardPrintLogId) {
        this.stuCardPrintLogId = stuCardPrintLogId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(Integer stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
