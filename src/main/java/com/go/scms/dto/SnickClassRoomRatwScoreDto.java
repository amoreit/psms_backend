package com.go.scms.dto;

import java.util.Date;

public class SnickClassRoomRatwScoreDto {
    
    private String classRoomRatwScoreId;
    private String scCode;
    private Integer yearGroupId;
    private String yearGroupName;
    private Integer classRoomMemberId;
    private Integer classRoomSubjectId;
    private String classRoomSnameTh;
    private String stuCode;
    private String fullname;
    private Integer behavioursSkillsId;
    private String behavioursSkillsDesc;
    private Integer itemno;
    private String itemDesc;
    private String score1;
    private String score2;
    private String score3;
    private String score4;
    private String score5;
    private Integer orders;
    private String recordStatus;
    private String userName;
    private Date createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private Date updatedDate;
    private String updatedPage;
    private String updatedUser;
    private Boolean isactive;
    private String result;
    private String p;

    public String getClassRoomRatwScoreId() {
        return classRoomRatwScoreId;
    }

    public void setClassRoomRatwScoreId(String classRoomRatwScoreId) {
        this.classRoomRatwScoreId = classRoomRatwScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public Integer getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(Integer classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public Integer getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(Integer classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getBehavioursSkillsId() {
        return behavioursSkillsId;
    }

    public void setBehavioursSkillsId(Integer behavioursSkillsId) {
        this.behavioursSkillsId = behavioursSkillsId;
    }

    public String getBehavioursSkillsDesc() {
        return behavioursSkillsDesc;
    }

    public void setBehavioursSkillsDesc(String behavioursSkillsDesc) {
        this.behavioursSkillsDesc = behavioursSkillsDesc;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScore3() {
        return score3;
    }

    public void setScore3(String score3) {
        this.score3 = score3;
    }

    public String getScore4() {
        return score4;
    }

    public void setScore4(String score4) {
        this.score4 = score4;
    }

    public String getScore5() {
        return score5;
    }

    public void setScore5(String score5) {
        this.score5 = score5;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }
    
    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }
    
}
