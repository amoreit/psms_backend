/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;


public class ClassRoomLifeskillScoreDto {
    private String classRoomLifeskillScoreId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String classRoomMemberId;
    private String classRoom;
    private String stuCode;
    private String fullname;
    private String behavioursSkillsId;
    private String behavioursSkillsDesc;
    private String item;
    private String itemDesc;
    private String score;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    

    public String getClassRoomLifeskillScoreId() {
        return classRoomLifeskillScoreId;
    }

    public void setClassRoomLifeskillScoreId(String classRoomLifeskillScoreId) {
        this.classRoomLifeskillScoreId = classRoomLifeskillScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getBehavioursSkillsId() {
        return behavioursSkillsId;
    }

    public void setBehavioursSkillsId(String behavioursSkillsId) {
        this.behavioursSkillsId = behavioursSkillsId;
    }

    public String getBehavioursSkillsDesc() {
        return behavioursSkillsDesc;
    }

    public void setBehavioursSkillsDesc(String behavioursSkillsDesc) {
        this.behavioursSkillsDesc = behavioursSkillsDesc;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
    
}
