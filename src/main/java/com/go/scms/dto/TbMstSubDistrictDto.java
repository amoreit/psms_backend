/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

/**
 *
 * @author lumin
 */
public class TbMstSubDistrictDto {

        private String subDistrictId;
        private String subDistrictCode;
        private String subDistrictTh;
        private String subDistrictEn;
        private String districtId;
        private String ProvinceId;
        private String postCode;
        private String createdDate;
        private String createdPage;
        private String createdUser;
        private String ipaddr;
        private String updatedDate;
        private String updatedPage;
        private String updatedUser;
        private String recordStatus;
        private String remark;
        private String isactive;
        private String p;
        private String result;

        public String getSubDistrictId() {
            return subDistrictId;
        }

        public void setSubDistrictId(String subDistrictId) {
            this.subDistrictId = subDistrictId;
        }

        public String getSubDistrictCode() {
            return subDistrictCode;
        }

        public void setSubDistrictCode(String subDistrictCode) {
            this.subDistrictCode = subDistrictCode;
        }

        public String getSubDistrictTh() {
            return subDistrictTh;
        }

        public void setSubDistrictTh(String subDistrictTh) {
            this.subDistrictTh = subDistrictTh;
        }

        public String getSubDistrictEn() {
            return subDistrictEn;
        }

        public void setSubDistrictEn(String subDistrictEn) {
            this.subDistrictEn = subDistrictEn;
        }

        public String getDistrictId() {
            return districtId;
        }

        public void setDistrictId(String districtId) {
            this.districtId = districtId;
        }

        public String getProvinceId() {
            return ProvinceId;
        }

        public void setProvinceId(String ProvinceId) {
            this.ProvinceId = ProvinceId;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public String getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(String updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getIsactive() {
            return isactive;
        }

        public void setIsactive(String isactive) {
            this.isactive = isactive;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
}
