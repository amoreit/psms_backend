/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;


public class TrnTeacherClassDto {
    private String teacherClassId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String classId;
    private String className;
    private String catClassId;
    private String teacherId;
    private String fullname;
    private String fullnameEn;
    private String leader;

    public String getTeacherClassId() {
        return teacherClassId;
    }

    public void setTeacherClassId(String teacherClassId) {
        this.teacherClassId = teacherClassId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(String catClassId) {
        this.catClassId = catClassId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFullnameEn() {
        return fullnameEn;
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }
    
}
