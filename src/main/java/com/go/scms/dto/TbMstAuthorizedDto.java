/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author kronn
 */
public class TbMstAuthorizedDto {
    
    private Long authorizedId;
    private String scCode;
    private String fullnameTh;
    private String fullnameEn;
    private String signatureImagePath;
    private String position;
    private Date createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private String updatedUser;
    private Date updatedDate;
    private String updatedPage;
    private Boolean isactive;
    private String langCode;
    private Boolean iscurrent;

    private String userName;
    private String recordStatus;
    private String result;

    public Boolean getIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(Boolean iscurrent) {
        this.iscurrent = iscurrent;
    }
    
    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }
   
    private String p;

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Long getAuthorizedId() {
        return authorizedId;
    }

    public void setAuthorizedId(Long authorizedId) {
        this.authorizedId = authorizedId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getFullnameTh() {
        return fullnameTh.trim();
    }

    public void setFullnameTh(String fullnameTh) {
        this.fullnameTh = fullnameTh.trim();
    }

    public String getFullnameEn() {
        return fullnameEn.trim();
    }

    public void setFullnameEn(String fullnameEn) {
        this.fullnameEn = fullnameEn.trim();
    }

    public String getSignatureImagePath() {
        return signatureImagePath;
    }

    public void setSignatureImagePath(String signatureImagePath) {
        this.signatureImagePath = signatureImagePath;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }
           
    public String toString() {
        return new Gson().toJson(this);
    }  
    
}
