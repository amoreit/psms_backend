package com.go.scms.dto;

import com.google.gson.Gson;

public class SnickMstSubjectTypeDto {
    
        private String subjectTypeId;
        private String subjectTypeCode;
        private String scCode;
        private String name;
        private String recordStatus;
        private String userName;
        private String updatedUser;
        private String p;
        private String result; 

        public String getSubjectTypeId() {
            return subjectTypeId;
        }

        public void setSubjectTypeId(String subjectTypeId) {
            this.subjectTypeId = subjectTypeId;
        }

        public String getSubjectTypeCode() {
            return subjectTypeCode;
        }

        public void setSubjectTypeCode(String subjectTypeCode) {
            this.subjectTypeCode = subjectTypeCode;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
        
        public String toString() {
            return new Gson().toJson(this);
        }
    
}
