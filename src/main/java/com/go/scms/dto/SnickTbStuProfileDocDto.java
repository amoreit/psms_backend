package com.go.scms.dto;

public class SnickTbStuProfileDocDto {
        private String stuProfileDocId;
        private Long stuProfileId;
        private String scCode;
        private String stuCode;
        private String dataPathImage;
        private String dataPathReligion;
        private String dataPathBirth;
        private String dataPathRegister;
        private String dataPathChangeName;
        private String dataPathTranscript7;
        private String dataPathTranscript1;
        private String dataPathRecord8;
        private String dataPathTransfer;
        private String dataPathFatherRegister;
        private String dataPathFatherCitizenId;
        private String dataPathMotherRegister;
        private String dataPathMotherCitizenId;
        private String dataPathParentRegister;
        private String dataPathParentCitizenId;
        private String userName;
        private String createdDate;
        private String createdPage;
        private String createdUser;
        private String ipaddr;
        private String updatedDate;
        private String updatedPage;
        private String updatedUser;
        private String recordStatus;
        private String isactive;

        public String getStuProfileDocId() {
            return stuProfileDocId;
        }

        public void setStuProfileDocId(String stuProfileDocId) {
            this.stuProfileDocId = stuProfileDocId;
        }

        public Long getStuProfileId() {
            return stuProfileId;
        }

        public void setStuProfileId(Long stuProfileId) {
            this.stuProfileId = stuProfileId;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getStuCode() {
            return stuCode;
        }

        public void setStuCode(String stuCode) {
            this.stuCode = stuCode;
        }

        public String getDataPathImage() {
            return dataPathImage;
        }

        public void setDataPathImage(String dataPathImage) {
            this.dataPathImage = dataPathImage;
        }

        public String getDataPathReligion() {
            return dataPathReligion;
        }

        public void setDataPathReligion(String dataPathReligion) {
            this.dataPathReligion = dataPathReligion;
        }

        public String getDataPathBirth() {
            return dataPathBirth;
        }

        public void setDataPathBirth(String dataPathBirth) {
            this.dataPathBirth = dataPathBirth;
        }

        public String getDataPathRegister() {
            return dataPathRegister;
        }

        public void setDataPathRegister(String dataPathRegister) {
            this.dataPathRegister = dataPathRegister;
        }

        public String getDataPathChangeName() {
            return dataPathChangeName;
        }

        public void setDataPathChangeName(String dataPathChangeName) {
            this.dataPathChangeName = dataPathChangeName;
        }

        public String getDataPathTranscript7() {
            return dataPathTranscript7;
        }

        public void setDataPathTranscript7(String dataPathTranscript7) {
            this.dataPathTranscript7 = dataPathTranscript7;
        }

        public String getDataPathTranscript1() {
            return dataPathTranscript1;
        }

        public void setDataPathTranscript1(String dataPathTranscript1) {
            this.dataPathTranscript1 = dataPathTranscript1;
        }

        public String getDataPathRecord8() {
            return dataPathRecord8;
        }

        public void setDataPathRecord8(String dataPathRecord8) {
            this.dataPathRecord8 = dataPathRecord8;
        }

        public String getDataPathTransfer() {
            return dataPathTransfer;
        }

        public void setDataPathTransfer(String dataPathTransfer) {
            this.dataPathTransfer = dataPathTransfer;
        }

        public String getDataPathFatherRegister() {
            return dataPathFatherRegister;
        }

        public void setDataPathFatherRegister(String dataPathFatherRegister) {
            this.dataPathFatherRegister = dataPathFatherRegister;
        }

        public String getDataPathFatherCitizenId() {
            return dataPathFatherCitizenId;
        }

        public void setDataPathFatherCitizenId(String dataPathFatherCitizenId) {
            this.dataPathFatherCitizenId = dataPathFatherCitizenId;
        }

        public String getDataPathMotherRegister() {
            return dataPathMotherRegister;
        }

        public void setDataPathMotherRegister(String dataPathMotherRegister) {
            this.dataPathMotherRegister = dataPathMotherRegister;
        }

        public String getDataPathMotherCitizenId() {
            return dataPathMotherCitizenId;
        }

        public void setDataPathMotherCitizenId(String dataPathMotherCitizenId) {
            this.dataPathMotherCitizenId = dataPathMotherCitizenId;
        }

        public String getDataPathParentRegister() {
            return dataPathParentRegister;
        }

        public void setDataPathParentRegister(String dataPathParentRegister) {
            this.dataPathParentRegister = dataPathParentRegister;
        }

        public String getDataPathParentCitizenId() {
            return dataPathParentCitizenId;
        }

        public void setDataPathParentCitizenId(String dataPathParentCitizenId) {
            this.dataPathParentCitizenId = dataPathParentCitizenId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public String getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(String updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getIsactive() {
            return isactive;
        }

        public void setIsactive(String isactive) {
            this.isactive = isactive;
        }
}
