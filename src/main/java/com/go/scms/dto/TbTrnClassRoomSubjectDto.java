/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import java.util.Date;

/**
 *
 * @author lumin
 */
public class TbTrnClassRoomSubjectDto {

    private Long classRoomSubjecctId;
    private String scCode;
    private Integer yearGroupId;
    private Integer classRoomId;
    private Integer departmentId;
    private String subjectCode;
    private String subjectNameTh;
    private String subjectNameEn;
    private Integer scoreId;
    private Integer teacher1;
    private Integer teacher2;
    private Integer teacher3;
    private Integer orders;
    private Date createDate;
    private String createPage;
    private String createUser;
    private String ipaddr;
    private Date updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private Boolean isactive;
    private String p;
    private String result;
    private String credit;
    private String lesson;
    private Integer classId;

    public Long getClassRoomSubjecctId() {
        return classRoomSubjecctId;
    }

    public void setClassRoomSubjecctId(Long classRoomSubjecctId) {
        this.classRoomSubjecctId = classRoomSubjecctId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(Integer yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public Integer getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(Integer classRoomId) {
        this.classRoomId = classRoomId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }

    public String getSubjectNameEn() {
        return subjectNameEn;
    }

    public void setSubjectNameEn(String subjectNameEn) {
        this.subjectNameEn = subjectNameEn;
    }

    public Integer getScoreId() {
        return scoreId;
    }

    public void setScoreId(Integer scoreId) {
        this.scoreId = scoreId;
    }

    public Integer getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(Integer teacher1) {
        this.teacher1 = teacher1;
    }

    public Integer getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(Integer teacher2) {
        this.teacher2 = teacher2;
    }

    public Integer getTeacher3() {
        return teacher3;
    }

    public void setTeacher3(Integer teacher3) {
        this.teacher3 = teacher3;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreatePage() {
        return createPage;
    }

    public void setCreatePage(String createPage) {
        this.createPage = createPage;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

}
