/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import java.util.Date;

public class GraduateLogDto {
    private String graduateLogId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String stuProfileId;
    private String stuCode;
    private String fullname;
    private String lastClassRoom;
    private String graduateReason;
    private String classOf;
    private Date graduateDate;

    public String getGraduateLogId() {
        return graduateLogId;
    }

    public void setGraduateLogId(String graduateLogId) {
        this.graduateLogId = graduateLogId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(String stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLastClassRoom() {
        return lastClassRoom;
    }

    public void setLastClassRoom(String lastClassRoom) {
        this.lastClassRoom = lastClassRoom;
    }

    public String getGraduateReason() {
        return graduateReason;
    }

    public void setGraduateReason(String graduateReason) {
        this.graduateReason = graduateReason;
    }

    public String getClassOf() {
        return classOf;
    }

    public void setClassOf(String classOf) {
        this.classOf = classOf;
    }

    public Date getGraduateDate() {
        return graduateDate;
    }

    public void setGraduateDate(Date graduateDate) {
        this.graduateDate = graduateDate;
    }
    
}
