package com.go.scms.dto;

import com.google.gson.Gson;

public class SnickMstClassDto {
    
        private String classId;
        private Integer catClassId;
        private String classLnameTh;
        private String classLnameEn;
        private String classSnameTh;
        private String classSnameEn;
        private String lnameTh;
        private String scCode;
        private String age;
        private String recordStatus;
        private String userName;
        private String updatedUser;
        private String p;
        private String result; 

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public Integer getCatClassId() {
            return catClassId;
        }

        public void setCatClassId(Integer catClassId) {
            this.catClassId = catClassId;
        }

        public String getClassLnameTh() {
            return classLnameTh;
        }

        public void setClassLnameTh(String classLnameTh) {
            this.classLnameTh = classLnameTh;
        }

        public String getClassLnameEn() {
            return classLnameEn;
        }

        public void setClassLnameEn(String classLnameEn) {
            this.classLnameEn = classLnameEn;
        }

        public String getClassSnameTh() {
            return classSnameTh;
        }

        public void setClassSnameTh(String classSnameTh) {
            this.classSnameTh = classSnameTh;
        }

        public String getClassSnameEn() {
            return classSnameEn;
        }

        public void setClassSnameEn(String classSnameEn) {
            this.classSnameEn = classSnameEn;
        }

        public String getLnameTh() {
            return lnameTh;
        }

        public void setLnameTh(String lnameTh) {
            this.lnameTh = lnameTh;
        }

        public String getScCode() {
            return scCode;
        }

        public void setScCode(String scCode) {
            this.scCode = scCode;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String toString() {
            return new Gson().toJson(this);
        }
}
