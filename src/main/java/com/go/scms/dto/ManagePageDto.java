package com.go.scms.dto;

public class ManagePageDto {

	private String pageId;
	private String pageCode;
	private String pageName;
        private String pageNameEn;
        private String pageGroupId;
	private String pageGroupCode;
	private String pageGroupName;
        private String pageGroupNameEn;
	private String isView;
	private String isAdd;
	private String isReport;
	private String isDelete;
	private String isApprove;
	
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getPageCode() {
		return pageCode;
	}
	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}
	public String getPageName() {
		return pageName;
	}
        public String getPageNameEn() {
            return pageNameEn;
        }

        public void setPageNameEn(String pageNameEn) {
            this.pageNameEn = pageNameEn;
        }
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getIsView() {
		return isView;
	}
	public void setIsView(String isView) {
		this.isView = isView;
	}
	public String getIsAdd() {
		return isAdd;
	}
	public void setIsAdd(String isAdd) {
		this.isAdd = isAdd;
	}
	public String getIsReport() {
		return isReport;
	}
	public void setIsReport(String isReport) {
		this.isReport = isReport;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getIsApprove() {
		return isApprove;
	}
	public void setIsApprove(String isApprove) {
		this.isApprove = isApprove;
	}
        public String getPageGroupId() {
            return pageGroupId;
        }
        public void setPageGroupId(String pageGroupId) {
            this.pageGroupId = pageGroupId;
        }

	public String getPageGroupCode() {
		return pageGroupCode;
	}
	public void setPageGroupCode(String pageGroupCode) {
		this.pageGroupCode = pageGroupCode;
	}
	public String getPageGroupName() {
		return pageGroupName;
	}
	public void setPageGroupName(String pageGroupName) {
		this.pageGroupName = pageGroupName;
	}
        public String getPageGroupNameEn() {
            return pageGroupNameEn;
        }

        public void setPageGroupNameEn(String pageGroupNameEn) {
            this.pageGroupNameEn = pageGroupNameEn;
        }     
}
