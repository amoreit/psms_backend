/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;

/**
 *
 * @author bamboo
 */
public class PaiReportTbStuTranscriptLogDto {
    
    private String yearId;
    private String classId;
    private String manTotal;
    private String womanTotal;
    private String genderTotal;

    public String getYearId() {
        return yearId;
    }

    public void setYearId(String yearId) {
        this.yearId = yearId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getManTotal() {
        return manTotal;
    }

    public void setManTotal(String manTotal) {
        this.manTotal = manTotal;
    }

    public String getWomanTotal() {
        return womanTotal;
    }

    public void setWomanTotal(String womanTotal) {
        this.womanTotal = womanTotal;
    }

    public String getGenderTotal() {
        return genderTotal;
    }

    public void setGenderTotal(String genderTotal) {
        this.genderTotal = genderTotal;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
}
