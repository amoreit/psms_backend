/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author kronn
 */
public class KornClassRoomMemberSubjectAttendanceReportDto {
    
    private String classId;
    private String classRoomId;
    private String classRoomSubjectId;
    private String searchDate;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(String searchDate) {
        this.searchDate = searchDate;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
    
}
