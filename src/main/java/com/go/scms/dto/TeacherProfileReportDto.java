/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;

/**
 *
 * @author bamboo
 */
public class TeacherProfileReportDto {
    
    private String classId;//ระดับชั้นเรียนที่สังกัด
    private String departmentId; //กลุ่มสาระวิชา
    private String firstnameTh; //กลุ่มสาระวิชา
    private String lastnameTh; //กลุ่มสาระวิชา

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
    
}
