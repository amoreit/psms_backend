/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

/**
 *
 * @author P3rdiscoz
 */
public class StuProfileDocDto {

    private String stuProfileDocId;
    private String scCode;
    private String stuProfileId;
    private String stuCode;
    private String itemno;
    private String dataPathImage;
    private String dataPathBirth;
    private String dataPathRegister;
    private String dataPathReligion;
    private String dataPathChangeName;
    private String dataPathTranscript7;
    private String dataPathTranscript1;
    private String dataPathRecord8;
    private String dataPathTransfer;
    private String dataPathFatherRegister;
    private String dataPathFatherCitizenId;
    private String dataPathMotherRegister;
    private String dataPathMotherCitizenId;
    private String dataPathParentRegister;
    private String dataPathParentCitizenId;

    public String getStuProfileDocId() {
        return stuProfileDocId;
    }

    public void setStuProfileDocId(String stuProfileDocId) {
        this.stuProfileDocId = stuProfileDocId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(String stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getItemno() {
        return itemno;
    }

    public void setItemno(String itemno) {
        this.itemno = itemno;
    }

    public String getDataPathImage() {
        return dataPathImage;
    }

    public void setDataPathImage(String dataPathImage) {
        this.dataPathImage = dataPathImage;
    }

    public String getDataPathBirth() {
        return dataPathBirth;
    }

    public void setDataPathBirth(String dataPathBirth) {
        this.dataPathBirth = dataPathBirth;
    }

    public String getDataPathRegister() {
        return dataPathRegister;
    }

    public void setDataPathRegister(String dataPathRegister) {
        this.dataPathRegister = dataPathRegister;
    }

    public String getDataPathReligion() {
        return dataPathReligion;
    }

    public void setDataPathReligion(String dataPathReligion) {
        this.dataPathReligion = dataPathReligion;
    }

    public String getDataPathChangeName() {
        return dataPathChangeName;
    }

    public void setDataPathChangeName(String dataPathChangeName) {
        this.dataPathChangeName = dataPathChangeName;
    }

    public String getDataPathTranscript7() {
        return dataPathTranscript7;
    }

    public void setDataPathTranscript7(String dataPathTranscript7) {
        this.dataPathTranscript7 = dataPathTranscript7;
    }

    public String getDataPathTranscript1() {
        return dataPathTranscript1;
    }

    public void setDataPathTranscript1(String dataPathTranscript1) {
        this.dataPathTranscript1 = dataPathTranscript1;
    }

    public String getDataPathRecord8() {
        return dataPathRecord8;
    }

    public void setDataPathRecord8(String dataPathRecord8) {
        this.dataPathRecord8 = dataPathRecord8;
    }

    public String getDataPathTransfer() {
        return dataPathTransfer;
    }

    public void setDataPathTransfer(String dataPathTransfer) {
        this.dataPathTransfer = dataPathTransfer;
    }

    public String getDataPathFatherRegister() {
        return dataPathFatherRegister;
    }

    public void setDataPathFatherRegister(String dataPathFatherRegister) {
        this.dataPathFatherRegister = dataPathFatherRegister;
    }

    public String getDataPathFatherCitizenId() {
        return dataPathFatherCitizenId;
    }

    public void setDataPathFatherCitizenId(String dataPathFatherCitizenId) {
        this.dataPathFatherCitizenId = dataPathFatherCitizenId;
    }

    public String getDataPathMotherRegister() {
        return dataPathMotherRegister;
    }

    public void setDataPathMotherRegister(String dataPathMotherRegister) {
        this.dataPathMotherRegister = dataPathMotherRegister;
    }

    public String getDataPathMotherCitizenId() {
        return dataPathMotherCitizenId;
    }

    public void setDataPathMotherCitizenId(String dataPathMotherCitizenId) {
        this.dataPathMotherCitizenId = dataPathMotherCitizenId;
    }

    public String getDataPathParentRegister() {
        return dataPathParentRegister;
    }

    public void setDataPathParentRegister(String dataPathParentRegister) {
        this.dataPathParentRegister = dataPathParentRegister;
    }

    public String getDataPathParentCitizenId() {
        return dataPathParentCitizenId;
    }

    public void setDataPathParentCitizenId(String dataPathParentCitizenId) {
        this.dataPathParentCitizenId = dataPathParentCitizenId;
    }

}
