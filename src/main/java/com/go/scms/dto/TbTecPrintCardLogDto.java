/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author Kittipong
 */
public class TbTecPrintCardLogDto {
    private long teacherCardPrintLogId;
    private String scCode;
    private Integer teacherId;
    private String teacherCardId;
    private Date startDate;
    private Date expiredDate;
    private String createdUser;
    private Date createdDate;
    private String updatedUser;
    private Date updatedDate;
    private String userName;
    private Boolean isactive;
    private Integer itemno;

    public long getTeacherCardPrintLogId() {
        return teacherCardPrintLogId;
    }

    public void setTeacherCardPrintLogId(long teacherCardPrintLogId) {
        this.teacherCardPrintLogId = teacherCardPrintLogId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherCardId() {
        return teacherCardId;
    }

    public void setTeacherCardId(String teacherCardId) {
        this.teacherCardId = teacherCardId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
}
