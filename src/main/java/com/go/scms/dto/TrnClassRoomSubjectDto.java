/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

public class TrnClassRoomSubjectDto {
    private String classRoomSubjectId;
    private String scCode;
    private String stuCode;
    private String yearGroupId;
    private String classId;
    private String classRoomId;
    private String departmentId;
    private String subjectCode;
    private String subjectCode2;
    private String subjectNameTh;
    private String subjectNameEn;
    private String subjectType;
    private String scoreId;
    private String orders;
    private String credit;
    private String lesson;
    private String weight;
    private String isEp;
    private String recorStatus;
    private String userName;
    private String teacher1;
    private String teacherFullname1;
    private String teacher2;
    private String teacherFullname2;
    private String langCode;
    private String citizenId;

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getIsEp() {
        return isEp;
    }

    public void setIsEp(String isEp) {
        this.isEp = isEp;
    }

    public String getSubjectCode2() {
        return subjectCode2;
    }

    public void setSubjectCode2(String subjectCode2) {
        this.subjectCode2 = subjectCode2;
    }


    public String getSubjectNameEn() {
        return subjectNameEn;
    }

    public void setSubjectNameEn(String subjectNameEn) {
        this.subjectNameEn = subjectNameEn;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(String teacher2) {
        this.teacher2 = teacher2;
    }

    public String getTeacherFullname2() {
        return teacherFullname2;
    }

    public void setTeacherFullname2(String teacherFullname2) {
        this.teacherFullname2 = teacherFullname2;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(String teacher1) {
        this.teacher1 = teacher1;
    }

    public String getTeacherFullname1() {
        return teacherFullname1;
    }

    public void setTeacherFullname1(String teacherFullname1) {
        this.teacherFullname1 = teacherFullname1;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }


    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getRecorStatus() {
        return recorStatus;
    }

    public void setRecorStatus(String recorStatus) {
        this.recorStatus = recorStatus;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
}
