package com.go.scms.dto;

import com.google.gson.Gson;

public class PageGroupDto {

	private String pageGroupId;
	private String pageGroupCode;
	private String pageGroupIcon;
	private String pageGroupName;
	private String pageGroupNameEn;
	private String pageGroupStatus;
	private String userName;
	private String p;
	private String result;
	
	public String getPageGroupId() {
		return pageGroupId;
	}
	public void setPageGroupId(String pageGroupId) {
		this.pageGroupId = pageGroupId;
	}
	public String getPageGroupCode() {
		return pageGroupCode;
	}
	public void setPageGroupCode(String pageGroupCode) {
		this.pageGroupCode = pageGroupCode;
	}
	public String getPageGroupName() {
		return pageGroupName;
	}
	public void setPageGroupName(String pageGroupName) {
		this.pageGroupName = pageGroupName;
	}
	public String getPageGroupStatus() {
		return pageGroupStatus;
	}
	public void setPageGroupStatus(String pageGroupStatus) {
		this.pageGroupStatus = pageGroupStatus;
	}
	public String getPageGroupIcon() {
		return pageGroupIcon;
	}
	public void setPageGroupIcon(String pageGroupIcon) {
		this.pageGroupIcon = pageGroupIcon;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

	public String getPageGroupNameEn() {
		return pageGroupNameEn;
	}

	public void setPageGroupNameEn(String pageGroupNameEn) {
		this.pageGroupNameEn = pageGroupNameEn;
	}

	public String toString() {
		return new Gson().toJson(this);
	}
}
