/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import java.util.Date;

/**
 *
 * @author Kittipong
 */
public class TbMstSchoolProfileDto {
    
    private String schoolProfileId;
    private String scCode;
    private String scName;
    private String scNameEn;
    private String scType;
    private String scRegisterAuthorized;
    private String scRegisterNo;
    private Date scDoe;
    private String scMotto;
    private String scVision;
    private String scMission;
    private String scAbout;
    private String scLogo;
    private String scAnnounce;
    private String scAnnounceStaff;
    private String scAnnounceStudent;
    private String scBankAccount;
    private String addrNo;
    private String village;
    private String villageNo;
    private String alley;
    private String road;
    private String subDistrict;
    private String district;
    private String province;
    private String country;
    private String postCode;
    private String tel;
    private String tel2;
    private String tel3;
    private String fax;
    private String email;
    private String email2;
    private String email3;
    private String website;
    private String facebook;
    private String twitter;
    private Date createdDate;
    private String createdPage;
    private String createdUser;
    private String ipaddr;
    private Date updatedDate;
    private String updatedPage;
    private String updatedUser;
    private String recordStatus;
    private Boolean isactive;
    private String userName;

    public String getAddrNo() {
        return addrNo;
    }

    public void setAddrNo(String addrNo) {
        this.addrNo = addrNo;
    }

    public String getSchoolProfileId() {
        return schoolProfileId;
    }

    public void setSchoolProfileId(String schoolProfileId) {
        this.schoolProfileId = schoolProfileId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getScName() {
        return scName;
    }

    public void setScName(String scName) {
        this.scName = scName;
    }

    public String getScNameEn() {
        return scNameEn;
    }

    public void setScNameEn(String scNameEn) {
        this.scNameEn = scNameEn;
    }

    public String getScType() {
        return scType;
    }

    public void setScType(String scType) {
        this.scType = scType;
    }

    public String getScRegisterAuthorized() {
        return scRegisterAuthorized;
    }

    public void setScRegisterAuthorized(String scRegisterAuthorized) {
        this.scRegisterAuthorized = scRegisterAuthorized;
    }

    public String getScRegisterNo() {
        return scRegisterNo;
    }

    public void setScRegisterNo(String scRegisterNo) {
        this.scRegisterNo = scRegisterNo;
    }

    public Date getScDoe() {
        return scDoe;
    }

    public void setScDoe(Date scDoe) {
        this.scDoe = scDoe;
    }

    public String getScMotto() {
        return scMotto;
    }

    public void setScMotto(String scMotto) {
        this.scMotto = scMotto;
    }

    public String getScVision() {
        return scVision;
    }

    public void setScVision(String scVision) {
        this.scVision = scVision;
    }

    public String getScMission() {
        return scMission;
    }

    public void setScMission(String scMission) {
        this.scMission = scMission;
    }

    public String getScAbout() {
        return scAbout;
    }

    public void setScAbout(String scAbout) {
        this.scAbout = scAbout;
    }

    public String getScLogo() {
        return scLogo;
    }

    public void setScLogo(String scLogo) {
        this.scLogo = scLogo;
    }

    public String getScAnnounce() {
        return scAnnounce;
    }

    public void setScAnnounce(String scAnnounce) {
        this.scAnnounce = scAnnounce;
    }

    public String getScAnnounceStaff() {
        return scAnnounceStaff;
    }

    public void setScAnnounceStaff(String scAnnounceStaff) {
        this.scAnnounceStaff = scAnnounceStaff;
    }

    public String getScAnnounceStudent() {
        return scAnnounceStudent;
    }

    public void setScAnnounceStudent(String scAnnounceStudent) {
        this.scAnnounceStudent = scAnnounceStudent;
    }

    public String getScBankAccount() {
        return scBankAccount;
    }

    public void setScBankAccount(String scBankAccount) {
        this.scBankAccount = scBankAccount;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getVillageNo() {
        return villageNo;
    }

    public void setVillageNo(String villageNo) {
        this.villageNo = villageNo;
    }

    public String getAlley() {
        return alley;
    }

    public void setAlley(String alley) {
        this.alley = alley;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getTel3() {
        return tel3;
    }

    public void setTel3(String tel3) {
        this.tel3 = tel3;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getEmail3() {
        return email3;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedPage() {
        return updatedPage;
    }

    public void setUpdatedPage(String updatedPage) {
        this.updatedPage = updatedPage;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getP() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getResult() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
