/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import java.util.Date;
import com.google.gson.Gson;

/**
 *
 * @author Kittipong
 */
public class TbPrintCardLogDto {
    
    private long stuCardPrintLogId;
    private String scCode;
    private String stuCode;
    private Integer stuProfileId;
    private Date startDate;
    private Date expiredDate;
    private String createdUser;
    private Date createdDate;
    private String updatedUser;
    private Date updatedDate;
    private String userName;
    private Boolean isactive;
    private Integer itemno;

    public long getStuCardPrintLogId() {
        return stuCardPrintLogId;
    }

    public void setStuCardPrintLogId(long stuCardPrintLogId) {
        this.stuCardPrintLogId = stuCardPrintLogId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public Integer getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(Integer stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdateUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }
    
}
