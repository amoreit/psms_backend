/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;
import java.util.Date;

/**
 *
 * @author kronn
 */
public class PositionDto {
    
    private Long positionId;
    private String scCode;
    private String positionNameTh;
    private String positionNameEn;
    private Date createdDate;
    private String createdUser;
    private String updatedUser;
    private Date updatedDate;
    private String createdPage;
    private String userName;
    private String recordStatus;
    private String result;
    private Boolean isactive;
    private String p;

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUpdatedUser() {
        return updatedUser;
    }

    public void setUpdatedUser(String updatedUser) {
        this.updatedUser = updatedUser;
    }

    
    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getPositionNameTh() {
        return positionNameTh.trim();
    }

    public void setPositionNameTh(String positionNameTh) {
        this.positionNameTh = positionNameTh.trim();
    }

    public String getPositionNameEn() {
        return positionNameEn.trim();
    }

    public void setPositionNameEn(String positionNameEn) {
        this.positionNameEn = positionNameEn.trim();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getCreatedPage() {
        return createdPage;
    }

    public void setCreatedPage(String createdPage) {
        this.createdPage = createdPage;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }
    


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
}
