package com.go.scms.dto;

import com.google.gson.Gson;

public class PageDto {

	private String pageId;
	private String pageCode;
	private String pageName;
	private String pageNameEn;
	private String pageIcon;
	private String pageUrl;
	private String pageStatus;
	private String pageGroupCode;
        private String pageGroupName;
	private String userName;
	private String p;
	private String result;
	
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public String getPageCode() {
		return pageCode;
	}
	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPageIcon() {
		return pageIcon;
	}
	public void setPageIcon(String pageIcon) {
		this.pageIcon = pageIcon;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	public String getPageStatus() {
		return pageStatus;
	}
	public void setPageStatus(String pageStatus) {
		this.pageStatus = pageStatus;
	}
	public String getPageGroupCode() {
		return pageGroupCode;
	}
	public void setPageGroupCode(String pageGroupCode) {
		this.pageGroupCode = pageGroupCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

	public String getPageNameEn() {
		return pageNameEn;
	}

	public void setPageNameEn(String pageNameEn) {
		this.pageNameEn = pageNameEn;
	}

        public String getPageGroupName() {
            return pageGroupName;
        }

        public void setPageGroupName(String pageGroupName) {
            this.pageGroupName = pageGroupName;
        }
        
	public String toString() {
		return new Gson().toJson(this);
	}
}
