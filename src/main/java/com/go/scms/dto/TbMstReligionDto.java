/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

/**
 *
 * @author lumin
 */
public class TbMstReligionDto {
    
        private String religionId;
        private String religionNameTh;
        private String religionNameEn;
        private String createdDate;
        private String createdPage;
        private String createdUser;
        private String ipaddr;
        private String updatedDate;
        private String updatedPage;
        private String updatedUser;
        private String recordStatus;
        private String isactive;
        private String p;
        private String result;

        public String getReligionId() {
            return religionId;
        }

        public void setReligionId(String religionId) {
            this.religionId = religionId;
        }

        public String getReligionNameTh() {
            return religionNameTh;
        }

        public void setReligionNameTh(String religionNameTh) {
            this.religionNameTh = religionNameTh;
        }

        public String getReligionNameEn() {
            return religionNameEn;
        }

        public void setReligionNameEn(String religionNameEn) {
            this.religionNameEn = religionNameEn;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public String getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(String updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getIsactive() {
            return isactive;
        }

        public void setIsactive(String isactive) {
            this.isactive = isactive;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

}
