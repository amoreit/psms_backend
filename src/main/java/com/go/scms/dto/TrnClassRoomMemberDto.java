/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;

public class TrnClassRoomMemberDto {
    private String classRoomMemberId;
    private String scCode;
    private String classRoom;
    private String className;
    private String classRoomNo;
    private String yearGroupId;
    private String yearGroupName;
    private String departmentId;
    private String nameDepartment;
    private String activity1id;
    private String activity1name;
    private String activity1lesson;
    private String activity2id;
    private String activity2name;
    private String activity2lesson;
    private String activity3id;
    private String activity3name;
    private String activity3lesson;
    private String activity4id;
    private String activity4name;
    private String activity4lesson;
    private String activity5id;
    private String activity5name;
    private String activity5lesson;
    private String userName;
    private String classId;
    private String classRoomId;
    private String orderBy;
    private String fullname;
    private String stuCode;
    private String isactive;
    private String memberId;
    private String role;
    private String classRoomSubjectId;
    private String competenciesResult;
    private String startYearId;
    private String endYearId;
    private String catClassId;
    private String isEp;

    public String getActivity1id() {
        return activity1id;
    }

    public void setActivity1id(String activity1id) {
        this.activity1id = activity1id;
    }

    public String getActivity1name() {
        return activity1name;
    }

    public void setActivity1name(String activity1name) {
        this.activity1name = activity1name;
    }

    public String getActivity1lesson() {
        return activity1lesson;
    }

    public void setActivity1lesson(String activity1lesson) {
        this.activity1lesson = activity1lesson;
    }

    public String getActivity2id() {
        return activity2id;
    }

    public void setActivity2id(String activity2id) {
        this.activity2id = activity2id;
    }

    public String getActivity2name() {
        return activity2name;
    }

    public void setActivity2name(String activity2name) {
        this.activity2name = activity2name;
    }

    public String getActivity2lesson() {
        return activity2lesson;
    }

    public void setActivity2lesson(String activity2lesson) {
        this.activity2lesson = activity2lesson;
    }

    public String getActivity3id() {
        return activity3id;
    }

    public void setActivity3id(String activity3id) {
        this.activity3id = activity3id;
    }

    public String getActivity3name() {
        return activity3name;
    }

    public void setActivity3name(String activity3name) {
        this.activity3name = activity3name;
    }

    public String getActivity3lesson() {
        return activity3lesson;
    }

    public void setActivity3lesson(String activity3lesson) {
        this.activity3lesson = activity3lesson;
    }

    public String getActivity4id() {
        return activity4id;
    }

    public void setActivity4id(String activity4id) {
        this.activity4id = activity4id;
    }

    public String getActivity4name() {
        return activity4name;
    }

    public void setActivity4name(String activity4name) {
        this.activity4name = activity4name;
    }

    public String getActivity4lesson() {
        return activity4lesson;
    }

    public void setActivity4lesson(String activity4lesson) {
        this.activity4lesson = activity4lesson;
    }

    public String getActivity5id() {
        return activity5id;
    }

    public void setActivity5id(String activity5id) {
        this.activity5id = activity5id;
    }

    public String getActivity5name() {
        return activity5name;
    }

    public void setActivity5name(String activity5name) {
        this.activity5name = activity5name;
    }

    public String getActivity5lesson() {
        return activity5lesson;
    }

    public void setActivity5lesson(String activity5lesson) {
        this.activity5lesson = activity5lesson;
    }

    public String getIsEp() {
        return isEp;
    }

    public void setIsEp(String isEp) {
        this.isEp = isEp;
    }

    public String getCatClassId() {
        return catClassId;
    }

    public void setCatClassId(String catClassId) {
        this.catClassId = catClassId;
    }

    public String getStartYearId() {
        return startYearId;
    }

    public void setStartYearId(String startYearId) {
        this.startYearId = startYearId;
    }

    public String getEndYearId() {
        return endYearId;
    }

    public void setEndYearId(String endYearId) {
        this.endYearId = endYearId;
    }

    public String getCompetenciesResult() {
        return competenciesResult;
    }

    public void setCompetenciesResult(String competenciesResult) {
        this.competenciesResult = competenciesResult;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getNameDepartment() {
        return nameDepartment;
    }

    public void setNameDepartment(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
    
    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }
    
    public String toString() {
        return new Gson().toJson(this);
    }
}
