/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

public class BehavioursSkillsDto {
    private String classId;
    private String classRoomMemberId;

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }
    
}
