/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

/**
 *
 * @author lumin
 */
public class TbMstProvinceDto {
    
        private String provinceId;
        private String countryId;
        private String provinceCode;
        private String provinceTh;
        private String provinceEn;
        private String createdDate;
        private String createdPage;
        private String createdUser;
        private String ipaddr;
        private String updatedDate;
        private String updatedPage;
        private String updatedUser;
        private String recordStatus;
        private String isactive;
        private String p;
        private String result;

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getProvinceCode() {
            return provinceCode;
        }

        public void setProvinceCode(String provinceCode) {
            this.provinceCode = provinceCode;
        }

        public String getProvinceTh() {
            return provinceTh;
        }

        public void setProvinceTh(String provinceTh) {
            this.provinceTh = provinceTh;
        }

        public String getProvinceEn() {
            return provinceEn;
        }

        public void setProvinceEn(String provinceEn) {
            this.provinceEn = provinceEn;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getCreatedPage() {
            return createdPage;
        }

        public void setCreatedPage(String createdPage) {
            this.createdPage = createdPage;
        }

        public String getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(String createdUser) {
            this.createdUser = createdUser;
        }

        public String getIpaddr() {
            return ipaddr;
        }

        public void setIpaddr(String ipaddr) {
            this.ipaddr = ipaddr;
        }

        public String getUpdatedDate() {
            return updatedDate;
        }

        public void setUpdatedDate(String updatedDate) {
            this.updatedDate = updatedDate;
        }

        public String getUpdatedPage() {
            return updatedPage;
        }

        public void setUpdatedPage(String updatedPage) {
            this.updatedPage = updatedPage;
        }

        public String getUpdatedUser() {
            return updatedUser;
        }

        public void setUpdatedUser(String updatedUser) {
            this.updatedUser = updatedUser;
        }

        public String getRecordStatus() {
            return recordStatus;
        }

        public void setRecordStatus(String recordStatus) {
            this.recordStatus = recordStatus;
        }

        public String getIsactive() {
            return isactive;
        }

        public void setIsactive(String isactive) {
            this.isactive = isactive;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }        
    
}
