package com.go.scms.dto;

import com.google.gson.Gson;

public class MaxStuProfileDto {

    private String yearGroupId;
    private String toYearGroup;
    private String classId;
    private String classRoomId;
    private String className;
    private String classRoom;
    private String toClass;
    private String citizenId;
    private String stuStatusId;
    private String firstnameTh;
    private String lastnameTh;

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getToYearGroup() {
        return toYearGroup;
    }

    public void setToYearGroup(String toYearGroup) {
        this.toYearGroup = toYearGroup;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getToClass() {
        return toClass;
    }

    public void setToClass(String toClass) {
        this.toClass = toClass;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getStuStatusId() {
        return stuStatusId;
    }

    public void setStuStatusId(String stuStatusId) {
        this.stuStatusId = stuStatusId;
    }

    public String toString() {
        return new Gson().toJson(this);
    }

}
