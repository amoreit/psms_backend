/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.dto;

import com.google.gson.Gson;

public class ScoreDto {
    private String scoreId;
    private String scCode;
    private String scoreName;
    private String fullScoreTotal;
    private String fullScore1;
    private String fullScoreMidterm;
    private String fullScore2;
    private String fullScoreFinal;
    private String iscatclass1;
    private String p;
    private String result;
    private String userName;

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getScoreName() {
        return scoreName;
    }

    public void setScoreName(String scoreName) {
        this.scoreName = scoreName;
    }

    public String getFullScoreTotal() {
        return fullScoreTotal;
    }

    public void setFullScoreTotal(String fullScoreTotal) {
        this.fullScoreTotal = fullScoreTotal;
    }

    public String getFullScore1() {
        return fullScore1;
    }

    public void setFullScore1(String fullScore1) {
        this.fullScore1 = fullScore1;
    }

    public String getFullScoreMidterm() {
        return fullScoreMidterm;
    }

    public void setFullScoreMidterm(String fullScoreMidterm) {
        this.fullScoreMidterm = fullScoreMidterm;
    }

    public String getFullScore2() {
        return fullScore2;
    }

    public void setFullScore2(String fullScore2) {
        this.fullScore2 = fullScore2;
    }

    public String getFullScoreFinal() {
        return fullScoreFinal;
    }

    public void setFullScoreFinal(String fullScoreFinal) {
        this.fullScoreFinal = fullScoreFinal;
    }

    public String getIscatclass1() {
        return iscatclass1;
    }

    public void setIscatclass1(String iscatclass1) {
        this.iscatclass1 = iscatclass1;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
     public String toString() {
        return new Gson().toJson(this);
    }

}
