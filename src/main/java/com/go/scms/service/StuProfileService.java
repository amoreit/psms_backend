/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.StuProfileDocDto;
import com.go.scms.dto.StuProfileDto;
import com.go.scms.entity.StuProfileDocEntity;
import com.go.scms.entity.StuProfileEntity;
import com.go.scms.entity.StuStatusEntity;
import com.go.scms.repository.StuProfileDocRepository;
import com.go.scms.repository.StuProfileRepository;
import com.go.scms.repository.StuStatusRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author P3rdiscoz
 */
@Service
public class StuProfileService {

    @Autowired
    private StuProfileRepository stuProfileRepo;
    
     @Autowired
    private StuStatusRepository stuStatusRepo;

    @Autowired
    private StuProfileDocRepository stuProfileDocRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode().trim());
        String citizenId = ValiableUtils.stringIsEmpty(stuProfileDto.getCitizenId());
        String firstnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getFirstnameTh().trim());
        String lastnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getLastnameTh().trim());
        String classId = ValiableUtils.stringIsEmpty(stuProfileDto.getClassId().trim());
        String classRoomId = ValiableUtils.stringIsEmpty(stuProfileDto.getClassRoomId());
        String stuStatus = ValiableUtils.stringIsEmpty(stuProfileDto.getStuStatus());

        /**
         * * search all and citizenId,firstnameTh,lastnameTh,stuStatus **
         */
        if (stuCode.equals("") && classId.equals("") && classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.find(citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * search stuCode **
         */
        if (!stuCode.equals("") && classId.equals("") && classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucode(stuCode, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * search classId **
         */
        if (!classId.equals("") && classRoomId.equals("") && stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findclassid(classId, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * search classId and classRoomId **
         */
        if (!classId.equals("") && !classRoomId.equals("") && stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findclassidandclassroomid(classId, classRoomId, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * search stuCode and classId and classRoomId **
         */
        if (!stuCode.equals("") && !classId.equals("") && !classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeandclassidandclassroomid(classId, classRoomId, stuCode, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * search stuCode and classId **
         */
        if (!stuCode.equals("") && !classId.equals("") && classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeandclassid(classId, stuCode, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(stuProfileRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updatemini(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            
            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            StuStatusEntity entityStatus = stuStatusRepo.findByStuStatusId(Integer.parseInt(stuProfileDto.getStuStatusId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entity);

            entity.setJoinStuStatus(entityStatus); 
            entity.setStuStatus(entityStatus.getStatusDesc());
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
   
            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileRepo.save(entity);
            
            StuProfileDocEntity entityDoc = stuProfileDocRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileDocId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entityDoc);

            entityDoc.setRecordStatus(RecordStatus.UPDATE.getCode());
            entityDoc.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileDocRepo.save(entityDoc);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updateandinsert(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            
            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileRepo.save(entity);

            StuProfileDocEntity entityDoc = modelMapper.map(stuProfileDto, StuProfileDocEntity.class);
            entityDoc.setRecordStatus(RecordStatus.SAVE.getCode());
            entityDoc.setCreatedUser(stuProfileDto.getUserName());

            stuProfileDocRepo.save(entityDoc);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updateSmartPurse(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
    
     public ResponseOutput updateisleft(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entity);

            entity.setIsleft(Boolean.parseBoolean(stuProfileDto.getIsleft()));
            entity.setLeftReason(stuProfileDto.getLeftReason());
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
     
     public ResponseOutput cancelisleft(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(stuProfileDto, entity);

            entity.setIsleft(false);
            entity.setLeftReason(null);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(stuProfileDto.getUserName());

            stuProfileRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            
            StuProfileEntity entity = stuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            entity.setRecordStatus(RecordStatus.DELETE.getCode());

            stuProfileRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------
    // by milo
    public ResponseOutput searchCer002(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode().trim());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(stuProfileDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(stuProfileDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(stuProfileDto.getClassRoomId());
        String firstnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getFirstnameTh().trim());
        String lastnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getLastnameTh().trim());

        /**
         * * find all year class room stuCode firstnameTh lastnameTh **
         */
        if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && !stuCode.equals("") && !firstnameTh.equals("") && !lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findall(yearGroupId, classId, classRoomId, stuCode, firstnameTh, lastnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find year and class and room **
         */
        else if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && stuCode.equals("") && firstnameTh.equals("") && lastnameTh.equals("")) {
            
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findyearandclassandroom(yearGroupId, classId, classRoomId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find stuCode **
         */
        else if (yearGroupId.equals(0) && classId.equals(0) && classRoomId.equals(0) && !stuCode.equals("") && firstnameTh.equals("") && lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstuCode(stuCode, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find stuCode and firstnameTh **
         */
        else if (yearGroupId.equals(0) && classId.equals(0) && classRoomId.equals(0) && !stuCode.equals("") && !firstnameTh.equals("") && lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeandfirstnameth(stuCode, firstnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find stuCode and lastnameTh **
         */
        else if (yearGroupId.equals(0) && classId.equals(0) && classRoomId.equals(0) && !stuCode.equals("") && firstnameTh.equals("") && !lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeandlastnameth(stuCode, lastnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find firstnameTh **
         */
        else if (yearGroupId.equals(0) && classId.equals(0) && classRoomId.equals(0) && stuCode.equals("") && !firstnameTh.equals("") && lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findfirstnameTh(firstnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find firstnameTh and lastnameTh **
         */
        else if (yearGroupId.equals(0) && classId.equals(0) && classRoomId.equals(0) && stuCode.equals("") && !firstnameTh.equals("") && !lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findfirstnamethandlastnameth(firstnameTh, lastnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find lastnameTh **
         */
        else if (yearGroupId.equals(0) && classId.equals(0) && classRoomId.equals(0) && stuCode.equals("") && firstnameTh.equals("") && !lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findlastnameTh(lastnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find year and class and room and stuCode **
         */
        else if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && !stuCode.equals("") && firstnameTh.equals("") && lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findyearandclassandroomandstucode(yearGroupId, classId, classRoomId, stuCode, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find year and class and room and firstnameTh **
         */
        else if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && stuCode.equals("") && !firstnameTh.equals("") && lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findyearandclassandroomandfirstnameth(yearGroupId, classId, classRoomId, firstnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find year and class and room and lastnameTh **
         */
        else if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && stuCode.equals("") && firstnameTh.equals("") && !lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findyearandclassandroomandlastnameth(yearGroupId, classId, classRoomId, lastnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find year and class and room and stuCode and firstnameTh **
         */
        else if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && !stuCode.equals("") && !firstnameTh.equals("") && lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findyearandclassandroomandstucodeandfirstnameth(yearGroupId, classId, classRoomId, stuCode, firstnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**
         * * find year and class and room and stuCode and lastnameTh **
         */
        else if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && !stuCode.equals("") && firstnameTh.equals("") && !lastnameTh.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findyearandclassandroomandstucodeandlastnameth(yearGroupId, classId, classRoomId, stuCode, lastnameTh, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }
    //________________________________________________________________________________________________________
    //find rep 006 by nock

    public ResponseOutput searchRep006(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(stuProfileDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(stuProfileDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(stuProfileDto.getClassRoomId());
        Integer catClassId = 1;
        
        /**
         * * find class and year and room **
         */
        if (!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findclassandyearandroom(yearGroupId, classId, classRoomId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }

    //________________________________________________________________________________________________________
    //find gra005 by nock
    public ResponseOutput searchGra005(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(stuProfileDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(stuProfileDto.getClassId());
        List list = stuProfileRepo.findGra005(yearGroupId, classId);

        output.setResponseData(list);
        return output;
    }
    
    //by Moss
    public ResponseOutput updategraduate(String userName, StuProfileDto[] stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (StuProfileDto map : stuProfileDto) {
                stuProfileRepo.updategraduate(Integer.parseInt(map.getStuStatusId()),map.getStuStatus(), RecordStatus.UPDATE.getCode(), userName, Long.parseLong(map.getStuProfileId()));
            }

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }
    
    //by milo
     public ResponseOutput searchStu010(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode().trim());
        String citizenId = ValiableUtils.stringIsEmpty(stuProfileDto.getCitizenId());
        String firstnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getFirstnameTh().trim());
        String lastnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getLastnameTh().trim());
        Integer classId = (int) ValiableUtils.IntIsZero(stuProfileDto.getClassId());
        String classRoomId = ValiableUtils.stringIsEmpty(stuProfileDto.getClassRoomId());
        String stuStatus = ValiableUtils.stringIsEmpty(stuProfileDto.getStuStatus());

        /** search all and citizenId,firstnameTh,lastnameTh,stuStatus **/
        if (stuCode.equals("") && classId.equals(0) && classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findStu010(citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /** search stuCode **/
        if (!stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeStu010(stuCode, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /**  search classId **/
        if (!classId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findclassidStu010(classId, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /** search classId and classRoomId **/
        if (!classId.equals(0) && !classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findclassidandclassroomidStu010(classId, classRoomId, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        /** search stuCode and classId and classRoomId **/
        if (!stuCode.equals("") && !classId.equals(0) && !classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeandclassidandclassroomidStu010(classId, classRoomId, stuCode, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        
        //Aong
        /** search stuCode and classId **/
        if (!stuCode.equals("") && !classId.equals(0) && classRoomId.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findstucodeandclassidStu010(classId, stuCode, citizenId, firstnameTh, lastnameTh, stuStatus, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        
        //Aong
        /** search FirstnameTh **/
        if (!firstnameTh.equals("")&& stuCode.equals("")&& citizenId.equals("")&& lastnameTh.equals("")&& classId.equals("")&& classRoomId.equals("")){
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findfirstnamethStu010(firstnameTh, stuStatus, pageable);
            
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        /** search LastnameTh **/
        if (!lastnameTh.equals("")&& stuCode.equals("")&& citizenId.equals("")&& firstnameTh.equals("")&& classId.equals("")&& classRoomId.equals("")){
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findlastnameThStu010(lastnameTh, stuStatus, pageable);
            
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }
    
    //aong
    public ResponseOutput searchstu(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode().trim());
        List list = stuProfileRepo.getStp(stuCode);
        output.setResponseData(list);
        return output;
    }
    
    //aong
     public ResponseOutput searchstuPic(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode().trim());
        List list = stuProfileRepo.getStuPic(stuCode);
        output.setResponseData(list);
        return output;
    }
    
}
