/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.entity.TbTrnClassRoomMemberEntity;
import com.go.scms.repository.TbTrnClassRoomMemberRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class TbTrnClassRoomMemberService {

    @Autowired
    private TbTrnClassRoomMemberRepository tbTrnClassRoomMemberRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search **
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName());
        String className = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getClassName());
        String role = "student";

        Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()), Sort.by("classRoomMemberId").ascending());
        Page<TbTrnClassRoomMemberEntity> list = tbTrnClassRoomMemberRepo.findAllByyearGroupNameContainingAndClassNameContainingAndRole(yearGroupName, className, role, pageable);
        /**
         * * Set **
         */
        output.setResponseData(list.getContent().stream().map(e -> {
            Map m = new HashMap<>();
            m.put("yearGroupName", e.getYearGroupName());
            m.put("memberId", e.getMemberId());
            m.put("stuCode", e.getStuCode());
            m.put("fullname", e.getFullname());
            m.put("classRoom", e.getClassRoom());
            m.put("role", e.getRole());
            m.put("createdDate", e.getCreatedDate());
            m.put("createdUser", e.getCreatedUser());
            return m;
        }).collect(Collectors.toList()));
        output.setResponseSize(list.getTotalElements());
        return output;
    }

    public ResponseOutput searchAca004Sub06(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        String role = "student";

        List<Map> list = tbTrnClassRoomMemberRepo.searchAca004Sub06(yearGroupId, classId, classRoomId);
        output.setResponseData(list);

        return output;
    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(tbTrnClassRoomMemberRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput searchAca004Sub05Form(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search **
         */
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getYearGroupId());
        Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomSubjectId());

        List list = tbTrnClassRoomMemberRepo.findSub05Form(classRoomSubjectId, yearGroupId).stream().map(e -> {
            SearchAca004Sub05FormOutput m = new SearchAca004Sub05FormOutput();
            m.setClassRoomDesiredCharScoreId(e.get("class_room_desired_char_score_id"));
            m.setScCode(e.get("sc_code"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setYearGroupName(e.get("year_group_name"));
            m.setClassRoomMemberId(e.get("class_room_member_id"));
            m.setClassRoomSubjectId(e.get("class_room_subject_id"));
            m.setClassRoomSnameTh(e.get("class_room"));
            m.setFullname(e.get("fullname"));
            m.setStuCode(e.get("stu_code"));
            m.setSubjectCode(e.get("subject_code"));
            m.setSubjectName(e.get("subject_name_th"));
            m.setScore1(e.get("score1"));
            m.setScore2(e.get("score2"));
            m.setScore3(e.get("score3"));
            m.setScore4(e.get("score4"));
            m.setScore5(e.get("score5"));
            m.setScore6(e.get("score6"));
            m.setScore7(e.get("score7"));
            m.setScore8(e.get("score8"));
            m.setScore9(e.get("score9"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);
        return output;
    }

        public ResponseOutput searchRep001(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปฐมวัย ** nock
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        Integer catClassId = 1;

        if ( yearGroupName.equals("") || !yearGroupName.equals("") && classId.equals(0) && classRoomId.equals(0)) {
            
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findrep001All(catClassId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        else if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findrep001(yearGroupName, classId, classRoomId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        return output;
    }

    public ResponseOutput searchRep002(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปพ **
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());


        Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
        Page<Map> list = tbTrnClassRoomMemberRepo.findrep002(yearGroupName, classId, classRoomId, pageable);

        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());
        
        return output;
    } 

    public ResponseOutput searchRep003(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปพ **
         */
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getStuCode().trim());

        if(!stuCode.equals("")){
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findSturep003(stuCode, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        else{
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findrep003(yearGroupId, classId, classRoomId, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        return output;
    }

    public ResponseOutput searchRep004(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ต 2 ก **
         */
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());

        Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
        Page<Map> list = tbTrnClassRoomMemberRepo.findrep004(yearGroupId, classId, classRoomId, pageable);

        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());
        return output;
    }

    public ResponseOutput insert(String userName, TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            TbTrnClassRoomMemberEntity entity = modelMapper.map(tbTrnClassRoomMemberDto, TbTrnClassRoomMemberEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(tbTrnClassRoomMemberDto.getUserName());

            tbTrnClassRoomMemberRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(String userName, TbTrnClassRoomMemberDto[] tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            for (TbTrnClassRoomMemberDto map : tbTrnClassRoomMemberDto) {
                TbTrnClassRoomMemberEntity entity = modelMapper.map(map, TbTrnClassRoomMemberEntity.class);
                tbTrnClassRoomMemberRepo.updateDesiredCharScore(
                        (map.getDesiredCharResult()),
                        RecordStatus.UPDATE.getCode(),
                        userName,
                        Long.parseLong(map.getClassRoomMemberId()));
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
    
    //max
    public ResponseOutput searchAca006(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getYearGroupId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());

        List<Map> list = tbTrnClassRoomMemberRepo.findAca006Form(yearGroupId, classRoomId);

        output.setResponseData(list);
        return output;

    }
    
    //milo
    public ResponseOutput searchRep001ReportTerm1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        /**
         * * Search ปฐมวัย ** nock
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName().replace("2/", "1/"));
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getStuCode().trim());
        
        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0) && !stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findRep001ReportTerm1( yearGroupName, classId, classRoomId, stuCode, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());

        }

        return output;
    }
    
    public ResponseOutput searchRep001ReportCountTerm1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปฐมวัย ** nock
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName().replace("2/", "1/"));
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());

        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findrep001(yearGroupName, classId, classRoomId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }

    public ResponseOutput searchRep001ReportTerm2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปฐมวัย ** nock
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName().replace("1/", "2/"));
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getStuCode());


        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0) && !stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findRep001ReportTerm2( yearGroupName, classId, classRoomId, stuCode, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
            
        }
        return output;
    }
    
    public ResponseOutput searchRep001ReportCountTerm2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปฐมวัย ** nock
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName().replace("1/", "2/"));
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());

        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findrep001(yearGroupName, classId, classRoomId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        return output;
    }
    
    public ResponseOutput searchRep001ReportTeacher(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search ปฐมวัย ** nock
         */
        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        String role = "teacher";

        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0) ) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = tbTrnClassRoomMemberRepo.findRep001ReportTeacher( yearGroupName, classId, classRoomId, role, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
            
        }
        return output;
    }
    
    //aong
    public ResponseOutput acticityScore(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getStuCode()).trim();
        List list = tbTrnClassRoomMemberRepo.acticityScore(stuCode);
        output.setResponseData(list);
        return output;
    }
    
    //aong
    public ResponseOutput ratwResult(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getStuCode()).trim();
        List list = tbTrnClassRoomMemberRepo.ratwResult(stuCode);
        output.setResponseData(list);
        return output;
    }
    
    //aong
    public ResponseOutput desiredCharResult(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getStuCode()).trim();
        List list = tbTrnClassRoomMemberRepo.desiredCharResult(stuCode);
        output.setResponseData(list);
        return output;
    }
    
}

class SearchAca004Sub05FormOutput {

    private String classRoomDesiredCharScoreId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String classRoomMemberId;
    private String classRoomSubjectId;
    private String classRoomSnameTh;
    private String stuCode;
    private String fullname;
    private String subjectCode;
    private String subjectName;
    private String score1;
    private String score2;
    private String score3;
    private String score4;
    private String score5;
    private String score6;
    private String score7;
    private String score8;
    private String score9;

    public String getClassRoomDesiredCharScoreId() {
        return classRoomDesiredCharScoreId;
    }

    public void setClassRoomDesiredCharScoreId(String classRoomDesiredCharScoreId) {
        this.classRoomDesiredCharScoreId = classRoomDesiredCharScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScore3() {
        return score3;
    }

    public void setScore3(String score3) {
        this.score3 = score3;
    }

    public String getScore4() {
        return score4;
    }

    public void setScore4(String score4) {
        this.score4 = score4;
    }

    public String getScore5() {
        return score5;
    }

    public void setScore5(String score5) {
        this.score5 = score5;
    }

    public String getScore6() {
        return score6;
    }

    public void setScore6(String score6) {
        this.score6 = score6;
    }

    public String getScore7() {
        return score7;
    }

    public void setScore7(String score7) {
        this.score7 = score7;
    }

    public String getScore8() {
        return score8;
    }

    public void setScore8(String score8) {
        this.score8 = score8;
    }

    public String getScore9() {
        return score9;
    }

    public void setScore9(String score9) {
        this.score9 = score9;
    }

}
