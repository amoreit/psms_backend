/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.BehavioursSkillsDto;
import com.go.scms.repository.BehavioursSkillsRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BehavioursSkillsService {
    
    @Autowired
    private BehavioursSkillsRepository behavioursSkillsRepo;

    @Autowired
    private ModelMapper modelMapper;
    
    public ResponseOutput searchbehavioursskills(BehavioursSkillsDto behavioursSkillsDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classId = Integer.parseInt(behavioursSkillsDto.getClassId());
        Integer classRoomMemberId = Integer.parseInt(behavioursSkillsDto.getClassRoomMemberId());

        List list = behavioursSkillsRepo.findbehavioursskills(classId, classRoomMemberId).stream().map(e -> {
                BehavioursskillsOutput m = new BehavioursskillsOutput();
                m.setClassRoomLifeskillScoreId(e.get("class_room_lifeskill_score_id"));
                m.setScCode(e.get("sc_code"));
                m.setYearGroupId(e.get("year_group_id"));
                m.setYearGroupName(e.get("year_group_name"));
                m.setClassRoomMemberId(e.get("class_room_member_id"));
                m.setClassRoom(e.get("class_room"));
                m.setStuCode(e.get("stu_code"));
                m.setFullname(e.get("fullname"));
                m.setBehavioursSkillsId(e.get("behaviours_skills_id"));
                m.setBehavioursSkillsDesc(e.get("behaviours_skills_desc"));
                m.setItem(e.get("item"));
                m.setItemDesc(e.get("item_desc"));
                m.setScore(e.get("score")); 
                
                return m;
            }).collect(Collectors.toList());
            output.setResponseData(list);

        return output;
    }
}

class BehavioursskillsOutput{
    private String classRoomLifeskillScoreId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String classRoomMemberId;
    private String classRoom;
    private String stuCode;
    private String fullname;
    private String behavioursSkillsId;
    private String behavioursSkillsDesc;
    private String item;
    private String itemDesc;
    private String score;

    public String getClassRoomLifeskillScoreId() {
        return classRoomLifeskillScoreId;
    }

    public void setClassRoomLifeskillScoreId(String classRoomLifeskillScoreId) {
        this.classRoomLifeskillScoreId = classRoomLifeskillScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getBehavioursSkillsId() {
        return behavioursSkillsId;
    }

    public void setBehavioursSkillsId(String behavioursSkillsId) {
        this.behavioursSkillsId = behavioursSkillsId;
    }

    public String getBehavioursSkillsDesc() {
        return behavioursSkillsDesc;
    }

    public void setBehavioursSkillsDesc(String behavioursSkillsDesc) {
        this.behavioursSkillsDesc = behavioursSkillsDesc;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}
