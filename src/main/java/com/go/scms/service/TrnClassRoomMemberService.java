/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.Admrp002sub01Dto;
import com.go.scms.dto.StuProfileDto;
import com.go.scms.dto.TeacherProfileDto;
import com.go.scms.dto.TrnClassRoomMemberDto;
import com.go.scms.entity.TrnClassRoomMemberEntity;
import com.go.scms.repository.StuProfileRepository;
import com.go.scms.repository.TeacherProfileRepository;
import com.go.scms.repository.TrnClassRoomMemberRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TrnClassRoomMemberService {

    @Autowired
    private TrnClassRoomMemberRepository trnClassRoomMemberRepo;
    
    @Autowired
    private TeacherProfileRepository teacherProfileRepo;
    
    @Autowired
    private StuProfileRepository stuProfileRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoom = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoom());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        List<Map> list = trnClassRoomMemberRepo.find(classRoom, yearGroupId);

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchstufordialog(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode().trim());
        String firstnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getFirstnameTh().trim());
        String lastnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getLastnameTh().trim());

        Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
        Page<Map> list = trnClassRoomMemberRepo.findstufordialog(firstnameTh, lastnameTh, stuCode, pageable);

        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());
        return output;

    }
    
    public ResponseOutput searchclassidandclassnamebyclassroomid(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());

        List<Map> list = trnClassRoomMemberRepo.findclassidandclassnamebyclassroomid(classRoomId);

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchallnumstuandgender() {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        List<Map> list = trnClassRoomMemberRepo.findallnumstuandgender();

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchallnumteacherandgender() {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        List<Map> list = trnClassRoomMemberRepo.findallnumteacherandgender();

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchgraphallnumstubyyear() {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        List<Map> list = trnClassRoomMemberRepo.findgraphallnumstubyyear();

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchnumstuallcatclasspie() {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        List<Map> list = trnClassRoomMemberRepo.findnumstuallcatclasspie();

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchteacher(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoom = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoom());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        List<Map> list = trnClassRoomMemberRepo.findteacher(classRoom, yearGroupId);

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchstuforevaluation(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());

        List<Map> list = trnClassRoomMemberRepo.findstuforevaluation(yearGroupId, classId, classRoomId);

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchdepartmentfromclass(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());

        List<Map> list = trnClassRoomMemberRepo.finddepartmentfromclass(classId);

        output.setResponseData(list);

        return output;
    }

    public ResponseOutput searchsubjectfromclass(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer departmentId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getDepartmentId());

        List<Map> list = trnClassRoomMemberRepo.findsubjectfromclass(classId, yearGroupId, departmentId);

        output.setResponseData(list);

        return output;
    }

    public ResponseOutput searchdepartment(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoom = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoom());

        List<Map> list = trnClassRoomMemberRepo.finddepartment(classRoom);

        output.setResponseData(list);

        return output;
    }

    public ResponseOutput searchsubjectclassroom(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoom = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoom());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer departmentId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getDepartmentId());

        List<Map> list = trnClassRoomMemberRepo.findsubjectfromclassroom(classRoom, yearGroupId, departmentId);

        output.setResponseData(list);

        return output;
    }

    public ResponseOutput searchactivity(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoomMemberId = Integer.parseInt(trnClassRoomMemberDto.getClassRoomMemberId());

        List<Map> list = trnClassRoomMemberRepo.findactivity(classRoomMemberId);

        output.setResponseData(list);

        return output;
    }

    public ResponseOutput searchstufornumber(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        String orderBy = ValiableUtils.stringIsEmpty(trnClassRoomMemberDto.getOrderBy());

        if (orderBy.equals("")) {
            List list = trnClassRoomMemberRepo.findstufornumberstucode(classId, classRoomId, yearGroupId).stream().map(e -> {
                NumberOfMember m = new NumberOfMember();
                m.setClassRoomMemberId(e.get("class_room_member_id"));
                m.setClassRoomNo(e.get("class_room_no"));
                m.setFullname(e.get("fullname"));
                m.setStuCode(e.get("stu_code"));
                m.setNo(e.get("no"));
                return m;
            }).collect(Collectors.toList());
            output.setResponseData(list);
        }

        if (orderBy.equals("1")) {
            List list = trnClassRoomMemberRepo.findstufornumberstucode(classId, classRoomId, yearGroupId).stream().map(e -> {
                NumberOfMember m = new NumberOfMember();
                m.setClassRoomMemberId(e.get("class_room_member_id"));
                m.setClassRoomNo(e.get("class_room_no"));
                m.setFullname(e.get("fullname"));
                m.setStuCode(e.get("stu_code"));
                m.setNo(e.get("no"));
                return m;
            }).collect(Collectors.toList());
            output.setResponseData(list);
        }
        if (orderBy.equals("2")) {
            List list = trnClassRoomMemberRepo.findstufornumberfullname(classId, classRoomId, yearGroupId).stream().map(e -> {
                NumberOfMember m = new NumberOfMember();
                m.setClassRoomMemberId(e.get("class_room_member_id"));
                m.setClassRoomNo(e.get("class_room_no"));
                m.setFullname(e.get("fullname"));
                m.setStuCode(e.get("stu_code"));
                m.setNo(e.get("no"));
                return m;
            }).collect(Collectors.toList());
            output.setResponseData(list);
        }

        return output;

    }

    public ResponseOutput searchmemberfromsubject(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());
        Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomSubjectId());

        List list = trnClassRoomMemberRepo.findmemberfromsubject(yearGroupId, classRoomId, classRoomSubjectId).stream().map(e -> {
            MemberOfCompetenciesScore m = new MemberOfCompetenciesScore();
            m.setClassRoomCompetenciesScoreId(e.get("class_room_competencies_score_id"));
            m.setScCode(e.get("sc_code"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setYearGroupName(e.get("year_group_name"));
            m.setClassRoomMemberId(e.get("class_room_member_id"));
            m.setClassRoomSubjectId(e.get("class_room_subject_id"));
            m.setClassRoom(e.get("class_room"));
            m.setFullname(e.get("fullname"));
            m.setClassRoomNo(e.get("class_room_no"));
            m.setStuCode(e.get("stu_code"));
            m.setScore1(e.get("score1"));
            m.setScore2(e.get("score2"));
            m.setScore3(e.get("score3"));
            m.setScore4(e.get("score4"));
            m.setScore5(e.get("score5"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput searchstuforcompetencies(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());

        List list = trnClassRoomMemberRepo.findstuforcompetencies(yearGroupId, classId, classRoomId).stream().map(e -> {
            MemberOfCompetenciesResult m = new MemberOfCompetenciesResult();
            m.setClassRoomMemberId(e.get("class_room_member_id"));
            m.setClassRoomNo(e.get("class_room_no"));
            m.setStuCode(e.get("stu_code"));
            m.setFullname(e.get("fullname"));
            m.setCompetenciesResult(e.get("competencies_result"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput searchstuforstu007(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());

        List<Map> list = trnClassRoomMemberRepo.findstuforstu007(yearGroupId, classId, classRoomId);
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput searchstuforstu009(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());

        List list = trnClassRoomMemberRepo.findstuforstu009(yearGroupId, classId, classRoomId).stream().map(e -> {
            MemberForStu009 m = new MemberForStu009();
            m.setNumberstu(e.get("numberstu"));
            m.setScCode(e.get("sc_code"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setYearGroupName(e.get("year_group_name"));
            m.setStuProfileId(e.get("stu_profile_id"));
            m.setStuCode(e.get("stu_code"));
            m.setFullname(e.get("fullname"));
            m.setLastClassRoom(e.get("last_class_room"));
            m.setGraduateReason(e.get("graduate_reason"));
            m.setClassOf(e.get("class_of"));
            m.setGraduateDate(e.get("graduate_date"));
            m.setStuStatusId(e.get("stu_status_id"));
            m.setStuStatus(e.get("stu_status"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput searchnumberstu(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer startYearId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getStartYearId());
        Integer endYearId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getEndYearId());

        if (startYearId != 0 && endYearId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstubystart(startYearId);
            output.setResponseData(list.stream().map(e -> {
                Map m = new HashMap<>();
                m.put("name", e.get("year"));
                m.put("series", e.get("series"));
                m.put("number", e.get("number"));
                m.put("value", e.get("value"));

                return m;
            }).collect(Collectors.toList()));
        }
        if (startYearId == 0 && endYearId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstubyend(endYearId);
            output.setResponseData(list.stream().map(e -> {
                Map m = new HashMap<>();
                m.put("name", e.get("year"));
                m.put("series", e.get("series"));
                m.put("number", e.get("number"));
                m.put("value", e.get("value"));

                return m;
            }).collect(Collectors.toList()));
        }
        if (startYearId != 0 && endYearId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstu(startYearId, endYearId);
            output.setResponseData(list.stream().map(e -> {
                Map m = new HashMap<>();
                m.put("name", e.get("year"));
                m.put("series", e.get("series"));
                m.put("number", e.get("number"));
                m.put("value", e.get("value"));

                return m;
            }).collect(Collectors.toList()));
        }
        if (startYearId == 0 && endYearId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstuall();
            output.setResponseData(list.stream().map(e -> {
                Map m = new HashMap<>();
                m.put("name", e.get("year"));
                m.put("series", e.get("series"));
                m.put("number", e.get("number"));
                m.put("value", e.get("value"));

                return m;
            }).collect(Collectors.toList()));
        }

        return output;
    }

    public ResponseOutput searchnumberstuforcatclass(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer catClassId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getCatClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        if (yearGroupId != 0 && catClassId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstuforcatclassbyyeargroupid(yearGroupId);
            output.setResponseData(list);
        }
        if (yearGroupId != 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstuforcatclass(yearGroupId, catClassId);
            output.setResponseData(list);
        }
        return output;
    }

    public ResponseOutput searchnumberstuforcatclasspie(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer catClassId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getCatClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        if (yearGroupId != 0 && catClassId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstuforcatclassbyyeargroupidpie(yearGroupId);
            output.setResponseData(list);
        }
        if (yearGroupId != 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findnumberstuforcatclasspie(yearGroupId, catClassId);
            output.setResponseData(list);
        }

        return output;
    }

    public ResponseOutput searchforadmrp001sub02pie(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer catClassId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getCatClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        if (yearGroupId != 0 && catClassId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp001sub02yeargroupidpie(yearGroupId);
            output.setResponseData(list);
        }
        if (yearGroupId != 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp001sub02pie(catClassId, yearGroupId);
            output.setResponseData(list);
        }

        return output;
    }

    public ResponseOutput searchforadmrp001sub02(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer catClassId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getCatClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        if (yearGroupId != 0 && catClassId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp001sub02yeargroupid(yearGroupId);
            output.setResponseData(list);
        }
        if (yearGroupId != 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp001sub02(yearGroupId, catClassId);
            output.setResponseData(list);
        }

        return output;
    }

    public ResponseOutput searchforadmrp001sub01(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer catClassId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getCatClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());

        if (yearGroupId != 0 && catClassId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp001sub01yeargroup(yearGroupId);
            output.setResponseData(list);
        }
        if (yearGroupId != 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp001sub01(catClassId, yearGroupId);
            output.setResponseData(list);
        }

        return output;
    }

    public ResponseOutput searchforadmrp002sub1(Admrp002sub01Dto admrp002sub01Dto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer departmentId = (int) ValiableUtils.IntIsZero(admrp002sub01Dto.getDepartmentId());
        Integer catClassId = (int) ValiableUtils.IntIsZero(admrp002sub01Dto.getCatClassId());
        String section = ValiableUtils.stringIsEmpty(admrp002sub01Dto.getSection());

        if (section.equals("1")) {
            if (departmentId == 0 && catClassId != 0) {
                List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub1byedutioncatclass(catClassId);
                output.setResponseData(list);
            }
            if (departmentId == 0 && catClassId == 0) {
                List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub1all();
                output.setResponseData(list);
            }
            if (departmentId != 0 && catClassId != 0) {
                List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub1(departmentId, catClassId);
                output.setResponseData(list);
            }
        }
        if (section.equals("2")) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub1byage(departmentId, catClassId);
            output.setResponseData(list);
        }

        return output;
    }

    public ResponseOutput searchforadmrp002sub2(Admrp002sub01Dto admrp002sub01Dto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer departmentId = (int) ValiableUtils.IntIsZero(admrp002sub01Dto.getDepartmentId());
        Integer catClassId = (int) ValiableUtils.IntIsZero(admrp002sub01Dto.getCatClassId());

        if (departmentId == 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub2catclass(catClassId);
            output.setResponseData(list);
        }
        if (departmentId != 0 && catClassId != 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub2(departmentId, catClassId);
            output.setResponseData(list);
        }
        if (departmentId == 0 && catClassId == 0) {
            List<Map> list = trnClassRoomMemberRepo.findforadmrp002sub2all();
            output.setResponseData(list);
        }

        return output;
    }

    public ResponseOutput searchteachertec010(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomId());

        List<Map> list = trnClassRoomMemberRepo.findteachertec010(yearGroupId, classId, classRoomId);

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(trnClassRoomMemberRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insertcopy(String userName, TrnClassRoomMemberDto[] trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (TrnClassRoomMemberDto map : trnClassRoomMemberDto) {
                TrnClassRoomMemberEntity entity = modelMapper.map(map, TrnClassRoomMemberEntity.class);
                entity.setCreatedUser(userName);
                trnClassRoomMemberRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
    
    public ResponseOutput insertstufordialog(String userName, TrnClassRoomMemberDto[] trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (TrnClassRoomMemberDto map : trnClassRoomMemberDto) {
                TrnClassRoomMemberEntity entity = modelMapper.map(map, TrnClassRoomMemberEntity.class);
                entity.setCreatedUser(userName);
                trnClassRoomMemberRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insertcopyteachertec010(String userName, TrnClassRoomMemberDto[] trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (TrnClassRoomMemberDto map : trnClassRoomMemberDto) {
                TrnClassRoomMemberEntity entity = modelMapper.map(map, TrnClassRoomMemberEntity.class);
                entity.setCreatedUser(userName);
                trnClassRoomMemberRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            //Check class_room_member_id 
            TrnClassRoomMemberEntity entity = trnClassRoomMemberRepo.findById(ValiableUtils.LongIsZero(trnClassRoomMemberDto.getClassRoomMemberId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            // Merge data 
            modelMapper.map(trnClassRoomMemberDto, entity);

            // Set 
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(trnClassRoomMemberDto.getUserName());

            trnClassRoomMemberRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updatenumber(TrnClassRoomMemberDto[] trnClassRoomMemberDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            //Check role by id 
            TrnClassRoomMemberEntity trnClassRoomMemberEntity = trnClassRoomMemberRepo.findById(ValiableUtils.LongIsZero(trnClassRoomMemberDto.length)).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            // Merge data 
            // int i = 0;
            for (TrnClassRoomMemberDto map : trnClassRoomMemberDto) {
                TrnClassRoomMemberEntity entity = modelMapper.map(map, TrnClassRoomMemberEntity.class);
                //i++;
                trnClassRoomMemberRepo.updateclassroomno(map.getClassRoomNo(), RecordStatus.UPDATE.getCode(), new Date(), Long.parseLong(map.getClassRoomMemberId()));
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput updatecompetencies(String userName, TrnClassRoomMemberDto[] trnClassRoomMemberDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (TrnClassRoomMemberDto map : trnClassRoomMemberDto) {
                TrnClassRoomMemberEntity entity = modelMapper.map(map, TrnClassRoomMemberEntity.class);
                trnClassRoomMemberRepo.updatecompetencies(Integer.parseInt(map.getCompetenciesResult()), RecordStatus.UPDATE.getCode(), userName, Long.parseLong(map.getClassRoomMemberId()));
            }

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }
    
    public ResponseOutput updateclassroom(TrnClassRoomMemberDto trnClassRoomMemberDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            TrnClassRoomMemberEntity entity = trnClassRoomMemberRepo.findById(ValiableUtils.LongIsZero(trnClassRoomMemberDto.getClassRoomMemberId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            
            modelMapper.map(trnClassRoomMemberDto, entity);

            entity.setClassRoomId(Integer.parseInt(trnClassRoomMemberDto.getClassRoomId()));
            entity.setClassRoom(trnClassRoomMemberDto.getClassRoom());
            entity.setIsEp(Boolean.parseBoolean(trnClassRoomMemberDto.getIsEp()));
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(trnClassRoomMemberDto.getUserName());

            trnClassRoomMemberRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }
    
     public ResponseOutput delete(TrnClassRoomMemberDto trnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            Integer classRoomMemberId = (int) ValiableUtils.IntIsZero(trnClassRoomMemberDto.getClassRoomMemberId());

            trnClassRoomMemberRepo.deletestufromclassroommember(classRoomMemberId);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
     
      public ResponseOutput updateclassroomidfromteacher(TeacherProfileDto teacherProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            Integer classRoomId = teacherProfileDto.getClassRoomId();
            Long teacherId = ValiableUtils.LongIsZero(teacherProfileDto.getTeacherId());

            teacherProfileRepo.updateclassroomfromteacher(classRoomId,RecordStatus.UPDATE.getCode(),teacherProfileDto.getUserName(),teacherId);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
      
      public ResponseOutput updateclassroomidfromstu(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            Integer classRoomId = Integer.parseInt(stuProfileDto.getClassRoomId());
            String classRoom = stuProfileDto.getClassRoom();        
            Long stuProfileId = ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId());

            stuProfileRepo.updateclassroomfromstu(classRoomId,classRoom,RecordStatus.UPDATE.getCode(),stuProfileDto.getUserName(),stuProfileId);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
     
      public ResponseOutput deleteclassroomidfromteacher(TeacherProfileDto teacherProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            
            Long teacherId = ValiableUtils.LongIsZero(teacherProfileDto.getTeacherId());

            teacherProfileRepo.deleteclassroomfromteacher(RecordStatus.UPDATE.getCode(),teacherProfileDto.getUserName(),teacherId);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
      
    public ResponseOutput deleteclassroomidfromstudent(StuProfileDto stuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            
            Long stuProfileId = ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId());

            stuProfileRepo.deleteclassroomfromstudent(RecordStatus.UPDATE.getCode(),stuProfileDto.getUserName(),stuProfileId);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}

class NumberOfMember {

    private String classRoomMemberId;
    private String classRoomNo;
    private String fullname;
    private String stuCode;
    private String no;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

}

class MemberOfCompetenciesScore {

    private String classRoomCompetenciesScoreId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String classRoomMemberId;
    private String classRoomSubjectId;
    private String classRoom;
    private String classRoomNo;
    private String stuCode;
    private String fullname;
    private String score1;
    private String score2;
    private String score3;
    private String score4;
    private String score5;

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getClassRoomCompetenciesScoreId() {
        return classRoomCompetenciesScoreId;
    }

    public void setClassRoomCompetenciesScoreId(String classRoomCompetenciesScoreId) {
        this.classRoomCompetenciesScoreId = classRoomCompetenciesScoreId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScore3() {
        return score3;
    }

    public void setScore3(String score3) {
        this.score3 = score3;
    }

    public String getScore4() {
        return score4;
    }

    public void setScore4(String score4) {
        this.score4 = score4;
    }

    public String getScore5() {
        return score5;
    }

    public void setScore5(String score5) {
        this.score5 = score5;
    }

}

class MemberOfCompetenciesResult {

    private String classRoomMemberId;
    private String classRoomNo;
    private String stuCode;
    private String fullname;
    private String competenciesResult;

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCompetenciesResult() {
        return competenciesResult;
    }

    public void setCompetenciesResult(String competenciesResult) {
        this.competenciesResult = competenciesResult;
    }

}

class MemberForStu009 {

    private String numberstu;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String stuProfileId;
    private String stuCode;
    private String fullname;
    private String lastClassRoom;
    private String graduateReason;
    private String classOf;
    private String graduateDate;
    private String stuStatusId;
    private String stuStatus;

    public String getNumberstu() {
        return numberstu;
    }

    public void setNumberstu(String numberstu) {
        this.numberstu = numberstu;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getStuProfileId() {
        return stuProfileId;
    }

    public void setStuProfileId(String stuProfileId) {
        this.stuProfileId = stuProfileId;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLastClassRoom() {
        return lastClassRoom;
    }

    public void setLastClassRoom(String lastClassRoom) {
        this.lastClassRoom = lastClassRoom;
    }

    public String getGraduateReason() {
        return graduateReason;
    }

    public void setGraduateReason(String graduateReason) {
        this.graduateReason = graduateReason;
    }

    public String getClassOf() {
        return classOf;
    }

    public void setClassOf(String classOf) {
        this.classOf = classOf;
    }

    public String getGraduateDate() {
        return graduateDate;
    }

    public void setGraduateDate(String graduateDate) {
        this.graduateDate = graduateDate;
    }

    public String getStuStatusId() {
        return stuStatusId;
    }

    public void setStuStatusId(String stuStatusId) {
        this.stuStatusId = stuStatusId;
    }

    public String getStuStatus() {
        return stuStatus;
    }

    public void setStuStatus(String stuStatus) {
        this.stuStatus = stuStatus;
    }

}
