package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTbStuProfileDto;
import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.entity.ClassEntity;
import com.go.scms.entity.ClassRoomEntity;
import com.go.scms.entity.TbMstGenderEntity;
import com.go.scms.entity.TbMstStuStatusEntity;
import com.go.scms.entity.TbMstTitleEntity;
import com.go.scms.entity.SnickTbStuProfileEntity;
import com.go.scms.repository.ClassRepository;
import com.go.scms.repository.ClassRoomRepository;
import com.go.scms.repository.TbMstGenderRepository;
import com.go.scms.repository.TbMstStuStatusRepository;
import com.go.scms.repository.TbMstTitleRepository;
import com.go.scms.repository.SnickTbStuProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class SnickTbStuProfileService {
    
        @Autowired
	private SnickTbStuProfileRepository tbStuProfileRepo;
        
        @Autowired
	private TbMstGenderRepository tbMstGenderRepo;
        
        @Autowired
	private TbMstStuStatusRepository tbMstStuStatusRepo;
	
	@Autowired
	private ModelMapper modelMapper;
        
        public ResponseOutput searchRefNo(SnickTbStuProfileDto stuProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
               
                List<Map> list = tbStuProfileRepo.findmaxrefno();
                output.setResponseData(list);
                
                return output;
        }
        
        public ResponseOutput insert(SnickTbStuProfileDto stuProfileDto) throws Throwable {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check cat_class by id ***/
                        TbMstStuStatusEntity stuStatusId = tbMstStuStatusRepo.findByStuStatusId(stuProfileDto.getStuStatusId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Merge data ***/
                        SnickTbStuProfileEntity entity = modelMapper.map(stuProfileDto,SnickTbStuProfileEntity.class);
                        entity.setStuCode(null);
                        entity.setIsleft(false);
                        entity.setStuStatusJoin(stuStatusId);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
                        entity.setCreatedDate(new Date());
                        entity.setCreatedUser(stuProfileDto.getUserName());

                        tbStuProfileRepo.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput update(SnickTbStuProfileDto stuProfileDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {			
			/*** Check id ***/
			SnickTbStuProfileEntity entity = tbStuProfileRepo.findById(ValiableUtils.LongIsZero(stuProfileDto.getStuProfileId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        TbMstStuStatusEntity stuStatusId = tbMstStuStatusRepo.findByStuStatusId(stuProfileDto.getStuStatusId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
                        /*** Merge data ***/
			modelMapper.map(stuProfileDto,entity);

			/*** Set ***/
                        entity.setStuStatusJoin(stuStatusId);
                        entity.setIsleft(false);
			entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(stuProfileDto.getUserName());
			
			tbStuProfileRepo.save(entity);
                                 
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
