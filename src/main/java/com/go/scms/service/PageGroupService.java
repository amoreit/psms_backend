package com.go.scms.service;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.PageGroupDto;
import com.go.scms.entity.PageGroupEntity;
import com.go.scms.repository.PageGroupRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;

@Service
public class PageGroupService {

	@Autowired
	private PageGroupRepository pageGroupRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ResponseOutput search(PageGroupDto pageGroupDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		/*** Search ***/
		String pageGroupCode = ValiableUtils.stringIsEmpty(pageGroupDto.getPageGroupCode());
		String pageGroupName = ValiableUtils.stringIsEmpty(pageGroupDto.getPageGroupName());
		
		Pageable pageable = PageRequest.of(PageableUtils.offset(pageGroupDto.getP()),PageableUtils.result(pageGroupDto.getResult()),Sort.by("pageGroupCode").ascending());
		Page<PageGroupEntity> list = pageGroupRepo.findAllByPageGroupCodeContainingAndPageGroupNameContaining(pageGroupCode, pageGroupName, pageable);
		
		/*** Set ***/
		output.setResponseData(list.getContent());
		output.setResponseSize(list.getTotalElements());
		return output;
	}
	
	public ResponseOutput findById(Long id) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(pageGroupRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput insert(PageGroupDto pageGroupDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
			PageGroupEntity entity = modelMapper.map(pageGroupDto,PageGroupEntity.class);
			entity.setPageGroupStatus(RecordStatus.SAVE.getCode());
			entity.setPageGroupCreatedBy(pageGroupDto.getUserName());
			
			pageGroupRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput update(PageGroupDto pageGroupDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check page group by id ***/
			PageGroupEntity entity = pageGroupRepo.findById(ValiableUtils.LongIsZero(pageGroupDto.getPageGroupId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(pageGroupDto,entity);

			/*** Set ***/
			entity.setPageGroupStatus(RecordStatus.UPDATE.getCode());
			entity.setPageGroupUpdatedDate(new Date());
			entity.setPageGroupUpdatedBy(pageGroupDto.getUserName());
			
			pageGroupRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput delete(PageGroupDto pageGroupDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check page group by id***/
			PageGroupEntity entity = pageGroupRepo.findById(ValiableUtils.LongIsZero(pageGroupDto.getPageGroupId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Set ***/
			entity.setPageGroupStatus(RecordStatus.DELETE.getCode());
			entity.setPageGroupUpdatedDate(new Date());
			entity.setPageGroupUpdatedBy(pageGroupDto.getUserName());
			
			pageGroupRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}

}
