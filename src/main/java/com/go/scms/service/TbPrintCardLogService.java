/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ActivityPaiDto;
import com.go.scms.dto.TbPrintCardLogDto;
import com.go.scms.entity.ActivityPaiEntity;
import com.go.scms.entity.TbPrintCardLogEntity;
import com.go.scms.repository.TbPrintCardLogRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kittipong
 */
@Service
public class TbPrintCardLogService {
        
        @Autowired
        private TbPrintCardLogRepository tbPrintCardLogRepository;

        @Autowired
        private ModelMapper modelMapper;
    
        public ResponseOutput insert(TbPrintCardLogDto tbPrintCardLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                        /*** Merge data ***/
                        TbPrintCardLogEntity entity = modelMapper.map(tbPrintCardLogDto,TbPrintCardLogEntity.class);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
                        entity.setCreatedDate(new Date());
                        entity.setCreatedUser(tbPrintCardLogDto.getUserName());
                        System.out.print(tbPrintCardLogDto);
                        tbPrintCardLogRepository.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;  
        }
}
