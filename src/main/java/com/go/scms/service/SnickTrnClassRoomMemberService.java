package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTrnClassRoomMemberDto;
import com.go.scms.entity.SnickClassRoomMemberRatwEntity;
import com.go.scms.entity.SnickTrnClassRoomMemberEntity;
import com.go.scms.repository.SnickClassRoomMemberRatwRepository;
import com.go.scms.repository.SnickTrnClassRoomMemberRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SnickTrnClassRoomMemberService {
    
        @Autowired
        private SnickTrnClassRoomMemberRepository classRoomMemberRepo;
        
        @Autowired
        private SnickClassRoomMemberRatwRepository classRoomMemberRatwRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput search(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                Integer classRoomId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getClassRoomId());
                Integer yearGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getYearGroupId());
                String recordStatus = "D";
                String role = "student";
                   
                if(classRoomId.equals(0) && yearGroupId.equals(0)){
                    List<SnickTrnClassRoomMemberEntity> list = classRoomMemberRepo.findAllByClassRoomIdAndYearGroupIdAndRecordStatusNotInOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(classRoomId, yearGroupId, recordStatus);
                    output.setResponseData(list);
                }
                
                if(!classRoomId.equals(0) && !yearGroupId.equals(0)){
                    List<SnickTrnClassRoomMemberEntity> list = classRoomMemberRepo.findAllByClassRoomIdAndYearGroupIdAndRecordStatusNotInOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(classRoomId, yearGroupId, recordStatus);
                    output.setResponseData(list);
                }
                
                if(!classRoomId.equals(0) && yearGroupId.equals(0)){
                    List<SnickTrnClassRoomMemberEntity> list = classRoomMemberRepo.findAllByClassRoomIdAndRecordStatusNotInAndRoleOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(classRoomId, recordStatus, role);
                    output.setResponseData(list);
                }
                
                if(classRoomId.equals(0) && !yearGroupId.equals(0)){
                    List<SnickTrnClassRoomMemberEntity> list = classRoomMemberRepo.findAllByYearGroupIdAndRecordStatusNotInAndRoleOrderByClassRoomIdAscStuCodeAscClassRoomMemberIdAsc(yearGroupId, recordStatus, role);
                    output.setResponseData(list);
                }
          
                return output;
        }
        
        public ResponseOutput searchMultiSkill(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                Integer yearGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getYearGroupId());
                Integer classId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getClassId());
                Integer classRoomId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getClassRoomId());
                
                List list = classRoomMemberRepo.multiall(yearGroupId, classId, classRoomId).stream().map(e->{
                        SnickTrnClassRoomMemberList m = new SnickTrnClassRoomMemberList();
                            m.setClassRoomMemberId(e.get("class_room_member_id"));
                            m.setYearGroupId(e.get("year_group_id"));
                            m.setYearGroupName(e.get("year_group_name"));
                            m.setClassId(e.get("class_id"));
                            m.setClassRoomId(e.get("class_room_id"));
                            m.setClassRoom(e.get("class_room"));
                            m.setClassRoomNo(e.get("class_room_no"));
                            m.setStuCode(e.get("stu_code"));
                            m.setFullname(e.get("fullname"));
                            m.setRatwResult(e.get("ratw_result"));
                        return m;
                }).collect(Collectors.toList());

                /*** Set ***/
                output.setResponseData(list);
                output.setResponseSize(Integer.toUnsignedLong(list.size()));
                
                return output;
        }
        
        public ResponseOutput updateMultiSkill(String userName, SnickTrnClassRoomMemberDto[] classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** loop and mapping ***/
                    for (SnickTrnClassRoomMemberDto map : classRoomMemberDto) {
                        SnickTrnClassRoomMemberEntity entity = modelMapper.map(map, SnickTrnClassRoomMemberEntity.class);
                        classRoomMemberRepo.updateMultiSkill(
                                (Integer.parseInt(map.getRatwResult())),
                                 userName,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getClassRoomMemberId())));
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput searchMember(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                Long classRoomSubjectId = ValiableUtils.LongIsZero(classRoomMemberDto.getClassRoomSubjectId());
                String yearGroupName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getYearGroupName());
                String classRoom = ValiableUtils.stringIsEmpty(classRoomMemberDto.getClassRoom());
                
                /*** Mapping ***/
		List list = classRoomMemberRatwRepo.findmember(classRoomSubjectId, classRoom, yearGroupName).stream().map(e->{
			SnickTrnClassRoomMemberList m = new SnickTrnClassRoomMemberList();
                            m.setClassRoomMemberId(e.get("class_room_member_id"));
                            m.setScCode(e.get("sc_code"));
                            m.setYearGroupId(e.get("year_group_id"));
                            m.setYearGroupName(e.get("year_group_name"));
                            m.setClassRoomSubjectId(e.get("class_room_subject_id"));
                            m.setClassRoomSnameTh(e.get("class_room_sname_th"));
                            m.setClassRoomNo(e.get("class_room_no"));
                            m.setStuCode(e.get("stu_code"));
                            m.setFullname(e.get("fullname"));
                            m.setScore1(e.get("score1"));
                            m.setScore2(e.get("score2"));
                            m.setScore3(e.get("score3"));
                            m.setScore4(e.get("score4"));
                            m.setScore5(e.get("score5"));
                            m.setClassRoomRatwScoreId(e.get("class_room_ratw_score_id"));
			return m;
		}).collect(Collectors.toList());

                /*** Set ***/
                output.setResponseData(list);
                output.setResponseSize(Integer.toUnsignedLong(list.size()));
                
                return output;
        }
        
        //--------------------------------------- aca008
        public ResponseOutput searchActivityStu(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getYearGroupName());
                Integer classId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getClassId());
                Integer activityGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getActivityGroupId());
                String activityName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getActivityName());
                String recordStatus = "D";
                
                if(activityGroupId.equals(1) || activityGroupId.equals(5)){
                    List<Map> list = classRoomMemberRepo.findactivity1(yearGroupName, classId, activityName,  recordStatus);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(2) || activityGroupId.equals(6)){
                    List<Map> list = classRoomMemberRepo.findactivity2(yearGroupName, classId, activityName,  recordStatus);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(3) || activityGroupId.equals(7)){
                    List<Map> list = classRoomMemberRepo.findactivity3(yearGroupName, classId, activityName,  recordStatus);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(4) || activityGroupId.equals(8)){
                    List<Map> list = classRoomMemberRepo.findactivity4(yearGroupName, classId, activityName,  recordStatus);
                    output.setResponseData(list);
                }
                
                return output;
        }
        
        public ResponseOutput searchDialogActivityStu(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getYearGroupName());
                Integer classId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getClassId());
                Integer activityGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getActivityGroupId());
                String recordStatus = "D";
                
                if(activityGroupId.equals(1) || activityGroupId.equals(5)){
                    List<Map> list = classRoomMemberRepo.finddialogactivity1(yearGroupName, classId, recordStatus);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(2) || activityGroupId.equals(6)){
                    List<Map> list = classRoomMemberRepo.finddialogactivity2(yearGroupName, classId, recordStatus);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(3) || activityGroupId.equals(7)){
                    List<Map> list = classRoomMemberRepo.finddialogactivity3(yearGroupName, classId, recordStatus);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(4) || activityGroupId.equals(8)){
                    List<Map> list = classRoomMemberRepo.finddialogactivity4(yearGroupName, classId, recordStatus);
                    output.setResponseData(list);
                }
                
                return output;
        }
        
        public ResponseOutput updateActivityStu(String userName, SnickTrnClassRoomMemberDto[] classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** loop and mapping ***/
                    for (SnickTrnClassRoomMemberDto map : classRoomMemberDto) {
                        SnickTrnClassRoomMemberEntity entity = modelMapper.map(map, SnickTrnClassRoomMemberEntity.class);
                        if(map.getActivityGroupId() == 1 || map.getActivityGroupId() == 5){
                            classRoomMemberRepo.updateActivity1(
                                    map.getActivity1id(),
                                    map.getActivity1name(),
                                    map.getActivity1lesson(),
                                    map.getFullname(),
                                    userName,
                                    RecordStatus.UPDATE.getCode(),
                                    Long.parseLong(map.getClassRoomMemberId())
                            );
                        }else if(map.getActivityGroupId() == 2 || map.getActivityGroupId() == 6){
                            classRoomMemberRepo.updateActivity2(
                                    map.getActivity2id(),
                                    map.getActivity2name(),
                                    map.getActivity2lesson(),
                                    map.getFullname(),
                                     userName,
                                     RecordStatus.UPDATE.getCode(),
                                    Long.parseLong(map.getClassRoomMemberId())
                            );
                        }else if(map.getActivityGroupId() == 3 || map.getActivityGroupId() == 7){
                            classRoomMemberRepo.updateActivity3(
                                    map.getActivity3id(),
                                    map.getActivity3name(),
                                    map.getActivity3lesson(),
                                    map.getFullname(),
                                    userName,
                                    RecordStatus.UPDATE.getCode(),
                                    Long.parseLong(map.getClassRoomMemberId())
                            );
                        }else{
                            classRoomMemberRepo.updateActivity4(
                                    map.getActivity4id(),
                                    map.getActivity4name(),
                                    map.getActivity4lesson(),
                                    map.getFullname(),
                                    userName,
                                    RecordStatus.UPDATE.getCode(),
                                    Long.parseLong(map.getClassRoomMemberId())
                            );
                        }
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput updateDeleteActivityStu(String userName, SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
                        Long classRoomMemberId = Long.parseLong(ValiableUtils.stringIsEmpty(classRoomMemberDto.getClassRoomMemberId()));
                        Integer activityGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getActivityGroupId());
                        String updatedUser = userName;
                        String recordStatus = "U";
			
                        if(activityGroupId.equals(1) || activityGroupId.equals(5)){
                            classRoomMemberRepo.updateDeleteActivity1(updatedUser, recordStatus, classRoomMemberId);
                        }
                        
                        if(activityGroupId.equals(2) || activityGroupId.equals(6)){
                            classRoomMemberRepo.updateDeleteActivity2(updatedUser, recordStatus, classRoomMemberId);
                        }
                        
                        if(activityGroupId.equals(3) || activityGroupId.equals(7)){
                            classRoomMemberRepo.updateDeleteActivity3(updatedUser, recordStatus, classRoomMemberId);
                        }
                        
                        if(activityGroupId.equals(4) || activityGroupId.equals(8)){
                            classRoomMemberRepo.updateDeleteActivity4(updatedUser, recordStatus, classRoomMemberId);
                        }
                        
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
        }
        
        //--------------------------------------- aca004-sub02 search
        public ResponseOutput searchActivityStuResult(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getYearGroupName());
                Integer classId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getClassId());
                Integer activityGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getActivityGroupId());
                String activityName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getActivityName());
                String recordStatus = "D";
                
                if(activityGroupId.equals(1) || activityGroupId.equals(5)){
                    List list = classRoomMemberRepo.findactivity1result(yearGroupName, classId, activityName, recordStatus).stream().map(e->{
                            Aca004sub02 m = new Aca004sub02();
                                m.setClassRoomMemberId(e.get("class_room_member_id"));
                                m.setYearGroupName(e.get("year_group_name"));
                                m.setClassId(e.get("class_id"));
                                m.setClassName(e.get("class_name"));
                                m.setClassRoomId(e.get("class_room_id"));
                                m.setClassRoom(e.get("class_room"));
                                m.setClassRoomNo(e.get("class_room_no"));
                                m.setFullname(e.get("fullname"));
                                m.setTitle(e.get("title"));
                                m.setFirstnameTh(e.get("firstname_th"));
                                m.setLastnameTh(e.get("lastname_th"));
                                m.setActivity1name(e.get("ac_name1"));
                                m.setActivity1lesson(e.get("ac_lesson1"));
                                m.setActivity1result(e.get("activity1result"));
                            return m;
                    }).collect(Collectors.toList());

                    /*** Set ***/
                    output.setResponseData(list);
                    output.setResponseSize(Integer.toUnsignedLong(list.size()));
                }
                
                if(activityGroupId.equals(2) || activityGroupId.equals(6)){
                    List list = classRoomMemberRepo.findactivity2result(yearGroupName, classId, activityName, recordStatus).stream().map(e->{
                            Aca004sub02 m = new Aca004sub02();
                                m.setClassRoomMemberId(e.get("class_room_member_id"));
                                m.setYearGroupName(e.get("year_group_name"));
                                m.setClassId(e.get("class_id"));
                                m.setClassName(e.get("class_name"));
                                m.setClassRoomId(e.get("class_room_id"));
                                m.setClassRoom(e.get("class_room"));
                                m.setClassRoomNo(e.get("class_room_no"));
                                m.setFullname(e.get("fullname"));
                                m.setTitle(e.get("title"));
                                m.setFirstnameTh(e.get("firstname_th"));
                                m.setLastnameTh(e.get("lastname_th"));
                                m.setActivity2name(e.get("ac_name2"));
                                m.setActivity2lesson(e.get("ac_lesson2"));
                                m.setActivity2result(e.get("activity2result"));
                            return m;
                    }).collect(Collectors.toList());

                    /*** Set ***/
                    output.setResponseData(list);
                    output.setResponseSize(Integer.toUnsignedLong(list.size()));
                }
                
                if(activityGroupId.equals(3) || activityGroupId.equals(7)){
                    List list = classRoomMemberRepo.findactivity3result(yearGroupName, classId, activityName, recordStatus).stream().map(e->{
                            Aca004sub02 m = new Aca004sub02();
                                m.setClassRoomMemberId(e.get("class_room_member_id"));
                                m.setYearGroupName(e.get("year_group_name"));
                                m.setClassId(e.get("class_id"));
                                m.setClassName(e.get("class_name"));
                                m.setClassRoomId(e.get("class_room_id"));
                                m.setClassRoom(e.get("class_room"));
                                m.setClassRoomNo(e.get("class_room_no"));
                                m.setFullname(e.get("fullname"));
                                m.setTitle(e.get("title"));
                                m.setFirstnameTh(e.get("firstname_th"));
                                m.setLastnameTh(e.get("lastname_th"));
                                m.setActivity3name(e.get("ac_name3"));
                                m.setActivity3lesson(e.get("ac_lesson3"));
                                m.setActivity3result(e.get("activity3result"));
                            return m;
                    }).collect(Collectors.toList());

                    /*** Set ***/
                    output.setResponseData(list);
                    output.setResponseSize(Integer.toUnsignedLong(list.size()));
                }
                
                if(activityGroupId.equals(4)){
                    List list = classRoomMemberRepo.findactivity4result(yearGroupName, classId, activityName, recordStatus).stream().map(e->{
                            Aca004sub02 m = new Aca004sub02();
                                m.setClassRoomMemberId(e.get("class_room_member_id"));
                                m.setYearGroupName(e.get("year_group_name"));
                                m.setClassId(e.get("class_id"));
                                m.setClassName(e.get("class_name"));
                                m.setClassRoomId(e.get("class_room_id"));
                                m.setClassRoom(e.get("class_room"));
                                m.setClassRoomNo(e.get("class_room_no"));
                                m.setFullname(e.get("fullname"));
                                m.setTitle(e.get("title"));
                                m.setFirstnameTh(e.get("firstname_th"));
                                m.setLastnameTh(e.get("lastname_th"));
                                m.setActivity4name(e.get("ac_name4"));
                                m.setActivity4lesson(e.get("ac_lesson4"));
                                m.setActivity4result(e.get("activity4result"));
                            return m;
                    }).collect(Collectors.toList());

                    /*** Set ***/
                    output.setResponseData(list);
                    output.setResponseSize(Integer.toUnsignedLong(list.size()));
                }
                
                return output;
        }
        
        //--------------------------------------- aca004-sub02 update
        public ResponseOutput updateActivity1result(String userName, SnickTrnClassRoomMemberDto[] classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** loop and mapping ***/
                    for (SnickTrnClassRoomMemberDto map : classRoomMemberDto) {
                        SnickTrnClassRoomMemberEntity entity = modelMapper.map(map, SnickTrnClassRoomMemberEntity.class);
                        classRoomMemberRepo.updateActivity1result(
                                map.getActivity1result(),
                                userName,
                                RecordStatus.UPDATE.getCode(),
                                Long.parseLong(map.getClassRoomMemberId())
                        ); 
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput updateActivity2result(String userName, SnickTrnClassRoomMemberDto[] classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** loop and mapping ***/
                    for (SnickTrnClassRoomMemberDto map : classRoomMemberDto) {
                        SnickTrnClassRoomMemberEntity entity = modelMapper.map(map, SnickTrnClassRoomMemberEntity.class);
                        classRoomMemberRepo.updateActivity2result(
                                map.getActivity2result(),
                                userName,
                                RecordStatus.UPDATE.getCode(),
                                Long.parseLong(map.getClassRoomMemberId())
                        ); 
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput updateActivity3result(String userName, SnickTrnClassRoomMemberDto[] classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** loop and mapping ***/
                    for (SnickTrnClassRoomMemberDto map : classRoomMemberDto) {
                        SnickTrnClassRoomMemberEntity entity = modelMapper.map(map, SnickTrnClassRoomMemberEntity.class);
                        classRoomMemberRepo.updateActivity3result(
                                map.getActivity3result(),
                                userName,
                                RecordStatus.UPDATE.getCode(),
                                Long.parseLong(map.getClassRoomMemberId())
                        ); 
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput updateActivity4result(String userName, SnickTrnClassRoomMemberDto[] classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** loop and mapping ***/
                    for (SnickTrnClassRoomMemberDto map : classRoomMemberDto) {
                        SnickTrnClassRoomMemberEntity entity = modelMapper.map(map, SnickTrnClassRoomMemberEntity.class);
                        classRoomMemberRepo.updateActivity4result(
                                map.getActivity4result(),
                                userName,
                                RecordStatus.UPDATE.getCode(),
                                Long.parseLong(map.getClassRoomMemberId())
                        ); 
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        //--------------------------------------- aca008-report
        public ResponseOutput searchActivityStuReport(SnickTrnClassRoomMemberDto classRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getYearGroupName());
                Integer activityGroupId = (int) ValiableUtils.IntIsZero(classRoomMemberDto.getActivityGroupId());
                String activityName = ValiableUtils.stringIsEmpty(classRoomMemberDto.getActivityName());
                
                if(activityGroupId.equals(1)){
                    List<Map> list = classRoomMemberRepo.findactivity1report(yearGroupName, activityName);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(2)){
                    List<Map> list = classRoomMemberRepo.findactivity2report(yearGroupName, activityName);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(3)){
                    List<Map> list = classRoomMemberRepo.findactivity3report(yearGroupName, activityName);
                    output.setResponseData(list);
                }
                
                if(activityGroupId.equals(4)){
                    List<Map> list = classRoomMemberRepo.findactivity4report(yearGroupName, activityName);
                    output.setResponseData(list);
                }
                
                return output;
        }
        
}

class SnickTrnClassRoomMemberList{
    
    private String classRoomMemberId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String classRoomSubjectId;
    private String classRoomSnameTh;
    private String classRoomNo;
    private String stuCode;
    private String fullname;
    private String score1;
    private String score2;
    private String score3;
    private String score4;
    private String score5;
    private String classRoomRatwScoreId;
    private String classRoom;
    private String classRoomId;
    private String classId;
    private String ratwResult;

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }
    
    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScore3() {
        return score3;
    }

    public void setScore3(String score3) {
        this.score3 = score3;
    }

    public String getScore4() {
        return score4;
    }

    public void setScore4(String score4) {
        this.score4 = score4;
    }

    public String getScore5() {
        return score5;
    }

    public void setScore5(String score5) {
        this.score5 = score5;
    }

    public String getClassRoomRatwScoreId() {
        return classRoomRatwScoreId;
    }

    public void setClassRoomRatwScoreId(String classRoomRatwScoreId) {
        this.classRoomRatwScoreId = classRoomRatwScoreId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }
    
    public String getRatwResult() {
        return ratwResult;
    }

    public void setRatwResult(String ratwResult) {
        this.ratwResult = ratwResult;
    }
}

class Aca004sub02{
    private String classRoomMemberId;
    private String scCode;
    private String yearGroupId;
    private String yearGroupName;
    private String stuCode;
    private String classId;
    private String className;
    private String classRoomId;
    private String classRoom;
    private String classRoomNo;
    private String activity1id;
    private String activity1name;
    private String activity1lesson;
    private String activity1result;
    private String activity2id;
    private String activity2name;
    private String activity2lesson;
    private String activity2result;
    private String activity3id;
    private String activity3name;
    private String activity3lesson;
    private String activity3result;
    private String activity4id;
    private String activity4name;
    private String activity4lesson;
    private String activity4result;
    private String fullname;
    private String title;
    private String firstnameTh;
    private String lastnameTh;

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }
    
    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(String classRoom) {
        this.classRoom = classRoom;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getActivity1id() {
        return activity1id;
    }

    public void setActivity1id(String activity1id) {
        this.activity1id = activity1id;
    }

    public String getActivity1name() {
        return activity1name;
    }

    public void setActivity1name(String activity1name) {
        this.activity1name = activity1name;
    }

    public String getActivity1lesson() {
        return activity1lesson;
    }

    public void setActivity1lesson(String activity1lesson) {
        this.activity1lesson = activity1lesson;
    }

    public String getActivity1result() {
        return activity1result;
    }

    public void setActivity1result(String activity1result) {
        this.activity1result = activity1result;
    }

    public String getActivity2id() {
        return activity2id;
    }

    public void setActivity2id(String activity2id) {
        this.activity2id = activity2id;
    }

    public String getActivity2name() {
        return activity2name;
    }

    public void setActivity2name(String activity2name) {
        this.activity2name = activity2name;
    }

    public String getActivity2lesson() {
        return activity2lesson;
    }

    public void setActivity2lesson(String activity2lesson) {
        this.activity2lesson = activity2lesson;
    }

    public String getActivity2result() {
        return activity2result;
    }

    public void setActivity2result(String activity2result) {
        this.activity2result = activity2result;
    }

    public String getActivity3id() {
        return activity3id;
    }

    public void setActivity3id(String activity3id) {
        this.activity3id = activity3id;
    }

    public String getActivity3name() {
        return activity3name;
    }

    public void setActivity3name(String activity3name) {
        this.activity3name = activity3name;
    }

    public String getActivity3lesson() {
        return activity3lesson;
    }

    public void setActivity3lesson(String activity3lesson) {
        this.activity3lesson = activity3lesson;
    }

    public String getActivity3result() {
        return activity3result;
    }

    public void setActivity3result(String activity3result) {
        this.activity3result = activity3result;
    }

    public String getActivity4id() {
        return activity4id;
    }

    public void setActivity4id(String activity4id) {
        this.activity4id = activity4id;
    }

    public String getActivity4name() {
        return activity4name;
    }

    public void setActivity4name(String activity4name) {
        this.activity4name = activity4name;
    }

    public String getActivity4lesson() {
        return activity4lesson;
    }

    public void setActivity4lesson(String activity4lesson) {
        this.activity4lesson = activity4lesson;
    }

    public String getActivity4result() {
        return activity4result;
    }

    public void setActivity4result(String activity4result) {
        this.activity4result = activity4result;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }
    
    
}
