/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbMstDivisionDto;
import com.go.scms.entity.TbMstDivisionEntity;
import com.go.scms.repository.TbMstDivisionRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import static jdk.nashorn.internal.codegen.CompilerConstants.className;
import static org.apache.commons.lang.StringUtils.trim;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sir. Harinn
 */
@Service
public class TbMstDivisionService {
    
        @Autowired
        private TbMstDivisionRepository tbMstDivisionRepository;
        
        @Autowired
	private ModelMapper modelMapper;
    
        
        public ResponseOutput search(TbMstDivisionDto tbMstDivisionDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                String divisionNameTh = trim(ValiableUtils.stringIsEmpty(tbMstDivisionDto.getDivisionNameTh()));
                String divisionNameEn = trim(ValiableUtils.stringIsEmpty(tbMstDivisionDto.getDivisionNameEn()));
                String recordStatus = "D";

                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstDivisionDto.getP()), PageableUtils.result(tbMstDivisionDto.getResult()), Sort.by("divisionNameTh").ascending());
                Page<TbMstDivisionEntity> list = tbMstDivisionRepository.findAllByDivisionNameThContainingAndDivisionNameEnContainingAndRecordStatusNotIn(divisionNameTh, divisionNameEn, recordStatus,  pageable);
                /**
                 * * Set **
                 */
                output.setResponseData(list.getContent().stream().map(e -> {
                    Map m = new HashMap<>();
                    m.put("divisionId", e.getDivisionId());
                    m.put("divisionNameTh", e.getDivisionNameTh());
                    m.put("divisionNameEn", e.getDivisionNameEn());
                    m.put("updatedDate", e.getUpdatedDate());
                    m.put("updatedUser", e.getUpdatedUser());
                    m.put("createdDate", e.getCreatedDate());
                    m.put("createdUser", e.getCreatedUser());
                    return m;
                }).collect(Collectors.toList()));
                output.setResponseSize(list.getTotalElements());

                return output;
        }
        
	public ResponseOutput findById(Long id) throws Throwable {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(tbMstDivisionRepository.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND)));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}        
        
	public ResponseOutput insert(TbMstDivisionDto tbMstDivisionDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
			TbMstDivisionEntity entity = modelMapper.map(tbMstDivisionDto, TbMstDivisionEntity.class);
                        entity.setRecordStatus("A");
			entity.setCreatedUser(tbMstDivisionDto.getUserName());
			
			tbMstDivisionRepository.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}   
        
        public ResponseOutput update(TbMstDivisionDto tbMstDivisionDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			TbMstDivisionEntity entity = tbMstDivisionRepository.findById(ValiableUtils.LongIsZero(tbMstDivisionDto.getDivisionId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(tbMstDivisionDto,entity);

			/*** Set ***/
                        entity.setRecordStatus("U");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(tbMstDivisionDto.getUserName());
			
			tbMstDivisionRepository.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
       
        public ResponseOutput delete(TbMstDivisionDto tbMstDivisionDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check role by id ***/
                        TbMstDivisionEntity entity = tbMstDivisionRepository.findById(ValiableUtils.LongIsZero(tbMstDivisionDto.getDivisionId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Set ***/
                        entity.setRecordStatus("D");
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(tbMstDivisionDto.getUpdatedUser());

                        tbMstDivisionRepository.save(entity);
                }catch(ResponseException ex) {
                        output.setResponseCode(ex.getCode());
                        output.setResponseMsg(ex.getMessage());
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
}
