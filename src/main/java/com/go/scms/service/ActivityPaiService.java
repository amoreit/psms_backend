/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ActivityPaiDto;
import com.go.scms.entity.ActivityPaiEntity;
import com.go.scms.repository.ActivityPaiRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.validation.ConstraintViolationException;

@Service
public class ActivityPaiService {
    
    @Autowired
    private ActivityPaiRepository ActivityRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    public ResponseOutput search(ActivityPaiDto activityDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
                Integer classId =(int)ValiableUtils.IntIsZero(activityDto.getClassId());
                Integer activityGroupId =(int)ValiableUtils.IntIsZero(activityDto.getActivityGroupId());
		String activityName = ValiableUtils.stringIsEmpty(activityDto.getActivityName().trim());
                String location = ValiableUtils.stringIsEmpty(activityDto.getLocation().trim());
		
		if(classId.equals(0) && activityGroupId.equals(0) && activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.show(pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && activityGroupId.equals(0) && activityName.equals("")&& location.equals("")){
                    
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classid(classId,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(classId.equals(0) && !activityGroupId.equals(0) && activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.activityGroupId(activityGroupId,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(classId.equals(0) && activityGroupId.equals(0) && !activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.ActivityName(activityName,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(classId.equals(0) && activityGroupId.equals(0) && activityName.equals("")&& !location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.location(location,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(classId.equals(0) && activityGroupId.equals(0) && !activityName.equals("")&& !location.equals("")){
                 Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                 Page<Map> list = ActivityRepo.activityNamelocation(activityName,location,pageable);

                 /*** Set ***/
                 output.setResponseData(list.getContent());
                 output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && !activityGroupId.equals(0) && activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdactivityGroupId(classId,activityGroupId,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && activityGroupId.equals(0) && !activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdActivityName(classId,activityName,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                 if(!classId.equals(0) && activityGroupId.equals(0) && activityName.equals("")&& !location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdlocation(classId,location,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                 
                if(classId.equals(0) && !activityGroupId.equals(0) && !activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.ActivityGroupIdActivityName(activityGroupId,activityName,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                 
                if(classId.equals(0) && !activityGroupId.equals(0) && activityName.equals("")&& !location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.ActivityGroupIdlocation(activityGroupId,location,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                  
                if(!classId.equals(0) && !activityGroupId.equals(0) && !activityName.equals("")&& location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdaActivityGroupIdActivityName(classId,activityGroupId,activityName,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                 
                if(!classId.equals(0) && !activityGroupId.equals(0) && !activityName.equals("")&& !location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdaActivityGroupIdActivityNamelocation(classId,activityGroupId,activityName,location,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && activityGroupId.equals(0) && !activityName.equals("")&& !location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdActivityNamelocation(classId,activityName,location,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                    
                if(!classId.equals(0) && !activityGroupId.equals(0) && activityName.equals("")&& !location.equals("")){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(activityDto.getP()),PageableUtils.result(activityDto.getResult()));
                   Page<Map> list = ActivityRepo.classIdActivityGroupIdlocation(classId,activityGroupId,location,pageable);

                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }
                
		return output;
	}
    
        public ResponseOutput findById(Long id) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(ActivityRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput insert(ActivityPaiDto activityDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {

			/*** Merge data ***/
			ActivityPaiEntity entity = modelMapper.map(activityDto,ActivityPaiEntity.class);
                        entity.setRecordStatus("A");
			entity.setCreatedUser(activityDto.getUserName());
                        
			ActivityRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput insertIsactive(ActivityPaiDto activityDto) {
               ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
               try {              
                           /*** Merge data ***/
                           ActivityPaiEntity entity = modelMapper.map(activityDto,ActivityPaiEntity.class);
                           entity.setRecordStatus(RecordStatus.SAVE.getCode());
                           entity.setCreatedDate(new Date());
                           entity.setCreatedUser(activityDto.getUserName());

                           ActivityRepo.save(entity);
                   }catch(ConstraintViolationException e) {
                           return ExceptionUtils.validate(e);
                   }catch(Exception ex) {
                           return ExceptionUtils.convertRollbackToString(ex);
                   }
                   return output;  
        }
    
        public ResponseOutput update(ActivityPaiDto activityDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			ActivityPaiEntity entity = ActivityRepo.findById(ValiableUtils.LongIsZero(activityDto.getActivityId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(activityDto,entity);

			/*** Set ***/
                        entity.setRecordStatus("U");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(activityDto.getUserName());
			
			ActivityRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput updateIsactive(ActivityPaiDto activityDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {			

			ActivityPaiEntity entity = ActivityRepo.findById(ValiableUtils.LongIsZero(activityDto.getActivityId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
                        modelMapper.map(activityDto,entity);
			entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                        entity.setUpdatedUser(activityDto.getUserName());
                        
			ActivityRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
                return output;
        }
    
        public ResponseOutput delete(ActivityPaiDto activityDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			ActivityPaiEntity entity = ActivityRepo.findById(ValiableUtils.LongIsZero(activityDto.getActivityId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Set ***/
                        entity.setRecordStatus("D");
                        entity.setIsactive(false);
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(activityDto.getUpdatedUser());
			
			ActivityRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
