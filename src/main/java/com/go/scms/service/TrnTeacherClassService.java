/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TrnTeacherClassDto;
import com.go.scms.entity.TrnTeacherClassEntity;
import com.go.scms.repository.TrnTeacherClassRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrnTeacherClassService {
    
        @Autowired
        private TrnTeacherClassRepository trnTeacherClassRepo;

        @Autowired
        private ModelMapper modelMapper;

        public ResponseOutput searchteachertec009(TrnTeacherClassDto trnTeacherClassDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnTeacherClassDto.getYearGroupId());
            Integer catClassId = (int) ValiableUtils.IntIsZero(trnTeacherClassDto.getCatClassId());
            Integer classId = (int) ValiableUtils.IntIsZero(trnTeacherClassDto.getClassId());

            List<Map> list = trnTeacherClassRepo.findteachertec009(yearGroupId,catClassId,classId);

            output.setResponseData(list);
            return output;

        }

        public ResponseOutput insertcopyteacherclass(String userName, TrnTeacherClassDto[] trnTeacherClassDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                for (TrnTeacherClassDto map : trnTeacherClassDto) {
                    TrnTeacherClassEntity entity = modelMapper.map(map, TrnTeacherClassEntity.class);
                    entity.setCreatedUser(userName);
                    trnTeacherClassRepo.save(entity);
                }
            } catch (ConstraintViolationException e) {
                return ExceptionUtils.validate(e);
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
        }
}
