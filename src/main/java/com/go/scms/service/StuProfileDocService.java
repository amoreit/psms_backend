/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.repository.StuProfileDocRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author P3rdiscoz
 */
@Service
public class StuProfileDocService {
    
    @Autowired
    private StuProfileDocRepository stuProfileDocRepo;
    
    @Autowired
    private ModelMapper modelMapper;
    
    public ResponseOutput findById(Integer id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(stuProfileDocRepo.findByStuProfileId(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
