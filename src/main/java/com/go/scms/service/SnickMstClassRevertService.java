package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickMstClassDto;
import com.go.scms.entity.SnickMstClassEntity;
import com.go.scms.repository.SnickMstClassRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickMstClassRevertService {
    
        @Autowired
	private SnickMstClassRepository mstClassRepo;
        
        @Autowired
	private ModelMapper modelMapper;
        
        public ResponseOutput update(SnickMstClassDto mstClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			SnickMstClassEntity entity = mstClassRepo.findById(ValiableUtils.LongIsZero(mstClassDto.getClassId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        
			/*** Merge data ***/
			modelMapper.map(mstClassDto,entity);

			/*** Set ***/
                        entity.setRecordStatus(RecordStatus.UPDATE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(mstClassDto.getUpdatedUser());
			
			mstClassRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
