/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TitleDto;
import com.go.scms.entity.TitleEntity;
import com.go.scms.repository.TitleRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TitleService {

    @Autowired
    private TitleRepository titleRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TitleDto titleDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String name = ValiableUtils.stringIsEmpty(titleDto.getName());
        String descTh = ValiableUtils.stringIsEmpty(titleDto.getDescTh());
        Integer gender = (int) ValiableUtils.IntIsZero(titleDto.getGender());

        if (gender.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(titleDto.getP()), PageableUtils.result(titleDto.getResult()));
            Page<Map> list = titleRepo.findalltitle(name, descTh, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (!gender.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(titleDto.getP()), PageableUtils.result(titleDto.getResult()));
            Page<Map> list = titleRepo.findalltitlebygender(name, descTh, gender, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;

    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(titleRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(TitleDto titleDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            TitleEntity entity = modelMapper.map(titleDto, TitleEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(titleDto.getUserName());

            titleRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(TitleDto titleDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            TitleEntity entity = titleRepo.findById(ValiableUtils.LongIsZero(titleDto.getTitleId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(titleDto, entity);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(titleDto.getUserName());
            titleRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(TitleDto titleDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            TitleEntity entity = titleRepo.findById(ValiableUtils.LongIsZero(titleDto.getTitleId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            entity.setRecordStatus(RecordStatus.DELETE.getCode());
            titleRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
