package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickOnetDto;
import com.go.scms.entity.SnickOnetEntity;
import com.go.scms.repository.SnickOnetRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickOnetService {
    
        @Autowired
        private SnickOnetRepository onetRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput insert(SnickOnetDto onetDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
                        
			/*** Merge data ***/
			SnickOnetEntity entity = modelMapper.map(onetDto,SnickOnetEntity.class);
                        entity.setScCode("PRAMANDA");
                        entity.setIsactive(true);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
			entity.setCreatedUser(onetDto.getUserName());
			
			onetRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) { 
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput update(SnickOnetDto onetDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			SnickOnetEntity entity = onetRepo.findById(ValiableUtils.LongIsZero(onetDto.getStuOnetScoreId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                       
			/*** Merge data ***/
			modelMapper.map(onetDto,entity);

			/*** Set ***/
                        entity.setRecordStatus(RecordStatus.UPDATE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(onetDto.getUserName());
			
			onetRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
