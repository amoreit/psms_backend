/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbTecPrintCardLogDto;
import com.go.scms.entity.TbTecPrintCardLogEntity;
import com.go.scms.repository.TbTecPrintCardLogRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kittipong
 */
@Service
public class TbTecPrintCardLogService {
    
    @Autowired
    private TbTecPrintCardLogRepository tbTecPrintCardLogRepository;

    @Autowired
    private ModelMapper modelMapper;
    public ResponseOutput insert(TbTecPrintCardLogDto tbTecPrintCardLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                        /*** Merge data ***/
                        TbTecPrintCardLogEntity entity = modelMapper.map(tbTecPrintCardLogDto,TbTecPrintCardLogEntity.class);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
                        entity.setCreatedDate(new Date());
                        entity.setCreatedUser(tbTecPrintCardLogDto.getUserName());
                        tbTecPrintCardLogRepository.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;  
        }
    
     public ResponseOutput find(TbTecPrintCardLogDto tbTecPrintCardLogDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String teacherCardId = ValiableUtils.stringIsEmpty(tbTecPrintCardLogDto.getTeacherCardId());
                List<Map> list = tbTecPrintCardLogRepository.find(teacherCardId);
                output.setResponseData(list);
                
                return output;
        }
}
