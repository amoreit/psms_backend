/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbStuTimeAttendanceDto;
import com.go.scms.entity.StuProfileEntity;
import com.go.scms.entity.TbStuTimeAttendanceEntity;
import com.go.scms.repository.StuProfileRepository;
import com.go.scms.repository.TbStuTimeAttendanceRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TbStuTimeAttendanceService {

    @Autowired
    private TbStuTimeAttendanceRepository timeAttendanceRepo;

    @Autowired
    private StuProfileRepository stuProfileRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TbStuTimeAttendanceDto tbstutimeattendanceDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbstutimeattendanceDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbstutimeattendanceDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbstutimeattendanceDto.getClassRoomId());
        Integer condition = (int) ValiableUtils.IntIsZero(tbstutimeattendanceDto.getCondition());
        String startDate = tbstutimeattendanceDto.getStartDate();
        String endDate = tbstutimeattendanceDto.getEndDate();

        if (yearGroupId.equals(0)) {
            if (classId.equals(0)) {
                if (classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                ////////////////////////////////////////////// con 0,1 show
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.show1(pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0,1 endDate
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.end1(endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0,1 start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.start1(startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0,1 start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.startEnd1(startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////  con2 show
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.show2(pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 endDate
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.end2(endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.start2(startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.startEnd2(startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                } else if (!classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.room1(classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.roomEnd1(classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.roomStart1(classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.roomStartEnd1(classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.room2(classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.roomEnd2(classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.roomStart2(classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.roomStartEnd2(classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                }//end
            } else if (!classId.equals(0)) {
                if (classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.class1(classId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classEnd1(classId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classStart1(classId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classStartEnd1(classId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.class2(classId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classEnd2(classId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classStart2(classId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classStartEnd2(classId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                } else if (!classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoom1(classId, classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoomEnd1(classId, classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoomStart1(classId, classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 class room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoomStartEnd1(classId, classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoom2(classId, classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoomEnd2(classId, classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoomStart2(classId, classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 class room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.classRoomStartEnd2(classId, classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                }//end
            }//end
        } else if (!yearGroupId.equals(0)) {
            if (classId.equals(0)) {
                if (classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.year1(yearGroupId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearEnd1(yearGroupId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearStart1(yearGroupId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());
                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearStartEnd1(yearGroupId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.year2(yearGroupId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearEnd2(yearGroupId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearStart2(yearGroupId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearStartEnd2(yearGroupId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                } else if (!classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoom1(yearGroupId, classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoomEnd1(yearGroupId, classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoomStart1(yearGroupId, classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoomStartEnd1(yearGroupId, classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoom2(yearGroupId, classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoomEnd2(yearGroupId, classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoomStart2(yearGroupId, classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearRoomStartEnd2(yearGroupId, classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                }//end
            } else if (!classId.equals(0)) {
                if (classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClass1(yearGroupId, classId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassEnd1(yearGroupId, classId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());
                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class start 
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassStart1(yearGroupId, classId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassStartEnd1(yearGroupId, classId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class 
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClass2(yearGroupId, classId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassEnd2(yearGroupId, classId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassStart2(yearGroupId, classId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassStartEnd2(yearGroupId, classId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                } else if (!classRoomId.equals(0)) {
                    if (condition.equals(1)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoom1(yearGroupId, classId, classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoomEnd1(yearGroupId, classId, classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoomStart1(yearGroupId, classId, classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con0, 1 year class room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoomStartEnd1(yearGroupId, classId, classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    } else if (condition.equals(0) || condition.equals(2)) {
                        if (startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class room
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoom2(yearGroupId, classId, classRoomId, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class room end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoomEnd2(yearGroupId, classId, classRoomId, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        } else if (!startDate.equals("NaN-NaN-NaN")) {
                            if (endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class room start
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoomStart2(yearGroupId, classId, classRoomId, startDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            } else if (!endDate.equals("NaN-NaN-NaN")) {
                                /////////////////////////////////////////////search con2 year class room start end
                                Pageable pageable = PageRequest.of(PageableUtils.offset(tbstutimeattendanceDto.getP()), PageableUtils.result(tbstutimeattendanceDto.getResult()));
                                Page<Map> list = timeAttendanceRepo.yearClassRoomStartEnd2(yearGroupId, classId, classRoomId, startDate, endDate, pageable);

                                output.setResponseData(list.getContent());
                                output.setResponseSize(list.getTotalElements());

                            }//end
                        }//end
                    }//end
                }//end
            }//end
        }//end

        return output;
    }

    public ResponseOutput insert(TbStuTimeAttendanceDto tbstutimeattendanceDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
             
          Boolean check;  
          SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
             Date date1 = sdf.parse("08:30:00");
             Date date2 = sdf.parse(sdf.format(new Date()));
        
            if(date2.compareTo(date1) > 0){
                check = false;
            }else{
                check = true;
            }
            
            TbStuTimeAttendanceEntity entity = modelMapper.map(tbstutimeattendanceDto, TbStuTimeAttendanceEntity.class);
            entity.setRecordStatus("A");
            entity.setCreatedUser(tbstutimeattendanceDto.getUserName());
            entity.setCheckTime(new Date());
            entity.setIslate(check);

            timeAttendanceRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

}
