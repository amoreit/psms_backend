package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTeacherClassRoomMemberDto;
import com.go.scms.entity.SnickTeacherClassRoomMemberEntity;
import com.go.scms.repository.SnickTeacherClassRoomMemberRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SnickTeacherClassRoomMemberService {
    
        @Autowired
        private SnickTeacherClassRoomMemberRepository teacherClassRoomMemberRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput search(SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(teacherClassRoomMemberDto.getYearGroupName().trim());
                String className = ValiableUtils.stringIsEmpty(teacherClassRoomMemberDto.getClassName().trim());
                String classRoom = ValiableUtils.stringIsEmpty(teacherClassRoomMemberDto.getClassRoom().trim());
                String role = "teacher";
                String recordStatus = "D";
                
                if(yearGroupName.equals("") && className.equals("") && classRoom.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassRoomMemberDto.getP()),PageableUtils.result(teacherClassRoomMemberDto.getResult()),Sort.by("class_id","class_room_id","class_room_member_id").ascending());
                    Page<Map> list = teacherClassRoomMemberRepo.findrecordstatus(recordStatus, role, pageable);
                    
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!yearGroupName.equals("") && className.equals("") && classRoom.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassRoomMemberDto.getP()),PageableUtils.result(teacherClassRoomMemberDto.getResult()),Sort.by("class_id","class_room_id","class_room_member_id").ascending());
                    Page<Map> list = teacherClassRoomMemberRepo.findyeargroupname(yearGroupName, recordStatus, role, pageable);
                    
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!yearGroupName.equals("") && !className.equals("") && classRoom.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassRoomMemberDto.getP()),PageableUtils.result(teacherClassRoomMemberDto.getResult()),Sort.by("class_id","class_room_id","class_room_member_id").ascending());
                    Page<Map> list = teacherClassRoomMemberRepo.findyeargroupnameclassname(yearGroupName, className, recordStatus, role, pageable);
                    
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(yearGroupName.equals("") && !className.equals("") && classRoom.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassRoomMemberDto.getP()),PageableUtils.result(teacherClassRoomMemberDto.getResult()),Sort.by("class_id","class_room_id","class_room_member_id").ascending());
                    Page<Map> list = teacherClassRoomMemberRepo.findclassname(className, recordStatus, role, pageable);
                    
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(yearGroupName.equals("") && !className.equals("") && !classRoom.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassRoomMemberDto.getP()),PageableUtils.result(teacherClassRoomMemberDto.getResult()),Sort.by("class_id","class_room_id","class_room_member_id").ascending());
                    Page<Map> list = teacherClassRoomMemberRepo.findclassnameclassroom(className, classRoom, recordStatus, role, pageable);
                    
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!yearGroupName.equals("") && !className.equals("") && !classRoom.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassRoomMemberDto.getP()),PageableUtils.result(teacherClassRoomMemberDto.getResult()),Sort.by("class_id","class_room_id","class_room_member_id").ascending());
                    Page<Map> list = teacherClassRoomMemberRepo.findall(yearGroupName, className, classRoom, recordStatus, role, pageable);
                    
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                return output;
        }
        
        public ResponseOutput insert(SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {                       
			/*** Merge data ***/
			SnickTeacherClassRoomMemberEntity entity = modelMapper.map(teacherClassRoomMemberDto,SnickTeacherClassRoomMemberEntity.class);
                        entity.setScCode("PRAMANDA");
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
			entity.setCreatedUser(teacherClassRoomMemberDto.getUserName());
                        entity.setCreatedDate(new Date());
			
			teacherClassRoomMemberRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput update(SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check id ***/
			SnickTeacherClassRoomMemberEntity entity = teacherClassRoomMemberRepo.findById(ValiableUtils.LongIsZero(teacherClassRoomMemberDto.getClassRoomMemberId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			modelMapper.map(teacherClassRoomMemberDto,entity);

			/*** Set ***/
                        entity.setRecordStatus(RecordStatus.UPDATE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(teacherClassRoomMemberDto.getUserName());
			
			teacherClassRoomMemberRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput delete(SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    String classRoomMemberId = ValiableUtils.stringIsEmpty(teacherClassRoomMemberDto.getClassRoomMemberId()); 
                    teacherClassRoomMemberRepo.deleteTeacherClassRoomMember(Long.parseLong(classRoomMemberId));
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
}
