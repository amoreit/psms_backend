/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ActivityGroupDto;
import com.go.scms.entity.ActivityGroupEntity;
import com.go.scms.repository.ActivityGroupRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ActivityGroupService {
    @Autowired
    private ActivityGroupRepository activityGroupRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(ActivityGroupDto activityGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String activityGroup = ValiableUtils.stringIsEmpty(activityGroupDto.getActivityGroup());
        String recordStatus = "D";

            Pageable pageable = PageRequest.of(PageableUtils.offset(activityGroupDto.getP()), PageableUtils.result(activityGroupDto.getResult()));
            Page<ActivityGroupEntity> list = activityGroupRepo.findAllByRecordStatusNotInAndActivityGroupContainingOrderByActivityGroupId(recordStatus,activityGroup, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());

        return output;

    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(activityGroupRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(ActivityGroupDto activityGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            ActivityGroupEntity entity = modelMapper.map(activityGroupDto, ActivityGroupEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(activityGroupDto.getUserName());

            activityGroupRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(ActivityGroupDto activityGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            ActivityGroupEntity entity = activityGroupRepo.findById(ValiableUtils.LongIsZero(activityGroupDto.getActivityGroupId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(activityGroupDto, entity);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(activityGroupDto.getUserName());
            activityGroupRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(ActivityGroupDto activityGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            ActivityGroupEntity entity = activityGroupRepo.findById(ValiableUtils.LongIsZero(activityGroupDto.getActivityGroupId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            entity.setRecordStatus(RecordStatus.DELETE.getCode());
            activityGroupRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
