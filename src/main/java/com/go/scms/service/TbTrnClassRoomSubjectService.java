/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbTrnClassRoomSubjectDto;
import com.go.scms.entity.TbTrnClassRoomMemberEntity;
import com.go.scms.entity.TbTrnClassRoomSubjectEntity;
import com.go.scms.repository.TbTrnClassRoomSubjectRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static javassist.CtMethod.ConstParameter.integer;
import static javassist.CtMethod.ConstParameter.integer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class TbTrnClassRoomSubjectService {

    @Autowired
    private TbTrnClassRoomSubjectRepository tbTrnClassRoomSubjectRepository;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TbTrnClassRoomSubjectDto tbTrnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectDto.getYearGroupId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectDto.getClassRoomId());
        List<Map> list = tbTrnClassRoomSubjectRepository.findAca004Sub01(yearGroupId, classRoomId);

        output.setResponseData(list);
        return output;
    }

    public ResponseOutput search2(TbTrnClassRoomSubjectDto tbTrnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectDto.getClassRoomId());
        List<Map> list = tbTrnClassRoomSubjectRepository.findSubject(yearGroupId, classId, classRoomId);

        output.setResponseData(list);
        return output;
    }
}
