/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.dto.TbTrnClassRoomMemberDcsDto;
import com.go.scms.entity.TbStuProfileEntity;
import com.go.scms.entity.TbTrnClassRoomMemberDcsEntity;
import com.go.scms.repository.TbTrnClassRoomMemberDcsRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class TbTrnClassRoomMemberDcsService {

    @Autowired
    private TbTrnClassRoomMemberDcsRepository tbTrnClassRoomMemberDcsRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput searchAca004Sub06Form(TbTrnClassRoomMemberDcsDto tbTrnClassRoomMemberDcsDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoomMemberId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDcsDto.getClassRoomMemberId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDcsDto.getYearGroupId());

        List list = tbTrnClassRoomMemberDcsRepo.findAca004Sub06Form(classRoomMemberId, yearGroupId).stream().map(e -> {
            SearchAca004Sub06FormOutput m = new SearchAca004Sub06FormOutput();
            m.setClassRoomMemberDcsId(e.get("class_room_member_dcs_id"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setYearGroupName(e.get("year_group_name"));
            m.setClassRoomMemberId(e.get("class_room_member_id"));
            m.setClassId(e.get("class_id"));
            m.setClassName(e.get("class_name"));
            m.setClassRoomId(e.get("class_room_id"));
            m.setClassRoom(e.get("class_room"));
            m.setClassRoomNo(e.get("class_room_no"));
            m.setStuCode(e.get("stu_code"));
            m.setFullName(e.get("fullname"));
            m.setFirstnameTh(e.get("firstname_th"));
            m.setLastnameTh(e.get("lastname_th"));
            m.setBehavioursSkillsDetailId(e.get("behaviours_skills_detail_id"));
            m.setItem(e.get("item"));
            m.setItemDesc(e.get("item_desc"));
            m.setItemno(e.get("itemno"));
            m.setDescription(e.get("description"));
            m.setScore1(e.get("score1"));
            m.setScore2(e.get("score2"));
            m.setSumScore(e.get("sum_score"));
            m.setItemResult(e.get("item_result"));
            m.setYearGroupResult(e.get("year_group_result"));
            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);
        return output;
    }

    public ResponseOutput searchAca014(TbStuProfileDto tbStuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbStuProfileDto.getStuCode());
        String firstnameth = ValiableUtils.stringIsEmpty(tbStuProfileDto.getFirstnameTh());
        String lastnameth = ValiableUtils.stringIsEmpty(tbStuProfileDto.getLastnameTh());
        long stuStatusId = 2;
        if (stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuProfileDto.getP()), PageableUtils.result(tbStuProfileDto.getResult()), Sort.by("stu_profile_id").ascending());
            Page<Map> list = tbTrnClassRoomMemberDcsRepo.findAca014_2(stuStatusId, firstnameth, lastnameth, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        } else if (!stuCode.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuProfileDto.getP()), PageableUtils.result(tbStuProfileDto.getResult()), Sort.by("stu_profile_id").ascending());
            Page<Map> list = tbTrnClassRoomMemberDcsRepo.findAca014_1(stuStatusId, stuCode, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }
    public ResponseOutput searchAca014Html(TbStuProfileDto tbStuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbStuProfileDto.getStuCode());
        
         List list = tbTrnClassRoomMemberDcsRepo.findAca014Html(stuCode).stream().map(e -> {
            SearchAca004Sub06FormOutput m = new SearchAca004Sub06FormOutput();
            m.setClassRoomMemberDcsId(e.get("class_room_member_dcs_id"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setYearGroupName(e.get("year_group_name"));
            m.setClassRoomMemberId(e.get("class_room_member_id"));
            m.setClassId(e.get("class_id"));
            m.setClassName(e.get("class_name"));
            m.setClassRoomId(e.get("class_room_id"));
            m.setClassRoom(e.get("class_room"));
            m.setClassRoomNo(e.get("class_room_no"));
            m.setStuCode(e.get("stu_code"));
            m.setFullName(e.get("fullname"));
            m.setFirstnameTh(e.get("firstname_th"));
            m.setLastnameTh(e.get("lastname_th"));
            m.setBehavioursSkillsDetailId(e.get("behaviours_skills_detail_id"));
            m.setItem(e.get("item"));
            m.setItemDesc(e.get("item_desc"));
            m.setItemno(e.get("itemno"));
            m.setDescription(e.get("description"));
            m.setScore1(e.get("score1"));
            m.setScore2(e.get("score2"));
            m.setSumScore(e.get("sum_score"));
            m.setItemResult(e.get("item_result"));
            m.setYearGroupResult(e.get("year_group_result"));
            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;
    }

    public ResponseOutput update(String userName, TbTrnClassRoomMemberDcsDto[] tbTrnClassRoomMemberDcsDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (TbTrnClassRoomMemberDcsDto map : tbTrnClassRoomMemberDcsDto) {
                tbTrnClassRoomMemberDcsRepo.updateScore(
                        (Integer.parseInt(map.getScore1())), (Integer.parseInt(map.getScore2())),
                        RecordStatus.UPDATE.getCode(),
                        userName,
                        Long.parseLong(map.getClassRoomMemberDcsId()));
            }

        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    class SearchAca004Sub06FormOutput {

        private String classRoomMemberDcsId;
        private String yearGroupId;
        private String yearGroupName;
        private String classRoomMemberId;
        private String classId;
        private String className;
        private String classRoomId;
        private String classRoom;
        private String classRoomNo;
        private String stuCode;
        private String fullName;
        private String behavioursSkillsDetailId;
        private String item;
        private String itemDesc;
        private String itemno;
        private String description;
        private String score1;
        private String score2;
        private String sumScore;
        private String itemResult;
        private String yearGroupResult;
        private String firstnameTh;
        private String lastnameTh;

        public String getClassRoomMemberDcsId() {
            return classRoomMemberDcsId;
        }

        public void setClassRoomMemberDcsId(String classRoomMemberDcsId) {
            this.classRoomMemberDcsId = classRoomMemberDcsId;
        }

        public String getYearGroupId() {
            return yearGroupId;
        }

        public void setYearGroupId(String yearGroupId) {
            this.yearGroupId = yearGroupId;
        }

        public String getYearGroupName() {
            return yearGroupName;
        }

        public void setYearGroupName(String yearGroupName) {
            this.yearGroupName = yearGroupName;
        }

        public String getClassRoomMemberId() {
            return classRoomMemberId;
        }

        public void setClassRoomMemberId(String classRoomMemberId) {
            this.classRoomMemberId = classRoomMemberId;
        }

        public String getClassId() {
            return classId;
        }

        public void setClassId(String classId) {
            this.classId = classId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getClassRoomId() {
            return classRoomId;
        }

        public void setClassRoomId(String classRoomId) {
            this.classRoomId = classRoomId;
        }

        public String getClassRoom() {
            return classRoom;
        }

        public void setClassRoom(String classRoom) {
            this.classRoom = classRoom;
        }

        public String getClassRoomNo() {
            return classRoomNo;
        }

        public void setClassRoomNo(String classRoomNo) {
            this.classRoomNo = classRoomNo;
        }

        public String getStuCode() {
            return stuCode;
        }

        public void setStuCode(String stuCode) {
            this.stuCode = stuCode;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getBehavioursSkillsDetailId() {
            return behavioursSkillsDetailId;
        }

        public void setBehavioursSkillsDetailId(String behavioursSkillsDetailId) {
            this.behavioursSkillsDetailId = behavioursSkillsDetailId;
        }

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }

        public String getItemDesc() {
            return itemDesc;
        }

        public void setItemDesc(String itemDesc) {
            this.itemDesc = itemDesc;
        }

        public String getItemno() {
            return itemno;
        }

        public void setItemno(String itemno) {
            this.itemno = itemno;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getScore1() {
            return score1;
        }

        public void setScore1(String score1) {
            this.score1 = score1;
        }

        public String getScore2() {
            return score2;
        }

        public void setScore2(String score2) {
            this.score2 = score2;
        }

        public String getSumScore() {
            return sumScore;
        }

        public void setSumScore(String sumScore) {
            this.sumScore = sumScore;
        }

        public String getItemResult() {
            return itemResult;
        }

        public void setItemResult(String itemResult) {
            this.itemResult = itemResult;
        }

        public String getYearGroupResult() {
            return yearGroupResult;
        }

        public void setYearGroupResult(String yearGroupResult) {
            this.yearGroupResult = yearGroupResult;
        }

        public String getFirstnameTh() {
            return firstnameTh;
        }

        public void setFirstnameTh(String firstnameTh) {
            this.firstnameTh = firstnameTh;
        }

        public String getLastnameTh() {
            return lastnameTh;
        }

        public void setLastnameTh(String lastnameTh) {
            this.lastnameTh = lastnameTh;
        }

    }
}
