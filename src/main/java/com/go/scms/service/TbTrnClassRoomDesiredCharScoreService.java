/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbTrnClassRoomDesiredCharScoreDto;
import com.go.scms.entity.TbTrnClassRoomDesiredCharScoreEntity;
import com.go.scms.repository.TbTrnClassRoomDesiredCharScoreRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class TbTrnClassRoomDesiredCharScoreService {

    @Autowired
    private TbTrnClassRoomDesiredCharScoreRepository tbTrnClassRoomDesiredCharScoreRepository;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput insert(String userName, TbTrnClassRoomDesiredCharScoreDto[] tbTrnClassRoomDesiredCharScoreDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            for (TbTrnClassRoomDesiredCharScoreDto map : tbTrnClassRoomDesiredCharScoreDto) {
                TbTrnClassRoomDesiredCharScoreEntity entity = modelMapper.map(map, TbTrnClassRoomDesiredCharScoreEntity.class);
                if (map.getScore1() == null) {
                    entity.setScore1(0);
                }
                if (map.getScore2() == null) {
                    entity.setScore2(0);
                }
                if (map.getScore3() == null) {
                    entity.setScore3(0);
                }
                if (map.getScore4() == null) {
                    entity.setScore4(0);
                }
                if (map.getScore5() == null) {
                    entity.setScore5(0);
                }
                if (map.getScore6() == null) {
                    entity.setScore6(0);
                }
                if (map.getScore7() == null) {
                    entity.setScore7(0);
                }
                if (map.getScore8() == null) {
                    entity.setScore8(0);
                }
                if (map.getScore9() == null) {
                    entity.setScore9(0);
                }
                entity.setCreatedUser(userName);
                tbTrnClassRoomDesiredCharScoreRepository.save(entity);
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput update(String userName, TbTrnClassRoomDesiredCharScoreDto[] tbTrnClassRoomDesiredCharScoreDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TbTrnClassRoomDesiredCharScoreDto map : tbTrnClassRoomDesiredCharScoreDto) {
                TbTrnClassRoomDesiredCharScoreEntity entity = modelMapper.map(map, TbTrnClassRoomDesiredCharScoreEntity.class);
                tbTrnClassRoomDesiredCharScoreRepository.updateDesiredCharScore(
                        (map.getScore1()),
                        (map.getScore2()),
                        (map.getScore3()),
                        (map.getScore4()),
                        (map.getScore5()),
                        (map.getScore6()),
                        (map.getScore7()),
                        (map.getScore8()),
                        (map.getScore9()),
                        RecordStatus.UPDATE.getCode(),
                        userName,
                        (map.getClassRoomDesiredCharScoreId()));
            }

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
