package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickMstSubjectTypeDto;
import com.go.scms.entity.SnickMstSubjectTypeEntity;
import com.go.scms.repository.SnickMstSubjectTypeRepository;
import com.go.scms.repository.RoleRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class SnickMstSubjectTypeService {
    
        @Autowired
	private SnickMstSubjectTypeRepository mstSubjectTypeRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ResponseOutput search(SnickMstSubjectTypeDto mstSubjectTypeDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
		String subjectTypeCode = ValiableUtils.stringIsEmpty(mstSubjectTypeDto.getSubjectTypeCode());
		String name = ValiableUtils.stringIsEmpty(mstSubjectTypeDto.getName());
                String recordStatus = "D";
		
		Pageable pageable = PageRequest.of(PageableUtils.offset(mstSubjectTypeDto.getP()),PageableUtils.result(mstSubjectTypeDto.getResult()),Sort.by("subjectTypeCode").ascending());
		Page<SnickMstSubjectTypeEntity> list = mstSubjectTypeRepo.findAllBySubjectTypeCodeContainingAndNameContainingAndRecordStatusNotIn(subjectTypeCode, name, recordStatus, pageable);
		
		/*** Set ***/
		output.setResponseData(list.getContent());
		output.setResponseSize(list.getTotalElements());
		    
		return output;
	}
	
	public ResponseOutput findById(Long id) throws Throwable {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(mstSubjectTypeRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND)));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput insert(SnickMstSubjectTypeDto mstSubjectTypeDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
			SnickMstSubjectTypeEntity entity = modelMapper.map(mstSubjectTypeDto,SnickMstSubjectTypeEntity.class);
                        entity.setRecordStatus("A");
			entity.setCreatedUser(mstSubjectTypeDto.getUserName());
			
			mstSubjectTypeRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput update(SnickMstSubjectTypeDto mstSubjectTypeDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			SnickMstSubjectTypeEntity entity = mstSubjectTypeRepo.findById(ValiableUtils.LongIsZero(mstSubjectTypeDto.getSubjectTypeId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(mstSubjectTypeDto,entity);

			/*** Set ***/
                        entity.setRecordStatus("U");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(mstSubjectTypeDto.getUserName());
			
			mstSubjectTypeRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput delete(SnickMstSubjectTypeDto mstSubjectTypeDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			SnickMstSubjectTypeEntity entity = mstSubjectTypeRepo.findById(ValiableUtils.LongIsZero(mstSubjectTypeDto.getSubjectTypeId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Set ***/
                        entity.setRecordStatus("D");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(mstSubjectTypeDto.getUpdatedUser());
			
			mstSubjectTypeRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
}
