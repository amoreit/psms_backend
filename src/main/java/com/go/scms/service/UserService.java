package com.go.scms.service;

import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseStatus;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.go.scms.entity.UserEntity;
import com.go.scms.repository.RoleRepository;
import com.go.scms.repository.UserRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Service
public class UserService implements UserDetailsService{
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = userRepo.findByUsrName(username).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"ชื่อผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง"));
		if(user.getLoginCount()>=5){
			throw new ResponseException(ResponseStatus.LOCK.getCode(),"User Lock");
		}
                if(user.getActivated().equals(false)){
                        throw new ResponseException(ResponseStatus.LOCK.getCode(),"กรุณารอยืนยันการลงทะเบียน เข้าใช้งาน");
                }
		JsonArray arr = userRepo.getMenu(user.getRole().getRoleId()).stream().map(e->{
			JsonObject json = new JsonObject();
			json.addProperty("id",e.get("page_id"));
			json.addProperty("title",e.get("page_name"));
			json.addProperty("titleEn",e.get("page_name_en"));
			json.addProperty("routerLink",e.get("page_url") != null ?"/"+e.get("page_url"):null);
			json.addProperty("icon",e.get("page_icon"));
                        json.addProperty("hasSubMenu",(e.get("page_url") == null)?true:false);
			json.addProperty("parentId",e.get("parent_id"));
			return json;
		}).reduce(new JsonArray(), (jsonArray, jsonObject) -> {
	        jsonArray.add(jsonObject);
	        return jsonArray;
	    }, (jsonArray, otherJsonArray) -> {
	        jsonArray.addAll(otherJsonArray);
	        return jsonArray;
	    });
		return new UserEntity(user.getUsrName(),user.getEmail(),user.getTitle(),user.getPassword(),user.getFirstName(),user.getLastName(),user.getCitizenId(),user.getRole(),arr);
	}
	
}
