package com.go.scms.service;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.RoleDto;
import com.go.scms.entity.RoleEntity;
import com.go.scms.repository.RoleRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;

@Service
public class RoleService{

	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ResponseOutput search(RoleDto roleDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
		String roleCode = ValiableUtils.stringIsEmpty(roleDto.getRoleCode());
		String roleName = ValiableUtils.stringIsEmpty(roleDto.getRoleName());
                String roleStatus = "D";
		
		Pageable pageable = PageRequest.of(PageableUtils.offset(roleDto.getP()),PageableUtils.result(roleDto.getResult()),Sort.by("roleCode").ascending());
		Page<RoleEntity> list = roleRepo.findAllByRoleCodeContainingAndRoleNameContainingAndRoleStatusNotIn(roleCode, roleName,roleStatus, pageable);
		
		/*** Set ***/
		output.setResponseData(list.getContent());
		output.setResponseSize(list.getTotalElements());
		
		return output;
	}
	
	public ResponseOutput findById(Long id) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(roleRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput insert(RoleDto roleDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
			RoleEntity entity = modelMapper.map(roleDto,RoleEntity.class);
			entity.setRoleStatus(RecordStatus.SAVE.getCode());
			entity.setRoleCreatedBy(roleDto.getUserName());
			
			roleRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput update(RoleDto roleDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			RoleEntity entity = roleRepo.findById(ValiableUtils.LongIsZero(roleDto.getRoleId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(roleDto,entity);

			/*** Set ***/
			entity.setRoleStatus(RecordStatus.UPDATE.getCode());
			entity.setRoleUpdatedDate(new Date());
			entity.setRoleUpdatedBy(roleDto.getUserName());
			
			roleRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput delete(RoleDto roleDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			RoleEntity entity = roleRepo.findById(ValiableUtils.LongIsZero(roleDto.getRoleId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Set ***/
			entity.setRoleStatus(RecordStatus.DELETE.getCode());
			entity.setRoleUpdatedDate(new Date());
			entity.setRoleUpdatedBy(roleDto.getUserName());
			
			roleRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
