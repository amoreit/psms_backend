package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTbStuProfileDocDto;
import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.entity.ClassEntity;
import com.go.scms.entity.MaxClassRoomEntity;
import com.go.scms.entity.SnickTbStuProfileDocEntity;
import com.go.scms.entity.SnickTbStuProfileEntity;
import com.go.scms.entity.TbMstGenderEntity;
import com.go.scms.entity.TbMstStuStatusEntity;
import com.go.scms.entity.TbStuProfileEntity;
import com.go.scms.repository.ClassRepository;
import com.go.scms.repository.MaxClassRoomRepository;
import com.go.scms.repository.SnickTbStuProfileDocRepository;
import com.go.scms.repository.SnickTbStuProfileRepository;
import com.go.scms.repository.TbMstGenderRepository;
import com.go.scms.repository.TbMstStuStatusRepository;
import com.go.scms.repository.TbStuProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TbStuProfileService {

    @Autowired
    private TbStuProfileRepository tbStuProfileRepo;

    @Autowired
    private TbMstGenderRepository tbMstGenderRepo;

    @Autowired
    private TbMstStuStatusRepository tbMstStuStatusRepo;

    @Autowired
    private ClassRepository classRepo;

    @Autowired
    private MaxClassRoomRepository maxclassRoomRepo;

    @Autowired
    private SnickTbStuProfileDocRepository tbStuProfileDocRepo;

    @Autowired
    private SnickTbStuProfileRepository snicktbStuProfileRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TbStuProfileDto tbStuProfileDto) throws ParseException {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String firstnameTh = ValiableUtils.stringIsEmpty(tbStuProfileDto.getFirstnameTh());
        String lastnameTh = ValiableUtils.stringIsEmpty(tbStuProfileDto.getLastnameTh());
        String citizenId = ValiableUtils.stringIsEmpty(tbStuProfileDto.getCitizenId());
        String toClass = ValiableUtils.stringIsEmpty(tbStuProfileDto.getToClass());
        String toYearGroup = ValiableUtils.stringIsEmpty(tbStuProfileDto.getToYearGroup());
        String stuCode = ValiableUtils.stringIsEmpty(tbStuProfileDto.getStuCode());
        Integer stuStatusId = (int) ValiableUtils.IntIsZero(tbStuProfileDto.getStuStatusId());

        if (stuStatusId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuProfileDto.getP()), PageableUtils.result(tbStuProfileDto.getResult()));
            Page<Map> list = tbStuProfileRepo.findstu003(firstnameTh, lastnameTh, toClass, toYearGroup, citizenId, stuCode, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (!stuStatusId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuProfileDto.getP()), PageableUtils.result(tbStuProfileDto.getResult()));
            Page<Map> list = tbStuProfileRepo.findstu003bystustatusid(firstnameTh, lastnameTh, toClass, toYearGroup, citizenId, stuCode,stuStatusId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }

    public ResponseOutput searchCode(TbStuProfileDto tbStuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String firstnameTh = ValiableUtils.stringIsEmpty(tbStuProfileDto.getFirstnameTh());
        String lastnameTh = ValiableUtils.stringIsEmpty(tbStuProfileDto.getLastnameTh());
        String toClass = ValiableUtils.stringIsEmpty(tbStuProfileDto.getToClass());
        String toYearGroup = ValiableUtils.stringIsEmpty(tbStuProfileDto.getToYearGroup());

        Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuProfileDto.getP()), PageableUtils.result(tbStuProfileDto.getResult()));
        Page<Map> list = tbStuProfileRepo.findstu004(firstnameTh, lastnameTh, toClass, toYearGroup, pageable);

        output.setResponseData(list.getContent().stream().map(e -> {
            Map m = new HashMap<>();
            m.put("stuCode2", tbStuProfileRepo.find());
            m.put("stuProfileId", e.get("stu_profile_id"));
            m.put("title", e.get("title"));
            m.put("firstnameTh", e.get("firstname_th"));
            m.put("lastnameTh", e.get("lastname_th"));
            m.put("citizenId", e.get("citizen_id"));
            m.put("toClass", e.get("to_class"));
            return m;
        }).collect(Collectors.toList()));
        output.setResponseSize(list.getTotalElements());
        return output;
    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(tbStuProfileRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND)));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(TbStuProfileDto tbStuProfileDto) throws Throwable {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check cat_class by id **
             */
            TbMstGenderEntity genderId = tbMstGenderRepo.findByGenderId(tbStuProfileDto.getGenderId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            TbMstStuStatusEntity stuStatusId = tbMstStuStatusRepo.findByStuStatusId(tbStuProfileDto.getStuStatusId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            /**
             * * Merge data **
             */
            TbStuProfileEntity entity = modelMapper.map(tbStuProfileDto, TbStuProfileEntity.class);
            entity.setGenderJoin(genderId);
            entity.setStuStatusJoin(stuStatusId);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(tbStuProfileDto.getUserName());

            tbStuProfileRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(String userName, TbStuProfileDto tbStuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
 
            // update stu_profile
            ClassEntity classJoin = classRepo.findByClassId(tbStuProfileDto.getClassId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            MaxClassRoomEntity classRoomJoin = maxclassRoomRepo.findByClassRoomId(tbStuProfileDto.getClassRoomId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            TbMstStuStatusEntity stuStatusId = tbMstStuStatusRepo.findByStuStatusId(tbStuProfileDto.getStuStatusId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            TbMstGenderEntity genderId = tbMstGenderRepo.findByGenderId(tbStuProfileDto.getGenderId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            TbStuProfileEntity entity = tbStuProfileRepo.findById(ValiableUtils.LongIsZero(tbStuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
      
            modelMapper.map(tbStuProfileDto, entity);
       
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setGenderJoin(genderId);
            entity.setStuStatusJoin(stuStatusId);
            entity.setClassJoin(classJoin);
            entity.setClassRoomJoin(classRoomJoin);
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(userName);

            tbStuProfileRepo.save(entity);           
           
            // insert stu_profile_doc
            SnickTbStuProfileDocEntity entityDoc = modelMapper.map(tbStuProfileDto, SnickTbStuProfileDocEntity.class);
            SnickTbStuProfileEntity stuProfileId = snicktbStuProfileRepo.findByStuProfileId(Long.parseLong(tbStuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            entityDoc.setStuProfile(stuProfileId);
            entityDoc.setRecordStatus(RecordStatus.SAVE.getCode());
            entityDoc.setCreatedUser(tbStuProfileDto.getUserName());

            tbStuProfileDocRepo.save(entityDoc);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updateCode(String userName, TbStuProfileDto tbStuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            TbStuProfileEntity entity = tbStuProfileRepo.findById(ValiableUtils.LongIsZero(tbStuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(tbStuProfileDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(userName);

            tbStuProfileRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updateUploadFile(String userName, TbStuProfileDto tbStuProfileDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            
            // update stu_profile
            ClassEntity classJoin = classRepo.findByClassId(tbStuProfileDto.getClassId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            MaxClassRoomEntity classRoomJoin = maxclassRoomRepo.findByClassRoomId(tbStuProfileDto.getClassRoomId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            TbMstStuStatusEntity stuStatusId = tbMstStuStatusRepo.findByStuStatusId(tbStuProfileDto.getStuStatusId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            TbMstGenderEntity genderId = tbMstGenderRepo.findByGenderId(tbStuProfileDto.getGenderId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            
            TbStuProfileEntity entityStu = tbStuProfileRepo.findById(ValiableUtils.LongIsZero(tbStuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(tbStuProfileDto, entityStu);

            entityStu.setGenderJoin(genderId);
            entityStu.setStuStatusJoin(stuStatusId);
            entityStu.setClassJoin(classJoin);
            entityStu.setClassRoomJoin(classRoomJoin);
            entityStu.setRecordStatus(RecordStatus.UPDATE.getCode());
            entityStu.setUpdatedUser(tbStuProfileDto.getUserName());

            tbStuProfileRepo.save(entityStu);
            
            // update stu_profile_doc 
            SnickTbStuProfileDocEntity entity = tbStuProfileDocRepo.findById(ValiableUtils.LongIsZero(tbStuProfileDto.getStuProfileDocId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            SnickTbStuProfileEntity stuProfileId = snicktbStuProfileRepo.findByStuProfileId(Long.parseLong(tbStuProfileDto.getStuProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
       
            modelMapper.map(tbStuProfileDto, entity);

            entity.setStuProfile(stuProfileId);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(userName);

            tbStuProfileDocRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
