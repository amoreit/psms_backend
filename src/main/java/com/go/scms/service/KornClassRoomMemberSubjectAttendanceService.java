/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.KornClassRoomMemberSubjectAttendanceDto;
import com.go.scms.entity.KornClassRoomMemberSubjectAttendanceEntity;
import com.go.scms.repository.KornClassRoomMemberSubjectAttendanceRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author kronn
 */
@Service
public class KornClassRoomMemberSubjectAttendanceService {
    
    @Autowired
    private KornClassRoomMemberSubjectAttendanceRepository kornClassRoomMemberSubjectAttendanceRepo;

    @Autowired
    private ModelMapper modelMapper;
                       
    public ResponseOutput searchall(KornClassRoomMemberSubjectAttendanceDto kornClassRoomMemberDto) throws ParseException {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassRoomId());
        Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassRoomSubjectId());
        String searchDate = ValiableUtils.stringIsEmpty(kornClassRoomMemberDto.getSearchDate());
 
        if(!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && !classRoomSubjectId.equals(0) && searchDate.equals("")){
            List list = kornClassRoomMemberSubjectAttendanceRepo.searchallDontDate(yearGroupId, classId, classRoomId,classRoomSubjectId).stream().map(e->{
                            ClassRoomSubjectAttendance m = new ClassRoomSubjectAttendance();
                                m.setClassRoomMemberSubjectAttendanceId(e.get("class_room_member_subject_attendance_id"));
                                m.setYearGroupId(e.get("year_group_id"));
                                m.setYearGroupName(e.get("year_group_name"));
                                m.setScCode(e.get("sc_code"));
                                m.setClassRoomId(e.get("class_room_id"));
                                m.setClassId(e.get("class_id"));
                                m.setClassRoomMemberId(e.get("class_room_member_id"));
                                m.setClassRoomSubjectId(e.get("class_room_subject_id"));
                                m.setClassRoomSnameTh(e.get("class_room"));
                                m.setStuCode(e.get("stu_code"));
                                m.setFullname(e.get("fullname"));
                                m.setClassRoomNo(e.get("class_room_no"));
                                m.setSubjectCode(e.get("subject_code"));
                                m.setSubjectName(e.get("subject_name_th"));
                                m.setReason(e.get("reason"));
                                m.setDateTaken(e.get("date_taken"));
                                m.setRemark(e.get("remark"));
                            return m;

                    }).collect(Collectors.toList());

                    /*** Set ***/
                    output.setResponseData(list);
                    output.setResponseSize(Integer.toUnsignedLong(list.size()));
        }
        
        if(!yearGroupId.equals(0) && !classId.equals(0) && !classRoomId.equals(0) && !classRoomSubjectId.equals(0) && !searchDate.equals("")){
            List list = kornClassRoomMemberSubjectAttendanceRepo.searchallDate(yearGroupId, classId, classRoomId,classRoomSubjectId, searchDate).stream().map(e->{

                            ClassRoomSubjectAttendance m = new ClassRoomSubjectAttendance();
                                m.setClassRoomMemberSubjectAttendanceId(e.get("class_room_member_subject_attendance_id"));
                                m.setYearGroupId(e.get("year_group_id"));
                                m.setYearGroupName(e.get("year_group_name"));
                                m.setScCode(e.get("sc_code"));
                                m.setClassRoomId(e.get("class_room_id"));
                                m.setClassId(e.get("class_id"));
                                m.setClassRoomMemberId(e.get("class_room_member_id"));
                                m.setClassRoomSubjectId(e.get("class_room_subject_id"));
                                m.setClassRoomSnameTh(e.get("class_room"));
                                m.setStuCode(e.get("stu_code"));
                                m.setFullname(e.get("fullname"));
                                m.setClassRoomNo(e.get("class_room_no"));
                                m.setSubjectCode(e.get("subject_code"));
                                m.setSubjectName(e.get("subject_name_th"));
                                m.setReason(e.get("reason"));
                                m.setDateTaken(e.get("date_taken"));
                                m.setRemark(e.get("remark"));
                            return m;

                    }).collect(Collectors.toList());

                    /*** Set ***/ 
                    output.setResponseData(list);
                    output.setResponseSize(Integer.toUnsignedLong(list.size()));
        }
        return output;
    }      
    
     public ResponseOutput manageSubjectAttendance(String userName, KornClassRoomMemberSubjectAttendanceDto[] kornClassRoomMemberSubjectAttendanceDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                      // insert
                    /*** loop and mapping ***/
                    for (KornClassRoomMemberSubjectAttendanceDto map : kornClassRoomMemberSubjectAttendanceDto) {                    
                        KornClassRoomMemberSubjectAttendanceEntity entity = modelMapper.map(map, KornClassRoomMemberSubjectAttendanceEntity.class);
                        if(map.getClassRoomMemberSubjectAttendanceId() == null){
                            entity.setScCode("PRAMANDA");
                            entity.setIsactive(true);
                            entity.setDateTaken(new Date());
                            entity.setRecordStatus(RecordStatus.SAVE.getCode());
                            entity.setCreatedUser(userName);
                            kornClassRoomMemberSubjectAttendanceRepo.save(entity);
                        }else{
                            kornClassRoomMemberSubjectAttendanceRepo.manageSubjectAttendance(
                                map.getYearGroupId(),
                                map.getYearGroupName(),
                                map.getClassRoomId(),
                                map.getClassId(),
                                map.getClassRoomMemberId(),
                                map.getClassRoomSubjectId(),
                                map.getClassRoomSnameTh(),
                                map.getStuCode(),
                                map.getFullname(),
                                map.getClassRoomNo(),
                                map.getSubjectCode(),
                                map.getSubjectName(),
                                map.getReason(),
                                map.getRemark(),      
                                userName,
                                Long.parseLong(map.getClassRoomMemberSubjectAttendanceId()));
                        }
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
}

class ClassRoomSubjectAttendance {
    private String classRoomMemberSubjectAttendanceId;
    private String yearGroupId;
    private String yearGroupName;
    private String scCode;
    private String classRoomId;
    private String classId;
    private String classRoomMemberId;
    private String classRoomSubjectId;
    private String classRoomSnameTh;
    private String stuCode;
    private String fullname;
    private String classRoomNo;
    private String subjectCode;
    private String subjectName;
    private String reason;
    private String dateTaken;
    private String remark;

    public String getClassRoomMemberSubjectAttendanceId() {
        return classRoomMemberSubjectAttendanceId;
    }

    public void setClassRoomMemberSubjectAttendanceId(String classRoomMemberSubjectAttendanceId) {
        this.classRoomMemberSubjectAttendanceId = classRoomMemberSubjectAttendanceId;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassRoomMemberId() {
        return classRoomMemberId;
    }

    public void setClassRoomMemberId(String classRoomMemberId) {
        this.classRoomMemberId = classRoomMemberId;
    }

    public String getClassRoomSubjectId() {
        return classRoomSubjectId;
    }

    public void setClassRoomSubjectId(String classRoomSubjectId) {
        this.classRoomSubjectId = classRoomSubjectId;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
