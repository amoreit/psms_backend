package com.go.scms.service;

import com.go.scms.dto.SnickClassRoomSubjectDto;
import com.go.scms.entity.SnickClassRoomSubjectEntity;
import com.go.scms.repository.SnickClassRoomSubjectRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SnickClassRoomSubjectService {
    
        @Autowired
        private SnickClassRoomSubjectRepository classRoomSubjectRepo;

        @Autowired
        private ModelMapper modelMapper; 
        
        public ResponseOutput search(SnickClassRoomSubjectDto classRoomSubjectDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                Integer classId = (int) ValiableUtils.IntIsZero(classRoomSubjectDto.getClassId());
                Integer classRoomId = (int) ValiableUtils.IntIsZero(classRoomSubjectDto.getClassRoomId());
                Integer yearGroupId = (int) ValiableUtils.IntIsZero(classRoomSubjectDto.getYearGroupId());
                String recordStatus = "D";
                long catClassId = 1;
                   
                if(classId.equals(0) && classRoomId.equals(0) && yearGroupId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomSubjectDto.getP()),PageableUtils.result(classRoomSubjectDto.getResult()),Sort.by("classRoomJoin.classRoomId", "departmentJoin.departmentId").ascending());
                    Page<SnickClassRoomSubjectEntity> list = classRoomSubjectRepo.findAllByRecordStatusNotInAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(recordStatus, catClassId, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && !classRoomId.equals(0) && !yearGroupId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomSubjectDto.getP()),PageableUtils.result(classRoomSubjectDto.getResult()),Sort.by("classRoomJoin.classRoomId", "departmentJoin.departmentId").ascending());
                    Page<SnickClassRoomSubjectEntity> list = classRoomSubjectRepo.findAllByRecordStatusNotInAndClassRoomJoinClassJoinClassIdAndClassRoomJoinClassRoomIdAndYearGroupJoinYearGroupIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(recordStatus, classId, classRoomId, yearGroupId, catClassId, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(classId.equals(0) && classRoomId.equals(0) && !yearGroupId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomSubjectDto.getP()),PageableUtils.result(classRoomSubjectDto.getResult()),Sort.by("classRoomJoin.classRoomId", "departmentJoin.departmentId").ascending());
                    Page<SnickClassRoomSubjectEntity> list = classRoomSubjectRepo.findAllByRecordStatusNotInAndYearGroupJoinYearGroupIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(recordStatus, yearGroupId, catClassId, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && classRoomId.equals(0) && yearGroupId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomSubjectDto.getP()),PageableUtils.result(classRoomSubjectDto.getResult()),Sort.by("classRoomJoin.classRoomId", "departmentJoin.departmentId").ascending());
                    Page<SnickClassRoomSubjectEntity> list = classRoomSubjectRepo.findAllByRecordStatusNotInAndClassRoomJoinClassJoinClassIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(recordStatus, classId, catClassId, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!classId.equals(0) && !classRoomId.equals(0) && yearGroupId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomSubjectDto.getP()),PageableUtils.result(classRoomSubjectDto.getResult()),Sort.by("classRoomJoin.classRoomId", "departmentJoin.departmentId").ascending());
                    Page<SnickClassRoomSubjectEntity> list = classRoomSubjectRepo.findAllByRecordStatusNotInAndClassRoomJoinClassJoinClassIdAndClassRoomJoinClassRoomIdAndClassRoomJoinClassJoinCatClassJoinCatClassIdNotIn(recordStatus, classId, classRoomId, catClassId, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                return output;
        }
}
