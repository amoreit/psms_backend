package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickClassRoomRatwScoreDto;
import com.go.scms.entity.SnickClassRoomMemberEntity;
import com.go.scms.entity.SnickClassRoomRatwScoreEntity;
import com.go.scms.repository.SnickClassRoomMemberRepository;
import com.go.scms.repository.SnickClassRoomRatwScoreRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import static java.lang.Integer.parseInt;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SnickClassRoomRatwScoreService {
    
        @Autowired
        private SnickClassRoomRatwScoreRepository classRoomRatwScoreRepo;

        @Autowired
        private ModelMapper modelMapper; 
        
        public ResponseOutput allmange(String userName, SnickClassRoomRatwScoreDto[] classRoomRatwScoreDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** Loop update data ***/
                    for (SnickClassRoomRatwScoreDto map : classRoomRatwScoreDto) {
                        SnickClassRoomRatwScoreEntity entity = modelMapper.map(map, SnickClassRoomRatwScoreEntity.class);
                        if(map.getClassRoomRatwScoreId() == null){
                            if(map.getScore1() == null || map.getScore1() == ""){
                                entity.setScore1(0);
                            }
                            
                            if(map.getScore2() == null || map.getScore2() == ""){
                                entity.setScore2(0);
                            }
                            
                            if(map.getScore3() == null || map.getScore3() == ""){
                                entity.setScore3(0);
                            }
                            
                            if(map.getScore4() == null || map.getScore4() == ""){
                                entity.setScore4(0);
                            }
                            
                            if(map.getScore5() == null || map.getScore5() == ""){
                                entity.setScore5(0);
                            }
                            
                            entity.setStuCode(map.getStuCode());
                            entity.setCreatedUser(userName);
                            entity.setIsactive(true);
                            entity.setRecordStatus("A");
                            
                            classRoomRatwScoreRepo.save(entity);
                        }else{
                            classRoomRatwScoreRepo.updateRatwscore(
                                (Integer.parseInt(map.getScore1())),
                                (Integer.parseInt(map.getScore2())),
                                (Integer.parseInt(map.getScore3())),
                                (Integer.parseInt(map.getScore4())),
                                (Integer.parseInt(map.getScore5())),
                                 userName,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getClassRoomRatwScoreId()))
                            );
                        }
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
}
