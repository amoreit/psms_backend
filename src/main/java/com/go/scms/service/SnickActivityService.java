package com.go.scms.service;

import com.go.scms.dto.SnickActivityDto;
import com.go.scms.repository.SnickActivityRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickActivityService {
    
    @Autowired
    private SnickActivityRepository activityRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(SnickActivityDto activityDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            /*** Search ***/
            Integer activityGroupId = (int) ValiableUtils.IntIsZero(activityDto.getActivityGroupId());
            Integer classId = (int) ValiableUtils.IntIsZero(activityDto.getClassId());
            String recordStatus = "D";

            List list = activityRepo.findall(activityGroupId, classId, recordStatus).stream().map(e->{
                    SnickActivityList m = new SnickActivityList();
                        m.setActivityId(e.get("activity_id"));
                        m.setScCode(e.get("sc_code"));
                        m.setActivityGroupId(e.get("activity_group_id"));
                        m.setClassId(e.get("class_id"));
                        m.setActivityId(e.get("activity_id"));
                        m.setActivityName(e.get("activity_name"));
                        m.setLesson(e.get("lesson"));
                    return m;
                        
            }).collect(Collectors.toList());

            /*** Set ***/
            output.setResponseData(list);
            output.setResponseSize(Integer.toUnsignedLong(list.size()));

        return output;
    }
    
    public ResponseOutput searchActivityStuReport(SnickActivityDto activityDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            /*** Search ***/
            Integer activityGroupId = (int) ValiableUtils.IntIsZero(activityDto.getActivityGroupId());
            Integer classId = 1;
            String recordStatus = "D";

            List list = activityRepo.findallreport(activityGroupId, classId, recordStatus).stream().map(e->{
                    SnickActivityList m = new SnickActivityList();
                        m.setActivityId(e.get("activity_id"));
                        m.setScCode(e.get("sc_code"));
                        m.setActivityGroupId(e.get("activity_group_id"));
                        m.setClassId(e.get("class_id"));
                        m.setActivityId(e.get("activity_id"));
                        m.setActivityName(e.get("activity_name"));
                        m.setLesson(e.get("lesson"));
                    return m;
                        
            }).collect(Collectors.toList());

            /*** Set ***/
            output.setResponseData(list);
            output.setResponseSize(Integer.toUnsignedLong(list.size()));

        return output;
    }
}

class SnickActivityList{
    
    private String activityId;
    private String scCode;
    private String activityGroupId;
    private String catClass;
    private String classId;
    private String activityCode;
    private String activityName;
    private String activityDesc;
    private String lesson;
    
    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getActivityGroupId() {
        return activityGroupId;
    }

    public void setActivityGroupId(String activityGroupId) {
        this.activityGroupId = activityGroupId;
    }

    public String getCatClass() {
        return catClass;
    }

    public void setCatClass(String catClass) {
        this.catClass = catClass;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }
    
}
