package com.go.scms.service;

import com.go.scms.dto.SnickDepartmentDto;
import com.go.scms.repository.SnickDepartmentRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickDepartmentService {
    
        @Autowired
        private SnickDepartmentRepository departmentRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput search(SnickDepartmentDto departmentDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                Integer catClass = (int) ValiableUtils.IntIsZero(departmentDto.getCatClass());
                String recordStatus = "D";
                
                List list = departmentRepo.findAllByCatClassAndRecordStatusNotInOrderByDepartmentIdAsc(catClass, recordStatus);
                output.setResponseData(list);
                
                return output;
        }
    
}
