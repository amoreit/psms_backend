/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.KornClassRoomMemberDto;
import com.go.scms.entity.KornClassRoomMemberEntity;
import com.go.scms.repository.KornClassRoomMemberRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author kronn
 */
@Service
public class KornClassRoomMemberService {
    
    @Autowired
    private KornClassRoomMemberRepository kornClassRoomMemberRepo;

    @Autowired
    private ModelMapper modelMapper;
    
    public ResponseOutput searchSub(KornClassRoomMemberDto kornClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassRoomId());
        Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassRoomSubjectId());
                
        Pageable pageable = PageRequest.of(PageableUtils.offset(kornClassRoomMemberDto.getP()), PageableUtils.result(kornClassRoomMemberDto.getResult()));
        Page<Map> list = kornClassRoomMemberRepo.sub1(yearGroupId, classId, classRoomId,classRoomSubjectId, pageable);

        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());    
        return output;
        
    }
        
    public ResponseOutput searchtran(KornClassRoomMemberDto kornClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
    
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(kornClassRoomMemberDto.getClassRoomId());
        
        Pageable pageable = PageRequest.of(PageableUtils.offset(kornClassRoomMemberDto.getP()), PageableUtils.result(kornClassRoomMemberDto.getResult()));
        Page<Map> list = kornClassRoomMemberRepo.findtran(yearGroupId, classId, classRoomId, pageable);

        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());
        return output;
    }
    
}
