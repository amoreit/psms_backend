package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.MstCatClassDto;
import com.go.scms.dto.PageGroupDto;
import com.go.scms.dto.RoleDto;
import com.go.scms.dto.YearGroupDto;
import com.go.scms.entity.MstCatClassMainEntity;
import com.go.scms.entity.PageGroupEntity;
import com.go.scms.entity.RoleEntity;
import com.go.scms.entity.YearEntity;
import com.go.scms.entity.YearGroupEntity;
import com.go.scms.entity.YearTermEntity;
import com.go.scms.repository.MstCatClassMainRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class MstCatClassService {

    @Autowired
    private MstCatClassMainRepository mstCatClassMainRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(MstCatClassDto mstCatClassDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        /**
         * * Search **
         */
        String lnameTh = ValiableUtils.stringIsEmpty(mstCatClassDto.getLnameTh());
        String recordStatus = "D";

        Pageable pageable = PageRequest.of(PageableUtils.offset(mstCatClassDto.getP()), PageableUtils.result(mstCatClassDto.getResult()), Sort.by("catClassId").ascending());
        Page<MstCatClassMainEntity> list = mstCatClassMainRepo.findAllByLnameThContainingAndRecordStatusNotIn(lnameTh, recordStatus, pageable);

        /**
         * * Set **
         */
        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());
        return output;
    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(mstCatClassMainRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(MstCatClassDto mstCatClassDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            MstCatClassMainEntity entity = modelMapper.map(mstCatClassDto, MstCatClassMainEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(mstCatClassDto.getCreatedUser());

            mstCatClassMainRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(MstCatClassDto mstCatClassDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            MstCatClassMainEntity entity = mstCatClassMainRepo.findById(ValiableUtils.LongIsZero(mstCatClassDto.getCatClassId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Merge data **
             */
            modelMapper.map(mstCatClassDto, entity);

            /**
             * * Set **
             */
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(mstCatClassDto.getCreatedUser());

            mstCatClassMainRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(MstCatClassDto mstCatClassDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            MstCatClassMainEntity entity = mstCatClassMainRepo.findById(ValiableUtils.LongIsZero(mstCatClassDto.getCatClassId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Set **
             */
            entity.setRecordStatus(RecordStatus.DELETE.getCode());
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(mstCatClassDto.getCreatedUser());

            mstCatClassMainRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

}
