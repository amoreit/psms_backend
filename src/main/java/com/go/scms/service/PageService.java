package com.go.scms.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.PageDto;
import com.go.scms.entity.PageEntity;
import com.go.scms.entity.PageGroupEntity;
import com.go.scms.repository.PageGroupRepository;
import com.go.scms.repository.PageRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;

@Service
public class PageService{

	@Autowired
	private PageRepository pageRepo;

	@Autowired
	private PageGroupRepository pageGroupRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ResponseOutput search(PageDto pageDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
		String pageCode = ValiableUtils.stringIsEmpty(pageDto.getPageCode());
		String pageGroupName = ValiableUtils.stringIsEmpty(pageDto.getPageGroupName());
		String pageGroupCode = ValiableUtils.stringIsEmpty(pageDto.getPageGroupCode());

		Pageable pageable = PageRequest.of(PageableUtils.offset(pageDto.getP()),PageableUtils.result(pageDto.getResult()),Sort.by("pageCode").ascending());
		Page<PageEntity> list = pageRepo.findAllByPageGroupPageGroupNameContainingAndPageGroupPageGroupCodeContaining(pageGroupName,pageGroupCode, pageable);
		
		/*** Set ***/
		output.setResponseData(list.getContent().stream().map(e->{
			Map m = new HashMap<>();
			m.put("pageId",e.getPageId());
			m.put("pageCode",e.getPageCode());
			m.put("pageName",e.getPageName());
			m.put("pageStatus",e.getPageStatus());
			m.put("pageUrl",e.getPageUrl());
			m.put("pageIcon",e.getPageIcon());
			m.put("pageCreatedBy",e.getPageCreatedBy());
			m.put("pageCreatedDate",e.getPageCreatedDate());
			m.put("pageUpdatedBy",e.getPageUpdatedBy());
			m.put("pageUpdatedDate",e.getPageUpdatedDate());
			m.put("pageGroupCode",e.getPageGroup().getPageGroupCode());
			m.put("pageGroupName",e.getPageGroup().getPageGroupName());
			
			return m;
		}).collect(Collectors.toList())); 
		output.setResponseSize(list.getTotalElements());
		return output;
	}
	
	public ResponseOutput findById(Long id) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(pageRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput insert(PageDto pageDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check page group by code ***/
			PageGroupEntity pageGroup = pageGroupRepo.findByPageGroupCode(pageDto.getPageGroupCode()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			PageEntity entity = modelMapper.map(pageDto,PageEntity.class);
			entity.setPageGroup(pageGroup);
			entity.setPageStatus(RecordStatus.SAVE.getCode());
			entity.setPageCreatedBy(pageDto.getUserName());
			
			pageRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput update(PageDto pageDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check page group by code ***/
			PageGroupEntity pageGroup = pageGroupRepo.findByPageGroupCode(pageDto.getPageGroupCode()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Check page ***/
			PageEntity entity = pageRepo.findById(ValiableUtils.LongIsZero(pageDto.getPageId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(pageDto,entity);

			/*** Set ***/
			entity.setPageGroup(pageGroup);
			entity.setPageStatus(RecordStatus.UPDATE.getCode());
			entity.setPageUpdatedDate(new Date());
			entity.setPageUpdatedBy(pageDto.getUserName());
			
			pageRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput delete(PageDto pageDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check page by id ***/
			PageEntity entity = pageRepo.findById(ValiableUtils.LongIsZero(pageDto.getPageId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Set ***/
			entity.setPageStatus(RecordStatus.DELETE.getCode());
			entity.setPageUpdatedDate(new Date());
			entity.setPageUpdatedBy(pageDto.getUserName());
			
			pageRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}

