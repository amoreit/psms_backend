/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ClassRoomDto;
import com.go.scms.entity.ClassEntity;
import com.go.scms.entity.ClassRoomEntity;
import com.go.scms.repository.ClassRepository;
import com.go.scms.repository.ClassRoomRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ClassRoomService {

        @Autowired
        private ClassRoomRepository classRoomRepo;

        @Autowired
        private ClassRepository classRepo;

        @Autowired
        private ModelMapper modelMapper;

        public ResponseOutput search(ClassRoomDto classRoomDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            String nameClass = ValiableUtils.stringIsEmpty(classRoomDto.getNameClass());

            Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomDto.getP()), PageableUtils.result(classRoomDto.getResult()));
            Page<Map> list = classRoomRepo.find(nameClass, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
            return output;

        }

        public ResponseOutput searchClassroomReligion(ClassRoomDto classRoomDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            Integer classId = (int) ValiableUtils.IntIsZero(classRoomDto.getClassId());

            if (classId.equals(0)) {
                Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomDto.getP()), PageableUtils.result(classRoomDto.getResult()));
                Page<Map> list = classRoomRepo.findClassRoom2(pageable);
                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());
            }
            if (!classId.equals(0)) {
                Pageable pageable = PageRequest.of(PageableUtils.offset(classRoomDto.getP()), PageableUtils.result(classRoomDto.getResult()));
                Page<Map> list = classRoomRepo.findClassRoom1(classId, pageable);
                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());
            }

            return output;
        }

        public ResponseOutput findById(Long id) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                output.setResponseData(classRoomRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
            } catch (ResponseException ex) {
                output.setResponseCode(ex.getCode());
                output.setResponseMsg(ex.getMessage());
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
        }

        public ResponseOutput insert(ClassRoomDto classRoomDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                ClassEntity classJoin = classRepo.findByClassId(classRoomDto.getNameClassId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

                ClassRoomEntity entity = modelMapper.map(classRoomDto, ClassRoomEntity.class);
                entity.setRecordStatus(RecordStatus.SAVE.getCode());
                entity.setClassJoin(classJoin);
                entity.setCreatedUser(classRoomDto.getUserName());

                classRoomRepo.save(entity);
            } catch (ConstraintViolationException e) {
                return ExceptionUtils.validate(e);
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
        }

        public ResponseOutput update(ClassRoomDto classRoomDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                ClassEntity classJoin = classRepo.findByClassId(classRoomDto.getNameClassId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
                ClassRoomEntity entity = classRoomRepo.findById(ValiableUtils.LongIsZero(classRoomDto.getClassRoomId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

                modelMapper.map(classRoomDto, entity);
                entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                entity.setClassJoin(classJoin);
                entity.setUpdatedUser(classRoomDto.getUserName());

                classRoomRepo.save(entity);
            } catch (ResponseException ex) {
                output.setResponseCode(ex.getCode());
                output.setResponseMsg(ex.getMessage());
            } catch (ConstraintViolationException e) {
                return ExceptionUtils.validate(e);
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
        }

        public ResponseOutput delete(ClassRoomDto classRoomDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                ClassRoomEntity entity = classRoomRepo.findById(ValiableUtils.LongIsZero(classRoomDto.getClassRoomId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

                entity.setRecordStatus(RecordStatus.DELETE.getCode());

                classRoomRepo.save(entity);
            } catch (ResponseException ex) {
                output.setResponseCode(ex.getCode());
                output.setResponseMsg(ex.getMessage());
            } catch (ConstraintViolationException e) {
                return ExceptionUtils.validate(e);
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
        }
}
