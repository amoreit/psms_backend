/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TrnTeacherDepartmentDto;
import com.go.scms.entity.TrnTeacherDepartmentEntity;
import com.go.scms.repository.TrnTeacherDepartmentRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrnTeacherDepartmentService {

    @Autowired
    private TrnTeacherDepartmentRepository trnTeacherDepartmentRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput searchteachertec008(TrnTeacherDepartmentDto trnTeacherDepartmentDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer catClassId = (int) ValiableUtils.IntIsZero(trnTeacherDepartmentDto.getCatClassId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnTeacherDepartmentDto.getYearGroupId());

        List<Map> list = trnTeacherDepartmentRepo.findteachertec008(catClassId, yearGroupId);
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput insertcopyteacherdepartment(String userName, TrnTeacherDepartmentDto[] trnTeacherDepartmentDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TrnTeacherDepartmentDto map : trnTeacherDepartmentDto) {
                TrnTeacherDepartmentEntity entity = modelMapper.map(map, TrnTeacherDepartmentEntity.class);
                entity.setCreatedUser(userName);
                trnTeacherDepartmentRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
