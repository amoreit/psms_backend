package com.go.scms.service;

import com.go.scms.entity.LogUserEntity;
import com.go.scms.entity.UserEntity;
import com.go.scms.repository.LogUserRepository;
import com.go.scms.repository.UserRepository;
import com.go.scms.response.ResponseOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
public class LogUserService {

    @Autowired
    private LogUserRepository logUserRepo;

    @Autowired
    private UserRepository userRepo;

    @Async
    public void loginSave(HttpServletRequest request, ResponseOutput output,String user){
        LogUserEntity log = new LogUserEntity();

        /*** Log ***/
        log.setScCode("PRAMANDA");
        log.setFromIpaddr(request.getRemoteAddr());
        log.setLoginCode(output.getResponseCode());
        log.setLoginSuccess(output.getResponseCode() == 200);
        log.setLoginReason(output.getResponseMsg());
        log.setLoginUser(user);
        log.setLoginDate(new Date());
        logUserRepo.save(log);

        /*** User ***/
        UserEntity entity = userRepo.findByUsrName(user).orElse(null);
        if(entity == null){
            return;
        }
        if(!log.getLoginSuccess()){
            entity.setLoginCount(((int)entity.getLoginCount())+1);
        }else{
            entity.setLoginCount(0);
        }
        userRepo.save(entity);
    }

    @Async
    public void logoutSave(HttpServletRequest request, ResponseOutput output,String email){
        LogUserEntity log = new LogUserEntity();

        /*** Set ***/
        log.setScCode("PRAMANDA");
        log.setFromIpaddr(request.getRemoteAddr());
        log.setLoginCode(output.getResponseCode());
        log.setLoginSuccess(output.getResponseCode() == 200);
        log.setLoginReason(output.getResponseMsg());
        log.setLoginUser(email);
        log.setLogoutDate(new Date());

        logUserRepo.save(log);
    }
}
