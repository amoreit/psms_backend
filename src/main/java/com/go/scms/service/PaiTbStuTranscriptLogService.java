/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbStuTranscriptLogDto;
import com.go.scms.repository.PaiTbStuTranscriptLogRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author bamboo
 */
@Service
public class PaiTbStuTranscriptLogService {
        @Autowired
        private ModelMapper modelMapper;
     
        @Autowired
        private PaiTbStuTranscriptLogRepository paiTbStuTranscriptLogRepository;

        public ResponseOutput search(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
    
                Integer yearId =  (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getYearId());
                Integer classId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getClassId());
                
                if(!yearId.equals(0) && classId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuTranscriptLogDto.getP()),PageableUtils.result(tbStuTranscriptLogDto.getResult()),Sort.by("stu_transcript_log_id").ascending());
                    Page<Map> list = paiTbStuTranscriptLogRepository.findyear(yearId,pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                  
                if(yearId.equals(0) && !classId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuTranscriptLogDto.getP()),PageableUtils.result(tbStuTranscriptLogDto.getResult()),Sort.by("stu_transcript_log_id").ascending());
                    Page<Map> list = paiTbStuTranscriptLogRepository.findclass(classId,pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                   
                if(!yearId.equals(0) && !classId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbStuTranscriptLogDto.getP()),PageableUtils.result(tbStuTranscriptLogDto.getResult()),Sort.by("stu_transcript_log_id").ascending());
                    Page<Map> list = paiTbStuTranscriptLogRepository.findAll(yearId,classId,pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
            return output;
       }
        
}
