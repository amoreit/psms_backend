/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.repository.StudyPlanRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sir. Harinn
 */
@Service
public class StudyPlanService {
    
    @Autowired
    private StudyPlanRepository studyPlanRepository;
  
    public ResponseOutput search(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());

        if (!yearGroupName.equals("") && !classId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = studyPlanRepository.findYearClass( yearGroupName, classId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
            
        }
        return output;
    }
       
          
    public ResponseOutput searchTerm1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName().replace("2/", "1/"));
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());
        
        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            List<Map> list = studyPlanRepository.findTerm1( yearGroupName, classId, classRoomId, pageable);

            output.setResponseData(list);
            
        }
        return output;
    }
    
    
    public ResponseOutput searchTerm2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String yearGroupName = ValiableUtils.stringIsEmpty(tbTrnClassRoomMemberDto.getYearGroupName().replace("1/", "2/"));
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomMemberDto.getClassRoomId());

        if (!yearGroupName.equals("") && !classId.equals(0) && !classRoomId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomMemberDto.getP()), PageableUtils.result(tbTrnClassRoomMemberDto.getResult()));
            Page<Map> list = studyPlanRepository.findTerm2( yearGroupName, classId, classRoomId, pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());           
        }
        return output;
    }
}
