package com.go.scms.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.go.scms.dto.ManagePageDto;
import com.go.scms.entity.ManagePageEntity;
import com.go.scms.entity.RoleEntity;
import com.go.scms.repository.ManagePageRepository;
import com.go.scms.repository.RoleRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;

@Service
public class ManagePageService {
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private ManagePageRepository managePageRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	public ResponseOutput search(Long roleId) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search and Mapping ***/
		List list = managePageRepo.findAll(roleId).stream().map(e->{
			ManagePageDto m = new ManagePageDto();
			m.setPageId(e.get("page_id"));
			m.setPageCode(e.get("page_code"));
			m.setPageName(e.get("page_name"));
                        m.setPageNameEn(e.get("page_name_en"));
                        m.setPageGroupId(e.get("page_group_id"));
			m.setPageGroupCode(e.get("page_group_code"));
			m.setPageGroupName(e.get("page_group_name"));
                        m.setPageGroupNameEn(e.get("page_group_name_en"));
			
			m.setIsAdd(e.get("is_add"));
			m.setIsView(e.get("is_view"));
			m.setIsDelete(e.get("is_delete"));
			m.setIsApprove(e.get("is_approve"));
			m.setIsReport(e.get("is_report"));
			return m;
		}).collect(Collectors.toList());
		
		/*** Set ***/
		output.setResponseData(list);
		output.setResponseSize(Integer.toUnsignedLong(list.size()));
		return output;
	}
	
	public ResponseOutput save(Long roleId,ManagePageDto[] managePageDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			RoleEntity roleEntity = roleRepo.findById(roleId).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			for(ManagePageDto map : managePageDto) {
				ManagePageEntity entity = modelMapper.map(map,ManagePageEntity.class);
				entity.setRoleGroup(roleEntity);
				entity.getPage().setPageId(Long.parseLong(map.getPageId()));
				managePageRepo.save(entity);
			}
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		
		return output;
	}
	
}