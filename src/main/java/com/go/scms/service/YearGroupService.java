/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.YearGroupDto;
import com.go.scms.entity.YearEntity;
import com.go.scms.entity.YearGroupEntity;
import com.go.scms.entity.YearTermEntity;
import com.go.scms.repository.YearGroupRepository;
import com.go.scms.repository.YearRepository;
import com.go.scms.repository.YearTermRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class YearGroupService {

    @Autowired
    private YearGroupRepository yearGroupRepo;

    @Autowired
    private YearRepository yearRepo;

    @Autowired
    private YearTermRepository yearTermRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(YearGroupDto yearGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        /**
         * * Search **
         */
        String name = ValiableUtils.stringIsEmpty(yearGroupDto.getName());
        String yearCode = ValiableUtils.stringIsEmpty(yearGroupDto.getYearCode());
        String recordStatus = "D";
        
        Pageable pageable = PageRequest.of(PageableUtils.offset(yearGroupDto.getP()), PageableUtils.result(yearGroupDto.getResult()), Sort.by("yearGroupId").descending());
        Page<YearGroupEntity> list = yearGroupRepo.findAllBynameContainingAndYearYearCodeContainingAndRecordStatusNotIn(name, yearCode, recordStatus, pageable);
        
        /**
         * * Set **
         */
        output.setResponseData(list.getContent().stream().map(e -> {
            Map m = new HashMap<>();
            m.put("yearGroupId", e.getYearGroupId());
            m.put("name", e.getName());
            m.put("createdDate", e.getCreatedDate());
            m.put("createdUser", e.getCreatedUser());
            m.put("updatedDate", e.getUpdatedDate());
            m.put("updatedUser", e.getUpdatedUser());
            m.put("yearCode", e.getYear().getYearCode());
            m.put("iscurrent", e.getIscurrent());
            m.put("yearTermName", e.getYearTerm().getName());
            m.put("firstday", e.getFirstday());
            m.put("lastday", e.getLastday());
            return m;
        }).collect(Collectors.toList()));
        output.setResponseSize(list.getTotalElements());
        return output;
    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(yearGroupRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND)));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(YearGroupDto yearGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check id **
             */
            YearEntity year = yearRepo.findByYearId(yearGroupDto.getYearId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            YearTermEntity yearTerm = yearTermRepo.findByYearTermId(yearGroupDto.getYearTermId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            /**
             * * Merge data **
             */
            YearGroupEntity entity = modelMapper.map(yearGroupDto, YearGroupEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(yearGroupDto.getUserName());
            entity.setYear(year);

            entity.setYearTerm(yearTerm);

            yearGroupRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
    
    public ResponseOutput insertIscurrent(YearGroupDto yearGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check id **
             */
            YearEntity year = yearRepo.findByYearId(yearGroupDto.getYearId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            YearTermEntity yearTerm = yearTermRepo.findByYearTermId(yearGroupDto.getYearTermId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            /**
             * * Merge data **
             */
            YearGroupEntity entity = modelMapper.map(yearGroupDto, YearGroupEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(yearGroupDto.getUserName());
            entity.setYear(year);

            entity.setYearTerm(yearTerm);

            yearGroupRepo.changeiscurrent(false);

            yearGroupRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(YearGroupDto yearGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            YearEntity year = yearRepo.findByYearId(yearGroupDto.getYearId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            YearTermEntity yearTerm = yearTermRepo.findByYearTermId(yearGroupDto.getYearTermId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            YearGroupEntity entity = yearGroupRepo.findById(ValiableUtils.LongIsZero(yearGroupDto.getYearGroupId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Merge data **
             */
            modelMapper.map(yearGroupDto, entity);

            /**
             * * Set **
             */
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setYear(year);
            entity.setYearTerm(yearTerm);
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(yearGroupDto.getUserName());
            
            yearGroupRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updateIscurrent(YearGroupDto yearGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            YearEntity year = yearRepo.findByYearId(yearGroupDto.getYearId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            YearTermEntity yearTerm = yearTermRepo.findByYearTermId(yearGroupDto.getYearTermId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            YearGroupEntity entity = yearGroupRepo.findById(ValiableUtils.LongIsZero(yearGroupDto.getYearGroupId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Merge data **
             */
            modelMapper.map(yearGroupDto, entity);

            /**
             * * Set **
             */
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setYear(year);
            entity.setYearTerm(yearTerm);
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(yearGroupDto.getUserName());
            
            yearGroupRepo.changeiscurrent(false);
            
            yearGroupRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(YearGroupDto yearGroupDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            YearGroupEntity entity = yearGroupRepo.findById(ValiableUtils.LongIsZero(yearGroupDto.getYearGroupId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Set **
             */
            entity.setRecordStatus(RecordStatus.DELETE.getCode());
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(yearGroupDto.getUpdatedUser());

            yearGroupRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

}
