/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbStuTimeAttendanceDto;
import com.go.scms.dto.TbTeaTimeAttendanceDto;
import com.go.scms.entity.StuProfileEntity;
import com.go.scms.entity.TbStuTimeAttendanceEntity;
import com.go.scms.entity.TbTeaTimeAttendanceEntity;
import com.go.scms.entity.TeacherProfileEntity;
import com.go.scms.repository.TbTeaTimeAttendanceRepository;
import com.go.scms.repository.TeacherProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author bamboo
 */
@Service
public class TbTeaTimeAttendanceService {
    
    @Autowired
    private TbTeaTimeAttendanceRepository tbTeaTimeAttendanceRepository;
    
    @Autowired
    private TeacherProfileRepository teacherProfileRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    public ResponseOutput insert(TbTeaTimeAttendanceDto tbTeaTimeAttendanceDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                    /*** Merge data ***/
                    TbTeaTimeAttendanceEntity entity = modelMapper.map(tbTeaTimeAttendanceDto,TbTeaTimeAttendanceEntity.class);
                    
                    entity.setRecordStatus("A"); 
                    entity.setCreatedUser(tbTeaTimeAttendanceDto.getUserName());
                    entity.setCheckTime(new Date());

                    tbTeaTimeAttendanceRepository.save(entity);
            }catch(ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
            }catch(Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
    }
     
    public ResponseOutput search(TbTeaTimeAttendanceDto tbTeaTimeAttendanceDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTeaTimeAttendanceDto.getYearGroupId());
            String fullName = tbTeaTimeAttendanceDto.getFullName().trim();
            Integer classId = (int) ValiableUtils.IntIsZero(tbTeaTimeAttendanceDto.getClassRoomId());
            Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTeaTimeAttendanceDto.getClassRoomId());
            Integer condition = (int) ValiableUtils.IntIsZero(tbTeaTimeAttendanceDto.getCondition());
            String startDate = tbTeaTimeAttendanceDto.getStartDate();
            String endDate = tbTeaTimeAttendanceDto.getEndDate();

            if (yearGroupId.equals(0)) {
                if (fullName.equals("")) {
                    if (classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    ////////////////////////////////////////////// con 0,1 show
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.show1(pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0,1 endDate
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.end1(endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0,1 start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.start1(startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0,1 start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.startEnd1(startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////  con2 show
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.show2(pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 endDate
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.end2(endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.start2(startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.startEnd2(startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    } 
                    else if (!classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.room1(classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.roomEnd1(classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.roomStart1(classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.roomStartEnd1(classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.room2(classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.roomEnd2(classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.roomStart2(classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.roomStartEnd2(classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    }//end
                } else if (!fullName.equals("")) {
                    if (classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.class1(fullName, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classEnd1(fullName, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classStart1(fullName, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classStartEnd1(fullName, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.class2(fullName, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classEnd2(fullName, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classStart2(fullName, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classStartEnd2(fullName, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    } else if (!classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoom1(classId, classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoomEnd1(classId, classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoomStart1(classId, classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 class room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoomStartEnd1(classId, classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoom2(classId, classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoomEnd2(classId, classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoomStart2(classId, classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 class room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.classRoomStartEnd2(classId, classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    }//end
                }//end
            } else if (!yearGroupId.equals(0)) {
                if (fullName.equals("")) {
                    if (classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.year1(yearGroupId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearEnd1(yearGroupId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearStart1(yearGroupId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());
                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearStartEnd1(yearGroupId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.year2(yearGroupId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearEnd2(yearGroupId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearStart2(yearGroupId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearStartEnd2(yearGroupId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end

                   } else if (!classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoom1(yearGroupId, classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoomEnd1(yearGroupId, classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoomStart1(yearGroupId, classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoomStartEnd1(yearGroupId, classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoom2(yearGroupId, classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoomEnd2(yearGroupId, classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoomStart2(yearGroupId, classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearRoomStartEnd2(yearGroupId, classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    }//end
                } else if (!fullName.equals("")) {
                    if (classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClass1(yearGroupId, fullName, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassEnd1(yearGroupId, fullName, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());
                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class start 
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassStart1(yearGroupId, fullName, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassStartEnd1(yearGroupId, fullName, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class 
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClass2(yearGroupId, fullName, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassEnd2(yearGroupId, fullName, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassStart2(yearGroupId, fullName, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassStartEnd2(yearGroupId, fullName, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    } else if (!classRoomId.equals(0)) {
                        if (condition.equals(1)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoom1(yearGroupId, classId, classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoomEnd1(yearGroupId, classId, classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoomStart1(yearGroupId, classId, classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con0, 1 year class room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoomStartEnd1(yearGroupId, classId, classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        } else if (condition.equals(0) || condition.equals(2)) {
                            if (startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class room
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoom2(yearGroupId, classId, classRoomId, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class room end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoomEnd2(yearGroupId, classId, classRoomId, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            } else if (!startDate.equals("NaN-NaN-NaN")) {
                                if (endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class room start
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoomStart2(yearGroupId, classId, classRoomId, startDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                } else if (!endDate.equals("NaN-NaN-NaN")) {
                                    /////////////////////////////////////////////search con2 year class room start end
                                    Pageable pageable = PageRequest.of(PageableUtils.offset(tbTeaTimeAttendanceDto.getP()), PageableUtils.result(tbTeaTimeAttendanceDto.getResult()));
                                    Page<Map> list = tbTeaTimeAttendanceRepository.yearClassRoomStartEnd2(yearGroupId, classId, classRoomId, startDate, endDate, pageable);

                                    output.setResponseData(list.getContent());
                                    output.setResponseSize(list.getTotalElements());

                                }//end
                            }//end
                        }//end
                    }//end
                }//end
            }//end
         return output;
    }                     
}

        
     
                 
  
        
        
      
        
       
    
     
