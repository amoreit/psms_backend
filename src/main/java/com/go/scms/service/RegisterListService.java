package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.RegisterListDto;
import com.go.scms.entity.RegisterListEntity;
import com.go.scms.mail.MailInput;
import com.go.scms.mail.MailService;
import com.go.scms.repository.RegisterListRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RegisterListService {
    
        @Autowired
	private RegisterListRepository registerListRepo;
        
        @Autowired
	private MailService mailService;
	
	@Autowired
	private ModelMapper modelMapper;
        
        @Autowired
        private BCryptPasswordEncoder cry;
	
	public ResponseOutput search(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
		String userType = ValiableUtils.stringIsEmpty(registerListDto.getUserType().trim());
                String firstName = ValiableUtils.stringIsEmpty(registerListDto.getFirstName().trim());
		String lastName = ValiableUtils.stringIsEmpty(registerListDto.getLastName().trim());
                String activatedCheck = ValiableUtils.stringIsEmpty(registerListDto.getActivatedCheck());
		
                if(userType.equals("") && firstName.equals("") && lastName.equals("") && activatedCheck.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findalluserstatus(pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!userType.equals("") && !firstName.equals("") && !lastName.equals("") && !activatedCheck.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findallfield(userType, firstName, lastName, pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!userType.equals("") && firstName.equals("") && lastName.equals("") && activatedCheck.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findusertype(userType, pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
               
                if(userType.equals("") && !firstName.equals("") && lastName.equals("") && activatedCheck.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findfirstname(firstName, pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(userType.equals("") && firstName.equals("") && !lastName.equals("") && activatedCheck.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findlastname(lastName, pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(userType.equals("") && !firstName.equals("") && !lastName.equals("") && activatedCheck.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findfirstnamelastname(firstName, lastName, pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(userType.equals("") && firstName.equals("") && lastName.equals("") && activatedCheck.equals("true")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findactivatedtrue(pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(userType.equals("") && firstName.equals("") && lastName.equals("") && activatedCheck.equals("false")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(registerListDto.getP()),PageableUtils.result(registerListDto.getResult()),Sort.by("user_id").ascending());
                    Page<Map> list = registerListRepo.findactivatedfalse(pageable);

                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
		return output;
	}
        
        public ResponseOutput searchUser(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
                String usrName = ValiableUtils.stringIsEmpty(registerListDto.getUsrName());
                List<RegisterListEntity> list = registerListRepo.findAllByUsrNameContaining(usrName);
                
                output.setResponseData(list);
		return output;
	}
	
        public ResponseOutput updateTrue(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			RegisterListEntity entity = registerListRepo.findByUserId((registerListDto.getUserId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        			
			/*** Merge data ***/
			modelMapper.map(registerListRepo,entity);

			/*** Set ***/
                        entity.setActivated(true);
			entity.setUserUpdatedDate(new Date());
			
			registerListRepo.save(entity);
                        
                        /** Send Mail**/
                        Map map = new HashMap<>();
                        map.put("firstName", entity.getFirstName());
                        map.put("lastName", entity.getLastName());
                        map.put("UsrName", entity.getUsrName());
                        
                        MailInput input = new MailInput();
                        input.setPathHtml("registerList.html");
                        input.setRecipients(entity.getEmail());
                        input.setSubject("PRAMANDANIJJANUKROAH SCHOOL");
                        input.setInput(map);
                        
                        mailService.sendMessage(input);
                        
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updateFalse(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			RegisterListEntity entity = registerListRepo.findByUserId((registerListDto.getUserId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        			
			/*** Merge data ***/
			modelMapper.map(registerListRepo,entity);

			/*** Set ***/
                        entity.setActivated(false);
			entity.setUserUpdatedDate(new Date());
			
			registerListRepo.save(entity);
                        
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput sendMail(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
                        /*** Check role by id ***/
			RegisterListEntity entity = registerListRepo.findByUserId((registerListDto.getUserId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			modelMapper.map(registerListRepo,entity);

			/*** Set ***/
                        entity.setActivated(true);
			registerListRepo.save(entity);
                        
                        /** Send Mail**/
                        Map map = new HashMap<>();
                        map.put("firstName", entity.getFirstName());
                        map.put("lastName", entity.getLastName());
                        map.put("UsrName", entity.getUsrName());
                        
                        MailInput input = new MailInput();
                        input.setPathHtml("registerList.html");
                        input.setRecipients(entity.getEmail());
                        input.setSubject("PRAMANDANIJJANUKROAH SCHOOL");
                        input.setInput(map);
                        
                        mailService.sendMessage(input);
                        
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updated(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check by id ***/
			RegisterListEntity entity = registerListRepo.findByUserId((registerListDto.getUserId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			modelMapper.map(registerListDto,entity);

			/*** Set ***/
                        entity.setUserStatus(RecordStatus.SAVE.getCode());
                        entity.setUserUpdatedDate(new Date());
			
			registerListRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updatedPW(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check by id ***/
			RegisterListEntity entity = registerListRepo.findByUserId((registerListDto.getUserId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			modelMapper.map(registerListDto,entity);

			/*** Set ***/
                        entity.setPassword(cry.encode(registerListDto.getPassword()));
                        entity.setUserStatus(RecordStatus.SAVE.getCode());
                        entity.setUserUpdatedDate(new Date());
			
			registerListRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}

        public ResponseOutput findById(Integer id) throws Throwable {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(registerListRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND)));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput insert(RegisterListDto registerListDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			RegisterListEntity entity = modelMapper.map(registerListDto,RegisterListEntity.class);
                        entity.setScCode("PRAMANDA");
                        entity.setLoginCount(0);
                        entity.setActivated(true);
                        entity.setPassword(cry.encode(registerListDto.getPassword()));
                        entity.setUserStatus(RecordStatus.SAVE.getCode());
			entity.setUserCreatedDate(new Date()); 
			
			registerListRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updatedCheckAll(RegisterListDto[] registerListDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {                  
                    /*** Loop update data ***/
                    for (RegisterListDto map : registerListDto) {
                        RegisterListEntity entity = modelMapper.map(map, RegisterListEntity.class);
                        if(map.getActivated() == false){
                              registerListRepo.updatedCheckAll(
                                  true,
                                  new Date(),
                                  map.getUserId()
                              );

                              /** Send Mail**/
                              Map mapData = new HashMap<>();
                              mapData.put("firstName", map.getFirstName());
                              mapData.put("lastName", map.getLastName());
                              mapData.put("UsrName", map.getUsrName());

                              MailInput input = new MailInput();
                              input.setPathHtml("registerList.html");
                              input.setRecipients(map.getEmail());
                              input.setSubject("PRAMANDANIJJANUKROAH SCHOOL");
                              input.setInput(mapData);

                              mailService.sendMessage(input);
                        }else{
                              registerListRepo.updatedCheckAll(
                                  false,
                                  new Date(),
                                  map.getUserId()
                              );
                        }
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
}
