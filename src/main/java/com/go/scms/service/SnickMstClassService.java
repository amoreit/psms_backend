package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickMstClassDto;
import com.go.scms.entity.SnickMstCatClassEntity;
import com.go.scms.entity.SnickMstClassEntity;
import com.go.scms.entity.SnickMstSubjectTypeEntity;
import com.go.scms.repository.SnickMstCatClassRepository;
import com.go.scms.repository.SnickMstClassRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.function.Supplier;
import javax.validation.ConstraintViolationException;
import static jdk.nashorn.internal.runtime.Debug.id;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class SnickMstClassService {
    
        @Autowired
	private SnickMstClassRepository mstClassRepo;
        
        @Autowired
	private SnickMstCatClassRepository mstCatClassRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public ResponseOutput search(SnickMstClassDto mstClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
		String classLnameTh = ValiableUtils.stringIsEmpty(mstClassDto.getClassLnameTh());
                String lnameTh = ValiableUtils.stringIsEmpty(mstClassDto.getLnameTh());
                String recordStatus = "D";
                
		Pageable pageable = PageRequest.of(PageableUtils.offset(mstClassDto.getP()),PageableUtils.result(mstClassDto.getResult()),Sort.by("classId").ascending());
		Page<SnickMstClassEntity> list = mstClassRepo.findAllByClassLnameThContainingAndCatclassLnameThContainingAndRecordStatusNotIn(classLnameTh, lnameTh, recordStatus, pageable);
                //Page<SnickMstClassEntity> list = mstClassRepo.findAllByClassLnameThContainingAndCatclassLnameThContaining(classLnameTh, lnameTh, pageable);

		/*** Set ***/
		output.setResponseData(list.getContent());
		output.setResponseSize(list.getTotalElements());
		
		return output;
	}
	
	public ResponseOutput findById(Long id) throws Throwable {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(mstClassRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND)));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput insert(SnickMstClassDto mstClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
                        /*** Check cat_class by id ***/
			SnickMstCatClassEntity catclass = mstCatClassRepo.findByCatClassId(mstClassDto.getCatClassId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			SnickMstClassEntity entity = modelMapper.map(mstClassDto,SnickMstClassEntity.class);
                        entity.setCatclass(catclass);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
			entity.setCreatedUser(mstClassDto.getUserName());
			
			mstClassRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput update(SnickMstClassDto mstClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			SnickMstClassEntity entity = mstClassRepo.findById(ValiableUtils.LongIsZero(mstClassDto.getClassId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        SnickMstCatClassEntity catclass = mstCatClassRepo.findById(mstClassDto.getCatClassId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			modelMapper.map(mstClassDto,entity);

			/*** Set ***/
                        entity.setCatclass(catclass);
                        entity.setRecordStatus(RecordStatus.UPDATE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(mstClassDto.getUserName());
			
			mstClassRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
	
	public ResponseOutput delete(SnickMstClassDto mstClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			SnickMstClassEntity entity = mstClassRepo.findById(ValiableUtils.LongIsZero(mstClassDto.getClassId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        
			/*** Set ***/
                        entity.setRecordStatus(RecordStatus.DELETE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(mstClassDto.getUpdatedUser());
			
			mstClassRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
