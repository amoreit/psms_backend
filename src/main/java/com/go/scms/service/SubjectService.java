/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SubjectDto;
import com.go.scms.entity.SubjectEntity;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.go.scms.repository.SubjectRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.utils.ExceptionUtils;
import java.util.List;
import javax.validation.ConstraintViolationException;


@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(SubjectDto subjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String subjectCode = ValiableUtils.stringIsEmpty(subjectDto.getSubjectCode());
        String nameTh = ValiableUtils.stringIsEmpty(subjectDto.getNameTh());
        Integer departmentId = (int) ValiableUtils.IntIsZero(subjectDto.getDepartmentId());
        Integer catClassId = (int) ValiableUtils.IntIsZero(subjectDto.getCatClassId());

        if (catClassId.equals(0) && departmentId.equals(0)) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(subjectDto.getP()), PageableUtils.result(subjectDto.getResult()));
            Page<Map> list = subjectRepo.findsubjectall(subjectCode, nameTh, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (!catClassId.equals(0) && departmentId.equals(0)) {
             Pageable pageable = PageRequest.of(PageableUtils.offset(subjectDto.getP()), PageableUtils.result(subjectDto.getResult()));
            Page<Map> list = subjectRepo.findsubjectallandcatclassid(catClassId,subjectCode, nameTh, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (!catClassId.equals(0) && !departmentId.equals(0)) {
             Pageable pageable = PageRequest.of(PageableUtils.offset(subjectDto.getP()), PageableUtils.result(subjectDto.getResult()));
            Page<Map> list = subjectRepo.findsubjectallandcatclassidanddepartmentid(catClassId,departmentId,subjectCode, nameTh, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;

    }
    
    public ResponseOutput searchsubjectbyid(SubjectDto subjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer subjectId = (int) ValiableUtils.IntIsZero(subjectDto.getSubjectId());
        List<Map> list = subjectRepo.findsubjectbyid(subjectId);
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(subjectRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(SubjectDto subjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            SubjectEntity entity = modelMapper.map(subjectDto, SubjectEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(subjectDto.getUserName());

            subjectRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(SubjectDto subjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            SubjectEntity entity = subjectRepo.findById(ValiableUtils.LongIsZero(subjectDto.getSubjectId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(subjectDto, entity);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(subjectDto.getUserName());
            subjectRepo.save(entity);
            
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(SubjectDto subjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            SubjectEntity entity = subjectRepo.findById(ValiableUtils.LongIsZero(subjectDto.getSubjectId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            entity.setRecordStatus(RecordStatus.DELETE.getCode());
            subjectRepo.save(entity);
            
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

}
