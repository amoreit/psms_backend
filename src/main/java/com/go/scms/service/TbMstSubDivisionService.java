/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TbMstSubDivisionDto;
import com.go.scms.entity.TbMstDivisionEntity;
import com.go.scms.entity.TbMstSubDivisionEntity;
import com.go.scms.repository.TbMstDivisionRepository;
import com.go.scms.repository.TbMstSubDivisionRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import static org.apache.commons.lang.StringUtils.trim;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sir. Harinn
 */
@Service
public class TbMstSubDivisionService {
    
        @Autowired
        private TbMstSubDivisionRepository tbMstSubDivisionRepository;
        
        @Autowired
        private TbMstDivisionRepository tbMstDivisionRepository;
        
        @Autowired
	private ModelMapper modelMapper;
        
        public ResponseOutput search(TbMstSubDivisionDto tbMstSubDivisionDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            /**
             * * Search **
             */
            String divisionNameTh =  ValiableUtils.stringIsEmpty(tbMstSubDivisionDto.getDivisionNameTh());
            String subDivisionNameTh = trim(ValiableUtils.stringIsEmpty(tbMstSubDivisionDto.getSubDivisionNameTh()));
            String subDivisionNameEn = trim(ValiableUtils.stringIsEmpty(tbMstSubDivisionDto.getSubDivisionNameEn()));
            String recordStatus = "D";
            
            if(divisionNameTh.equals("") && subDivisionNameTh.equals("") && subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.show( pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());
                
            }else if(!divisionNameTh.equals("") && subDivisionNameTh.equals("") && subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findByDivisionNameTh(divisionNameTh, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());

            }else if(divisionNameTh.equals("") && !subDivisionNameTh.equals("") && subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findBySubDivisionNameTh( subDivisionNameTh, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());

            }else if(divisionNameTh.equals("") && subDivisionNameTh.equals("") && !subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findBySubDivisionNameEn( subDivisionNameEn, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());
                
            }else if(!divisionNameTh.equals("") && !subDivisionNameTh.equals("") && subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findByDNT_SDNT(divisionNameTh, subDivisionNameTh, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());

            }else if(!divisionNameTh.equals("") && subDivisionNameTh.equals("") && !subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findByDNT_SDNE(divisionNameTh, subDivisionNameEn, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());

            }else if(divisionNameTh.equals("") && !subDivisionNameTh.equals("") && !subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findBySDNT_SDNE( subDivisionNameTh, subDivisionNameEn, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());
                
            }else if(!divisionNameTh.equals("") && !subDivisionNameTh.equals("") && !subDivisionNameEn.equals("")){
                Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstSubDivisionDto.getP()), PageableUtils.result(tbMstSubDivisionDto.getResult()));
                Page<Map> list = tbMstSubDivisionRepository.findByDNT_SDNT_SDNE(divisionNameTh, subDivisionNameTh, subDivisionNameEn, pageable);

                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());

            }
            return output;
        }
        
	public ResponseOutput findById(Long id) throws Throwable {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(tbMstSubDivisionRepository.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND)));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}        
        
	public ResponseOutput insert(TbMstSubDivisionDto tbMstSubDivisionDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
                        TbMstDivisionEntity division = tbMstDivisionRepository.findByDivisionId(tbMstSubDivisionDto.getDivisionId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			TbMstSubDivisionEntity entity = modelMapper.map(tbMstSubDivisionDto, TbMstSubDivisionEntity.class);
                        entity.setRecordStatus("A");
			entity.setCreatedUser(tbMstSubDivisionDto.getUserName());
                        entity.setDivision(division);
                        
			
			tbMstSubDivisionRepository.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}   
        
        public ResponseOutput update(TbMstSubDivisionDto tbMstSubDivisionDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
                        TbMstDivisionEntity division = tbMstDivisionRepository.findByDivisionId(tbMstSubDivisionDto.getDivisionId()).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
			TbMstSubDivisionEntity entity = tbMstSubDivisionRepository.findById(ValiableUtils.LongIsZero(tbMstSubDivisionDto.getSubDivisionId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(tbMstSubDivisionDto,entity);

			/*** Set ***/
                        entity.setRecordStatus("U");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(tbMstSubDivisionDto.getUserName());
                        entity.setDivision(division);
			
			tbMstSubDivisionRepository.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
       
        public ResponseOutput delete(TbMstSubDivisionDto tbMstSubDivisionDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check role by id ***/
                        TbMstSubDivisionEntity entity = tbMstSubDivisionRepository.findById(ValiableUtils.LongIsZero(tbMstSubDivisionDto.getSubDivisionId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Set ***/
                        entity.setRecordStatus("D");
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(tbMstSubDivisionDto.getUpdatedUser());

                        tbMstSubDivisionRepository.save(entity);
                }catch(ResponseException ex) {
                        output.setResponseCode(ex.getCode());
                        output.setResponseMsg(ex.getMessage());
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
}
