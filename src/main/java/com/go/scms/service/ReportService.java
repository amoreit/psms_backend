package com.go.scms.service;

import com.googlecode.jthaipdf.jasperreports.engine.ThaiExporterManager;
import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

@Transactional(readOnly = true)
@Service
public class ReportService {

    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected NamedParameterJdbcTemplate jdbcTemplate;

    @Transactional
    public ByteArrayOutputStream exportReport(Map params,JasperReport ... jasperReports) throws SQLException, JRException {
        ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
        Connection connection = null;
        try{
            JasperPrint jasperPrint = null;
            connection = dataSource.getConnection();
           /* List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            for(JasperReport jasperReport:jasperReports){
                jprintlist.add(JasperFillManager.fillReport(jasperReport, params, connection));
            }*/
            for(int i=0;i<jasperReports.length;i++){
                if(i==0){
                    jasperPrint = JasperFillManager.fillReport(jasperReports[i], params, connection);
                }else{
                    if(jasperPrint != null){
                        JasperPrint jp = JasperFillManager.fillReport(jasperReports[i], params, connection);
                        for(JRPrintPage page: jp.getPages()){
                            jasperPrint.addPage(page);
                        }
                    }
                }
            }
            /*** Export ***/
            ThaiExporterManager.exportReportToPdfStream(jasperPrint, pdfReportStream);

        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }finally {
            if(connection != null){
                connection.close();
            }
        }
        return pdfReportStream;
    }

}
