package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTbStuProfileDocDto;
import com.go.scms.entity.SnickTbStuProfileDocEntity;
import com.go.scms.entity.SnickTbStuProfileEntity;
import com.go.scms.repository.SnickTbStuProfileDocRepository;
import com.go.scms.repository.SnickTbStuProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickTbStuProfileDocService {
        
        @Autowired
	private SnickTbStuProfileDocRepository tbStuProfileDocRepo;
        
        @Autowired
	private SnickTbStuProfileRepository tbStuProfileRepo;
	
	@Autowired
	private ModelMapper modelMapper;
        
        public ResponseOutput insert(SnickTbStuProfileDocDto tbStuProfileDocDto) throws Throwable {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check id ***/
                        SnickTbStuProfileEntity stuProfileId = tbStuProfileRepo.findByStuProfileId(tbStuProfileDocDto.getStuProfileId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Merge data ***/
                        SnickTbStuProfileDocEntity entity = modelMapper.map(tbStuProfileDocDto,SnickTbStuProfileDocEntity.class);
                        entity.setStuProfile(stuProfileId);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
                        entity.setCreatedUser(tbStuProfileDocDto.getUserName());

                        tbStuProfileDocRepo.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput update(SnickTbStuProfileDocDto tbStuProfileDocDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {			
			/*** Check id ***/
			SnickTbStuProfileDocEntity entity = tbStuProfileDocRepo.findById(ValiableUtils.LongIsZero(tbStuProfileDocDto.getStuProfileDocId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        SnickTbStuProfileEntity stuProfileId = tbStuProfileRepo.findByStuProfileId(tbStuProfileDocDto.getStuProfileId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));	
                        
                        /*** Merge data ***/
			modelMapper.map(tbStuProfileDocDto,entity);

			/*** Set ***/
                        entity.setStuProfile(stuProfileId);
			entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(tbStuProfileDocDto.getUserName());
			
			tbStuProfileDocRepo.save(entity);
                                 
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
