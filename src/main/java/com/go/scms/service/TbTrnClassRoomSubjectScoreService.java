/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbTrnClassRoomSubjectScoreDto;
import com.go.scms.entity.TbTrnClassRoomSubjectScoreEntity;
import com.go.scms.repository.TbTrnClassRoomSubjectScoreRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author lumin
 */
@Service
public class TbTrnClassRoomSubjectScoreService {

    @Autowired
    private TbTrnClassRoomSubjectScoreRepository tbTrnClassRoomSubjectScoreRepository;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectScoreDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectScoreDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectScoreDto.getClassRoomId());
        Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectScoreDto.getClassRoomSubjectId());

        List list = tbTrnClassRoomSubjectScoreRepository.SubjectScore(yearGroupId, classId, classRoomId, classRoomSubjectId).stream().map(e -> {
            SubjectScore m = new SubjectScore();
            m.setClassRoomSubjectScoreId(e.get("class_room_subject_score_id"));
            m.setClassRoomNo(e.get("class_room_no"));
            m.setStuCode(e.get("stu_code"));
            m.setFullname(e.get("fullname"));
            m.setFirstnameTh(e.get("firstname"));
            m.setLastnameTh(e.get("lastname"));
            m.setSubjectCode(e.get("subject_code"));
            m.setSubjectName(e.get("subject_name"));
            m.setCredit(e.get("credit"));
            m.setClassRoomSnameTh(e.get("class_room_sname_th"));
            m.setYearGroupName(e.get("year_group_name"));
            m.setScore1(e.get("score1"));
            m.setScoreMidterm(e.get("score_midterm"));
            m.setScore2(e.get("score2"));
            m.setScoreFinal(e.get("score_final"));
            m.setSumScore(e.get("sum_score"));
            m.setGrade(e.get("grade"));
            m.setFullScore1(e.get("full_score1"));
            m.setFullScoreMidterm(e.get("full_score_midterm"));
            m.setFullScore2(e.get("full_score2"));
            m.setFullScoreFinal(e.get("full_score_final"));
            m.setFullScore(e.get("full_score"));
            m.setIsMorsor(e.get("is_morsor"));
            m.setIsExchange(e.get("is_exchange"));
            m.setRemark(e.get("remark"));
            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;
    }

    public ResponseOutput searchAca006Form(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classRoomMemberId = (int) ValiableUtils.IntIsZero(tbTrnClassRoomSubjectScoreDto.getClassRoomMemberId());
        Pageable pageable = PageRequest.of(PageableUtils.offset(tbTrnClassRoomSubjectScoreDto.getP()), PageableUtils.result(tbTrnClassRoomSubjectScoreDto.getResult()));
        Page<Map> list = tbTrnClassRoomSubjectScoreRepository.findAca006Form(classRoomMemberId, pageable);

        output.setResponseData(list.getContent());
        output.setResponseSize(list.getTotalElements());
        return output;
    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(tbTrnClassRoomSubjectScoreRepository.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND)));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            TbTrnClassRoomSubjectScoreEntity entity = tbTrnClassRoomSubjectScoreRepository.findById(ValiableUtils.LongIsZero(tbTrnClassRoomSubjectScoreDto.getClassRoomSubjectScoreId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            modelMapper.map(tbTrnClassRoomSubjectScoreDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(tbTrnClassRoomSubjectScoreDto.getUser());

            tbTrnClassRoomSubjectScoreRepository.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput save(String userName, TbTrnClassRoomSubjectScoreDto[] tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TbTrnClassRoomSubjectScoreDto map : tbTrnClassRoomSubjectScoreDto) {
                tbTrnClassRoomSubjectScoreRepository.updatescore(
                        (Double.parseDouble(map.getScore1())),
                        (Double.parseDouble(map.getScore2())),
                        (Double.parseDouble(map.getScoreMidterm())),
                        (Double.parseDouble(map.getScoreFinal())),
                        (map.getRemark()),
                        (Boolean.parseBoolean(map.getIsMorsor())),
                        (Boolean.parseBoolean(map.getIsExchange())),
                        RecordStatus.UPDATE.getCode(),
                        userName,
                        (map.getClassRoomSubjectScoreId()));
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
    
    public ResponseOutput save2(String userName, TbTrnClassRoomSubjectScoreDto[] tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TbTrnClassRoomSubjectScoreDto map : tbTrnClassRoomSubjectScoreDto) {
                tbTrnClassRoomSubjectScoreRepository.updatescore2(
                        (Double.parseDouble(map.getScoreFinal())),
                        RecordStatus.UPDATE.getCode(),
                        userName,
                        (map.getClassRoomSubjectScoreId()));
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    //aong
    public ResponseOutput getPrimarySubjectScore(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomSubjectScoreDto.getStuCode()).trim();
        List list = tbTrnClassRoomSubjectScoreRepository.getPrimarySubjectScore(stuCode);
        output.setResponseData(list);
        return output;
    }

    //aong
    public ResponseOutput getSecondarySubjectScore(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomSubjectScoreDto.getStuCode()).trim();
        List list = tbTrnClassRoomSubjectScoreRepository.getSecondarySubjectScore(stuCode);
        output.setResponseData(list);
        return output;
    }

    //aong
    public ResponseOutput getSumScore(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomSubjectScoreDto.getStuCode()).trim();
        List list = tbTrnClassRoomSubjectScoreRepository.getSumScore(stuCode);
        output.setResponseData(list);
        return output;
    }

    //aong
    public ResponseOutput getSumGpa(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomSubjectScoreDto.getStuCode()).trim();
        List list = tbTrnClassRoomSubjectScoreRepository.getSumGpa(stuCode);
        output.setResponseData(list);
        return output;
    }

    //aong
    public ResponseOutput getSumScorePrimary(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomSubjectScoreDto.getStuCode()).trim();
        List list = tbTrnClassRoomSubjectScoreRepository.getSumScorePrimary(stuCode);
        output.setResponseData(list);
        return output;
    }

    //aong
    public ResponseOutput getSumGpaPrimary(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(tbTrnClassRoomSubjectScoreDto.getStuCode()).trim();
        List list = tbTrnClassRoomSubjectScoreRepository.getSumGpaPrimary(stuCode);
        output.setResponseData(list);
        return output;
    }
}

// max
class SubjectScore {

    private String classRoomSubjectScoreId;
    private String classRoomNo;
    private String stuCode;
    private String fullname;
    private String firstnameTh;
    private String lastnameTh;
    private String subjectCode;
    private String subjectName;
    private String credit;
    private String classRoomSnameTh;
    private String yearGroupName;
    private String score1;
    private String scoreMidterm;
    private String score2;
    private String scoreFinal;
    private String sumScore;
    private String grade;
    private String fullScore1;
    private String fullScoreMidterm;
    private String fullScore2;
    private String fullScoreFinal;
    private String fullScore;
    private String remark;
    private String isMorsor;
    private String isExchange;

    public String getFirstnameTh() {
        return firstnameTh;
    }

    public void setFirstnameTh(String firstnameTh) {
        this.firstnameTh = firstnameTh;
    }

    public String getLastnameTh() {
        return lastnameTh;
    }

    public void setLastnameTh(String lastnameTh) {
        this.lastnameTh = lastnameTh;
    }

    public String getClassRoomSubjectScoreId() {
        return classRoomSubjectScoreId;
    }

    public void setClassRoomSubjectScoreId(String classRoomSubjectScoreId) {
        this.classRoomSubjectScoreId = classRoomSubjectScoreId;
    }

    public String getClassRoomNo() {
        return classRoomNo;
    }

    public void setClassRoomNo(String classRoomNo) {
        this.classRoomNo = classRoomNo;
    }

    public String getStuCode() {
        return stuCode;
    }

    public void setStuCode(String stuCode) {
        this.stuCode = stuCode;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getClassRoomSnameTh() {
        return classRoomSnameTh;
    }

    public void setClassRoomSnameTh(String classRoomSnameTh) {
        this.classRoomSnameTh = classRoomSnameTh;
    }

    public String getYearGroupName() {
        return yearGroupName;
    }

    public void setYearGroupName(String yearGroupName) {
        this.yearGroupName = yearGroupName;
    }

    public String getScore1() {
        return score1;
    }

    public void setScore1(String score1) {
        this.score1 = score1;
    }

    public String getScoreMidterm() {
        return scoreMidterm;
    }

    public void setScoreMidterm(String scoreMidterm) {
        this.scoreMidterm = scoreMidterm;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }

    public String getScoreFinal() {
        return scoreFinal;
    }

    public void setScoreFinal(String scoreFinal) {
        this.scoreFinal = scoreFinal;
    }

    public String getSumScore() {
        return sumScore;
    }

    public void setSumScore(String sumScore) {
        this.sumScore = sumScore;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getFullScore1() {
        return fullScore1;
    }

    public void setFullScore1(String fullScore1) {
        this.fullScore1 = fullScore1;
    }

    public String getFullScoreMidterm() {
        return fullScoreMidterm;
    }

    public void setFullScoreMidterm(String fullScoreMidterm) {
        this.fullScoreMidterm = fullScoreMidterm;
    }

    public String getFullScore2() {
        return fullScore2;
    }

    public void setFullScore2(String fullScore2) {
        this.fullScore2 = fullScore2;
    }

    public String getFullScoreFinal() {
        return fullScoreFinal;
    }

    public void setFullScoreFinal(String fullScoreFinal) {
        this.fullScoreFinal = fullScoreFinal;
    }

    public String getFullScore() {
        return fullScore;
    }

    public void setFullScore(String fullScore) {
        this.fullScore = fullScore;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsMorsor() {
        return isMorsor;
    }

    public void setIsMorsor(String isMorsor) {
        this.isMorsor = isMorsor;
    }

    public String getIsExchange() {
        return isExchange;
    }

    public void setIsExchange(String isExchange) {
        this.isExchange = isExchange;
    }

}
