/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.DepartmentPaiDto;
import com.go.scms.entity.DepartmentPaiEntity;
import com.go.scms.entity.SnickMstCatClassEntity;
import com.go.scms.entity.PageEntity;
import com.go.scms.repository.DepartmentPaiRepository;
import com.go.scms.repository.SnickMstCatClassRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import net.bytebuddy.dynamic.scaffold.TypeWriter.FieldPool.Record;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DepartmentPaiService {
    
        @Autowired
        private DepartmentPaiRepository DepartmentRepo;

        @Autowired
        private ModelMapper modelMapper;

        @Autowired
        private SnickMstCatClassRepository mstCatClassRepo;
    
        public ResponseOutput search(DepartmentPaiDto departmentDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		
		/*** Search ***/
		String departmentCode = ValiableUtils.stringIsEmpty(departmentDto.getDepartmentCode());
		String name = ValiableUtils.stringIsEmpty(departmentDto.getName());
                String lnameTh = ValiableUtils.stringIsEmpty(departmentDto.getLnameTh());
                String recordStatus = "D";
		
		Pageable pageable = PageRequest.of(PageableUtils.offset(departmentDto.getP()),PageableUtils.result(departmentDto.getResult()),Sort.by("cattclassCatClassId","departmentId").ascending());
		Page<DepartmentPaiEntity> list = DepartmentRepo.findAllByDepartmentCodeContainingAndCattclassLnameThContainingAndNameContainingAndRecordStatusNotIn(departmentCode, lnameTh,name, recordStatus, pageable);
                
		output.setResponseData(list.getContent());
		output.setResponseSize(list.getTotalElements());
		return output;
	}
    
        public ResponseOutput findById(Long id) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			output.setResponseData(DepartmentRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput insert(DepartmentPaiDto departmentDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
                        /*** Check role by id ***/
			SnickMstCatClassEntity cattClass = mstCatClassRepo.findByCatClassId(departmentDto.getCatClass()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			DepartmentPaiEntity entity = modelMapper.map(departmentDto,DepartmentPaiEntity.class);
                        entity.setCattclass(cattClass);
                        entity.setRecordStatus("A");
                        entity.setCreatedUser(departmentDto.getUserName());
			
			DepartmentRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput update(DepartmentPaiDto departmentDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			DepartmentPaiEntity entity = DepartmentRepo.findById(ValiableUtils.LongIsZero(departmentDto.getDepartmentId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Merge data ***/
			modelMapper.map(departmentDto,entity);

			/*** Set ***/
                        entity.setRecordStatus("U");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(departmentDto.getUserName());
			
			DepartmentRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput delete(DepartmentPaiDto departmentDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check role by id ***/
			DepartmentPaiEntity entity = DepartmentRepo.findById(ValiableUtils.LongIsZero(departmentDto.getDepartmentId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
			/*** Set ***/
                        entity.setRecordStatus("D");
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(departmentDto.getUpdatedUser());
			
			DepartmentRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}

