/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.GraduateLogDto;
import com.go.scms.entity.GraduateLogEntity;
import com.go.scms.repository.GraduateLogRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraduateLogService {
    
    @Autowired
    private GraduateLogRepository graduateLogRepo;

    @Autowired
    private ModelMapper modelMapper;
    
    public ResponseOutput insert(String userName, GraduateLogDto[] graduateLogDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (GraduateLogDto map : graduateLogDto) {
                GraduateLogEntity entity = modelMapper.map(map, GraduateLogEntity.class);
                entity.setCreatedUser(userName);
                graduateLogRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
