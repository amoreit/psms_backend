/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.PositionDto;
import com.go.scms.entity.PositionEntity;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.repository.PositionRepository;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.go.scms.utils.PageableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kronn
 */
@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(PositionDto positionDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String positionNameTh = ValiableUtils.stringIsEmpty(positionDto.getPositionNameTh());
        String positionNameEn = ValiableUtils.stringIsEmpty(positionDto.getPositionNameEn());
        String recordStatus = "D";

        if (positionNameTh.equals("") && positionNameEn.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(positionDto.getP()), PageableUtils.result(positionDto.getResult()), Sort.by("positionId").ascending());
            Page<PositionEntity> list = positionRepo.findAllByRecordStatusNotIn(recordStatus, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (!positionNameTh.equals("") && positionNameEn.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(positionDto.getP()), PageableUtils.result(positionDto.getResult()), Sort.by("positionId").ascending());
            Page<PositionEntity> list = positionRepo.findAllByPositionNameThContainingAndRecordStatusNotIn(positionNameTh, recordStatus, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (positionNameTh.equals("") && !positionNameEn.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(positionDto.getP()), PageableUtils.result(positionDto.getResult()), Sort.by("positionId").ascending());
            Page<PositionEntity> list = positionRepo.findAllByPositionNameEnContainingAndRecordStatusNotIn(positionNameEn, recordStatus, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }
        if (!positionNameTh.equals("") && !positionNameEn.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(positionDto.getP()), PageableUtils.result(positionDto.getResult()), Sort.by("positionId").ascending());
            Page<PositionEntity> list = positionRepo.findAllByPositionNameThContainingAndPositionNameEnContainingAndRecordStatusNotIn(positionNameTh, positionNameEn, recordStatus, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;

    }

    public ResponseOutput insert(PositionDto positionDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            PositionEntity entity = modelMapper.map(positionDto, PositionEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedDate(new Date());
//                        entity.setCreatedUser(positionDto.getUserName());

            positionRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(PositionDto positionDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            PositionEntity entity = positionRepo.findById(ValiableUtils.LongIsZero(positionDto.getPositionId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            modelMapper.map(positionDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(positionDto.getUserName());

            positionRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(PositionDto positionDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            PositionEntity entity = positionRepo.findById(ValiableUtils.LongIsZero(positionDto.getPositionId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Set **
             */
//                        entity.setRecordStatus(RecordStatus.DELETE.getCode());
            entity.setRecordStatus("D");
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(positionDto.getUpdatedUser());

            positionRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

}
