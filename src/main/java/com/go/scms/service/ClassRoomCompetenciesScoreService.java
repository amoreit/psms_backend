/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ClassRoomCompetenciesScoreDto;
import com.go.scms.entity.ClassRoomCompetenciesScoreEntity;
import com.go.scms.repository.ClassRoomCompetenciesScoreRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassRoomCompetenciesScoreService {

    @Autowired
    private ClassRoomCompetenciesScoreRepository classRoomCompetenciesScoreRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput update(String userName, ClassRoomCompetenciesScoreDto[] classRoomCompetenciesScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (ClassRoomCompetenciesScoreDto map : classRoomCompetenciesScoreDto) {
                ClassRoomCompetenciesScoreEntity entity = modelMapper.map(map, ClassRoomCompetenciesScoreEntity.class);
                if (map.getClassRoomCompetenciesScoreId() == null) {
                    entity.setCreatedUser(userName);
                    classRoomCompetenciesScoreRepo.save(entity);
                } else {
                    classRoomCompetenciesScoreRepo.updatecompetenciesscore(Integer.parseInt(map.getScore1()), Integer.parseInt(map.getScore2()), Integer.parseInt(map.getScore3()), Integer.parseInt(map.getScore4()), Integer.parseInt(map.getScore5()), RecordStatus.UPDATE.getCode(), userName, Long.parseLong(map.getClassRoomCompetenciesScoreId()));
                }
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }
}
