/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.TeacherProfileDto;
import com.go.scms.entity.DepartmentEntity;
import com.go.scms.entity.SnickClassRoomEntity;
import com.go.scms.entity.TbMstCountryEntity;
import com.go.scms.entity.TeacherProfileEntity;
import com.go.scms.mail.MailInput;
import com.go.scms.mail.MailService;
import com.go.scms.repository.TbMstCountryRepository;
import com.go.scms.repository.TeacherProfileRepository;
import com.go.scms.response.ResponseException;

import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class TeacherProfileService {
    
        @Autowired
        private TeacherProfileRepository teacherProfileRepo;

        @Autowired
        private ModelMapper modelMapper;

        @Autowired
        private TbMstCountryRepository countryRepo;


        public ResponseOutput search(TeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String firstnameTh = ValiableUtils.stringIsEmpty(teacherProfileDto.getFirstnameTh().trim());
                String lastnameTh = ValiableUtils.stringIsEmpty(teacherProfileDto.getLastnameTh().trim());
                Integer departmentId =(int)ValiableUtils.IntIsZero(teacherProfileDto.getDepartmentId());
                Integer classId =(int)ValiableUtils.IntIsZero(teacherProfileDto.getClassId());
                String recordStatus = "D";

                if(firstnameTh.equals("") && lastnameTh.equals("") && classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.show(pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(!firstnameTh.equals("") && lastnameTh.equals("") && classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstname(firstnameTh,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(firstnameTh.equals("") && !lastnameTh.equals("") && classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.lastname(lastnameTh,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(!firstnameTh.equals("") && !lastnameTh.equals("") && classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstnameLastname(firstnameTh,lastnameTh,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(firstnameTh.equals("") && lastnameTh.equals("") && !classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.classId(classId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(firstnameTh.equals("") && lastnameTh.equals("") && !classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.departmentIdClassId(departmentId,classId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(!firstnameTh.equals("") && !lastnameTh.equals("") && !classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.lastnameClassIdDepartmentIdFirstname(lastnameTh, classId, departmentId, firstnameTh,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(firstnameTh.equals("") && lastnameTh.equals("") && classId.equals(0) && !departmentId.equals(0)){
                   Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                   Page<Map> list = teacherProfileRepo.departmentId(departmentId,pageable);
                   /*** Set ***/
                   output.setResponseData(list.getContent());
                   output.setResponseSize(list.getTotalElements());
                }

                if(!firstnameTh.equals("") && lastnameTh.equals("") && !classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstclassId(firstnameTh,classId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }  

                if(!firstnameTh.equals("") && lastnameTh.equals("") && classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstdepartmentId(firstnameTh,departmentId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                } 

                if(!firstnameTh.equals("") && lastnameTh.equals("") && !classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstclassIdDepartmentId(firstnameTh,classId,departmentId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }  

                if(firstnameTh.equals("") && !lastnameTh.equals("") && !classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.lastnameClassId(lastnameTh,classId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }  

                if(firstnameTh.equals("") && !lastnameTh.equals("") && classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.lastnameDepartmentId(lastnameTh,departmentId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                } 

                if(firstnameTh.equals("") && !lastnameTh.equals("") && !classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.lastnameClassIdDepartmentId(lastnameTh,classId,departmentId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }  

                if(!firstnameTh.equals("") && !lastnameTh.equals("") && !classId.equals(0) && departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstLastClassId(firstnameTh,lastnameTh,classId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }

                if(!firstnameTh.equals("") && !lastnameTh.equals("") && classId.equals(0) && !departmentId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacher_id").ascending());
                    Page<Map> list = teacherProfileRepo.firstLastDepartmentId(firstnameTh,lastnameTh,departmentId,pageable);
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }  

                return output;
        }

        public ResponseOutput searchDepartment(TeacherProfileDto teacherProfileDto) {
           ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

           Integer department = (int) ValiableUtils.IntIsZero(teacherProfileDto.getDepartmentId());
           List<Map> list = teacherProfileRepo.finddepartment(department);

           output.setResponseData(list);
           return output;
        }

        public ResponseOutput searchClass(TeacherProfileDto teacherProfileDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            Integer classId = (int) ValiableUtils.IntIsZero(teacherProfileDto.getClassId());
            List<Map> list = teacherProfileRepo.findclass(classId);

            output.setResponseData(list);
            return output;

        }

        public ResponseOutput searchClassRoom(TeacherProfileDto teacherProfileDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            Integer classRoomId = (int) ValiableUtils.IntIsZero(teacherProfileDto.getClassRoomId());
            List<Map> list = teacherProfileRepo.findclassroom(classRoomId);

            output.setResponseData(list);
            return output;

        }
        
        public ResponseOutput searchclassidfromteacherprofile(TeacherProfileDto teacherProfileDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            String citizenId = ValiableUtils.stringIsEmpty(teacherProfileDto.getCitizenId());
            List<Map> list = teacherProfileRepo.findclassidfromteacherprofile(citizenId);

            output.setResponseData(list);
            return output;

        }

        public ResponseOutput findById(Long id) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        output.setResponseData(teacherProfileRepo.findById(id).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND)));
                }catch(ResponseException ex) {
                        output.setResponseCode(ex.getCode());
                        output.setResponseMsg(ex.getMessage());
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }

        public ResponseOutput update(TeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check role by id ***/
                        TeacherProfileEntity entity = teacherProfileRepo.findById(ValiableUtils.LongIsZero(teacherProfileDto.getTeacherId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Merge datTeacherProfileEntitya ***/
                        modelMapper.map(teacherProfileDto,entity);

                        /*** Set ***/
                        entity.setRecordStatus("U");
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(teacherProfileDto.getUserName());

                        teacherProfileRepo.save(entity);

                }catch(ResponseException ex) {
                        output.setResponseCode(ex.getCode());
                        output.setResponseMsg(ex.getMessage());
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }    

        public ResponseOutput insert(TeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Merge data ***/
                        TeacherProfileEntity entity = modelMapper.map(teacherProfileDto,TeacherProfileEntity.class);
                        entity.setClassRoomId(null);
                        entity.setRecordStatus("A");
                        entity.setCreatedUser(teacherProfileDto.getUserName());

                        teacherProfileRepo.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }

        public ResponseOutput delete(TeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check role by id ***/
                        TeacherProfileEntity entity = teacherProfileRepo.findById(ValiableUtils.LongIsZero(teacherProfileDto.getTeacherId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Set ***/
                        entity.setRecordStatus("D");
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(teacherProfileDto.getUpdatedUser());

                        teacherProfileRepo.save(entity);
                }catch(ResponseException ex) {
                        output.setResponseCode(ex.getCode());
                        output.setResponseMsg(ex.getMessage());
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
}
