/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbMstSchoolProfileDto;
import com.go.scms.entity.TbMstSchoolProfileEntity;
import com.go.scms.repository.TbMstSchoolProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kittipong
 */
@Service
public class TbMstSchoolProfileService {
    
        @Autowired
        private ModelMapper modelMapper;

        @Autowired
        private TbMstSchoolProfileRepository tbMstSchoolProfileRepository;

        public ResponseOutput search(TbMstSchoolProfileDto tbMstSchoolProfileDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            String scCode = ValiableUtils.stringIsEmpty(tbMstSchoolProfileDto.getScCode()).trim();
            List list = tbMstSchoolProfileRepository.findScProfile(scCode).stream().map(e -> {
                SchoolProfile m = new SchoolProfile();
                m.setSchoolProfileId(e.get("school_profile_id"));
                m.setScCode(e.get("sc_code"));
                m.setScName(e.get("sc_name"));
                m.setScNameEn(e.get("sc_name_en"));
                m.setScType(e.get("sc_type"));
                m.setScRegisterAuthorized(e.get("sc_tegister_authorized"));
                m.setScRegisterNo(e.get("sc_register_no"));
                m.setScDoe(e.get("sc_doe"));
                m.setScMotto(e.get("sc_motto"));
                m.setScVision(e.get("sc_vision"));
                m.setScMission(e.get("sc_mission"));
                m.setScAbout(e.get("sc_about"));
                m.setScLogo(e.get("sc_logo"));
                m.setScAnnounce(e.get("sc_announce"));
                m.setScAnnounceStaff(e.get("sc_announce_staff"));
                m.setScAnnounceStudent(e.get("sc_announce_student"));
                m.setScBankAccount(e.get("sc_bank_account"));
                m.setAddrNo(e.get("addr_no"));
                m.setVillage(e.get("village"));
                m.setVillageNo(e.get("village_no"));
                m.setAlley(e.get("alley"));
                m.setRoad(e.get("road"));
                m.setSubDistrict(e.get("sub_district"));
                m.setDistrict(e.get("district"));
                m.setProvince(e.get("province"));
                m.setCountry(e.get("country"));
                m.setPostCode(e.get("post_code"));
                m.setTel(e.get("tel"));
                m.setTel2(e.get("tel2"));
                m.setTel3(e.get("tel3"));
                m.setFax(e.get("fax"));
                m.setEmail(e.get("email"));
                m.setEmail2(e.get("email2"));
                m.setEmail3(e.get("email3"));
                m.setWebsite(e.get("website"));
                m.setFacebook(e.get("facebook"));
                m.setTwitter(e.get("twitter"));
                return m;
            }).collect(Collectors.toList());
            output.setResponseData(list);
            return output;
        }

        public ResponseOutput update(TbMstSchoolProfileDto tbMstSchoolProfileDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                //Check class_room_member_id 
                TbMstSchoolProfileEntity entity = tbMstSchoolProfileRepository.findById(ValiableUtils.LongIsZero(tbMstSchoolProfileDto.getSchoolProfileId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

                // Merge data 
                modelMapper.map(tbMstSchoolProfileDto, entity);

                // Set 
                entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                entity.setUpdatedUser(tbMstSchoolProfileDto.getUserName());

                tbMstSchoolProfileRepository.save(entity);
            } catch (ResponseException ex) {
                output.setResponseCode(ex.getCode());
                output.setResponseMsg(ex.getMessage());
            } catch (ConstraintViolationException e) {
                return ExceptionUtils.validate(e);
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
        }
}

class SchoolProfile{
    
    private String schoolProfileId;
    private String scCode;
    private String scName;
    private String scNameEn;
    private String scType;
    private String scRegisterAuthorized;
    private String scRegisterNo;
    private String scDoe;
    private String scMotto;
    private String scVision;
    private String scMission;
    private String scAbout;
    private String scLogo;
    private String scAnnounce;
    private String scAnnounceStaff;
    private String scAnnounceStudent;
    private String scBankAccount;
    private String addrNo;
    private String village;
    private String villageNo;
    private String alley;
    private String road;
    private String subDistrict;
    private String district;
    private String province;
    private String country;
    private String postCode;
    private String tel;
    private String tel2;
    private String tel3;
    private String fax;
    private String email;
    private String email2;
    private String email3;
    private String website;
    private String facebook;
    private String twitter;

    public String getSchoolProfileId() {
        return schoolProfileId;
    }

    public void setSchoolProfileId(String schoolProfileId) {
        this.schoolProfileId = schoolProfileId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getScName() {
        return scName;
    }

    public void setScName(String scName) {
        this.scName = scName;
    }

    public String getScNameEn() {
        return scNameEn;
    }

    public void setScNameEn(String scNameEn) {
        this.scNameEn = scNameEn;
    }

    public String getScType() {
        return scType;
    }

    public void setScType(String scType) {
        this.scType = scType;
    }

    public String getScRegisterAuthorized() {
        return scRegisterAuthorized;
    }

    public void setScRegisterAuthorized(String scRegisterAuthorized) {
        this.scRegisterAuthorized = scRegisterAuthorized;
    }

    public String getScRegisterNo() {
        return scRegisterNo;
    }

    public void setScRegisterNo(String scRegisterNo) {
        this.scRegisterNo = scRegisterNo;
    }

    public String getScDoe() {
        return scDoe;
    }

    public void setScDoe(String scDoe) {
        this.scDoe = scDoe;
    }

    public String getScMotto() {
        return scMotto;
    }

    public void setScMotto(String scMotto) {
        this.scMotto = scMotto;
    }

    public String getScVision() {
        return scVision;
    }

    public void setScVision(String scVision) {
        this.scVision = scVision;
    }

    public String getScMission() {
        return scMission;
    }

    public void setScMission(String scMission) {
        this.scMission = scMission;
    }

    public String getScAbout() {
        return scAbout;
    }

    public void setScAbout(String scAbout) {
        this.scAbout = scAbout;
    }

    public String getScLogo() {
        return scLogo;
    }

    public void setScLogo(String scLogo) {
        this.scLogo = scLogo;
    }

    public String getScAnnounce() {
        return scAnnounce;
    }

    public void setScAnnounce(String scAnnounce) {
        this.scAnnounce = scAnnounce;
    }

    public String getScAnnounceStaff() {
        return scAnnounceStaff;
    }

    public void setScAnnounceStaff(String scAnnounceStaff) {
        this.scAnnounceStaff = scAnnounceStaff;
    }

    public String getScAnnounceStudent() {
        return scAnnounceStudent;
    }

    public void setScAnnounceStudent(String scAnnounceStudent) {
        this.scAnnounceStudent = scAnnounceStudent;
    }

    public String getScBankAccount() {
        return scBankAccount;
    }

    public void setScBankAccount(String scBankAccount) {
        this.scBankAccount = scBankAccount;
    }

    public String getAddrNo() {
        return addrNo;
    }

    public void setAddrNo(String addrNo) {
        this.addrNo = addrNo;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getVillageNo() {
        return villageNo;
    }

    public void setVillageNo(String villageNo) {
        this.villageNo = villageNo;
    }

    public String getAlley() {
        return alley;
    }

    public void setAlley(String alley) {
        this.alley = alley;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getTel3() {
        return tel3;
    }

    public void setTel3(String tel3) {
        this.tel3 = tel3;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getEmail3() {
        return email3;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    } 
}
