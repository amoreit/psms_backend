/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ActivityGroupDto;
import com.go.scms.dto.ScoreDto;
import com.go.scms.entity.ActivityGroupEntity;
import com.go.scms.entity.ScoreEntity;
import com.go.scms.repository.ActivityGroupRepository;
import com.go.scms.repository.ScoreRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ScoreService {
     @Autowired
    private ScoreRepository scoreRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(ScoreDto scoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String scoreName = ValiableUtils.stringIsEmpty(scoreDto.getScoreName());
        String recordStatus = "D";

            Pageable pageable = PageRequest.of(PageableUtils.offset(scoreDto.getP()), PageableUtils.result(scoreDto.getResult()));
            Page<ScoreEntity> list = scoreRepo.findAllByRecordStatusNotInAndScoreNameContainingOrderByScoreId(recordStatus,scoreName, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());

        return output;

    }

    public ResponseOutput findById(Long id) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            output.setResponseData(scoreRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(), "Not found data")));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insert(ScoreDto scoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            ScoreEntity entity = modelMapper.map(scoreDto, ScoreEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedUser(scoreDto.getUserName());
            entity.setCreatedDate(new Date());

            scoreRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(ScoreDto scoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            ScoreEntity entity = scoreRepo.findById(ValiableUtils.LongIsZero(scoreDto.getScoreId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            modelMapper.map(scoreDto, entity);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(scoreDto.getUserName());
            entity.setUpdatedDate(new Date());
            scoreRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(ScoreDto scoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            ScoreEntity entity = scoreRepo.findById(ValiableUtils.LongIsZero(scoreDto.getScoreId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));
            entity.setRecordStatus(RecordStatus.DELETE.getCode());
            scoreRepo.save(entity);

        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}
