package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickClassRoomMemberDto;
import com.go.scms.entity.ClassRoomEntity;
import com.go.scms.entity.SnickClassRoomMemberEntity;
import com.go.scms.entity.SnickTbStuProfileEntity;
import com.go.scms.entity.SnickTeacherProfileEntity;
import com.go.scms.entity.YearGroupEntity;
import com.go.scms.repository.ClassRoomRepository;
import com.go.scms.repository.SnickClassRoomMemberRepository;
import com.go.scms.repository.SnickMstClassRepository;
import com.go.scms.repository.SnickTeacherProfileRepository;
import com.go.scms.repository.YearGroupRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickClassRoomMemberService {
    
        @Autowired
	private SnickClassRoomMemberRepository tbTrnClassRoomMemberRepo;
        
        @Autowired
	private SnickTeacherProfileRepository teacherProfileRepo;
        
        @Autowired
	private YearGroupRepository yearGroupRepo;
        
        @Autowired
	private ClassRoomRepository classRoomRepo;
        
	@Autowired
	private ModelMapper modelMapper;
        
        public ResponseOutput insert(SnickClassRoomMemberDto tbTrnClassRoomMemberDto) throws Throwable {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                        /*** Check id ***/
                        SnickTeacherProfileEntity teacherId = teacherProfileRepo.findByTeacherId(tbTrnClassRoomMemberDto.getMemberId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        YearGroupEntity yearGroupId = yearGroupRepo.findByYearGroupId(tbTrnClassRoomMemberDto.getYearGroupId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        ClassRoomEntity classRoomId = classRoomRepo.findByClassRoomId(tbTrnClassRoomMemberDto.getClassRoomId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

                        /*** Merge data ***/
                        SnickClassRoomMemberEntity entity = modelMapper.map(tbTrnClassRoomMemberDto,SnickClassRoomMemberEntity.class);
                        entity.setTeacherJoin(teacherId);
                        entity.setYearGroupJoin(yearGroupId);
                        entity.setClassRoomJoin(classRoomId);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
                        entity.setCreatedUser(tbTrnClassRoomMemberDto.getUserName());

                        tbTrnClassRoomMemberRepo.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput update(SnickClassRoomMemberDto tbTrnClassRoomMemberDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {			
			/*** Check id ***/
			SnickClassRoomMemberEntity entity = tbTrnClassRoomMemberRepo.findById(ValiableUtils.LongIsZero(tbTrnClassRoomMemberDto.getClassRoomMemberId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        SnickTeacherProfileEntity teacherId = teacherProfileRepo.findByTeacherId(tbTrnClassRoomMemberDto.getMemberId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        YearGroupEntity yearGroupId = yearGroupRepo.findByYearGroupId(tbTrnClassRoomMemberDto.getYearGroupId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        ClassRoomEntity classRoomId = classRoomRepo.findByClassRoomId(tbTrnClassRoomMemberDto.getClassRoomId()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                        
                        /*** Merge data ***/
			modelMapper.map(tbTrnClassRoomMemberDto,entity);

			/*** Set ***/
                        entity.setTeacherJoin(teacherId);
                        entity.setYearGroupJoin(yearGroupId);
                        entity.setClassRoomJoin(classRoomId);
			entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(tbTrnClassRoomMemberDto.getUserName());
			
			tbTrnClassRoomMemberRepo.save(entity);
                                 
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
}
