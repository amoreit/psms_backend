/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbStuTranscriptLogDto;
import com.go.scms.entity.TbStuTranscriptLogEntity;
import com.go.scms.repository.TbStuTranscriptLogRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kittipong
 */
@Service
public class TbStuTranscriptLogService {
    
        @Autowired
        private TbStuTranscriptLogRepository tbStuTranscriptLogRepository;

        @Autowired
        private ModelMapper modelMapper;
     
        public ResponseOutput search(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
            Integer doctypeId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getDoctypeId());
            String setNo = ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getSetNo());
            String seqNo = ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getSeqNo());
            List<Map> list = tbStuTranscriptLogRepository.checkSetNoSeqNo(doctypeId, setNo, seqNo);
            output.setResponseData(list);
            return output;
        }
      
        public ResponseOutput searchUpdate(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
            Integer doctypeId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getDoctypeId());
            Long stuTranscriptLogId = Long.parseLong(ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getStuTranscriptLogId()));
            String setNo = ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getSetNo());
            String seqNo = ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getSeqNo());
            List<Map> list = tbStuTranscriptLogRepository.checkUpdataSetNoSeqNo(doctypeId, setNo, seqNo, stuTranscriptLogId);
            output.setResponseData(list);
            return output;
        }
     
        public ResponseOutput insert(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                        /*** Merge data ***/
                        TbStuTranscriptLogEntity entity = modelMapper.map(tbStuTranscriptLogDto,TbStuTranscriptLogEntity.class);
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
                        entity.setCreatedDate(new Date());
                        entity.setCreatedUser(tbStuTranscriptLogDto.getUserName());

                        tbStuTranscriptLogRepository.save(entity);
                }catch(ConstraintViolationException e) {
                        return ExceptionUtils.validate(e);
                }catch(Exception ex) {
                        return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;  
        }
      
        public ResponseOutput update(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {			
			/*** Check subject ***/
			TbStuTranscriptLogEntity entity = tbStuTranscriptLogRepository.findById(ValiableUtils.LongIsZero(tbStuTranscriptLogDto.getStuTranscriptLogId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
                        /*** Merge data ***/
			modelMapper.map(tbStuTranscriptLogDto,entity);

			/*** Set ***/
			entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                        entity.setUpdatedUser(tbStuTranscriptLogDto.getUserName());
			
			tbStuTranscriptLogRepository.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
                return output;
        }
      
        public ResponseOutput searchCheck(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
            Integer doctypeId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getDoctypeId());
            String setNo = ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getSetNo());
            Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getYearGroupId());
            List<Map> list = tbStuTranscriptLogRepository.checkSetNo(doctypeId, setNo,yearGroupId);
            output.setResponseData(list);
            return output;

        }
      
        public ResponseOutput searchUpdateCheck(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
            Integer doctypeId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getDoctypeId());
            Long stuTranscriptLogId = Long.parseLong(ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getStuTranscriptLogId()));
            Integer yearGroupId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getYearGroupId());
            String setNo = ValiableUtils.stringIsEmpty(tbStuTranscriptLogDto.getSetNo());
            List<Map> list = tbStuTranscriptLogRepository.checkUpdataSetNo(doctypeId, setNo,yearGroupId, stuTranscriptLogId);
            output.setResponseData(list);
            return output;
        }
      
        //aong
        public ResponseOutput TranscriptDate(TbStuTranscriptLogDto tbStuTranscriptLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        
                Integer stuTranscriptLogId = (int) ValiableUtils.IntIsZero(tbStuTranscriptLogDto.getStuTranscriptLogId());
                List<Map> list = tbStuTranscriptLogRepository.TranscriptDate(stuTranscriptLogId);
                output.setResponseData(list);
                return output;
        }
    
}
