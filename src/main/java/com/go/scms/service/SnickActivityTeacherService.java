package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickActivityTeacherDto;
import com.go.scms.entity.SnickActivityTeacherEntity;
import com.go.scms.repository.SnickActivityTeacherRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnickActivityTeacherService {
    
        @Autowired
        private SnickActivityTeacherRepository activityTeacherRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        //---------------------------- aca004 search teacher
        public ResponseOutput search(SnickActivityTeacherDto activityTeacherDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(activityTeacherDto.getYearGroupName());
                Integer classId = (int) ValiableUtils.IntIsZero(activityTeacherDto.getClassId());
                Integer activityGroupId = (int) ValiableUtils.IntIsZero(activityTeacherDto.getActivityGroupId());
                String activityName = ValiableUtils.stringIsEmpty(activityTeacherDto.getActivityName());
                String recordStatus = "D";
                
                List<Map> list = activityTeacherRepo.findall(yearGroupName, classId, activityGroupId, activityName, recordStatus);
                output.setResponseData(list);
                    
                return output;
        }
        
        public ResponseOutput insert(SnickActivityTeacherDto activityTeacherDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
                       
			/*** Merge data ***/
			SnickActivityTeacherEntity entity = modelMapper.map(activityTeacherDto,SnickActivityTeacherEntity.class);
                        entity.setScCode("PRAMANDA");
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
			entity.setCreatedUser(activityTeacherDto.getUserName());
			
			activityTeacherRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
    
        public ResponseOutput delete(SnickActivityTeacherDto activityTeacherDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    
                    String activityTeacherId = ValiableUtils.stringIsEmpty(activityTeacherDto.getActivityTeacherId()); 
                    activityTeacherRepo.deleteActivityTeacher(Long.parseLong(activityTeacherId));
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        //---------------------------- aca004 search teacher report
        public ResponseOutput searchActivityTeaReport(SnickActivityTeacherDto activityTeacherDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(activityTeacherDto.getYearGroupName());
                Integer activityGroupId = (int) ValiableUtils.IntIsZero(activityTeacherDto.getActivityGroupId());
                String activityName = ValiableUtils.stringIsEmpty(activityTeacherDto.getActivityName());
                
                List<Map> list = activityTeacherRepo.findallreport(yearGroupName, activityGroupId, activityName);
                output.setResponseData(list);
                    
                return output;
        }
}
