/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.dto.OnetScoreDto;
import com.go.scms.dto.TbTrnClassRoomSubjectScoreDto;
import com.go.scms.repository.OnetSocreRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kittipong
 */
@Service
public class OnetScoreService {
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired
    private OnetSocreRepository onetSocreRepository;
    
    public ResponseOutput getOnetScore(OnetScoreDto onetScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(onetScoreDto.getStuCode()).trim();
        List list = onetSocreRepository.getOnetScore(stuCode);
        output.setResponseData(list);
        return output;
    }
    
}
