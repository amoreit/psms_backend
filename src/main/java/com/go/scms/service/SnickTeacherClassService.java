package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTeacherClassDto;
import com.go.scms.entity.SnickTeacherClassEntity;
import com.go.scms.repository.SnickTeacherClassRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SnickTeacherClassService {
    
        @Autowired
        private SnickTeacherClassRepository teacherClassRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput search(SnickTeacherClassDto teacherClassDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(teacherClassDto.getYearGroupName());
                Integer catClassId = (int) ValiableUtils.IntIsZero(teacherClassDto.getCatClassId());
                String className = ValiableUtils.stringIsEmpty(teacherClassDto.getClassName());
                String recordStatus = "D";
               
                if(yearGroupName.equals("") && catClassId.equals(0) && className.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassDto.getP()),PageableUtils.result(teacherClassDto.getResult()),Sort.by("catClassId","classId","teacherClassId").ascending());
                    Page<SnickTeacherClassEntity> list = teacherClassRepo.findAllByYearGroupName(yearGroupName, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(yearGroupName.equals("") && !catClassId.equals(0) && className.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassDto.getP()),PageableUtils.result(teacherClassDto.getResult()),Sort.by("catClassId","classId","teacherClassId").ascending());
                    Page<SnickTeacherClassEntity> list = teacherClassRepo.findAllByCatClassIdAndRecordStatusNotIn(catClassId, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(yearGroupName.equals("") && !catClassId.equals(0) && !className.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassDto.getP()),PageableUtils.result(teacherClassDto.getResult()),Sort.by("catClassId","classId","teacherClassId").ascending());
                    Page<SnickTeacherClassEntity> list = teacherClassRepo.findAllByCatClassIdAndClassNameAndRecordStatusNotIn(catClassId, className, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!yearGroupName.equals("") && catClassId.equals(0) && className.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassDto.getP()),PageableUtils.result(teacherClassDto.getResult()),Sort.by("catClassId","classId","teacherClassId").ascending());
                    Page<SnickTeacherClassEntity> list = teacherClassRepo.findAllByYearGroupNameAndRecordStatusNotIn(yearGroupName, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!yearGroupName.equals("") && !catClassId.equals(0) && className.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassDto.getP()),PageableUtils.result(teacherClassDto.getResult()),Sort.by("catClassId","classId","teacherClassId").ascending());
                    Page<SnickTeacherClassEntity> list = teacherClassRepo.findAllByYearGroupNameAndCatClassIdAndRecordStatusNotIn(yearGroupName, catClassId, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!yearGroupName.equals("") && !catClassId.equals(0) && !className.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherClassDto.getP()),PageableUtils.result(teacherClassDto.getResult()),Sort.by("catClassId","classId","teacherClassId").ascending());
                    Page<SnickTeacherClassEntity> list = teacherClassRepo.findAllByYearGroupNameAndCatClassIdAndClassNameAndRecordStatusNotIn(yearGroupName, catClassId, className, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                return output;
        }
        
        public ResponseOutput searchLeader(SnickTeacherClassDto teacherClassDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(teacherClassDto.getYearGroupName().trim()); 
                Integer catClassId = (int) ValiableUtils.IntIsZero(teacherClassDto.getCatClassId());
                String className = ValiableUtils.stringIsEmpty(teacherClassDto.getClassName());
                String recordStatus = "D";
                
                List list = teacherClassRepo.findAllByYearGroupNameAndCatClassIdAndClassNameAndRecordStatusNotInOrderByTeacherClassIdAsc(yearGroupName, catClassId, className, recordStatus);
                output.setResponseData(list);
                
                return output;
        }
        
        public ResponseOutput insert(SnickTeacherClassDto teacherClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {                       
			/*** Merge data ***/
			SnickTeacherClassEntity entity = modelMapper.map(teacherClassDto,SnickTeacherClassEntity.class);
                        entity.setScCode("PRAMANDA");
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
			entity.setCreatedUser(teacherClassDto.getUserName());
			
			teacherClassRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput update(SnickTeacherClassDto teacherClassDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check id ***/
			SnickTeacherClassEntity entity = teacherClassRepo.findById(ValiableUtils.LongIsZero(teacherClassDto.getTeacherClassId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));

			/*** Merge data ***/
			modelMapper.map(teacherClassDto,entity);

			/*** Set ***/
                        entity.setRecordStatus(RecordStatus.UPDATE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(teacherClassDto.getUserName());
			
			teacherClassRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updateLeader(String userName, SnickTeacherClassDto[] teacherClassDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {                   
                    /*** Loop update data ***/
                    for (SnickTeacherClassDto map : teacherClassDto) {
                        SnickTeacherClassEntity entity = modelMapper.map(map, SnickTeacherClassEntity.class);
                        teacherClassRepo.updateLeader(
                                (map.getLeader()),
                                 userName,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getTeacherClassId())));
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput updateTrue(SnickTeacherClassDto teacherClassDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** Check id ***/
                    SnickTeacherClassEntity entity = teacherClassRepo.findById(ValiableUtils.LongIsZero(teacherClassDto.getTeacherClassId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

                    /*** Merge data ***/
                    modelMapper.map(teacherClassDto, entity);

                    /*** Set ***/
                    entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                    entity.setLeader(true);
                    entity.setUpdatedDate(new Date());
                    entity.setUpdatedUser(teacherClassDto.getUserName());
                    
                    teacherClassRepo.save(entity);
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput delete(SnickTeacherClassDto teacherClassDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    String teacherClassId = ValiableUtils.stringIsEmpty(teacherClassDto.getTeacherClassId()); 
                    teacherClassRepo.deleteTeacherClass(Long.parseLong(teacherClassId));
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
}
