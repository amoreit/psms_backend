/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TbMstAuthorizedDto;
import com.go.scms.entity.TbMstAuthorizedEntity;
import com.go.scms.repository.TbMstAuthorizedRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author kronn
 */
@Service
public class TbMstAuthorizedService {

    @Autowired
    private TbMstAuthorizedRepository tbMstAuthorizedRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String position = ValiableUtils.stringIsEmpty(tbMstAuthorizedDto.getPosition().trim());
        String fullnameTh = ValiableUtils.stringIsEmpty(tbMstAuthorizedDto.getFullnameTh().trim());
        String fullnameEn = ValiableUtils.stringIsEmpty(tbMstAuthorizedDto.getFullnameEn().trim());
        String recordStatus = "D";

        if (!position.equals("")) {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstAuthorizedDto.getP()), PageableUtils.result(tbMstAuthorizedDto.getResult()), Sort.by("authorizedId").ascending());
            Page<TbMstAuthorizedEntity> list = tbMstAuthorizedRepo.findAllByFullnameThContainingAndFullnameEnContainingAndPositionAndRecordStatusNotIn(fullnameTh, fullnameEn, position, recordStatus, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        } else {
            Pageable pageable = PageRequest.of(PageableUtils.offset(tbMstAuthorizedDto.getP()), PageableUtils.result(tbMstAuthorizedDto.getResult()), Sort.by("authorizedId").ascending());
            Page<TbMstAuthorizedEntity> list = tbMstAuthorizedRepo.findAllByFullnameThContainingAndFullnameEnContainingAndRecordStatusNotIn(fullnameTh, fullnameEn, recordStatus, pageable);
            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
        }

        return output;
    }

    public ResponseOutput insert(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            TbMstAuthorizedEntity entity = modelMapper.map(tbMstAuthorizedDto, TbMstAuthorizedEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedDate(new Date());
            entity.setCreatedUser(tbMstAuthorizedDto.getUserName());

            tbMstAuthorizedRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insertIscurrent(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Merge data **
             */
            TbMstAuthorizedEntity entity = modelMapper.map(tbMstAuthorizedDto, TbMstAuthorizedEntity.class);
            entity.setRecordStatus(RecordStatus.SAVE.getCode());
            entity.setCreatedDate(new Date());
            entity.setCreatedUser(tbMstAuthorizedDto.getUserName());

            tbMstAuthorizedRepo.changeiscurrent(false);

            tbMstAuthorizedRepo.save(entity);
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput update(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            TbMstAuthorizedEntity entity = tbMstAuthorizedRepo.findById(ValiableUtils.LongIsZero(tbMstAuthorizedDto.getAuthorizedId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            modelMapper.map(tbMstAuthorizedDto, entity);

            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(tbMstAuthorizedDto.getUserName());

            tbMstAuthorizedRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updateIscurrent(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            TbMstAuthorizedEntity entity = tbMstAuthorizedRepo.findById(ValiableUtils.LongIsZero(tbMstAuthorizedDto.getAuthorizedId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            modelMapper.map(tbMstAuthorizedDto, entity);
            entity.setRecordStatus(RecordStatus.UPDATE.getCode());
            entity.setUpdatedUser(tbMstAuthorizedDto.getUserName());

            tbMstAuthorizedRepo.changeiscurrent(false);
            tbMstAuthorizedRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput delete(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            /**
             * * Check role by id **
             */
            TbMstAuthorizedEntity entity = tbMstAuthorizedRepo.findById(ValiableUtils.LongIsZero(tbMstAuthorizedDto.getAuthorizedId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

            /**
             * * Set **
             */
            entity.setRecordStatus("D");
            entity.setUpdatedDate(new Date());
            entity.setUpdatedUser(tbMstAuthorizedDto.getUpdatedUser());

            tbMstAuthorizedRepo.save(entity);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    //Aong
    public ResponseOutput getPosition(TbMstAuthorizedDto tbMstAuthorizedDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        List list = tbMstAuthorizedRepo.getPosition();
        output.setResponseData(list);
        return output;
    }
}
