/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.ClassRoomLifeskillScoreDto;
import com.go.scms.entity.ClassRoomLifeskillScoreEntity;
import com.go.scms.repository.ClassRoomLifeskillScoreRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassRoomLifeskillScoreService {

    @Autowired
    private ClassRoomLifeskillScoreRepository classRoomLifeskillScoreRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput insert(String userName,ClassRoomLifeskillScoreDto[] classRoomLifeskillScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (ClassRoomLifeskillScoreDto map : classRoomLifeskillScoreDto) {
                ClassRoomLifeskillScoreEntity entity = modelMapper.map(map, ClassRoomLifeskillScoreEntity.class);
                if (map.getScore() == null) {
                    entity.setScore(0);
                }
                entity.setCreatedUser(userName);
                classRoomLifeskillScoreRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        
        return output;
    }

    public ResponseOutput update(String userName,ClassRoomLifeskillScoreDto[] classRoomLifeskillScoreDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (ClassRoomLifeskillScoreDto map : classRoomLifeskillScoreDto) {
                ClassRoomLifeskillScoreEntity entity = modelMapper.map(map, ClassRoomLifeskillScoreEntity.class);
                classRoomLifeskillScoreRepo.updatescore(Integer.parseInt(map.getScore()), RecordStatus.UPDATE.getCode(),userName, Long.parseLong(map.getClassRoomLifeskillScoreId()));
            }
            
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }
}
