package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTeacherProfileDto;
import com.go.scms.entity.SnickTeacherProfileEntity;
import com.go.scms.repository.SnickTeacherProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class SnickTeacherProfileService {
    
        @Autowired
        private SnickTeacherProfileRepository teacherProfileRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput search(SnickTeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String firstnameTh = ValiableUtils.stringIsEmpty(teacherProfileDto.getFirstnameTh().trim());
                String lastnameTh = ValiableUtils.stringIsEmpty(teacherProfileDto.getLastnameTh().trim());
                Integer classId = (int) ValiableUtils.IntIsZero(teacherProfileDto.getClassId());
                Integer classRoomId = (int) ValiableUtils.IntIsZero(teacherProfileDto.getClassRoomId());
                String recordStatus = "D";
                
                if(firstnameTh.equals("") && lastnameTh.equals("") && classId.equals(0) && classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByRecordStatusNotIn(recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!firstnameTh.equals("") && lastnameTh.equals("") && classId.equals(0) && classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByFirstnameThContainingAndRecordStatusNotIn(firstnameTh, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(firstnameTh.equals("") && !lastnameTh.equals("") && classId.equals(0) && classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByLastnameThContainingAndRecordStatusNotIn(lastnameTh, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!firstnameTh.equals("") && !lastnameTh.equals("") && classId.equals(0) && classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByFirstnameThContainingAndLastnameThContainingAndRecordStatusNotIn(firstnameTh, lastnameTh, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(firstnameTh.equals("") && lastnameTh.equals("") && !classId.equals(0) && classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByClassIdAndRecordStatusNotIn(classId, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(firstnameTh.equals("") && lastnameTh.equals("") && !classId.equals(0) && !classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByClassIdAndClassRoomIdAndRecordStatusNotIn(classId, classRoomId, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                if(!firstnameTh.equals("") && !lastnameTh.equals("") && !classId.equals(0) && !classRoomId.equals(0)){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByFirstnameThContainingAndLastnameThContainingAndClassIdAndClassRoomIdAndRecordStatusNotIn(firstnameTh, lastnameTh, classId, classRoomId, recordStatus, pageable);

                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                return output;
        }
        
        public ResponseOutput searchClassRoom(SnickTeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                Integer classId = (int) ValiableUtils.IntIsZero(teacherProfileDto.getClassId());
                String recordStatus = "D";
                
                Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByClassIdAndRecordStatusNotIn(classId, recordStatus, pageable);

                /*** Set ***/
                output.setResponseData(list.getContent());
                output.setResponseSize(list.getTotalElements());
                
                return output;
        }
        
        public ResponseOutput searchauto(SnickTeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String fullname = ValiableUtils.stringIsEmpty(teacherProfileDto.getFullname());
                String firstnameTh = ValiableUtils.stringIsEmpty(teacherProfileDto.getFirstnameTh().trim());
                String lastnameTh = ValiableUtils.stringIsEmpty(teacherProfileDto.getLastnameTh().trim());
                String firstnameEn = ValiableUtils.stringIsEmpty(teacherProfileDto.getFirstnameEn());
                String lastnameEn = ValiableUtils.stringIsEmpty(teacherProfileDto.getLastnameEn());
                String recordStatus = "D";
                
                if(!firstnameTh.equals("") || !lastnameTh.equals("") ||  !firstnameEn.equals("") || !lastnameEn.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByFirstnameThContainingAndLastnameThContainingAndFirstnameEnContainingAndLastnameEnContainingAndRecordStatusNotIn(firstnameTh ,lastnameTh ,firstnameEn, lastnameEn,recordStatus, pageable);
                
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                    
                if(firstnameTh.equals("") && lastnameTh.equals("") && firstnameEn.equals("") && lastnameEn.equals("")){
                    Pageable pageable = PageRequest.of(PageableUtils.offset(teacherProfileDto.getP()),PageableUtils.result(teacherProfileDto.getResult()),Sort.by("teacherId").ascending());
                    Page<SnickTeacherProfileEntity> list = teacherProfileRepo.findAllByRecordStatusNotIn(recordStatus, pageable);
                
                    /*** Set ***/
                    output.setResponseData(list.getContent());
                    output.setResponseSize(list.getTotalElements());
                }
                
                return output;
        }
        
        public ResponseOutput update(SnickTeacherProfileDto teacherProfileDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {			
			/*** Check id ***/
			SnickTeacherProfileEntity entity = teacherProfileRepo.findById(ValiableUtils.LongIsZero(teacherProfileDto.getTeacherId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
			
                        /*** Merge data ***/
			modelMapper.map(teacherProfileDto, entity);

			/*** Set ***/
			entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                        entity.setUpdatedDate(new Date());
                        entity.setUpdatedUser(teacherProfileDto.getUserName());
			
			teacherProfileRepo.save(entity);
                                 
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updateDepartment(String userName, SnickTeacherProfileDto[] teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {                  
                    /*** Loop update data ***/
                    for (SnickTeacherProfileDto map : teacherProfileDto) {
                        SnickTeacherProfileEntity entity = modelMapper.map(map, SnickTeacherProfileEntity.class);
                        teacherProfileRepo.updateDepartment(
                                (Integer.parseInt(map.getDepartmentId())),
                                (map.getDepartmentName()),
                                 userName,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getTeacherId())));
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput updateDeleteDepartment(String userNameDepartment, SnickTeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
                        String fullname = ValiableUtils.stringIsEmpty(teacherProfileDto.getFullname());
                        String updatedUser = userNameDepartment;
                        String recordStatus = "U";
                        Boolean isactive = true;
			
			teacherProfileRepo.updateDeleteDepartment(updatedUser, recordStatus, isactive, fullname);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
        }
        
        public ResponseOutput updateClass(String userNameClass, SnickTeacherProfileDto[] teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {                   
                    /*** Loop update data ***/
                    for (SnickTeacherProfileDto map : teacherProfileDto) {
                        SnickTeacherProfileEntity entity = modelMapper.map(map, SnickTeacherProfileEntity.class);
                        teacherProfileRepo.updateClass(
                                (map.getClassId()),
                                 userNameClass,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getTeacherId())));
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput updateDeleteClass(String userNameClass, SnickTeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
                        String fullname = ValiableUtils.stringIsEmpty(teacherProfileDto.getFullname());
                        String updatedUser = userNameClass;
                        String recordStatus = "U";
                        Boolean isactive = true;
			
			teacherProfileRepo.updateDeleteClass(updatedUser, recordStatus, isactive, fullname);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
        }
        
        public ResponseOutput updateClassRoom(String userNameClassRoom, SnickTeacherProfileDto[] teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {                  
                    /*** Loop update data ***/
                    for (SnickTeacherProfileDto map : teacherProfileDto) {
                        SnickTeacherProfileEntity entity = modelMapper.map(map, SnickTeacherProfileEntity.class);
                        teacherProfileRepo.updateClassRoom(
                                (map.getClassId()),
                                (map.getClassRoomId()),
                                 userNameClassRoom,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getTeacherId())));
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput updateDeleteClassRoom(String userNameClassRoom, SnickTeacherProfileDto teacherProfileDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Merge data ***/
                        String teacherCardId = ValiableUtils.stringIsEmpty(teacherProfileDto.getTeacherCardId());
                        String updatedUser = userNameClassRoom;
                        String recordStatus = "U";
                        Boolean isactive = true;
			
			teacherProfileRepo.updateDeleteClassRoom(updatedUser, recordStatus, isactive, teacherCardId);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
        }
}
