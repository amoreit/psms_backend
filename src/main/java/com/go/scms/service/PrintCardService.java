/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.PrintCardLogDto;
import com.go.scms.dto.StuProfileDto;
import com.go.scms.entity.PrintCardLogEntity;
import com.go.scms.repository.PrintCardLogRepository;
import com.go.scms.repository.StuProfileRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author P3rdiscoz
 */
@Service
public class PrintCardService {
    @Autowired
    private StuProfileRepository stuProfileRepo;
    
    @Autowired
    private PrintCardLogRepository printCardLogRepo;
    
    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput search(StuProfileDto stuProfileDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            String stuCode = ValiableUtils.stringIsEmpty(stuProfileDto.getStuCode());
            String firstnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getFirstnameTh());
            String lastnameTh = ValiableUtils.stringIsEmpty(stuProfileDto.getLastnameTh());
            String classId = ValiableUtils.stringIsEmpty(stuProfileDto.getClassId());
            String classRoomId = ValiableUtils.stringIsEmpty(stuProfileDto.getClassRoomId());

            /*** search all and citizenId,firstnameTh,lastnameTh,stuStatus ***/
            Pageable pageable = PageRequest.of(PageableUtils.offset(stuProfileDto.getP()), PageableUtils.result(stuProfileDto.getResult()));
            Page<Map> list = stuProfileRepo.findprintcardstu(classId,classRoomId,stuCode,firstnameTh,lastnameTh,pageable);

            output.setResponseData(list.getContent());
            output.setResponseSize(list.getTotalElements());
            return output;
    }
    
    public ResponseOutput searchitemno(Long id) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

            List<Map> list = printCardLogRepo.finditemno(id);
            output.setResponseData(list);
            return output;
    }
    
    public ResponseOutput findById(Long id) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
                output.setResponseData(stuProfileRepo.findById(id).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"Not found data")));
            } catch (ResponseException ex) {
                output.setResponseCode(ex.getCode());
                output.setResponseMsg(ex.getMessage());
            } catch (Exception ex) {
                return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
    }
     
    public ResponseOutput insert(PrintCardLogDto printCardLogDto) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {         
                    /*** Merge data ***/
                    PrintCardLogEntity entity = modelMapper.map(printCardLogDto,PrintCardLogEntity.class);
                    entity.setRecordStatus(RecordStatus.SAVE.getCode());
                    entity.setCreatedUser(printCardLogDto.getUserName());

                    printCardLogRepo.save(entity);
            }catch(ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
            }catch(Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
            }
            return output;
    }
}
