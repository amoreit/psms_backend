package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.SnickTeacherDepartmentDto;
import com.go.scms.entity.SnickTeacherDepartmentEntity;
import com.go.scms.repository.SnickTeacherDepartmentRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.PageableUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;  

@Service
public class SnickTeacherDepartmentService {
    
        @Autowired
        private SnickTeacherDepartmentRepository teacherDepartmentRepo;

        @Autowired
        private ModelMapper modelMapper;
        
        public ResponseOutput search(SnickTeacherDepartmentDto teacherDepartmentDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String yearGroupName = ValiableUtils.stringIsEmpty(teacherDepartmentDto.getYearGroupName().trim()); 
                Integer catClassId = (int) ValiableUtils.IntIsZero(teacherDepartmentDto.getCatClassId());
                String departmentName = ValiableUtils.stringIsEmpty(teacherDepartmentDto.getDepartmentName().trim());
                String recordStatus = "D";
                
                List<Map> list = teacherDepartmentRepo.findall(yearGroupName, catClassId, departmentName, recordStatus);
                output.setResponseData(list);
                
                return output;
        }
        
        public ResponseOutput searchLeader(SnickTeacherDepartmentDto teacherDepartmentDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

                /*** Search ***/
                String fullname = ValiableUtils.stringIsEmpty(teacherDepartmentDto.getFullname().trim()); 
                String yearGroupName = ValiableUtils.stringIsEmpty(teacherDepartmentDto.getYearGroupName().trim()); 
                Integer catClassId = (int) ValiableUtils.IntIsZero(teacherDepartmentDto.getCatClassId());
                String departmentName = ValiableUtils.stringIsEmpty(teacherDepartmentDto.getDepartmentName().trim());
                String recordStatus = "D";
                
                if(fullname.equals("")){
                    List list = teacherDepartmentRepo.findAllByYearGroupNameAndCatClassIdAndDepartmentNameAndRecordStatusNotInOrderByTeacherDepartmentIdAsc(yearGroupName, catClassId, departmentName, recordStatus);
                    output.setResponseData(list);
                }
                
                if(!fullname.equals("")){
                    List list = teacherDepartmentRepo.findAllByFullnameContainingAndYearGroupNameAndCatClassIdAndDepartmentNameAndRecordStatusNotInOrderByTeacherDepartmentIdAsc(fullname, yearGroupName, catClassId, departmentName, recordStatus);
                    output.setResponseData(list);
                }
                
                return output;
        }
        
        public ResponseOutput insert(SnickTeacherDepartmentDto teacherDepartmentDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {                     
			/*** Merge data ***/
			SnickTeacherDepartmentEntity entity = modelMapper.map(teacherDepartmentDto,SnickTeacherDepartmentEntity.class);
                        entity.setScCode("PRAMANDA");
                        entity.setRecordStatus(RecordStatus.SAVE.getCode());
			entity.setCreatedUser(teacherDepartmentDto.getUserName());
			
			teacherDepartmentRepo.save(entity);
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput update(SnickTeacherDepartmentDto teacherDepartmentDto) {
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
		try {
			/*** Check id ***/
			SnickTeacherDepartmentEntity entity = teacherDepartmentRepo.findById(ValiableUtils.LongIsZero(teacherDepartmentDto.getTeacherDepartmentId())).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                       
			/*** Merge data ***/
			modelMapper.map(teacherDepartmentDto,entity);

			/*** Set ***/
                        entity.setRecordStatus(RecordStatus.UPDATE.getCode());
			entity.setUpdatedDate(new Date());
			entity.setUpdatedUser(teacherDepartmentDto.getUserName());
			
			teacherDepartmentRepo.save(entity);
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(ConstraintViolationException e) {
			return ExceptionUtils.validate(e);
		}catch(Exception ex) {
			return ExceptionUtils.convertRollbackToString(ex);
		}
		return output;
	}
        
        public ResponseOutput updateLeader(String userName, SnickTeacherDepartmentDto[] teacherDepartmentDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {                   
                    /*** Loop update data ***/
                    for (SnickTeacherDepartmentDto map : teacherDepartmentDto) {
                        SnickTeacherDepartmentEntity entity = modelMapper.map(map, SnickTeacherDepartmentEntity.class);
                        teacherDepartmentRepo.updateLeader(
                                (map.getLeader()),
                                 userName,
                                 RecordStatus.UPDATE.getCode(),
                                (Long.parseLong(map.getTeacherDepartmentId())));
                    }
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }

                return output;
        }
        
        public ResponseOutput updateTrue(SnickTeacherDepartmentDto teacherDepartmentDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    /*** Check id ***/
                    SnickTeacherDepartmentEntity entity = teacherDepartmentRepo.findById(ValiableUtils.LongIsZero(teacherDepartmentDto.getTeacherDepartmentId())).orElseThrow(() -> new ResponseException(ResponseStatus.NOT_FOUND));

                    /*** Merge data ***/
                    modelMapper.map(teacherDepartmentDto, entity);

                    /*** Set ***/
                    entity.setRecordStatus(RecordStatus.UPDATE.getCode());
                    entity.setLeader(true);
                    entity.setUpdatedDate(new Date());
                    entity.setUpdatedUser(teacherDepartmentDto.getUserName());
                    
                    teacherDepartmentRepo.save(entity);
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
        
        public ResponseOutput delete(SnickTeacherDepartmentDto teacherDepartmentDto) {
                ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
                try {
                    
                    String teacherDepartmentId = ValiableUtils.stringIsEmpty(teacherDepartmentDto.getTeacherDepartmentId()); 
                    teacherDepartmentRepo.deleteTeacherDapartment(Long.parseLong(teacherDepartmentId));
                } catch (ResponseException ex) {
                    output.setResponseCode(ex.getCode());
                    output.setResponseMsg(ex.getMessage());
                } catch (ConstraintViolationException e) {
                    return ExceptionUtils.validate(e);
                } catch (Exception ex) {
                    return ExceptionUtils.convertRollbackToString(ex);
                }
                return output;
        }
}
