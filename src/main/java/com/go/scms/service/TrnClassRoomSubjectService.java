/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.service;

import com.go.scms.contant.RecordStatus;
import com.go.scms.dto.TrnClassRoomSubjectDto;
import com.go.scms.entity.TrnClassRoomSubjectEntity;
import com.go.scms.repository.TrnClassRoomSubjectRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrnClassRoomSubjectService {

    @Autowired
    private TrnClassRoomSubjectRepository trnClassRoomSubjectRepo;

    @Autowired
    private ModelMapper modelMapper;

    public ResponseOutput searchsubjectinstudent(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        String stuCode = ValiableUtils.stringIsEmpty(trnClassRoomSubjectDto.getStuCode());
        Integer yearGroupId = Integer.parseInt(trnClassRoomSubjectDto.getYearGroupId());
        Integer classRoomId = Integer.parseInt(trnClassRoomSubjectDto.getClassRoomId());

        List<Map> list = trnClassRoomSubjectRepo.findsubjectinstudent(stuCode, yearGroupId, classRoomId);

        output.setResponseData(list);
        return output;
    }

    public ResponseOutput searchsubjectaca004sub07(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());
        Integer classRoomId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassRoomId());
        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getYearGroupId());
        String ciztizenId = ValiableUtils.stringIsEmpty(trnClassRoomSubjectDto.getCitizenId()); 

        List<Map> list = trnClassRoomSubjectRepo.findsubjectaca004sub07(classId, classRoomId, yearGroupId,ciztizenId);

        output.setResponseData(list);
        return output;

    }

    public ResponseOutput searchdetailsubject(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        Integer classRoomSubjectId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassRoomSubjectId());
        List<Map> list = trnClassRoomSubjectRepo.finddetailsubject(classRoomSubjectId);

        output.setResponseData(list);
        return output;
    }

    public ResponseOutput searchclassroomteacherinaca002sub03(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());
        String subjectCode = ValiableUtils.stringIsEmpty(trnClassRoomSubjectDto.getSubjectCode());

        List<Map> list = trnClassRoomSubjectRepo.findclassroomteacherinaca002sub03(yearGroupId, classId, subjectCode);

        output.setResponseData(list);
        return output;
    }

    public ResponseOutput searchclassroomindialog(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());

        List list = trnClassRoomSubjectRepo.findclassroomindialog(classId).stream().map(e -> {
            ClassRoominDialog m = new ClassRoominDialog();
            m.setScCode(e.get("sc_code"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setDepartmentId(e.get("department_id"));
            m.setSubjectCode(e.get("subject_code"));
            m.setSubjectCode2(e.get("subject_code2"));
            m.setSubjectNameTh(e.get("subject_name_th"));
            m.setSubjectNameEn(e.get("subject_name_en"));
            m.setSubjectType(e.get("subject_type"));
            m.setClassRoomId(e.get("class_room_id"));
            m.setScoreId(e.get("score_id"));
            m.setOrders(e.get("orders"));
            m.setLesson(e.get("lesson"));
            m.setWeight(e.get("weight"));
            m.setIsEp(e.get("is_ep"));
            m.setTeacher1(e.get("teacher1"));
            m.setTeacherFullname1(e.get("teacher_fullname1"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput searchsubjectincopy(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());

        List<Map> list = trnClassRoomSubjectRepo.findsubjectincopy(yearGroupId, classId);

        output.setResponseData(list);
        return output;
    }

    public ResponseOutput searchsubjectacac002update(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());
        String subjectCode = ValiableUtils.stringIsEmpty(trnClassRoomSubjectDto.getSubjectCode());

        List list = trnClassRoomSubjectRepo.findsubjectacac002update(yearGroupId, classId, subjectCode).stream().map(e -> {
            SubjectAca002Update m = new SubjectAca002Update();
            m.setSubjectCode(e.get("subject_code"));
            m.setSubjectNameTh(e.get("subject_name_th"));
            m.setScoreId(e.get("score_id"));
            m.setTeacher1(e.get("teacher1"));
            m.setTeacherFullname1(e.get("teacher_fullname1"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);
        
        return output;
    }

    public ResponseOutput searchsubjectallinclassroomcopy(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);

        Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getYearGroupId());
        Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());

        List list = trnClassRoomSubjectRepo.findsubjectallinclassroomcopy(yearGroupId, classId).stream().map(e -> {
            SubjectCopy m = new SubjectCopy();
            m.setScCode(e.get("sc_code"));
            m.setYearGroupId(e.get("year_group_id"));
            m.setDepartmentId(e.get("department_id"));
            m.setSubjectCode(e.get("subject_code"));
            m.setSubjectCode2(e.get("subject_code2"));
            m.setSubjectNameTh(e.get("subject_name_th"));
            m.setSubjectNameEn(e.get("subject_name_en"));
            m.setClassRoomId(e.get("class_room_id"));
            m.setScoreId(e.get("score_id"));
            m.setOrders(e.get("orders"));
            m.setCredit(e.get("credit"));
            m.setLesson(e.get("lesson"));
            m.setWeight(e.get("weight"));
            m.setTeacher1(e.get("teacher1"));
            m.setTeacher2(e.get("teacher2"));
            m.setTeacherFullname1(e.get("teacher_fullname1"));
            m.setTeacherFullname2(e.get("teacher_fullname2"));

            return m;
        }).collect(Collectors.toList());
        output.setResponseData(list);

        return output;

    }

    public ResponseOutput insert(String userName, TrnClassRoomSubjectDto[] trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TrnClassRoomSubjectDto map : trnClassRoomSubjectDto) {
                TrnClassRoomSubjectEntity entity = modelMapper.map(map, TrnClassRoomSubjectEntity.class);
                entity.setCreatedUser(userName);
                trnClassRoomSubjectRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput insertcopy(String userName, TrnClassRoomSubjectDto[] trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {

            for (TrnClassRoomSubjectDto map : trnClassRoomSubjectDto) {
                TrnClassRoomSubjectEntity entity = modelMapper.map(map, TrnClassRoomSubjectEntity.class);
                entity.setCreatedUser(userName);
                trnClassRoomSubjectRepo.save(entity);
            }
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }

    public ResponseOutput updatesubjectaca002sub01(String userName, TrnClassRoomSubjectDto[] trnClassRoomSubjectDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TrnClassRoomSubjectDto map : trnClassRoomSubjectDto) {
                 if (map.getTeacher1() == null || map.getTeacher1().equals("")) {
                      trnClassRoomSubjectRepo.updatesubjectaca002sub01setnull(Integer.parseInt(map.getScoreId()), RecordStatus.UPDATE.getCode(), userName, Integer.parseInt(map.getYearGroupId()), Integer.parseInt(map.getClassId()), map.getSubjectCode());
                 }else{
                      trnClassRoomSubjectRepo.updatesubjectaca002sub01(Integer.parseInt(map.getScoreId()), Integer.parseInt(map.getTeacher1()), map.getTeacherFullname1(), RecordStatus.UPDATE.getCode(), userName, Integer.parseInt(map.getYearGroupId()), Integer.parseInt(map.getClassId()), map.getSubjectCode());
                 }
               
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput updatesubjectaca002sub01catclass1(String userName, TrnClassRoomSubjectDto[] trnClassRoomSubjectDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            for (TrnClassRoomSubjectDto map : trnClassRoomSubjectDto) {
                if (map.getTeacher1() == null || map.getTeacher1().equals("")) {
                  trnClassRoomSubjectRepo.updatesubjectaca002sub01catclass1setnull(Integer.parseInt(map.getScoreId()),RecordStatus.UPDATE.getCode(), userName, Integer.parseInt(map.getYearGroupId()), Integer.parseInt(map.getClassId()), map.getSubjectCode());
                }else{
                    trnClassRoomSubjectRepo.updatesubjectaca002sub01catclass1(Integer.parseInt(map.getScoreId()),Integer.parseInt(map.getTeacher1()), map.getTeacherFullname1(), RecordStatus.UPDATE.getCode(), userName, Integer.parseInt(map.getYearGroupId()), Integer.parseInt(map.getClassId()), map.getSubjectCode());
                }
            }
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput updateteacher1inaca002sub03(String userName, TrnClassRoomSubjectDto trnClassRoomSubjectDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            trnClassRoomSubjectRepo.updateteacher1inaca002sub03(Integer.parseInt(trnClassRoomSubjectDto.getTeacher1()), trnClassRoomSubjectDto.getTeacherFullname1(), RecordStatus.UPDATE.getCode(), userName, Long.parseLong(trnClassRoomSubjectDto.getClassRoomSubjectId()));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput deleteteacher1inaca002sub03(String userName, TrnClassRoomSubjectDto trnClassRoomSubjectDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            trnClassRoomSubjectRepo.deleteteacher1inaca002sub03(RecordStatus.UPDATE.getCode(), userName, Long.parseLong(trnClassRoomSubjectDto.getClassRoomSubjectId()));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }
    
    public ResponseOutput updateteacher2inaca002sub03(String userName, TrnClassRoomSubjectDto trnClassRoomSubjectDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            trnClassRoomSubjectRepo.updateteacher2inaca002sub03(Integer.parseInt(trnClassRoomSubjectDto.getTeacher2()), trnClassRoomSubjectDto.getTeacherFullname2(), RecordStatus.UPDATE.getCode(), userName, Long.parseLong(trnClassRoomSubjectDto.getClassRoomSubjectId()));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput deleteteacher2inaca002sub03(String userName, TrnClassRoomSubjectDto trnClassRoomSubjectDto) {

        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            trnClassRoomSubjectRepo.deleteteacher2inaca002sub03(RecordStatus.UPDATE.getCode(), userName, Long.parseLong(trnClassRoomSubjectDto.getClassRoomSubjectId()));
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }

        return output;
    }

    public ResponseOutput delete(TrnClassRoomSubjectDto trnClassRoomSubjectDto) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            Integer yearGroupId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getYearGroupId());
            Integer classId = (int) ValiableUtils.IntIsZero(trnClassRoomSubjectDto.getClassId());
            String subjectCode = ValiableUtils.stringIsEmpty(trnClassRoomSubjectDto.getSubjectCode());

            trnClassRoomSubjectRepo.deletesubjectindepartment(yearGroupId, classId, subjectCode);
        } catch (ResponseException ex) {
            output.setResponseCode(ex.getCode());
            output.setResponseMsg(ex.getMessage());
        } catch (ConstraintViolationException e) {
            return ExceptionUtils.validate(e);
        } catch (Exception ex) {
            return ExceptionUtils.convertRollbackToString(ex);
        }
        return output;
    }
}

class ClassRoominDialog {

    private String scCode;
    private String yearGroupId;
    private String departmentId;
    private String subjectCode;
    private String subjectCode2;
    private String subjectNameTh;
    private String subjectNameEn;
    private String subjectType;
    private String classRoomId;
    private String scoreId;
    private String orders;
    private String lesson;
    private String weight;
    private String isEp;
    private String teacher1;
    private String teacherFullname1;

    public String getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(String subjectType) {
        this.subjectType = subjectType;
    }

    public String getIsEp() {
        return isEp;
    }

    public void setIsEp(String isEp) {
        this.isEp = isEp;
    }

    public String getSubjectCode2() {
        return subjectCode2;
    }

    public void setSubjectCode2(String subjectCode2) {
        this.subjectCode2 = subjectCode2;
    }

    public String getSubjectNameEn() {
        return subjectNameEn;
    }

    public void setSubjectNameEn(String subjectNameEn) {
        this.subjectNameEn = subjectNameEn;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(String teacher1) {
        this.teacher1 = teacher1;
    }

    public String getTeacherFullname1() {
        return teacherFullname1;
    }

    public void setTeacherFullname1(String teacherFullname1) {
        this.teacherFullname1 = teacherFullname1;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

}

class SubjectCopy {

    private String scCode;
    private String yearGroupId;
    private String classRoomId;
    private String departmentId;
    private String subjectCode;
    private String subjectCode2;
    private String subjectNameTh;
    private String subjectNameEn;
    private String scoreId;
    private String orders;
    private String credit;
    private String lesson;
    private String weight;
    private String teacher1;
    private String teacherFullname1;
    private String teacher2;
    private String teacherFullname2;

    public String getSubjectCode2() {
        return subjectCode2;
    }

    public void setSubjectCode2(String subjectCode2) {
        this.subjectCode2 = subjectCode2;
    }

    public String getSubjectNameEn() {
        return subjectNameEn;
    }

    public void setSubjectNameEn(String subjectNameEn) {
        this.subjectNameEn = subjectNameEn;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(String teacher1) {
        this.teacher1 = teacher1;
    }

    public String getTeacherFullname1() {
        return teacherFullname1;
    }

    public void setTeacherFullname1(String teacherFullname1) {
        this.teacherFullname1 = teacherFullname1;
    }

    public String getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(String teacher2) {
        this.teacher2 = teacher2;
    }

    public String getTeacherFullname2() {
        return teacherFullname2;
    }

    public void setTeacherFullname2(String teacherFullname2) {
        this.teacherFullname2 = teacherFullname2;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getYearGroupId() {
        return yearGroupId;
    }

    public void setYearGroupId(String yearGroupId) {
        this.yearGroupId = yearGroupId;
    }

    public String getClassRoomId() {
        return classRoomId;
    }

    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

}

class SubjectAca002Update {

    private String subjectCode;
    private String subjectNameTh;
    private String scoreId;
    private String teacher1;
    private String teacherFullname1;

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectNameTh() {
        return subjectNameTh;
    }

    public void setSubjectNameTh(String subjectNameTh) {
        this.subjectNameTh = subjectNameTh;
    }

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(String teacher1) {
        this.teacher1 = teacher1;
    }

    public String getTeacherFullname1() {
        return teacherFullname1;
    }

    public void setTeacherFullname1(String teacherFullname1) {
        this.teacherFullname1 = teacherFullname1;
    }

}
