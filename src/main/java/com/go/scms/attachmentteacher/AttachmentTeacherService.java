package com.go.scms.attachmentteacher;

import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class AttachmentTeacherService {

    private static final Logger LOG = LoggerFactory.getLogger(AttachmentTeacherService.class);

    @Value("/PRAMANDA")
    private String pathFile;

    @Value("IMAGE_TEA")
    private String folderTmp;
    
    @Value("SIGNATURE_TEA")
    private String folderSig;
    
    @Value("IMAGE_USER")
    private String folderUserTmp;

    public ResponseOutput upload(MultipartFile file , String teacherCardId){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try{
            /*** Normalize file name ***/
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            /*** Check if the file's name contains invalid characters ***/
            if(fileName.contains("..")) {
                throw new ResponseException(ResponseStatus.ERROR.getCode(),"Sorry! Filename contains invalid path sequence " + fileName);
            }

            /*** Create Directory ***/
            String path = concatPath(pathFile,folderTmp,teacherCardId);
            Files.createDirectories(Paths.get(path));

            /*** Copy file to the target location (Replacing existing file with the same name) ***/
            Files.copy(file.getInputStream(),Paths.get(concatPath(path,fileName)), StandardCopyOption.REPLACE_EXISTING);

            /*** Response ***/
            AttachmentTeacherOutput upload = new AttachmentTeacherOutput();
            upload.setAttachmentCode(teacherCardId);
            upload.setAttachmentName(fileName);

            /*** Response ***/
            output.setResponseData(upload);
        }catch(Exception ex){
            LOG.error("Error attachment upload ",ex);
            output.setResponseCode(ResponseStatus.ERROR.getCode());
            output.setResponseMsg(ResponseStatus.ERROR.getMessage());
        }
        return output;
    }
    
     public ResponseOutput uploadSig(MultipartFile file , String fullnameTh){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try{
            /*** Normalize file name ***/
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            /*** Check if the file's name contains invalid characters ***/
            if(fileName.contains("..")) {
                throw new ResponseException(ResponseStatus.ERROR.getCode(),"Sorry! Filename contains invalid path sequence " + fileName);
            }

            /*** Create Directory ***/
            String path = concatPath(pathFile,folderSig,fullnameTh);
            Files.createDirectories(Paths.get(path));

            /*** Copy file to the target location (Replacing existing file with the same name) ***/
            Files.copy(file.getInputStream(),Paths.get(concatPath(path,fileName)), StandardCopyOption.REPLACE_EXISTING);

            /*** Response ***/
            AttachmentTeacherOutput upload = new AttachmentTeacherOutput();
            upload.setAttachmentCode(fullnameTh);
            upload.setAttachmentName(fileName);

            /*** Response ***/
            output.setResponseData(upload);
        }catch(Exception ex){
            LOG.error("Error attachment upload ",ex);
            output.setResponseCode(ResponseStatus.ERROR.getCode());
            output.setResponseMsg(ResponseStatus.ERROR.getMessage());
        }
        return output;
    }
     
    public ResponseOutput uploadUser(MultipartFile file , String citizenId){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try{
            /*** Normalize file name ***/
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            /*** Check if the file's name contains invalid characters ***/
            if(fileName.contains("..")) {
                throw new ResponseException(ResponseStatus.ERROR.getCode(),"Sorry! Filename contains invalid path sequence " + fileName);
            }

            /*** Create Directory ***/
            String path = concatPath(pathFile,folderUserTmp,citizenId);
            Files.createDirectories(Paths.get(path));

            /*** Copy file to the target location (Replacing existing file with the same name) ***/
            Files.copy(file.getInputStream(),Paths.get(concatPath(path,fileName)), StandardCopyOption.REPLACE_EXISTING);

            /*** Response ***/
            AttachmentTeacherOutput upload = new AttachmentTeacherOutput();
            upload.setAttachmentCode(citizenId);
            upload.setAttachmentName(fileName);

            /*** Response ***/
            output.setResponseData(upload);
        }catch(Exception ex){
            LOG.error("Error attachment upload ",ex);
            output.setResponseCode(ResponseStatus.ERROR.getCode());
            output.setResponseMsg(ResponseStatus.ERROR.getMessage());
        }
        return output;
    }

    public Resource loadFileAsResource(String fileName){
        try {
            /*** Concat path ex : xxxx/xxxxx/xxxx.png ***/
            String path = concatPath(pathFile,folderTmp,fileName);

            /*** Get file in folder ***/
            Resource resource = new UrlResource(Paths.get(path).toUri());
            if(!resource.exists()) {
                throw new NullPointerException("File not found " + fileName);
            }
            return resource;
        }catch(Exception ex) {
            LOG.error(ex.getMessage());
        }
        return null;
    }
    
    // uploadUser
    public Resource loadFileAsResourceUser(String fileName){
        try {
            /*** Concat path ex : xxxx/xxxxx/xxxx.png ***/
            String path = concatPath(pathFile,folderUserTmp,fileName);

            /*** Get file in folder ***/
            Resource resource = new UrlResource(Paths.get(path).toUri());
            if(!resource.exists()) {
                throw new NullPointerException("File not found " + fileName);
            }
            return resource;
        }catch(Exception ex) {
            LOG.error(ex.getMessage());
        }
        return null;
    }

    private String concatPath(String... str) {
        StringBuilder sb = new StringBuilder();
        for(String s : str) {
            sb.append(s).append("/");
        }
        return sb.toString().substring(0,sb.length()-1);
    }

    public Long generateCode() throws NoSuchAlgorithmException {
        Random random = SecureRandom.getInstanceStrong();
        String dtf = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
        String lpad = org.apache.commons.lang.StringUtils.leftPad(String.valueOf(random.nextInt(999)),3,"0");
        return Long.parseLong(dtf.concat(lpad));
    }
}
