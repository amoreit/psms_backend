package com.go.scms.attachmentteacher;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;

@RestController
@RequestMapping("/api/v1/attachmentteacher")
public class AttachmentTeacherController {

    @Autowired
    private AttachmentTeacherService service;

    @PostMapping("/upload/{teacherCardId}")
    public ResponseEntity<Object> upload(@RequestParam("file") MultipartFile file,@PathVariable("teacherCardId") String teacherCardId, HttpServletRequest request){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.upload(file,teacherCardId));
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }
    
    @PostMapping("/uploadsig/{fullnameTh}")
    public ResponseEntity<Object> uploadSig(@RequestParam("file") MultipartFile file,@PathVariable("fullnameTh") String fullnameTh, HttpServletRequest request){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.uploadSig(file,fullnameTh));
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }
    
    //upload user
    @PostMapping("/uploadUser/{citizenId}")
    public ResponseEntity<Object> uploadUser(@RequestParam("file") MultipartFile file,@PathVariable("citizenId") String citizenId, HttpServletRequest request){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.uploadUser(file,citizenId));
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }

    @GetMapping("/view/**")
    public ResponseEntity<Object> view(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        String path = request.getServletPath().replace("/api/v1/attachmentteacher/view/","");
        try {
            String type = FilenameUtils.getExtension(path);
            if(type.equals("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(type.equals("jpg") ||type.equals("jpeg") ) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else {
                throw new NullPointerException("File not found type");
            }

            /*** Output url ***/
            return new ResponseEntity<>(service.loadFileAsResource(path),headers, HttpStatus.OK);
        }catch(NullPointerException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }
    
    // uploadUser
    @GetMapping("/viewUser/**")
    public ResponseEntity<Object> viewUser(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        String path = request.getServletPath().replace("/api/v1/attachmentteacher/viewUser/","");
        try {
            String type = FilenameUtils.getExtension(path);
            if(type.equals("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(type.equals("jpg") ||type.equals("jpeg") ) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else {
                throw new NullPointerException("File not found type");
            }

            /*** Output url ***/
            return new ResponseEntity<>(service.loadFileAsResourceUser(path),headers, HttpStatus.OK);
        }catch(NullPointerException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }

    @GetMapping(value = "/download/**",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report(HttpServletRequest request,HttpServletResponse response){
        try(ByteArrayOutputStream sm = new ByteArrayOutputStream()){
            /*** Get path ***/
            String path = request.getServletPath().replace("/api/v1/attachmentteacher/download/","");

            /*** Output stream ***/
            return ResponseEntity.status(HttpStatus.OK).body(service.loadFileAsResource(path).getInputStream());
        }catch(Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/v/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename){
        try {
            Resource file = service.loadFileAsResource(filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
        }catch(Exception ex) {
            return null;
        }
    }

}
