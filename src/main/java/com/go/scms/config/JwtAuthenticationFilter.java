package com.go.scms.config;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.service.LogUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.go.scms.contant.Constants;
import com.go.scms.entity.UserEntity;
import com.go.scms.repository.UserRepository;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

    private static final Logger LOG = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
	
	private final AuthenticationManager authenticationManager;
	private final LogUserService logUser;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, ApplicationContext ctx) {
        this.authenticationManager = authenticationManager;
        this.setFilterProcessesUrl("/auth");
        this.logUser = ctx.getBean(LogUserService.class);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response){
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
    	LoginModel user = null;
		try(BufferedReader reader = request.getReader()){
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			/*** Convert json string to class ***/
			user = new Gson().fromJson(sb.toString().trim(),LoginModel.class);

			/*** Validate ***/
			if(user.getUsrName()== null || user.getUsrName().isEmpty() || user.getPassword() == null || user.getPassword().isEmpty()) {
				throw new ResponseException(ResponseStatus.NOT_FOUND.getCode(),"ชื่อผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง");
			}
			return this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsrName(),user.getPassword()));
		}catch(ResponseException ex) {
			output.setResponseCode(ex.getCode());
			output.setResponseMsg(ex.getMessage());
		}catch(Exception ex) {
			output.setResponseCode(ResponseStatus.NOT_FOUND.getCode());
			output.setResponseMsg("ชื่อผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง");
		}finally{
			if(user != null && (user.getUsrName()!= null && !user.getUsrName().isEmpty())){
				logUser.loginSave(request,output,user.getUsrName());
			}
		}

		/*** Response ***/
		try {
			response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
			response.setCharacterEncoding(StandardCharsets.UTF_8.name());
			ServletOutputStream out = response.getOutputStream();
			out.write(output.toString().getBytes(StandardCharsets.UTF_8.name()));
			out.flush();
			out.close();
		}catch(Exception ex) {
			LOG.warn(ex.getMessage());
		}
        return null;
	}

	@Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,FilterChain filterChain, Authentication authentication) {
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
		ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            UserEntity user = ((UserEntity) authentication.getPrincipal());
            String token = Jwts.builder().signWith(Keys.hmacShaKeyFor(Constants.PRIVATE_KEY.getBytes()), SignatureAlgorithm.HS512)
                    					 .setHeaderParam("typ","JWT")
                    					 .setIssuer("secure-api")
                    					 .setAudience("secure-app")
                    					 .setSubject(user.getUsrName())
                    					 .setExpiration(new Date(System.currentTimeMillis() + 360000000)) // 2 min
                    					 .claim("rol",user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                    					 .compact();
            
            JsonObject json = new JsonObject();
            json.addProperty("tokenNo",token);
            json.addProperty("userName",user.getUsername());
            json.addProperty("user",user.getUsrName());
            json.addProperty("email",user.getEmail());
            json.addProperty("citizenId",user.getCitizenId());
            json.addProperty("title",user.getTitle());
            json.addProperty("menu",user.getMenu().toString());
            json.addProperty("role",user.getRole().getRoleId());
            output.setResponseData(json);

            ServletOutputStream out = response.getOutputStream();
            out.write(output.toString().getBytes("UTF-8"));
            out.flush();
            out.close();
        }catch(Exception ex) {
        	ex.printStackTrace();
        	LOG.warn(ex.getMessage());
        }
    }
}

class LoginModel{
    private String usrName;
    private String password;

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }
    
    public String getPassword() {
            return password;
    }
    
    public void setPassword(String password) {
            this.password = password;
    }
}
