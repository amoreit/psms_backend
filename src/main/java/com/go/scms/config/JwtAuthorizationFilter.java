package com.go.scms.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.go.scms.contant.RecordStatus;
import com.go.scms.entity.UserEntity;
import com.go.scms.repository.UserRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseStatus;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.go.scms.contant.Constants;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
    
    private final UserRepository userRepo;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager, ApplicationContext ctx) {
        super(authenticationManager);
		this.userRepo = ctx.getBean(UserRepository.class);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,FilterChain filterChain) throws IOException{
    	String token = request.getHeader("Authorization");
    	try {
        	if(token == null || token.isEmpty()) {
        		throw new NullPointerException("Not accessible.");
        	}
        	
        	/*** Verify expired ***/
                Jws<Claims> cla = Jwts.parser().setSigningKey(Constants.PRIVATE_KEY.getBytes()).parseClaimsJws(token);

                /*** Verify user and mapper ***/
                UserEntity user = userRepo.findByUsrNameAndUserStatus(cla.getBody().getSubject(), RecordStatus.SAVE.getCode()).orElseThrow(()->new ResponseException(ResponseStatus.NOT_FOUND));
                request.setAttribute("user",user);

        	/*** Response ***/
        	filterChain.doFilter(request, response);
    	}catch(NullPointerException ex) {
        	response.setStatus(HttpServletResponse.SC_BAD_GATEWAY);
        	response.getOutputStream().println(ex.getMessage());
    	}catch(ExpiredJwtException ex) {
        	response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        	response.getOutputStream().println("Invalid token.");
    	}catch(Exception ex) {
        	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        	response.getOutputStream().println("Internal error.");
    	}
    }
}