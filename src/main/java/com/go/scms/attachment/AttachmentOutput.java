package com.go.scms.attachment;

public class AttachmentOutput {

    private Long attachmentCode;
    private String attachmentName;
    private String attachmentUrl;

    public Long getAttachmentCode() {
        return attachmentCode;
    }

    public void setAttachmentCode(Long attachmentCode) {
        this.attachmentCode = attachmentCode;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }
}
