package com.go.scms.attachment;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/api/v1/attachment")
public class AttachmentController {

    @Autowired
    private AttachmentService service;

    @PostMapping("/upload/{stuCode}")
    public ResponseEntity<Object> upload(@RequestParam("file") MultipartFile file,@PathVariable("stuCode") String stuCode, HttpServletRequest request){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.upload(file,stuCode));
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }
    
    @PostMapping("/uploaddoc/{citizenId}")
    public ResponseEntity<Object> uploaddoc(@RequestParam("file") MultipartFile file,@PathVariable("citizenId") String citizenId, HttpServletRequest request){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.uploaddoc(file,citizenId));
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }

    @GetMapping("/view/**")
    public ResponseEntity<Object> view(HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        String path = request.getServletPath().replace("/api/v1/attachment/view/","");
        try {
            String type = FilenameUtils.getExtension(path);
            if(type.equals("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if(type.equals("jpg") || type.equals("JPG") || type.equals("jpeg") ) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else {
                throw new NullPointerException("File not found type");
            }

            /*** Output url ***/
            return new ResponseEntity<>(service.loadFileAsResource(path),headers, HttpStatus.OK);
        }catch(NullPointerException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
        }catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        }
    }


    @GetMapping(value = "/download/**", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report(HttpServletRequest request, HttpServletResponse response) {
        try (ByteArrayOutputStream sm = new ByteArrayOutputStream()) {
            
            /**** Get path ***/
            String path = request.getServletPath().replace("/api/v1/attachment/download/", "");

            /**** Output stream ***/
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .header(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name(), "attachment; filename=\"" + service.loadFileAsResourceDoc(path).getFilename() + "\"")
                    .body(service.loadFileAsResourceDoc(path));
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/v/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename){
        try {
            Resource file = service.loadFileAsResource(filename);
            return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
        }catch(Exception ex) {
            return null;
        }
    }

}
