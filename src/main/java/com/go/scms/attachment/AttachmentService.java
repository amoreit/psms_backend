package com.go.scms.attachment;

import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class AttachmentService {

    private static final Logger LOG = LoggerFactory.getLogger(AttachmentService.class);

    //@Value("scms*/upload_files/stu")
    @Value("/PRAMANDA")
    private String pathFile;

    @Value("IMAGE_STU")
    private String folderImage;
    
    @Value("DOCUMENT_STU")
    private String folderDoc;

    public ResponseOutput upload(MultipartFile file,String stuCode){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try{
            /*** Normalize file name ***/
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            /*** Check if the file's name contains invalid characters ***/
            if(fileName.contains("..")) {
                throw new ResponseException(ResponseStatus.ERROR.getCode(),"Sorry! Filename contains invalid path sequence " + fileName);
            }

            /*** Create Directory ***/
            String path = concatPath(pathFile,folderImage,stuCode);
            Files.createDirectories(Paths.get(path));

            /*** Copy file to the target location (Replacing existing file with the same name) ***/
            Files.copy(file.getInputStream(),Paths.get(concatPath(path,fileName)), StandardCopyOption.REPLACE_EXISTING);

            /*** Response ***/
            AttachmentOutput upload = new AttachmentOutput();
            upload.setAttachmentCode(Long.parseLong(stuCode));
            upload.setAttachmentName(fileName);

            /*** Response ***/
            output.setResponseData(upload);
        }catch(Exception ex){
            LOG.error("Error attachment upload ",ex);
            output.setResponseCode(ResponseStatus.ERROR.getCode());
            output.setResponseMsg(ResponseStatus.ERROR.getMessage());
        }
        return output;
    }
    
    public ResponseOutput uploaddoc(MultipartFile file,String citizenId){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try{
            /*** Normalize file name ***/
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());

            /*** Check if the file's name contains invalid characters ***/
            if(fileName.contains("..")) {
                throw new ResponseException(ResponseStatus.ERROR.getCode(),"Sorry! Filename contains invalid path sequence " + fileName);
            }

            /*** Create Directory ***/
            String path = concatPath(pathFile,folderDoc,citizenId);
            Files.createDirectories(Paths.get(path));

            /*** Copy file to the target location (Replacing existing file with the same name) ***/
            Files.copy(file.getInputStream(),Paths.get(concatPath(path,fileName)), StandardCopyOption.REPLACE_EXISTING);

            /*** Response ***/
            AttachmentOutput uploaddoc = new AttachmentOutput();
            uploaddoc.setAttachmentCode(Long.parseLong(citizenId));
            uploaddoc.setAttachmentName(fileName);

            /*** Response ***/
            output.setResponseData(uploaddoc);
        }catch(Exception ex){
            LOG.error("Error attachment upload ",ex);
            output.setResponseCode(ResponseStatus.ERROR.getCode());
            output.setResponseMsg(ResponseStatus.ERROR.getMessage());
        }
        return output;
    }

    public Resource loadFileAsResource(String fileName){
        try {
            /*** Concat path ex : xxxx/xxxxx/xxxx.png ***/
            String path = concatPath(pathFile,folderImage,fileName);

            /*** Get file in folder ***/
            Resource resource = new UrlResource(Paths.get(path).toUri());
            if(!resource.exists()) {
                throw new NullPointerException("File not found " + fileName);
            }
            return resource;
        }catch(Exception ex) {
            LOG.error(ex.getMessage());
        }
        return null;
    }
    
    public Resource loadFileAsResourceDoc(String fileName){
        try {
            /*** Concat path ex : xxxx/xxxxx/xxxx.png ***/
            String path = concatPath(pathFile,folderDoc,fileName);

            /*** Get file in folder ***/
            Resource resource = new UrlResource(Paths.get(path).toUri());
            if(!resource.exists()) {
                throw new NullPointerException("File not found " + fileName);
            }
            return resource;
        }catch(Exception ex) {
            LOG.error(ex.getMessage());
        }
        return null;
    }

    private String concatPath(String... str) {
        StringBuilder sb = new StringBuilder();
        for(String s : str) {
            sb.append(s).append("/");
        }
        return sb.toString().substring(0,sb.length()-1);
    }

    public Long generateCode() throws NoSuchAlgorithmException {
        Random random = SecureRandom.getInstanceStrong();
        String dtf = new SimpleDateFormat("yyyyMMddhhmm").format(new Date());
        String lpad = org.apache.commons.lang.StringUtils.leftPad(String.valueOf(random.nextInt(999)),3,"0");
        return Long.parseLong(dtf.concat(lpad));
    }
}
