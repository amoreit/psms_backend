package com.go.scms.mail;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Stream;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;

@Service
public class MailService {
 
        @Autowired
        private JavaMailSender sender;

        public ResponseOutput sendMessage(MailInput input) {
            ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
            try {
             /*** HTML ***/
            File file = new ClassPathResource("/template/"+input.getPathHtml()).getFile();
            if(file == null) {
               throw new ResponseException(ResponseStatus.NOT_FOUND);
            }

            /*** Mapping ***/
            String content = readLine(file.getPath());
            for (Map.Entry<String, String> entry : input.getInput().entrySet()) {
               content = content.replaceAll("\\{"+entry.getKey()+"}",entry.getValue());
            }

            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setSubject(input.getSubject());
            helper.setTo(input.getRecipients());
            helper.setText(content, true);

            sender.send(message);
            
            }catch(ResponseException ex) {
            }catch(Exception ex) {}
            return output;
        }
 
        private String readLine(String filePath){
            StringBuilder contentBuilder = new StringBuilder();
            try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8)){
                stream.forEach(s -> contentBuilder.append(s).append("\n"));
            }catch (IOException e) {
                e.printStackTrace();
            }
            return contentBuilder.toString();
        }
}
