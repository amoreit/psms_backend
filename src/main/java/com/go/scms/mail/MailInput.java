package com.go.scms.mail;

import java.util.HashMap;
import java.util.Map;

public class MailInput {

	private String pathHtml;
	private String subject;
	private String recipients;
	private Map<String,String> input = new HashMap<>(); 
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getRecipients() {
		return recipients;
	}
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	public Map<String, String> getInput() {
		return input;
	}
	public void setInput(Map<String, String> input) {
		this.input = input;
	}
	public String getPathHtml() {
		return pathHtml;
	}
	public void setPathHtml(String pathHtml) {
		this.pathHtml = pathHtml;
	}
}
