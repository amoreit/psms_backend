package com.go.scms.controller;

import com.go.scms.dto.SnickTbStuProfileDocDto;
import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.service.TbStuProfileService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbStuProfile")
public class TbStuProfileController {
    
        @Autowired
	private TbStuProfileService tbStuProfileService;
        
        @GetMapping
        public ResponseEntity<?> search(TbStuProfileDto tbStuProfileDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.search(tbStuProfileDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/searchCode")
        public ResponseEntity<?> searchCode(TbStuProfileDto tbStuProfileDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.searchCode(tbStuProfileDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping("/{id}")
        public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.findById(id));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @PostMapping
	public ResponseEntity<?> insert(@RequestBody TbStuProfileDto tbStuProfileDto,HttpServletRequest request) throws Throwable{
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.insert(tbStuProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/updateUploadFile/{userName}")
	public ResponseEntity<?> updateUploadFile(@PathVariable("userName") String userName, @RequestBody TbStuProfileDto tbStuProfileDto,HttpServletRequest request) throws Throwable{
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.updateUploadFile(userName, tbStuProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/{userName}")
        public ResponseEntity<?> update(@PathVariable("userName") String userName, @RequestBody TbStuProfileDto tbStuProfileDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.update(userName, tbStuProfileDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @PutMapping("/updateCode/{userName}")
        public ResponseEntity<?> updateCode(@PathVariable("userName") String userName, @RequestBody TbStuProfileDto tbStuProfileDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.updateCode(userName, tbStuProfileDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}
