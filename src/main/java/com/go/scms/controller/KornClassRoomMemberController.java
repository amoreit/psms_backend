/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.KornClassRoomMemberDto;
import com.go.scms.service.KornClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kronn
 */
@RestController
@RequestMapping("/api/v1/kornClassRoom-member")
public class KornClassRoomMemberController {
    
    @Autowired
    private KornClassRoomMemberService kornClassRoomMemberService;
    
    @GetMapping("/sub")
    public ResponseEntity<?> searchSub(KornClassRoomMemberDto kornClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(kornClassRoomMemberService.searchSub(kornClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/tran")
    public ResponseEntity<?> searchtran(KornClassRoomMemberDto kornClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(kornClassRoomMemberService.searchtran(kornClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    
}
