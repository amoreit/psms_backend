package com.go.scms.controller;

import com.go.scms.dto.SnickTeacherDepartmentDto;
import com.go.scms.service.SnickTeacherDepartmentService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_teacherDepartment") 
public class SnickTeacherDepartmentController {
    
        @Autowired
	private SnickTeacherDepartmentService teacherDepartmentService;
    
        @GetMapping
	public ResponseEntity<?> search(SnickTeacherDepartmentDto teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.search(teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @GetMapping("/leader")
	public ResponseEntity<?> searchLeader(SnickTeacherDepartmentDto teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.searchLeader(teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickTeacherDepartmentDto teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.insert(teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickTeacherDepartmentDto teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.update(teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/updateLeader/{userName}")
	public ResponseEntity<?> updateLeader(@PathVariable("userName") String userName, @RequestBody SnickTeacherDepartmentDto[] teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.updateLeader(userName, teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/updateTrue")
	public ResponseEntity<?> updateTrue(@RequestBody SnickTeacherDepartmentDto teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.updateTrue(teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping
	public ResponseEntity<?> delete(SnickTeacherDepartmentDto teacherDepartmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherDepartmentService.delete(teacherDepartmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
       
}
