/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbStuTranscriptLogDto;
import com.go.scms.service.TbStuTranscriptLogService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Kittipong
 */
@RestController
@RequestMapping("/api/v1/TranscriptLog")
public class TbStuTranscriptLogController {
    
    @Autowired
	private TbStuTranscriptLogService tbStuTranscriptLogService;
    
    @GetMapping
    public ResponseEntity<?> search(TbStuTranscriptLogDto tbStuTranscriptLogDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.search(tbStuTranscriptLogDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/Update")
    public ResponseEntity<?> searchUpdate(TbStuTranscriptLogDto tbStuTranscriptLogDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.searchUpdate(tbStuTranscriptLogDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PostMapping
	public ResponseEntity<?> insert(@RequestBody TbStuTranscriptLogDto tbStuTranscriptLogDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.insert(tbStuTranscriptLogDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
    @PutMapping("/update")
	public ResponseEntity<?> update(@RequestBody TbStuTranscriptLogDto stuProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.update(stuProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
    @GetMapping("/searchCheck")
    public ResponseEntity<?> searchCheck(TbStuTranscriptLogDto tbStuTranscriptLogDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.searchCheck(tbStuTranscriptLogDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/UpdateCheck")
    public ResponseEntity<?> searchUpdateCheck(TbStuTranscriptLogDto tbStuTranscriptLogDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.searchUpdateCheck(tbStuTranscriptLogDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/TranDate")
    public ResponseEntity<?> TranscriptDate(TbStuTranscriptLogDto tbStuTranscriptLogDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbStuTranscriptLogService.TranscriptDate(tbStuTranscriptLogDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
}
