/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbStuTimeAttendanceDto;
import com.go.scms.service.TbStuTimeAttendanceService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbstutimeattendance")
public class TbStuTimeAttendanceController {

    @Autowired
    private TbStuTimeAttendanceService tbstutimeattendanceService;

    @GetMapping
    public ResponseEntity<?> search(TbStuTimeAttendanceDto tbstutimeattendanceDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbstutimeattendanceService.search(tbstutimeattendanceDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping
    public ResponseEntity<?> insert(@RequestBody TbStuTimeAttendanceDto tbstutimeattendanceDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbstutimeattendanceService.insert(tbstutimeattendanceDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}
