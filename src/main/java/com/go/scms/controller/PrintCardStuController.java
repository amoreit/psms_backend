/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.PrintCardLogDto;
import com.go.scms.dto.StuProfileDto;
import com.go.scms.dto.SubjectDto;
import com.go.scms.service.PrintCardService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/print-card")
public class PrintCardStuController {
     @Autowired
    private PrintCardService printCardService;
    
        @GetMapping
	public ResponseEntity<?> search(StuProfileDto stuProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(printCardService.search(stuProfileDto));
		}catch(Exception ex) {
                    ex.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        @GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(printCardService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody PrintCardLogDto printCardLogDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(printCardService.insert(printCardLogDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
