package com.go.scms.controller;

import com.go.scms.dto.SnickTeacherClassRoomMemberDto;
import com.go.scms.service.SnickTeacherClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_teacherClassRoomMember")
public class SnickTeacherClassRoomMemberController {
    
        @Autowired
	private SnickTeacherClassRoomMemberService teacherClassRoomMemberService;
    
        @GetMapping
	public ResponseEntity<?> search(SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherClassRoomMemberService.search(teacherClassRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherClassRoomMemberService.insert(teacherClassRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherClassRoomMemberService.update(teacherClassRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping
	public ResponseEntity<?> delete(SnickTeacherClassRoomMemberDto teacherClassRoomMemberDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherClassRoomMemberService.delete(teacherClassRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
