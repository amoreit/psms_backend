/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ClassRoomDto;
import com.go.scms.service.ClassRoomService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nui
 */
@RestController
@RequestMapping("/api/v1/classroom")
public class ClassRoomController {
    
    @Autowired
    private ClassRoomService classRoomService;

        @GetMapping
	public ResponseEntity<?> search(ClassRoomDto classRoomDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomService.search(classRoomDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/searchClassroomReligion")
	public ResponseEntity<?> searchClassroomReligion(ClassRoomDto classRoomDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomService.searchClassroomReligion(classRoomDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> insert(@RequestBody ClassRoomDto classRoomDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomService.insert(classRoomDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody ClassRoomDto classRoomDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomService.update(classRoomDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@DeleteMapping
	public ResponseEntity<?> delete(ClassRoomDto classRoomDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomService.delete(classRoomDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
    
}
