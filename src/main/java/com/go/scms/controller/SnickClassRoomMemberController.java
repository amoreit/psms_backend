package com.go.scms.controller;

import com.go.scms.dto.SnickClassRoomMemberDto;
import com.go.scms.service.SnickClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbTrnClassRoomMember")
public class SnickClassRoomMemberController {
    
        @Autowired
	private SnickClassRoomMemberService tbTrnClassRoomMemberService;

        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletRequest request) throws Throwable{
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.insert(tbTrnClassRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.update(tbTrnClassRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
