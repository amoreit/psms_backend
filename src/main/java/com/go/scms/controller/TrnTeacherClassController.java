/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TrnTeacherClassDto;
import com.go.scms.service.TrnTeacherClassService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/trn-teacher-class")
public class TrnTeacherClassController {

    @Autowired
    private TrnTeacherClassService trnTeacherClassService;

    @GetMapping("/teacher")
    public ResponseEntity<?> search(TrnTeacherClassDto trnTeacherClassDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnTeacherClassService.searchteachertec009(trnTeacherClassDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping("/copyteacherclass/{userName}")
    public ResponseEntity<?> insertcopy(@PathVariable("userName") String userName, @RequestBody TrnTeacherClassDto[] trnTeacherClassDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnTeacherClassService.insertcopyteacherclass(userName, trnTeacherClassDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
