package com.go.scms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import com.go.scms.contant.RecordStatus;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.entity.UserEntity;
import com.go.scms.mail.MailInput;
import com.go.scms.mail.MailService;
import com.go.scms.repository.UserRepository;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import java.util.HashMap;
import java.util.Map;

@RestController
public class IndexController {

    @Autowired
    private UserRepository user;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BCryptPasswordEncoder cry;

    @Autowired
    private MailService mail;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterModel reg, HttpServletRequest request) {
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            if (!reg.getPassword().equals(reg.getConfirmPassword())) {
                output.setResponseCode(ResponseStatus.VALIDATE.getCode());
                output.setResponseMsg("Password not match.");
            } else {
                UserEntity entity = modelMapper.map(reg, UserEntity.class);
                entity.setPassword(cry.encode(entity.getPassword()));
                entity.setUserStatus(RecordStatus.SAVE.getCode());

                if(reg.getUserType().equals("student")){
                    entity.getRole().setRoleId(Long.parseLong("777"));
                }
                if(reg.getUserType().equals("parent")){
                    entity.getRole().setRoleId(Long.parseLong("888"));
                }
                if(reg.getUserType().equals("alumni")){
                    entity.getRole().setRoleId(Long.parseLong("999"));
                }
                entity.setActivated(false);
                entity.setLoginCount(0);
                user.save(entity);
                // send mail
                Map map = new HashMap<>();
                map.put("title", reg.getTitle());
                map.put("firstName", reg.getFirstName());
                map.put("lastName", reg.getLastName());

                MailInput input = new MailInput();
                input.setPathHtml("register.html");
                input.setRecipients(reg.getUsrName());
                input.setSubject("PRAMANDANIJJANUKROAH SCHOOL");
                input.setInput(map);

                mail.sendMessage(input);

            }
           return ResponseEntity.status(HttpStatus.OK).body(output);
           
        }catch (ConstraintViolationException e) {
            return ResponseEntity.status(HttpStatus.OK).body(ExceptionUtils.validate(e));
        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.OK).body(ExceptionUtils.convertRollbackToString(ex));
        }
         
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}

class RegisterModel {

    private String firstName;
    private String lastName;
    private String scCode;
    private String email;
    private String usrName;
    private String password;
    private String confirmPassword;
    private String phoneNo;
    private String citizenId;
    private String userType;
    private String title;

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

   

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

}
