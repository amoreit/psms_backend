/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ActivityPaiDto;
import com.go.scms.service.ActivityPaiService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/activity")
public class ActivityPaiController {
    
    @Autowired
	private ActivityPaiService activityService;
    
    @GetMapping
	public ResponseEntity<?> search(ActivityPaiDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.search(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
    
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody ActivityPaiDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.insert(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PostMapping("/insertIsactive")
	public ResponseEntity<?> insertIsactive(@RequestBody ActivityPaiDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.insertIsactive(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/update")
	public ResponseEntity<?> update(@RequestBody ActivityPaiDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.update(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
         @PutMapping("/updateIsactive")
	public ResponseEntity<?> updateIscurrent(@RequestBody ActivityPaiDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.updateIsactive(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping
	public ResponseEntity<?> delete(ActivityPaiDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.delete(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
