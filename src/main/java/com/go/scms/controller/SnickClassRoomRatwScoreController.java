package com.go.scms.controller;

import com.go.scms.dto.SnickClassRoomRatwScoreDto;
import com.go.scms.service.SnickClassRoomRatwScoreService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_trnClassRoomRatwScore")
public class SnickClassRoomRatwScoreController {
    
        @Autowired
        private SnickClassRoomRatwScoreService classRoomRatwScoreService;
        
        @PutMapping("/{userName}")
	public ResponseEntity<?> allmange(@PathVariable("userName") String userName, @RequestBody SnickClassRoomRatwScoreDto[] classRoomRatwScoreDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomRatwScoreService.allmange(userName, classRoomRatwScoreDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
