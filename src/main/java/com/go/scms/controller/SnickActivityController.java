package com.go.scms.controller;

import com.go.scms.service.SnickActivityService;
import com.go.scms.dto.SnickActivityDto;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_activity") 
public class SnickActivityController {
    
        @Autowired
	private SnickActivityService activityService;
    
        @GetMapping
	public ResponseEntity<?> search(SnickActivityDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.search(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @GetMapping("/activity-stu-report")
	public ResponseEntity<?> searchActivityStuReport(SnickActivityDto activityDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityService.searchActivityStuReport(activityDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
}
