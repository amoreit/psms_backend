package com.go.scms.controller;

import com.go.scms.dto.SnickOnetDto;
import com.go.scms.service.SnickOnetService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_onet") 
public class SnickOnetController {
    
        @Autowired
	private SnickOnetService onetService;
    
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickOnetDto onetDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(onetService.insert(onetDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickOnetDto onetDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(onetService.update(onetDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
