package com.go.scms.controller;

import com.go.scms.dto.SnickMstClassDto;
import com.go.scms.response.ResponseException;
import com.go.scms.response.ResponseStatus;
import com.go.scms.service.SnickMstClassService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbMstClass")
public class SnickMstClassController {
    
        @Autowired
	private SnickMstClassService mstClassService;
	
	@GetMapping
	public ResponseEntity<?> search(SnickMstClassDto mstClassDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(mstClassService.search(mstClassDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request) throws Throwable{
		try {
			return ResponseEntity.status(HttpStatus.OK).body(mstClassService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickMstClassDto mstClassDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(mstClassService.insert(mstClassDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody SnickMstClassDto mstClassDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(mstClassService.update(mstClassDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
	@DeleteMapping
	public ResponseEntity<?> delete(SnickMstClassDto mstClassDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(mstClassService.delete(mstClassDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
