/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbTrnClassRoomDesiredCharScoreDto;
import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.service.TbTrnClassRoomDesiredCharScoreService;
import com.go.scms.service.TbTrnClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/desiredCharScore")
public class TbTrnClassRoomDesiredCharScoreController {

    @Autowired
    private TbTrnClassRoomDesiredCharScoreService tbTrnClassRoomDesiredCharScoreService;

    @PostMapping("/{userName}")
    public ResponseEntity<?> insert(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomDesiredCharScoreDto[] tbTrnClassRoomDesiredCharScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomDesiredCharScoreService.insert(userName, tbTrnClassRoomDesiredCharScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/{userName}")
    public ResponseEntity<?> update(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomDesiredCharScoreDto[] tbTrnClassRoomDesiredCharScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomDesiredCharScoreService.update(userName, tbTrnClassRoomDesiredCharScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
