package com.go.scms.controller;

import com.go.scms.dto.SnickTrnClassRoomMemberDto;
import com.go.scms.service.SnickTrnClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_trnClassRoomMember")
public class SnickTrnClassRoomMemberController {
    
        @Autowired
        private SnickTrnClassRoomMemberService classRoomMemberService;

        @GetMapping
        public ResponseEntity<?> search(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.search(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/multiSkill")
        public ResponseEntity<?> searchMultiSkill(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.searchMultiSkill(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/member")
        public ResponseEntity<?> searchMember(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.searchMember(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @PutMapping("/{userName}")
	public ResponseEntity<?> updateMultiSkill(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto[] classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateMultiSkill(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        //------------------------------------------ aca008
        @GetMapping("/activity-stu")
        public ResponseEntity<?> searchActivityStu(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.searchActivityStu(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @GetMapping("/dialog/activity-stu")
        public ResponseEntity<?> searchDialogActivityStu(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.searchDialogActivityStu(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @PutMapping("/dialog/update/{userName}")
	public ResponseEntity<?> updateActivityStu(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto[] classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateActivityStu(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/delete/{userName}")
	public ResponseEntity<?> updateDeleteActivityStu(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateDeleteActivityStu(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        //----------------------------------------- aca004-sub02 search
        @GetMapping("/activity-stu-result")
        public ResponseEntity<?> searchActivityStuResult(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.searchActivityStuResult(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        //----------------------------------------- aca004-sub02 update
        @PutMapping("/activity1/{userName}")
	public ResponseEntity<?> updateActivity1result(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto[] classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateActivity1result(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/activity2/{userName}")
	public ResponseEntity<?> updateActivity2result(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto[] classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateActivity2result(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/activity3/{userName}")
	public ResponseEntity<?> updateActivity3result(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto[] classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateActivity3result(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/activity4/{userName}")
	public ResponseEntity<?> updateActivity4result(@PathVariable("userName") String userName, @RequestBody SnickTrnClassRoomMemberDto[] classRoomMemberDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.updateActivity4result(userName, classRoomMemberDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        //------------------------------------------ aca008-report
        @GetMapping("/activity-stu-report")
        public ResponseEntity<?> searchActivityStuReport(SnickTrnClassRoomMemberDto classRoomMemberDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomMemberService.searchActivityStuReport(classRoomMemberDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

}
 