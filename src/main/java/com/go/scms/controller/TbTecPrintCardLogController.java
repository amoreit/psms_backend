/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbTecPrintCardLogDto;
import com.go.scms.service.TbTecPrintCardLogService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Kittipong
 */
@RestController
@RequestMapping("/api/v1/TecPrintCardLog")
public class TbTecPrintCardLogController {
    
    @Autowired
	private TbTecPrintCardLogService tbTecPrintCardLogService ;
    
    @PostMapping
	public ResponseEntity<?> insert(@RequestBody TbTecPrintCardLogDto tbTecPrintCardLogDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbTecPrintCardLogService.insert(tbTecPrintCardLogDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping
        public ResponseEntity<?> find(TbTecPrintCardLogDto tbTecPrintCardLogDto, HttpServletRequest request) {
                try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbTecPrintCardLogService.find(tbTecPrintCardLogDto));
                } catch (Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }
}
