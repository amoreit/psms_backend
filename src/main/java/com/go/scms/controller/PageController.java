package com.go.scms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.dto.PageDto;
import com.go.scms.service.PageService;

@RestController
@RequestMapping("/api/v1/page")
public class PageController {

	@Autowired
	private PageService pageService;
	
	@GetMapping
	public ResponseEntity<?> search(PageDto pageDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageService.search(pageDto));
		}catch(Exception ex) {
			ex.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> insert(@RequestBody PageDto pageDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageService.insert(pageDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody PageDto pageDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageService.update(pageDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@DeleteMapping
	public ResponseEntity<?> delete(PageDto pageDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageService.delete(pageDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

}
