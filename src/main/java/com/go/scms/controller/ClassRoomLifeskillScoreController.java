/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ClassRoomLifeskillScoreDto;
import com.go.scms.service.ClassRoomLifeskillScoreService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/class-room-lifeskill-score")
public class ClassRoomLifeskillScoreController {
      @Autowired
    private ClassRoomLifeskillScoreService classRoomLifeskillScoreService;
      @PostMapping("/{userName}")
	public ResponseEntity<?> insert(@PathVariable("userName") String userName,@RequestBody ClassRoomLifeskillScoreDto[] classRoomLifeskillScoreDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomLifeskillScoreService.insert(userName,classRoomLifeskillScoreDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
     @PutMapping("/{userName}")
	public ResponseEntity<?> update(@PathVariable("userName") String userName,@RequestBody ClassRoomLifeskillScoreDto[] classRoomLifeskillScoreDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomLifeskillScoreService.update(userName,classRoomLifeskillScoreDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
