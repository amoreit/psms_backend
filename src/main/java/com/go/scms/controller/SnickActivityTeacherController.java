package com.go.scms.controller;

import com.go.scms.dto.SnickActivityTeacherDto;
import com.go.scms.service.SnickActivityTeacherService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_activityTeacher")
public class SnickActivityTeacherController {
        
        @Autowired
        private SnickActivityTeacherService activityTeacherService;

        //---------------------------- aca004 search teacher
        @GetMapping
        public ResponseEntity<?> search(SnickActivityTeacherDto activityTeacherDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(activityTeacherService.search(activityTeacherDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickActivityTeacherDto activityTeacherDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityTeacherService.insert(activityTeacherDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping
	public ResponseEntity<?> delete(SnickActivityTeacherDto activityTeacherDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(activityTeacherService.delete(activityTeacherDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        //---------------------------- aca004 search teacher report
        @GetMapping("/activity-tea-report")
        public ResponseEntity<?> searchActivityTeaReport(SnickActivityTeacherDto activityTeacherDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(activityTeacherService.searchActivityTeaReport(activityTeacherDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}
