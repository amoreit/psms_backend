/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.GraduateLogDto;
import com.go.scms.service.GraduateLogService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/graduate-log")
public class GraduateLogController {

    @Autowired
    private GraduateLogService graduateLogService;

    @PostMapping("/{userName}")
    public ResponseEntity<?> insertcopy(@PathVariable("userName") String userName, @RequestBody GraduateLogDto[] graduateLogDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(graduateLogService.insert(userName, graduateLogDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
