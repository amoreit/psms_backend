/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;


import com.go.scms.dto.TbMstDivisionDto;
import com.go.scms.service.TbMstDivisionService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sir. Harinn
 */
@RestController
@RequestMapping("/api/v1/mst-division")
public class TbMstDivisionController {
    
    @Autowired
    private TbMstDivisionService tbMstDivisionService;
        
    @GetMapping
    public ResponseEntity<?> search(TbMstDivisionDto tbMstDivisionDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbMstDivisionService.search(tbMstDivisionDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request) throws Throwable{
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstDivisionService.findById(id));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    @PostMapping
    public ResponseEntity<?> insert(@RequestBody TbMstDivisionDto tbMstDivisionDto,HttpServletRequest request){
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstDivisionService.insert(tbMstDivisionDto));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    @PutMapping
    public ResponseEntity<?> update(@RequestBody TbMstDivisionDto tbMstDivisionDto,HttpServletRequest request){
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstDivisionService.update(tbMstDivisionDto));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
        
    @DeleteMapping
    public ResponseEntity<?> delete(TbMstDivisionDto tbMstDivisionDto,HttpServletRequest request){
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstDivisionService.delete(tbMstDivisionDto));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}
