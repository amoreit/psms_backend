/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.dto.TbTrnClassRoomMemberDcsDto;
import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.service.TbTrnClassRoomMemberDcsService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/classRoomMemberDcs")
public class TbTrnClassRoomMemberDcsController {
    
    @Autowired
    private TbTrnClassRoomMemberDcsService tbTrnClassRoomMemberDcsService;
    
    @GetMapping("/searchAca004Sub06Form")
    public ResponseEntity<?> searchAca004Sub06Form(TbTrnClassRoomMemberDcsDto tbTrnClassRoomMemberDcsDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberDcsService.searchAca004Sub06Form(tbTrnClassRoomMemberDcsDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/searchAca014")
    public ResponseEntity<?> searchAca014(TbStuProfileDto tbStuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberDcsService.searchAca014(tbStuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    @GetMapping("/searchAca014Html")
    public ResponseEntity<?> searchAca014Html(TbStuProfileDto tbStuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberDcsService.searchAca014Html(tbStuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/update/{userName}")
    public ResponseEntity<?> update(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomMemberDcsDto[] tbTrnClassRoomMemberDcsDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberDcsService.update(userName, tbTrnClassRoomMemberDcsDto));
            
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
