/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbMstSubDivisionDto;
import com.go.scms.service.TbMstSubDivisionService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sir. Harinn
 */
@RestController
@RequestMapping("/api/v1/mst-subDivision")
public class TbMstSubDivisionController {
     
    @Autowired
    private TbMstSubDivisionService tbMstSubDivisionService;
        
    @GetMapping
    public ResponseEntity<?> search(TbMstSubDivisionDto tbMstSubDivisionDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbMstSubDivisionService.search(tbMstSubDivisionDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request) throws Throwable{
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstSubDivisionService.findById(id));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    @PostMapping
    public ResponseEntity<?> insert(@RequestBody TbMstSubDivisionDto tbMstSubDivisionDto,HttpServletRequest request){
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstSubDivisionService.insert(tbMstSubDivisionDto));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
    
    @PutMapping
    public ResponseEntity<?> update(@RequestBody TbMstSubDivisionDto tbMstSubDivisionDto,HttpServletRequest request){
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstSubDivisionService.update(tbMstSubDivisionDto));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
        
    @DeleteMapping
    public ResponseEntity<?> delete(TbMstSubDivisionDto tbMstSubDivisionDto,HttpServletRequest request){
            try {
                    return ResponseEntity.status(HttpStatus.OK).body(tbMstSubDivisionService.delete(tbMstSubDivisionDto));
            }catch(Exception ex) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
    }
}
