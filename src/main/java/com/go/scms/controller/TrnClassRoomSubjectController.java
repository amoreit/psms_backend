/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TrnClassRoomSubjectDto;
import com.go.scms.service.TrnClassRoomSubjectService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@RequestMapping("/api/v1/trn-class-room-subject")
public class TrnClassRoomSubjectController {

    @Autowired
    private TrnClassRoomSubjectService TrnClassRoomSubjectService;

    @GetMapping("/subjectinstu")
    public ResponseEntity<?> searchsubjectinstudent(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchsubjectinstudent(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/subjectaca004sub07")
    public ResponseEntity<?> searchsubjectaca004sub07(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchsubjectaca004sub07(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/classroomteacherinaca002sub03")
    public ResponseEntity<?> searchclassroomteacherinaca002sub03(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchclassroomteacherinaca002sub03(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/detailsubject")
    public ResponseEntity<?> searchdetailsubject(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchdetailsubject(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/classroomindialog")
    public ResponseEntity<?> searchclassroomindialog(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchclassroomindialog(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/subjectincopy")
    public ResponseEntity<?> searchsubjectincopy(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchsubjectincopy(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/subjectallinclassroomcopy")
    public ResponseEntity<?> searchsubjectallinclassroomcopy(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchsubjectallinclassroomcopy(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/subjectacac002update")
    public ResponseEntity<?> searchsubjectacac002update(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.searchsubjectacac002update(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping("/{userName}")
    public ResponseEntity<?> insert(@PathVariable("userName") String userName,@RequestBody TrnClassRoomSubjectDto[] trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.insert(userName,trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PostMapping("/copy/{userName}")
    public ResponseEntity<?> insertcopy(@PathVariable("userName") String userName,@RequestBody TrnClassRoomSubjectDto[] trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.insertcopy(userName,trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/{userName}")
    public ResponseEntity<?> updatesubjectaca002sub01(@PathVariable("userName") String userName, @RequestBody TrnClassRoomSubjectDto[] trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.updatesubjectaca002sub01(userName, trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/catclass1/{userName}")
    public ResponseEntity<?> updatesubjectaca002sub01catclass1(@PathVariable("userName") String userName, @RequestBody TrnClassRoomSubjectDto[] trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.updatesubjectaca002sub01catclass1(userName, trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/addteacher1/{userName}")
    public ResponseEntity<?> updateteacher1inaca002sub03(@PathVariable("userName") String userName, @RequestBody TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.updateteacher1inaca002sub03(userName, trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/deleteteacher1/{userName}")
    public ResponseEntity<?> deleteteacher1inaca002sub03(@PathVariable("userName") String userName, @RequestBody TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.deleteteacher1inaca002sub03(userName, trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
     @PutMapping("/addteacher2/{userName}")
    public ResponseEntity<?> updateteacher2inaca002sub03(@PathVariable("userName") String userName, @RequestBody TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.updateteacher2inaca002sub03(userName, trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/deleteteacher2/{userName}")
    public ResponseEntity<?> deleteteacher2inaca002sub03(@PathVariable("userName") String userName, @RequestBody TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.deleteteacher2inaca002sub03(userName, trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(TrnClassRoomSubjectDto trnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(TrnClassRoomSubjectService.delete(trnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
