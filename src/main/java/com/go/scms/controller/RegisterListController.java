package com.go.scms.controller;

import com.go.scms.dto.RegisterListDto;
import com.go.scms.service.RegisterListService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admUser")
public class RegisterListController {
    
        @Autowired
        private RegisterListService registerListService;

        @GetMapping
        public ResponseEntity<?> search(RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.search(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }

        @GetMapping("/usermenu")
        public ResponseEntity<?> searchUser(RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.searchUser(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }
        
        @PostMapping
        public ResponseEntity<?> insert(@RequestBody RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.insert(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }

        @PutMapping
        public ResponseEntity<?> updateTrue(@RequestBody RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.updateTrue(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }

        @PutMapping("/updateFalse")
        public ResponseEntity<?> updateFalse(@RequestBody RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.updateFalse(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }

        @PutMapping("/sendMail")
        public ResponseEntity<?> sendMail(@RequestBody RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.sendMail(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }

        @GetMapping("/{id}")
        public ResponseEntity<?> findById(@PathVariable("id") Integer id,HttpServletRequest request) throws Throwable{
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.findById(id));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }

        @PutMapping("/updated")
        public ResponseEntity<?> updated(@RequestBody RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.updated(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }
        
        @PutMapping("/updatedPW")
        public ResponseEntity<?> updatedPW(@RequestBody RegisterListDto registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.updatedPW(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }
        
        @PutMapping("/updatedCheckAll")
        public ResponseEntity<?> updatedCheckAll(@RequestBody RegisterListDto[] registerListDto,HttpServletRequest request){
                try {
                        return ResponseEntity.status(HttpStatus.OK).body(registerListService.updatedCheckAll(registerListDto));
                }catch(Exception ex) {
                        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
                }
        }
  
}
