package com.go.scms.controller;

import com.go.scms.dto.SnickTeacherProfileDto;
import com.go.scms.service.SnickTeacherProfileService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbTeacherProfile")
public class SnickTeacherProfileController {
    
        @Autowired
	private SnickTeacherProfileService teacherProfileService;
    
        @GetMapping
	public ResponseEntity<?> search(SnickTeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.search(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @GetMapping("/classroom")
	public ResponseEntity<?> searchClassRoom(SnickTeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.searchClassRoom(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
          
        @GetMapping("/search")
	public ResponseEntity<?> searchauto(SnickTeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.searchauto(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickTeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.update(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/department/{userName}")
	public ResponseEntity<?> updateDepartment(@PathVariable("userName") String userName, @RequestBody SnickTeacherProfileDto[] teacherProfileDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.updateDepartment(userName, teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/department/delete/{userNameDepartment}")
	public ResponseEntity<?> updateDeleteDepartment(@PathVariable("userNameDepartment") String userNameDepartment, @RequestBody SnickTeacherProfileDto teacherProfileDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.updateDeleteDepartment(userNameDepartment, teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}  
        
        @PutMapping("/class/{userNameClass}")
	public ResponseEntity<?> updateClass(@PathVariable("userNameClass") String userNameClass, @RequestBody SnickTeacherProfileDto[] teacherProfileDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.updateClass(userNameClass, teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @PutMapping("/class/delete/{userNameClass}")
	public ResponseEntity<?> updateDeleteClass(@PathVariable("userNameClass") String userNameClass, @RequestBody SnickTeacherProfileDto teacherProfileDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.updateDeleteClass(userNameClass, teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}  
        
        @PutMapping("/classroom/{userNameClassRoom}")
	public ResponseEntity<?> updateClassRoom(@PathVariable("userNameClassRoom") String userNameClassRoom, @RequestBody SnickTeacherProfileDto[] teacherProfileDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.updateClassRoom(userNameClassRoom, teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @PutMapping("/classroom/delete/{userNameClassRoom}")
	public ResponseEntity<?> updateDeleteClassRoom(@PathVariable("userNameClassRoom") String userNameClassRoom, @RequestBody SnickTeacherProfileDto teacherProfileDto, HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.updateDeleteClassRoom(userNameClassRoom, teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}  
} 
