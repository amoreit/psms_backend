package com.go.scms.controller;

import com.go.scms.dto.SnickDepartmentDto;
import com.go.scms.service.SnickDepartmentService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_department") 
public class SnickDepartmentController {
    
        @Autowired
	private SnickDepartmentService departmentService;
    
        @GetMapping
	public ResponseEntity<?> search(SnickDepartmentDto departmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(departmentService.search(departmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
}
