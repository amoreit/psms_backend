/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbMstAuthorizedDto;
import com.go.scms.service.TbMstAuthorizedService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kronn
 */
@RestController
@RequestMapping("/api/v1/Authori")
public class TbMstAuthorizedController {
    
        @Autowired
	private TbMstAuthorizedService tbMstauthorizedService;
        
        @GetMapping
        public ResponseEntity<?> search(TbMstAuthorizedDto tbMstAuthorizedDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.search(tbMstAuthorizedDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody TbMstAuthorizedDto tbMstAuthorizedDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.insert(tbMstAuthorizedDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PostMapping("/insertIscurrent")
	public ResponseEntity<?> insertIscurrent(@RequestBody TbMstAuthorizedDto tbMstAuthorizedDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.insertIscurrent(tbMstAuthorizedDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
       
        @PutMapping("/update")
	public ResponseEntity<?> update(@RequestBody TbMstAuthorizedDto tbMstAuthorizedDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.update(tbMstAuthorizedDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/updateIscurrent")
	public ResponseEntity<?> updateIscurrent(@RequestBody TbMstAuthorizedDto tbMstAuthorizedDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.updateIscurrent(tbMstAuthorizedDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping("/delete")
	public ResponseEntity<?> delete(TbMstAuthorizedDto tbMstAuthorizedDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.delete(tbMstAuthorizedDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
    
        //aong
        @GetMapping("/getPosition")
        public ResponseEntity<?> position(TbMstAuthorizedDto tbMstAuthorizedDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(tbMstauthorizedService.getPosition(tbMstAuthorizedDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}
