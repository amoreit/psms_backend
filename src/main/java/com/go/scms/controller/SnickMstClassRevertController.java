package com.go.scms.controller;

import com.go.scms.dto.SnickMstClassDto;
import com.go.scms.service.SnickMstClassRevertService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbMstClassRevert")
public class SnickMstClassRevertController {
    
        @Autowired
	private SnickMstClassRevertService mstClassRevertService;
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickMstClassDto mstClassDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(mstClassRevertService.update(mstClassDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
