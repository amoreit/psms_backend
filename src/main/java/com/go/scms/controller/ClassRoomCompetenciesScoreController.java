/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ClassRoomCompetenciesScoreDto;
import com.go.scms.service.ClassRoomCompetenciesScoreService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/class-room-competencies-score")
public class ClassRoomCompetenciesScoreController {
     @Autowired
    private ClassRoomCompetenciesScoreService classRoomCompetenciesScoreService;
  
     @PutMapping("/{userName}")
	public ResponseEntity<?> update(@PathVariable("userName") String userName,@RequestBody ClassRoomCompetenciesScoreDto[] classRoomCompetenciesScoreDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(classRoomCompetenciesScoreService.update(userName,classRoomCompetenciesScoreDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
