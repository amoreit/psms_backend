/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TitleDto;
import com.go.scms.service.TitleService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/title")
public class TitleController {

    @Autowired
    private TitleService titleService;

    @GetMapping
    public ResponseEntity<?> search(TitleDto titleDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(titleService.search(titleDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(titleService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping
    public ResponseEntity<?> insert(@RequestBody TitleDto titleDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(titleService.insert(titleDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody TitleDto titleDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(titleService.update(titleDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(TitleDto titleDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(titleService.delete(titleDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
