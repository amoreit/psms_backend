package com.go.scms.controller;

import com.go.scms.dto.SnickTbStuProfileDocDto;
import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.service.SnickTbStuProfileDocService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbStuProfileDoc")
public class SnickTbStuProfileDocController {
    
        @Autowired
	private SnickTbStuProfileDocService tbStuProfileDocService;

        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickTbStuProfileDocDto tbStuProfileDocDto,HttpServletRequest request) throws Throwable{
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileDocService.insert(tbStuProfileDocDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickTbStuProfileDocDto tbStuProfileDocDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileDocService.update(tbStuProfileDocDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
