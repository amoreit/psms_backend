package com.go.scms.controller;

import com.go.scms.dto.TeacherProfileDto;
import com.go.scms.service.TeacherProfileService;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/teacherProfile")
public class TeacherProfileController {
    
        @Autowired
	private TeacherProfileService teacherProfileService;
    
        @GetMapping
	public ResponseEntity<?> search(TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.search(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/department")
	public ResponseEntity<?> searchDepartment(TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.searchDepartment(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/class")
	public ResponseEntity<?> searchClass(TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.searchClass(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/classromm")
	public ResponseEntity<?> searchClassRoom(TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.searchClassRoom(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/classidfromteacherprofile")
	public ResponseEntity<?> searchclassidfromteacherprofile(TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.searchclassidfromteacherprofile(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.update(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.insert(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping
	public ResponseEntity<?> delete(TeacherProfileDto teacherProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(teacherProfileService.delete(teacherProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
    
}
