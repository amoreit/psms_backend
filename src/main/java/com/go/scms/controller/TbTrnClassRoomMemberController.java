/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.PageGroupDto;
import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.service.PageGroupService;
import com.go.scms.service.TbTrnClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/classRoom-member")
public class TbTrnClassRoomMemberController {

    @Autowired
    private TbTrnClassRoomMemberService tbTrnClassRoomMemberService;

    @GetMapping 
    public ResponseEntity<?> search(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.search(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/searchAca004Sub06")
    public ResponseEntity<?> searchAca004Sub06(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchAca004Sub06(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/searchAca004Sub05Form")
    public ResponseEntity<?> searchAca004Sub05Form(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchAca004Sub05Form(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/rep001")
    public ResponseEntity<?> searchrep001(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep001(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/rep002")
    public ResponseEntity<?> searchrep002(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep002(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/rep003")
    public ResponseEntity<?> searchrep003(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep003(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/rep004")
    public ResponseEntity<?> searchrep004(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep004(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping("/{userName}")
    public ResponseEntity<?> insert(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.insert(userName, tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/{userName}")
    public ResponseEntity<?> update(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomMemberDto[] tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.update(userName, tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/searchAca006")
    public ResponseEntity<?> searchAca006(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchAca006(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //milo
    @GetMapping("/rep001/reportTerm1")
    public ResponseEntity<?> searchrep001ReportTerm1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep001ReportTerm1(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/rep001/reportCountTerm1")
    public ResponseEntity<?> searchrep001ReportCountTerm1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep001ReportCountTerm1(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/rep001/reportTerm2")
    public ResponseEntity<?> searchrep001ReportTerm2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep001ReportTerm2(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/rep001/reportCountTerm2")
    public ResponseEntity<?> searchrep001ReportCountTerm2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep001ReportCountTerm2(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/rep001/reportTeacher")
    public ResponseEntity<?> searchrep001ReportTeacher(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.searchRep001ReportTeacher(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/getActivityScore")
    public ResponseEntity<?> acticityScore(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.acticityScore(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/ratwResult")
    public ResponseEntity<?> ratwResult(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.ratwResult(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/desiredCharResult")
    public ResponseEntity<?> desiredCharResult(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomMemberService.desiredCharResult(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
}
