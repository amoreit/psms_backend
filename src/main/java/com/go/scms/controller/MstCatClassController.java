/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.MstCatClassDto;
import com.go.scms.dto.YearGroupDto;
import com.go.scms.service.MstCatClassService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/catClass")
public class MstCatClassController {

    @Autowired
    private MstCatClassService mstCatClassService;

    @GetMapping
    public ResponseEntity<?> search(MstCatClassDto mstCatClassDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(mstCatClassService.search(mstCatClassDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(mstCatClassService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping
    public ResponseEntity<?> insert(@RequestBody MstCatClassDto mstCatClassDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(mstCatClassService.insert(mstCatClassDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody MstCatClassDto mstCatClassDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(mstCatClassService.update(mstCatClassDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(MstCatClassDto mstCatClassDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(mstCatClassService.delete(mstCatClassDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
