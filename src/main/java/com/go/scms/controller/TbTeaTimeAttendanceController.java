/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;



import com.go.scms.dto.TbTeaTimeAttendanceDto;
import com.go.scms.service.TbTeaTimeAttendanceService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/tbteatime-attendance")
public class TbTeaTimeAttendanceController {
    
    @Autowired
         private TbTeaTimeAttendanceService tbTeaTimeAttendanceService;
    
      @GetMapping
    public ResponseEntity<?> search(TbTeaTimeAttendanceDto tbTeaTimeAttendanceDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTeaTimeAttendanceService.search(tbTeaTimeAttendanceDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PostMapping
	public ResponseEntity<?> insert(@RequestBody TbTeaTimeAttendanceDto tbTeaTimeAttendanceDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbTeaTimeAttendanceService.insert(tbTeaTimeAttendanceDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
    
}
