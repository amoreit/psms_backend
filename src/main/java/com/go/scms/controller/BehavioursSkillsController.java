/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.BehavioursSkillsDto;
import com.go.scms.service.BehavioursSkillsService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author P3rdiscoz
 */
@RestController
@RequestMapping("/api/v1/behaviours-skills")
public class BehavioursSkillsController {
    @Autowired
    private BehavioursSkillsService behavioursSkillsService;

        @GetMapping
	public ResponseEntity<?> search(BehavioursSkillsDto behavioursSkillsDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(behavioursSkillsService.searchbehavioursskills(behavioursSkillsDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
