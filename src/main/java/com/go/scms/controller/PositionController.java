/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ActivityPaiDto;
import com.go.scms.dto.PositionDto;
import com.go.scms.service.PositionService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author kronn
 */
@RestController
@RequestMapping("/api/v1/position")
public class PositionController {
    
        @Autowired
	private PositionService positionService;
        
        @GetMapping
        public ResponseEntity<?> search(PositionDto positionDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(positionService.search(positionDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
         @PostMapping
	public ResponseEntity<?> insert(@RequestBody PositionDto positionDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(positionService.insert(positionDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
       
      @PutMapping("/update")
	public ResponseEntity<?> update(@RequestBody PositionDto positionDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(positionService.update(positionDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping("/delete")
	public ResponseEntity<?> delete(PositionDto positionDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(positionService.delete(positionDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
    
}
