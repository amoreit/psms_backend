/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.StuProfileDto;
import com.go.scms.service.StuProfileService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/stu-profile")
public class StuProfileController {

    @Autowired
    private StuProfileService stuProfileService;

    @GetMapping
    public ResponseEntity<?> search(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.search(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/mini")
    public ResponseEntity<?> updatemini(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.updatemini(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.update(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/updateandinsert")
    public ResponseEntity<?> updateandinsert(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.updateandinsert(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/smartPurse")
    public ResponseEntity<?> updateSmartPurse(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.updateSmartPurse(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/updateisleft")
    public ResponseEntity<?> updateisleft(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.updateisleft(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/cancelisleft")
    public ResponseEntity<?> cancelisleft(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.cancelisleft(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.delete(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    //nock
    @GetMapping("/cer002")
    public ResponseEntity<?> searchCer002(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.searchCer002(stuProfileDto));
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    //nock
    @GetMapping("/rep006")
    public ResponseEntity<?> searchRep006(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.searchRep006(stuProfileDto));
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //nock
    @GetMapping("/gra005")
    public ResponseEntity<?> searchGra005(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.searchGra005(stuProfileDto));
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //by Moss
    @PutMapping("/{userName}")
    public ResponseEntity<?> updategraduate(@PathVariable("userName") String userName, @RequestBody StuProfileDto[] stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.updategraduate(userName, stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    //by milo
    @GetMapping("/stu010")
    public ResponseEntity<?> searchStu010(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.searchStu010(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/stp")
    public ResponseEntity<?> searchstu(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.searchstu(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong 
    @GetMapping("/stuPic")
    public ResponseEntity<?> searchstuPic(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(stuProfileService.searchstuPic(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    } 

    
}
