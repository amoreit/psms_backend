package com.go.scms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.dto.PageGroupDto;
import com.go.scms.service.PageGroupService;

@RestController
@RequestMapping("/api/v1/page-group")
public class PageGroupController {

	@Autowired
	private PageGroupService pageGroupService;
	
	@GetMapping
	public ResponseEntity<?> search(PageGroupDto pageGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageGroupService.search(pageGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageGroupService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> insert(@RequestBody PageGroupDto pageGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageGroupService.insert(pageGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody PageGroupDto pageGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageGroupService.update(pageGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@DeleteMapping
	public ResponseEntity<?> delete(PageGroupDto pageGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(pageGroupService.delete(pageGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
