package com.go.scms.controller;

import com.go.scms.dto.SnickClassRoomSubjectDto;
import com.go.scms.service.SnickClassRoomSubjectService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/_trnClassRoomSubject")
public class SnickClassRoomSubjectController {
    
        @Autowired
        private SnickClassRoomSubjectService classRoomSubjectService;

        @GetMapping
        public ResponseEntity<?> search(SnickClassRoomSubjectDto classRoomSubjectDto, HttpServletRequest request) {
            try {
                return ResponseEntity.status(HttpStatus.OK).body(classRoomSubjectService.search(classRoomSubjectDto));
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
}
