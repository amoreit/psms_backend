/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbTrnClassRoomSubjectDto;
import com.go.scms.service.TbTrnClassRoomSubjectService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/classRoom-subject")
public class TbTrnClassRoomSubjectController {
    
    @Autowired
    private TbTrnClassRoomSubjectService tbTrnClassRoomSubjectService;

    @GetMapping
    public ResponseEntity<?> search(TbTrnClassRoomSubjectDto tbTrnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectService.search(tbTrnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    @GetMapping("/search2")
    public ResponseEntity<?> search2(TbTrnClassRoomSubjectDto tbTrnClassRoomSubjectDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectService.search2(tbTrnClassRoomSubjectDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
