/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.PageDto;
import com.go.scms.dto.RoleDto;
import com.go.scms.dto.YearGroupDto;
import com.go.scms.repository.YearGroupRepository;
import com.go.scms.service.YearGroupService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
//@RequestMapping("/api/v1/yeargroup")
@RequestMapping("/api/v1/yeargroup")
public class YearGroupController {

    @Autowired
    private YearGroupService yearGroupService;

    @GetMapping
	public ResponseEntity<?> search(YearGroupDto yearGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.search(yearGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> insert(@RequestBody YearGroupDto yearGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.insert(yearGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PostMapping("/insertIscurrent")
	public ResponseEntity<?> insertIscurrent(@RequestBody YearGroupDto yearGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.insertIscurrent(yearGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody YearGroupDto yearGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.update(yearGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping("/updateIscurrent")
	public ResponseEntity<?> updateIscurrent(@RequestBody YearGroupDto yearGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.updateIscurrent(yearGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@DeleteMapping
	public ResponseEntity<?> delete(YearGroupDto yearGroupDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(yearGroupService.delete(yearGroupDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
