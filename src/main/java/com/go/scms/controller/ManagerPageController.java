package com.go.scms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.dto.ManagePageDto;
import com.go.scms.service.ManagePageService;

@RestController
@RequestMapping("/api/v1/manage-page/{roleId}")
public class ManagerPageController {

	@Autowired
	private ManagePageService managePageService;
	
	@GetMapping
	public ResponseEntity<?> search(@PathVariable("roleId") Long roleId,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(managePageService.search(roleId));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody ManagePageDto[] managePageDto,@PathVariable("roleId") Long roleId,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(managePageService.save(roleId, managePageDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
