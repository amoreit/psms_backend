/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ManagePageDto;
import com.go.scms.dto.TbTrnClassRoomSubjectScoreDto;
import com.go.scms.service.TbTrnClassRoomSubjectScoreService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/classRoom-subjectScore")
public class TbTrnClassRoomSubjectScoreController {

    @Autowired
    private TbTrnClassRoomSubjectScoreService tbTrnClassRoomSubjectScoreService;

    @GetMapping
    public ResponseEntity<?> search(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.search(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/searchAca006Form")
    public ResponseEntity<?> searchAca006Form(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.searchAca006Form(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/{userName}")
    public ResponseEntity<?> update(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.update(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/save/{userName}")
    public ResponseEntity<?> save(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomSubjectScoreDto[] tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.save(userName, tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @PutMapping("/save2/{userName}")
    public ResponseEntity<?> save2(@PathVariable("userName") String userName, @RequestBody TbTrnClassRoomSubjectScoreDto[] tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.save2(userName, tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/getPrimarySubjectScore")
    public ResponseEntity<?> getPrimarySubjectScore(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.getPrimarySubjectScore(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong 
    @GetMapping("/getSecondarySubjectScore")
    public ResponseEntity<?> getSecondarySubjectScore(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.getSecondarySubjectScore(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
        //aong
    @GetMapping("/getSumScorePrimary")
    public ResponseEntity<?> getSumScorePrimary(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.getSumScorePrimary(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/getSumScore")
    public ResponseEntity<?> getSumScore(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.getSumScore(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    //aong
    @GetMapping("/getSumGpa")
    public ResponseEntity<?> getSumGpa(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.getSumGpa(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    //aong
    @GetMapping("/getSumGpaPrimary")
    public ResponseEntity<?> getSumGpaPrimary(TbTrnClassRoomSubjectScoreDto tbTrnClassRoomSubjectScoreDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(tbTrnClassRoomSubjectScoreService.getSumGpaPrimary(tbTrnClassRoomSubjectScoreDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}
