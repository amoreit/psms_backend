/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.DepartmentPaiDto;
import com.go.scms.service.DepartmentPaiService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/department")
public class DepartmentPaiController {
    
    @Autowired
	private DepartmentPaiService departmentService;
    
    @GetMapping
	public ResponseEntity<?> search(DepartmentPaiDto departmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(departmentService.search(departmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(departmentService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody DepartmentPaiDto departmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(departmentService.insert(departmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody DepartmentPaiDto departmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(departmentService.update(departmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @DeleteMapping
	public ResponseEntity<?> delete(DepartmentPaiDto departmentDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(departmentService.delete(departmentDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
