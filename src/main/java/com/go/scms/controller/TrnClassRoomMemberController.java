/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.Admrp002sub01Dto;
import com.go.scms.dto.StuProfileDto;
import com.go.scms.dto.TeacherProfileDto;
import com.go.scms.dto.TrnClassRoomMemberDto;
import com.go.scms.service.TrnClassRoomMemberService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/trn-classroom-member")
public class TrnClassRoomMemberController {

    @Autowired
    private TrnClassRoomMemberService trnClassRoomMemberService;

    @GetMapping("/stu")
    public ResponseEntity<?> search(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.search(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/stufordialog")
    public ResponseEntity<?> searchstufordialog(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchstufordialog(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/classidandclassnamebyclassroomid")
    public ResponseEntity<?> searchclassidandclassnamebyclassroomid(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchclassidandclassnamebyclassroomid(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/allnumstuandgender")
    public ResponseEntity<?> searchallnumstuandgender(HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchallnumstuandgender());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/allnumteacherandgender")
    public ResponseEntity<?> searchallnumteacherandgender(HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchallnumteacherandgender());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/graphallnumstubyyear")
    public ResponseEntity<?> searchgraphallnumstubyyear(HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchgraphallnumstubyyear());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/numstuallcatclasspie")
    public ResponseEntity<?> searchnumstuallcatclasspie(HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchnumstuallcatclasspie());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/teacher")
    public ResponseEntity<?> searchteacher(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchteacher(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/stuforevaluation")
    public ResponseEntity<?> searchstuforevaluation(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchstuforevaluation(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/department")
    public ResponseEntity<?> searchdepartment(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchdepartment(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/departmentfromclass")
    public ResponseEntity<?> searchdepartmentfromclass(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchdepartmentfromclass(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/subjectfromclass")
    public ResponseEntity<?> searchsubjectfromclass(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchsubjectfromclass(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/activity")
    public ResponseEntity<?> searchactivity(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchactivity(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/subjectclassroom")
    public ResponseEntity<?> searchsubjectclassroom(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchsubjectclassroom(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/stufornumber")
    public ResponseEntity<?> searchstufornumber(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchstufornumber(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/memberfromsubject")
    public ResponseEntity<?> searchmemberfromsubject(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchmemberfromsubject(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/stuforcompetencies")
    public ResponseEntity<?> searchstuforcompetencies(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchstuforcompetencies(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/numberstu")
    public ResponseEntity<?> searchnumberstu(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchnumberstu(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/numberstuforcatclass")
    public ResponseEntity<?> searchnumberstuforcatclass(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchnumberstuforcatclass(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/numberstuforcatclasspie")
    public ResponseEntity<?> searchnumberstuforcatclasspie(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchnumberstuforcatclasspie(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/admrp001sub02pie")
    public ResponseEntity<?> searchforadmrp001sub02pie(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchforadmrp001sub02pie(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/admrp001sub02")
    public ResponseEntity<?> searchforadmrp001sub02(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchforadmrp001sub02(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/admrp001sub01")
    public ResponseEntity<?> searchforadmrp001sub01(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchforadmrp001sub01(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/admrp002sub1")
    public ResponseEntity<?> searchforadmrp002sub1(Admrp002sub01Dto admrp002sub01Dto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchforadmrp002sub1(admrp002sub01Dto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/admrp002sub2")
    public ResponseEntity<?> searchforadmrp002sub2(Admrp002sub01Dto admrp002sub01Dto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchforadmrp002sub2(admrp002sub01Dto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/stuforstu007")
    public ResponseEntity<?> searchstuforstu007(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchstuforstu007(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/stuforstu009")
    public ResponseEntity<?> searchstuforstu009(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchstuforstu009(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/teachertec010")
    public ResponseEntity<?> searchteachertec010(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.searchteachertec010(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping("/copy/{userName}")
    public ResponseEntity<?> insertcopy(@PathVariable("userName") String userName, @RequestBody TrnClassRoomMemberDto[] trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.insertcopy(userName, trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping("/stufordialog/{userName}")
    public ResponseEntity<?> insertstufordialog(@PathVariable("userName") String userName, @RequestBody TrnClassRoomMemberDto[] trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.insertstufordialog(userName, trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping("/copyteachertec010/{userName}")
    public ResponseEntity<?> insertcopyteachertec010(@PathVariable("userName") String userName, @RequestBody TrnClassRoomMemberDto[] trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.insertcopyteachertec010(userName, trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.update(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/updatenumber")
    public ResponseEntity<?> updatenumber(@RequestBody TrnClassRoomMemberDto[] trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.updatenumber(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/updateclassroom")
    public ResponseEntity<?> updateclassroom(@RequestBody TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.updateclassroom(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/{userName}")
    public ResponseEntity<?> update(@PathVariable("userName") String userName, @RequestBody TrnClassRoomMemberDto[] trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.updatecompetencies(userName, trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(TrnClassRoomMemberDto trnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.delete(trnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/updateclassroomteacher")
    public ResponseEntity<?> updateclassroomteacher(@RequestBody TeacherProfileDto teacherProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.updateclassroomidfromteacher(teacherProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping("/updateclassroomstu")
    public ResponseEntity<?> updateclassroomstu(@RequestBody StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.updateclassroomidfromstu(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping("/deleteclassroomteacher")
    public ResponseEntity<?> deleteclassroomteacher(TeacherProfileDto teacherProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.deleteclassroomidfromteacher(teacherProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping("/deleteclassroomstudent") 
    public ResponseEntity<?> deleteclassroomstudent(StuProfileDto stuProfileDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(trnClassRoomMemberService.deleteclassroomidfromstudent(stuProfileDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

}
