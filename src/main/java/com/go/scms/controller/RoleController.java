package com.go.scms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.dto.RoleDto;
import com.go.scms.service.RoleService;

@RestController
@RequestMapping("/api/v1/role")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	@GetMapping
	public ResponseEntity<?> search(RoleDto roleDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(roleService.search(roleDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(roleService.findById(id));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PostMapping
	public ResponseEntity<?> insert(@RequestBody RoleDto roleDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(roleService.insert(roleDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody RoleDto roleDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(roleService.update(roleDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@DeleteMapping
	public ResponseEntity<?> delete(RoleDto roleDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(roleService.delete(roleDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
}
