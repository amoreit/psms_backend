/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.ActivityGroupDto;
import com.go.scms.service.ActivityGroupService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/activity-group")
public class ActivityGroupController {
    
    @Autowired
    private ActivityGroupService activityGroupService;

    @GetMapping
    public ResponseEntity<?> search(ActivityGroupDto activityGroupDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(activityGroupService.search(activityGroupDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(activityGroupService.findById(id));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PostMapping
    public ResponseEntity<?> insert(@RequestBody ActivityGroupDto activityGroupDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(activityGroupService.insert(activityGroupDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody ActivityGroupDto activityGroupDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(activityGroupService.update(activityGroupDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(ActivityGroupDto activityGroupDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(activityGroupService.delete(activityGroupDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
