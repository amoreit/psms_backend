package com.go.scms.controller;

import com.go.scms.dto.SnickTbStuProfileDto;
import com.go.scms.dto.TbStuProfileDto;
import com.go.scms.service.SnickTbStuProfileService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/tbStuProfileSingup")
public class SnickTbStuProfileController {
    
        @Autowired
	private SnickTbStuProfileService tbStuProfileService; 

        @GetMapping
	public ResponseEntity<?> searchRefNo(SnickTbStuProfileDto stuProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.searchRefNo(stuProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	} 
        
        @PostMapping
	public ResponseEntity<?> insert(@RequestBody SnickTbStuProfileDto stuProfileDto,HttpServletRequest request) throws Throwable{
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.insert(stuProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @PutMapping
	public ResponseEntity<?> update(@RequestBody SnickTbStuProfileDto stuProfileDto,HttpServletRequest request){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(tbStuProfileService.update(stuProfileDto));
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
