/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.controller;

import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.service.StudyPlanService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sir. Harinn
 */
@RestController
@RequestMapping("/api/v1/stu-plan")
public class StudyPlanController {
    
    @Autowired
    private StudyPlanService studyPlanService;
    
    
    @GetMapping("/search")
    public ResponseEntity<?> search(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(studyPlanService.search(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
      
    
    @GetMapping("/term1")
    public ResponseEntity<?> searchTerm1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(studyPlanService.searchTerm1(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping("/term2")
    public ResponseEntity<?> searchTerm2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletRequest request) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(studyPlanService.searchTerm2(tbTrnClassRoomMemberDto));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
}
