package com.go.scms.controller;

import com.go.scms.dto.SnickForgotPwDto;
import com.go.scms.entity.SnickForgotPwEntity;
import com.go.scms.mail.MailInput;
import com.go.scms.mail.MailService;
import com.go.scms.repository.SnickForgotPwRepository;
import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;
import com.go.scms.utils.ExceptionUtils;
import com.go.scms.utils.ValiableUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SnickForgotPwController {
    
    @Autowired
    private SnickForgotPwRepository forgotPwRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BCryptPasswordEncoder cry;
    
    @Autowired
    private MailService mailService;

    @GetMapping("/forgot-password-check")
    public ResponseEntity<?> search(SnickForgotPwDto forgotPwDto,HttpServletRequest request){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            String citizenId = ValiableUtils.stringIsEmpty(forgotPwDto.getCitizenId().trim());
            String usrName = ValiableUtils.stringIsEmpty(forgotPwDto.getUsrName().trim());

            List<SnickForgotPwEntity> list = forgotPwRepo.findAllByCitizenIdAndUsrName(citizenId, usrName);
            output.setResponseData(list);

            return ResponseEntity.status(HttpStatus.OK).body(output);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.status(HttpStatus.OK).body(ExceptionUtils.validate(e));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    } 
    
    @PutMapping("/forgot-password")
    public ResponseEntity<?> updateforgotpw(@RequestBody SnickForgotPwDto forgotPwDto,HttpServletRequest request){
        ResponseOutput output = new ResponseOutput(ResponseStatus.SUCCESS);
        try {
            Integer userId = Integer.parseInt(ValiableUtils.stringIsEmpty(forgotPwDto.getUserId()));
            String firstName = ValiableUtils.stringIsEmpty(forgotPwDto.getFirstName());
            String lastName = ValiableUtils.stringIsEmpty(forgotPwDto.getLastName());
            String email = ValiableUtils.stringIsEmpty(forgotPwDto.getEmail());
            String password = cry.encode(ValiableUtils.stringIsEmpty(forgotPwDto.getPassword()));

            forgotPwRepo.updateforgotpw(password, userId);
            
            /** Send Mail**/
            Map map = new HashMap<>();
            map.put("firstName", firstName);
            map.put("lastName", lastName);
            map.put("password", forgotPwDto.getPassword());

            MailInput input = new MailInput();
            input.setPathHtml("forgotPassword.html");
            input.setRecipients(email);
            input.setSubject("PRAMANDANIJJANUKROAH SCHOOL");
            input.setInput(map);

            mailService.sendMessage(input);

            return ResponseEntity.status(HttpStatus.OK).body(output);
        } catch (ConstraintViolationException e) {
            return ResponseEntity.status(HttpStatus.OK).body(ExceptionUtils.validate(e));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    } 
    
}
