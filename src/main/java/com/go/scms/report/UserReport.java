package com.go.scms.report;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.dto.UserDto;
import com.go.scms.utils.ValiableUtils;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@RestController
@RequestMapping("/api/v1/user/report")
public class UserReport {
	
	@Autowired
	private EntityManager em;
	
	@GetMapping(value = "/pdf",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report(UserDto userDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(userDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/user-report.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@GetMapping(value = "/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportXls(UserDto userDto, HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(userDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/user-report.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

	@GetMapping(value = "/docx",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportDocx(UserDto userDto, HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(userDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/user-report.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
	
}
