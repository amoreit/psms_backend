/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.report;

import com.go.scms.dto.DepartmentPaiDto;
import com.go.scms.dto.TeacherProfileDto;
import com.go.scms.dto.TeacherProfileReportDto;
import com.go.scms.utils.ValiableUtils;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bamboo
 */
@RestController
@RequestMapping("/api/v1/teacherreport/report")
public class TeacherprofileReport {
    
        @Autowired
	private EntityManager em;
        
        @GetMapping(value = "/pdf",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report(TeacherProfileDto teacherProfileDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                    
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClass(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-class.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfdepartment",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportDepartment(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-department.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfdepartmentclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportDepartmentClass(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-departmentclass.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfdepartmentclassfrist",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportDepartmentClassfirst(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-departmentclassfirst.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdffirst",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportfirst(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-first.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/pdffirstclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportfirstClass(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-firstclass.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/pdffirstlastdepartmentclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportFirstLastDepartmentClass(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-firstlastdepartmentclass.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/pdffirstlastdepartment",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportFirstLastDepartment(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-firstlastdepartment.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/pdflastClass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportLastClass(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-lastclass.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/pdflastdepartment",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportLastDepartment(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-lastdepartment.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/pdflastdepartmentclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportLastDepartmentClass(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                  
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());
                        
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-lastdepartmentclass.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
                
	}
        
        @GetMapping(value = "/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportXls(TeacherProfileDto teacherProfileDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-class.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsdepartment",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassDepartmentXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-department.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsdepartmentclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassDepartmenClasstXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-departmentclass.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsdepartmentclassfirst",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassDepartmenClasstFirstXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-departmentclassfirst.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsfirst",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportFirstXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-first.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsfirstclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportFirstClasstXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-firstclass.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsfirstlastdepartmentclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportFirstLastDepartmentClasstXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-firstlastdepartmentclass.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsfirstlastdepartment",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportFirstLastDepartmentXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-firstlastdepartment.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlslastclasss",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportLastClassXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-lastclass.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlslastdepartment",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportLastDepartmentXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-lastdepartment.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlslastdepartmentclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportLastDepartmentClassXls(TeacherProfileReportDto teacherProfileReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(teacherProfileReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/tec001-lastdepartmentclass.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
