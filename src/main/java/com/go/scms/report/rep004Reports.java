/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.report;

import com.go.scms.dto.PageGroupDto;
import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.dto.report004Dto;
import com.go.scms.service.ReportService;
import com.go.scms.utils.ValiableUtils;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Kittipong
 */

@RestController
@RequestMapping("/api/v1/rep004/report")
public class rep004Reports {

    @Autowired
    private EntityManager em;

    @Autowired
    private ReportService reportService;

    @Value("classpath:reports/rec-013TH-finalA4-1_2.jrxml")
    Resource rec013THfinalA4;

    @GetMapping(value = "/pdfA3final", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report1(report004Dto Report004Dto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /*** Convert string to hashMap ***/
            HashMap map = ValiableUtils.stringToHashMap(Report004Dto.toString());
            /*** Query ***/
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-013TH-Final.jrxml"));

            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /*** Export ***/
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /*** Close connection ***/
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdfA3mid", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report2(report004Dto Report004Dto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /*** Convert string to hashMap ***/
            HashMap map = ValiableUtils.stringToHashMap(Report004Dto.toString());
            /*** Query ***/
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-013TH-Mid.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /*** Export ***/
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /*** Close connection ***/
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdfA41final", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report3(report004Dto Report004Dto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /*** Convert string to hashMap ***/
            HashMap map = ValiableUtils.stringToHashMap(Report004Dto.toString());

            /*** Query ***/
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-013TH-finalA4-1.jrxml"));

            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /*** Export ***/
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /*** Close connection ***/
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdfA42final", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report4(report004Dto Report004Dto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /*** Convert string to hashMap ***/
            HashMap map = ValiableUtils.stringToHashMap(Report004Dto.toString());
            /*** Query ***/
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-013TH-finalA4-2.jrxml"));

            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /*** Export ***/
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /*** Close connection ***/
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdfA41mid", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report5(report004Dto Report004Dto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /*** Convert string to hashMap ***/
            HashMap map = ValiableUtils.stringToHashMap(Report004Dto.toString());
            /*** Query ***/
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-013TH-midA4-1.jrxml"));

            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /*** Export ***/
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /*** Close connection ***/
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdfA42mid", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report6(report004Dto Report004Dto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /*** Convert string to hashMap ***/
            HashMap map = ValiableUtils.stringToHashMap(Report004Dto.toString());

            /*** Query ***/
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-013TH-midA4-2.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /*** Export ***/
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /*** Close connection ***/
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdfA4final", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<?> report3(@RequestParam("yearGroupId") Integer yearGroupId, @RequestParam("classId") Integer classId, @RequestParam("classRoomId") Integer classRoomId) {
        /*** Convert string to hashMap ***/
        ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
        Map param = new HashMap<String, Integer>();
        param.put("yearGroupId", yearGroupId);
        param.put("classId", classId);
        param.put("classRoomId", classRoomId);
        /*** Query ***/
        JasperDesign design = null;
        try {
            design = JRXmlLoader.load(rec013THfinalA4.getInputStream());
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            pdfReportStream = reportService.exportReport(param, jasperReport);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
    }
}
