/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.report;

import com.go.scms.dto.SubjectDto;
import com.go.scms.dto.TitleDto;
import com.go.scms.utils.ValiableUtils;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/title/report")
public class Mst014Report {

    @Autowired
    private EntityManager em;

    @GetMapping(value = "/pdfall", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> reportpdfall(TitleDto titleDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            HashMap map = ValiableUtils.stringToHashMap(titleDto.toString());

            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst014-1.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
     @GetMapping(value = "/pdfbygender", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> reportpdfbygender(TitleDto titleDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            HashMap map = ValiableUtils.stringToHashMap(titleDto.toString());

            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst014-2.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/xlsall", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> exportXlsall(TitleDto titleDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            HashMap map = ValiableUtils.stringToHashMap(titleDto.toString());

            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst014-1.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(false);
            configuration.setWhitePageBackground(false);
            exporter.setConfiguration(configuration);
            exporter.exportReport();

            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
    
    @GetMapping(value = "/xlsbygender", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> exportXlsbygender(TitleDto titleDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            HashMap map = ValiableUtils.stringToHashMap(titleDto.toString());

            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst014-2.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(false);
            configuration.setWhitePageBackground(false);
            exporter.setConfiguration(configuration);
            exporter.exportReport();

            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
