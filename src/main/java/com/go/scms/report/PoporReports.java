package com.go.scms.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.go.scms.dto.PoporDto;
import com.go.scms.service.ReportService;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Transactional(readOnly = true)
@RestController
@RequestMapping("/api/v1/popor/report")
public class PoporReports {

    @Autowired
    private ReportService reportService;

    @Autowired
    protected NamedParameterJdbcTemplate jdbcTemplate;

    @Value("classpath:reports/popor03A_01.jrxml")
    Resource popor03A_01;

    @Value("classpath:reports/popor03A_02.jrxml")
    Resource popor03A_02;

    @Value("classpath:reports/popor03B_01.jrxml")
    Resource popor03B_01;

    @Value("classpath:reports/popor03B_02.jrxml")
    Resource popor03B_02;

    @GetMapping(value = "/{yearGroupId}/{classId}", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<?> popor (@PathVariable Integer yearGroupId, @PathVariable("classId") Integer classId) throws UnsupportedEncodingException {
        /*** Convert string to hashMap ***/
        StringBuffer sql = new StringBuffer();
        sql.append("  SELECT count (*)                                                                                ");
        sql.append("            AS totals,                                                                            ");
        sql.append("         log.cat_class_id,                                                                        ");
        sql.append("         sum (CASE WHEN log.gender = 'หญิง' THEN 1 ELSE 0 END)                                        ");
        sql.append("            AS female,                                                                            ");
        sql.append("         sum (CASE WHEN log.gender <> 'หญิง' THEN 1 ELSE 0 END)                                       ");
        sql.append("            AS male                                                                               ");
        sql.append("    FROM tb_stu_transcript_log log                                                                ");
        sql.append("         INNER JOIN tb_mst_cat_class t ON t.cat_class_id = log.cat_class_id                       ");
        sql.append("   WHERE log.isactive = TRUE AND log.year_group_id =  :year_group_id AND log.class_id =  :class_id ");
        sql.append("GROUP BY log.cat_class_id                                                                         ");
        Map map = new HashMap();
        map.put("year_group_id",yearGroupId);
        map.put("class_id",classId);
        PoporDto poporDto = (PoporDto) jdbcTemplate.queryForObject(sql.toString(),map, new BeanPropertyRowMapper(PoporDto.class));
        // object -> Map
        Map<String, Object> poporDtoMap = new ObjectMapper().convertValue(poporDto, Map.class);
        poporDtoMap.putAll(map);
        poporDtoMap.putAll(jdbcTemplate.queryForMap("SELECT fullname_th FROM public.tb_mst_authorized where iscurrent=true and \"position\"='ผู้อำนวยการ'",map));
        ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
        //*** Query ***//*
        try{
            if(poporDto.getCatClassId() <= 2){
                pdfReportStream = reportService.exportReport(poporDtoMap,JasperCompileManager.compileReport(JRXmlLoader.load(popor03A_01.getInputStream())),JasperCompileManager.compileReport(JRXmlLoader.load(popor03A_02.getInputStream())));
            }else{
                pdfReportStream = reportService.exportReport(poporDtoMap,JasperCompileManager.compileReport(JRXmlLoader.load(popor03B_01.getInputStream())),JasperCompileManager.compileReport(JRXmlLoader.load(popor03B_02.getInputStream())));
            }
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
        sql = new StringBuffer();
        sql.append("SELECT (SELECT c.class_lname_th                 ");
        sql.append("          FROM tb_mst_class c                   ");
        sql.append("         WHERE c.class_id = :class_id),         ");
        sql.append("       (SELECT y.name                           ");
        sql.append("          FROM tb_mst_year_group y              ");
        sql.append("         WHERE y.year_group_id = :year_group_id)");
        Map report = jdbcTemplate.queryForMap(sql.toString(),map);
        String reportName = "ปพ3_"+report.get("class_lname_th").toString().replaceAll("\\s+","")+"_ภาคเรียน_ปีการศึกษา_"+report.get("name").toString().replaceAll("\\s+","");
        return ResponseEntity.status(HttpStatus.OK).header("Content-Disposition", "inline; filename="+ URLEncoder.encode (reportName,"utf-8")+".pdf").body(pdfReportStream.toByteArray());
    }
}
