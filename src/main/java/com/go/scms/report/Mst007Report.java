/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.report;

import com.go.scms.dto.ActivityPaiReportDto;
import com.go.scms.utils.ValiableUtils;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/mst007/report")
public class Mst007Report {
    
        @Autowired
	private EntityManager em;
        
        @GetMapping(value = "/pdf",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());     
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClass(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007class.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfclassActivityCode",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClassActivityCode(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classActivity.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        
        @GetMapping(value = "/pdfclassActivityName",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClassActivityName(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classActivityName.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdflocation",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportLocation(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007location.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfclasslocation",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClassLocation(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classlocation.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfactivitylocation",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportActivityLocation(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
                        System.out.print("4.5-------> "+ map+"\n");
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007activitylocation.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfactivitynamelocation",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportActivityNameLocation(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007activitynamelocation.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfclassActivityCodeActivityName",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClassActivityCodeActivityName(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
                        /*** Convert string to hashMap ***/
                        HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

                        /*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classActivityCodeActivityName.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

                        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfclassActivityName2",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClassActivityName2(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007-class-ActivityName.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

        	        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/pdfActivityCodeActivityName3",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> reportClassActivityName3(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){
			
			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());
			
			/*** Query ***/
                        JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst-ActivityGroup-ActivityName.jrxml"));
                        JasperReport jasperReport = JasperCompileManager.compileReport(design);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

        	        /*** Export ***/
                        JRPdfExporter pdfExporter = new JRPdfExporter();
                        pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                        pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                        pdfExporter.exportReport();

			/*** Close connection ***/
			connection.close();
	        
                        return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportXls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsclass",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassXls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007class.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}  
	}
        
        @GetMapping(value = "/xlsclassActivityCode",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassActivityXls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classActivity.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/xlsclassActivityName",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassActivityNameXls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classActivityName.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}  
	}
        
        @GetMapping(value = "/xlsclassActivityCodeActivityName",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportClassActivityCodeActivityNameXls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007classActivityCodeActivityName.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}        
	}
        
        @GetMapping(value = "/xlsclassActivityName2",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportclassActivityName2Xls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007-class-ActivityName.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}       
	}
        
        @GetMapping(value = "/xlsActivityCodeActivityName3",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportActivityCodeActivityName3Xls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst-ActivityGroup-ActivityName.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}       
	}
        
         @GetMapping(value = "/xlslocation",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportlocationXls(ActivityPaiReportDto activityPaiReportDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(activityPaiReportDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/mst007location.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}       
	}
}
