package com.go.scms.report;

import com.go.scms.dto.MiloTbTrnClassRoomMemberDto;
import com.go.scms.utils.ValiableUtils;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author M-S-I
 */
 
@RestController
@RequestMapping("/api/v1/cer002/report")
public class cer002Reports {
    
    @Autowired
    private EntityManager em;
    
    @GetMapping(value = "/cer002/docx",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportDocx(MiloTbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());
                        
			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/cer-002.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "/cer002_2/docx",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> exportDocx1(MiloTbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/; 
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/cer-002_2.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
