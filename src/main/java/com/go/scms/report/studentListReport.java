/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.report;

import com.go.scms.dto.StudentListDto;
import com.go.scms.utils.ValiableUtils;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lumin
 */
@RestController
@RequestMapping("/api/v1/studentList/report")
public class studentListReport {

    @Autowired
    private EntityManager em;

    @GetMapping(value = "/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report(StudentListDto studentListDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /**
             * * Convert string to hashMap **
             */
            HashMap map = ValiableUtils.stringToHashMap(studentListDto.toString());

            /**
             * * Query **
             */
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/studentlist.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /**
             * * Export **
             */
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /**
             * * Close connection **
             */
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdf2", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report2(StudentListDto studentListDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /**
             * * Convert string to hashMap **
             */
            HashMap map = ValiableUtils.stringToHashMap(studentListDto.toString());

            /**
             * * Query **
             */
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/studentlist2.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /**
             * * Export **
             */
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /**
             * * Close connection **
             */
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }

    @GetMapping(value = "/pdf3", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Object> report3(StudentListDto studentListDto, HttpServletResponse response) {
        SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
        try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

            /**
             * * Convert string to hashMap **
             */
            HashMap map = ValiableUtils.stringToHashMap(studentListDto.toString());

            /**
             * * Query **
             */
            JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/studentlistNoReligion.jrxml"));
            JasperReport jasperReport = JasperCompileManager.compileReport(design);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

            /**
             * * Export **
             */
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
            pdfExporter.exportReport();

            /**
             * * Close connection **
             */
            connection.close();

            return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
        }
    }
}
