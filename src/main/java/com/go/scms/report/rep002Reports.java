/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.go.scms.report;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.hibernate.Session; 
import org.hibernate.internal.SessionImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.go.scms.dto.TbTrnClassRoomMemberDto;
import com.go.scms.utils.ValiableUtils;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@RestController 
@RequestMapping("/api/v1/rep002/report")
public class rep002Reports {

        @Autowired
        private EntityManager em;

        // *** Export PDF ****
        @GetMapping(value = "/rec001/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report1(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-001.jrxml"));

                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec002/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report2(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-002.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec003/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report3(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-003.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec004/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report4(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-004.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec005/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report5(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap ** 
                 */ 
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-005.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec006/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report6(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-006.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec007/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report7(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-007.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec009/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report9(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-009.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }

        @GetMapping(value = "/rec010/pdf", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
        public ResponseEntity<Object> report10(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto, HttpServletResponse response) {
            SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
            try (ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream(); Connection connection = sessionImpl.connection()) {

                /**
                 * * Convert string to hashMap **
                 */
                HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

                /**
                 * * Query **
                 */
                JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-010.jrxml"));
                JasperReport jasperReport = JasperCompileManager.compileReport(design);
                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, connection);

                /**
                 * * Export **
                 */
                JRPdfExporter pdfExporter = new JRPdfExporter();
                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));
                pdfExporter.exportReport();

                /**
                 * * Close connection **
                 */
                connection.close();

                return ResponseEntity.status(HttpStatus.OK).body(pdfReportStream.toByteArray());
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
            }
        }
        
        // *** Export XLS ****
        @GetMapping(value = "rec001/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report1Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());
                        System.out.print("rec001");
			/*** Query ***/
                        
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-001.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);
                        

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
                        configuration.setDetectCellType(Boolean.FALSE);
                        configuration.setShrinkToFit(Boolean.TRUE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "rec002/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report2Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-002.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "rec004/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report4Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-004.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
			configuration.setWhitePageBackground(false);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "rec005/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report5Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-005.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
			configuration.setWhitePageBackground(false);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "rec006/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report6Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-006.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "rec007/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report7Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-007.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}

        @GetMapping(value = "rec009/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report9Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-009.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
        
        @GetMapping(value = "rec010/xls",produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<Object> report10Xls(TbTrnClassRoomMemberDto tbTrnClassRoomMemberDto,HttpServletResponse response){
		SessionImpl sessionImpl = (SessionImpl) em.unwrap(Session.class);
		try(ByteArrayOutputStream xlsReportStream = new ByteArrayOutputStream();Connection connection = sessionImpl.connection()){

			/*** Convert string to hashMap ***/;
			HashMap map = ValiableUtils.stringToHashMap(tbTrnClassRoomMemberDto.toString());

			/*** Query ***/
			JasperDesign design = JRXmlLoader.load(this.getClass().getResourceAsStream("/reports/rec-010.jrxml"));
			JasperReport jasperReport = JasperCompileManager.compileReport(design);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map,connection);

			/*** Export ***/
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReportStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(false);
			configuration.setWhitePageBackground(false);
                        configuration.setShrinkToFit(Boolean.TRUE);
                        configuration.setDetectCellType(Boolean.FALSE);
			exporter.setConfiguration(configuration);
			exporter.exportReport();

			/*** Close connection ***/
			connection.close();

			return ResponseEntity.status(HttpStatus.OK).body(xlsReportStream.toByteArray());
		}catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal error.");
		}
	}
}
