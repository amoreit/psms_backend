package com.go.scms.utils;

public class PageableUtils {
	
	public static Integer offset(String n) {
		int i;
		try {
			i = Integer.parseInt(n)-1;
			if(i<0) {i= 0;}
		}catch(Exception ex) {
			i = 0;
		}
		return i;
	}
	
	public static Integer result(String n) {
		int i;
		try {
			i = Integer.parseInt(n);
		}catch(Exception e) {
			i = 10;
		}
		return i;
	}
}
