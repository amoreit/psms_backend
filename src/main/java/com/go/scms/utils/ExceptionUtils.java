package com.go.scms.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.go.scms.response.ResponseOutput;
import com.go.scms.response.ResponseStatus;

public class ExceptionUtils {

	public static ResponseOutput convertRollbackToString(final Exception e) {
		ResponseOutput output = new ResponseOutput();
	    final List<Pattern> patterns = Arrays.asList(Pattern.compile("((?<key>[^']+))=((?<value>[^']+)) already exists"));
	    for (final Pattern pattern : patterns) {
	    	Matcher matcher = null;
	        try {
		    	matcher = pattern.matcher(e.getCause().getCause().getMessage());
	        }catch(NullPointerException ne) {
	        	matcher = pattern.matcher(e.getMessage());
	        }
	        
	        if (matcher.find()) {
	            final String key = matcher.group("key").toLowerCase();
	            final String value = matcher.group("value");
	            
	            output.setResponseCode(ResponseStatus.DUPLICATE.getCode());
	            output.setResponseMsg(ResponseStatus.DUPLICATE.getMessage()+" "+value);
	            
	    		return output;
	        }
	    }
	    output.setResponseCode(ResponseStatus.ERROR.getCode());
	    output.setResponseMsg(e.getMessage());
	    return output;
	}
	
	public static ResponseOutput validate(ConstraintViolationException e){
		ResponseOutput output = new ResponseOutput();
		final Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
		final ConstraintViolation<?> err = constraintViolations.iterator().next();
		
		output.setResponseCode(ResponseStatus.VALIDATE.getCode());
		output.setResponseMsg(err.getMessage());
		return output;
	}
	
}
