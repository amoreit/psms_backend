package com.go.scms.utils;

import java.util.HashMap;
import java.util.Optional;

import com.google.gson.Gson;

public class ValiableUtils {

	public static String stringIsEmpty(Object n) {
		try {
			return Optional.ofNullable(n.toString()).orElse("");
		}catch(Exception ex) {
			return "";
		}
	}
	
	public static long LongIsZero(Object n) {
		try {
			return Long.parseLong(n.toString());
		}catch(Exception ex) {
			return 0;
		}
	}
	
	public static long IntIsZero(Object n) {
		try {
			return Integer.parseInt(n.toString());
		}catch(Exception ex) {
			return 0;
		}
	}
	
	public static HashMap<String, String> stringToHashMap(String str) {
		HashMap<String, String> map = new HashMap<>();
		try {
			map = new Gson().fromJson(str,HashMap.class);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return map;
	}
	
}
