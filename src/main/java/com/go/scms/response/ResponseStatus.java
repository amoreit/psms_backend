package com.go.scms.response;

public enum ResponseStatus {
	VALIDATE{
		@Override
		public Integer getCode() {
			return 400;
		}
		@Override
		public String getMessage() {
			return "Validate";
		}
	},
	DUPLICATE{
		@Override
		public Integer getCode() {
			return 404;
		}
		@Override
		public String getMessage() {
			return "Duplicate";
		}
	},
	NOT_FOUND{
		@Override
		public Integer getCode() {
			return 404;
		}
		@Override
		public String getMessage() {
			return "Not found data.";
		}
	},
	SUCCESS{
		@Override
		public Integer getCode() {
			return 200;
		}
		@Override
		public String getMessage() {
			return "Success";
		}
	},
	ERROR{
		@Override
		public Integer getCode() {
			return 500;
		}
		@Override
		public String getMessage() {
			return "Error";
		}
	},
	LOCK{
		@Override
		public Integer getCode() {
			return 501;
		}
		@Override
		public String getMessage() {
			return "Lock";
		}
	};
	
	public abstract Integer getCode();
	public abstract String getMessage();
	
}
