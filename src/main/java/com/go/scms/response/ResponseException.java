package com.go.scms.response;

public class ResponseException extends Error{

	private static final long serialVersionUID = 1L;
	private Integer code;
	
	public ResponseException(ResponseStatus status) {
		super(status.getMessage());
		this.code = status.getCode();
	}
	
	public ResponseException(Integer code ,String msg) {
		super(msg);
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
