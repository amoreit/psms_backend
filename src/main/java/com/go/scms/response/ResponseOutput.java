package com.go.scms.response;

import com.google.gson.Gson;

public class ResponseOutput {

	private Integer responseCode;
	private String responseMsg;
	private Object responseData;
	private Long responseSize;
	
	public ResponseOutput() {}
	
	public ResponseOutput(ResponseStatus status) {
		this.responseCode = status.getCode();
		this.responseMsg = status.getMessage();
	}
	
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public Object getResponseData() {
		return responseData;
	}
	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}
	public Long getResponseSize() {
		return responseSize;
	}
	public void setResponseSize(Long responseSize) {
		this.responseSize = responseSize;
	}

	public String toString(){
		return new Gson().toJson(this);
	}
}
