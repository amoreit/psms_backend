package com.go.scms.contant;

public enum RecordStatus {

	SAVE{
		@Override
		public String getCode() {
			return "S";
		}
	},
	UPDATE{
		@Override
		public String getCode() {
			return "U";
		}
	},
	DELETE{
		@Override
		public String getCode() {
			return "D";
		}
	},
	APPROVE{
		@Override
		public String getCode() {
			return "A";
		}
	},
	UN_APPROVE{
		@Override
		public String getCode() {
			return "N";
		}
	};
	abstract public String getCode();
}
